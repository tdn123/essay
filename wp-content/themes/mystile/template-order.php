<?php
/**
 * Template Name: Order Page
 *
 * The archives page template displays a conprehensive archive of the current
 * content of your website on a single page. 
 *
 * @package WooFramework
 * @subpackage Template
 */
get_header();

the_content();


// $levels = get_attributes_by_code('levels');
// foreach($levels as $level){
// 	var_dump($level->name);
// }

wp_enqueue_style('common-css', get_template_directory_uri() . '/css/essay/common.css', array(), '20120208', 'all');
?>

<div id="customer_upload_dialog" data-finder="form.daialog.customer_upload_dialog" class="ui-dialog-content ui-widget-content">
	upload
</div>

<div class="content">
	<div class="row container container-fluid">
		<div class="row-fluid">
			<div class="span12">


				<script>

					if (jQuery.throttle == null && jQuery.debounce == null) {
		/*
		 * jQuery throttle / debounce - v1.1 - 3/7/2010
		 * http://benalman.com/projects/jquery-throttle-debounce-plugin/
		 * Copyright (c) 2010 "Cowboy" Ben Alman
		 */
		 (function(b, c) { var $ = b.jQuery || b.Cowboy || (b.Cowboy = {}), a;
		 	jQuery.throttle = a = function(e, f, j, i) {
		 		var h, d = 0; if (typeof f !== "boolean") { i = j; j = f; f = c } function g() {
		 			var o = this, m = +new Date() - d, n = arguments; function l() { d = +new Date(); j.apply(o, n) } function k() { h = c } if (i && !h) { l() } h && clearTimeout(h);
		 			if (i === c && m > e) { l() } else { if (f !== true) { h = setTimeout(i ? k : l, i === c ? e - m : e) } } }
		 				if (jQuery.guid) { g.guid = j.guid = j.guid || jQuery.guid++ } return g
		 			};
		 		jQuery.debounce = function(d, e, f) { return f === c ? a(d, e, false) : a(d, f, e !== false) }
		 	})(this);
		 }

		 (function() {

		 	jQuery.UVOformPotatoUI = jQuery.UVOformPotatoUI || {};

		/***************************************************
		 * Floating sidebar
		 **************************************************/

		 jQuery.UVOformPotatoUI.FloatingRightSidebar = function FloatingRightSidebar(o) {
		 	if (typeof o != "object" || o.constructor != Object) {
		 		throw new Error("FloatingSidebar ininializer requres object with options")
		 	}
		 	if (o.$el instanceof $ == false) {
		 		throw new Error("FloatingSidebar requres `$el: jQuery` option");
		 	}
		 	if (o.$container instanceof $ == false) {
		 		throw new Error("FloatingSidebar requres `$container: jQuery` option");
		 	}
		 	if (o.$pageContent instanceof $ == false) {
		 		throw new Error("FloatingSidebar requres `$pageContent: jQuery` option");
		 	}
		 	if (typeof o.spacer !== "number") {
		 		throw new Error("FloatingSidebar requres `spacer: number` option");
		 	}
		 	if (o.topSpacer != null && typeof o.topSpacer !== "number") {
		 		throw new Error("FloatingSidebar `topSpacer` option must be a number");
		 	}
		 	this._$el               = o.$el;
		 	this._$container        = o.$container;
		 	this._$pageContent      = o.$pageContent;
		 	this._spacer            = o.spacer;
		 	this._topSpacer         = o.topSpacer || 0;
		 	this._minElWith         = typeof o.minElWith === "number" ? o.minElWith : 0;
		 	this._currentPosition   = "";
		 	this._isModal           = false;
		 	this._$overlay          = jQuery("<div style=\"position: fixed; left: 0; top: 0; width: 100%; height: 100%; background: rgba(0,0,0,.5); z-index: 1000;  display: none;\"></div>");

		 	this.setWidth();

		 	jQuery("body").append(this._$overlay);
		 	this._$overlay.on("click", this.makeModal.bind(this, false));
		 	this._$el.on("click", ".close-modal", this.makeModal.bind(this, false));

		 	jQuery(window)
		 	.on("resize",        this.setWidth.bind(this))
		 	.on("resize scroll", this.reposition.bind(this));
		 };

		 jQuery.extend(jQuery.UVOformPotatoUI.FloatingRightSidebar.prototype, {
		 	setWidth: function() {
		 		if (this._currentPosition === "modal") {
		 			return;
		 		}
		 		this._$el.show();
		 		var elWidth = this._$container.width() - this._$pageContent.outerWidth() - this._spacer;
		 		var newStateMobile = elWidth < this._minElWith;
		 		var stateMobileChanged = this._mobile !== newStateMobile;
		 		this._mobile = newStateMobile;
		 		this._$el.css({
		 			width: this._mobile ? "" : elWidth,
		 			float: this._mobile ? "" : "right",
		 			clear: this._mobile ? "both" : ""
		 		});
		 		this._$container.css({ position: "relative" });
		 		this._$el.toggleClass("smaller-than-enough-for-floating", this._mobile);
		 		this._$el.css({
		 			width: this._mobile ? "" : this._elWidth
		 		});
		 		if (stateMobileChanged) {
		 			this._$el.trigger("stateMobileChanged");
		 		}
		 	},
		 	reposition: function(e) {
		 		e = e || {};
		 		if (this._currentPosition === "modal") {
		 			return;
		 		}
		 		if (this._mobile) {
		 			this._currentPosition = "";
		 			this._$el.css({
		 				position: "static"
		 			});
		 			return;
		 		}
		 		var topS = window.scrollY + this._topSpacer > this._$container.offset().top + parseInt(this._$container.css("padding-top"));
		 		var bottomS = window.scrollY > this._$container.offset().top + this._$container.height() - this._$el.outerHeight() -  parseInt(this._$container.css("padding-bottom"));
		 		if (topS && !bottomS && (this._currentPosition !== "inside" || e.type == "resize")) {
		 			this._currentPosition = "inside";
		 			this._$el.css({
		 				position: "fixed",
		 				top: this._topSpacer,
		 				left: (this._$container.offset().left + this._$container.outerWidth() - this._$el.outerWidth() - parseFloat(this._$container.css("padding-right"))),

		 				right: "auto",
		 				bottom: "auto"
		 			});
		 		} else if (bottomS && this._currentPosition !== "below") {
		 			this._currentPosition = "below";
		 			this._$el.css({
		 				position: "absolute",
		 				top: "auto",
		 				left: "auto",
		 				bottom: this._$container.css("padding-bottom"),
		 				right: this._$container.css("padding-right")
		 			});
		 		} else if (!topS && !bottomS && this._currentPosition !== "above") {
		 			this._currentPosition = "above";
		 			this._$el.css({
		 				position: "static"
		 			});
		 		}
		 	},
		 	getIfMobile: function() {
		 		return this._mobile;
		 	},
		 	get$el: function() {
		 		return this._$el;
		 	},
		 	makeModal: function(modal) {
		 		modal = typeof modal === "boolean" ? modal : true;
		 		if (modal) {
		 			if (this._currentPosition !== "modal") {
		 				window.__$sidebar = this._$el;
		 				var offset = jQuery(".right-floating-sidebar").offset();
		 				var desiredWidth = Math.min(400, jQuery(window).width());
		 				this._$el.css({
		 					position: "fixed",
							"z-index": 2000000001, // live help
							width: this._$el.width(),
							top: offset.top - jQuery(window).scrollTop(),
							left: offset.left - jQuery(window).scrollLeft()
						});
		 				this._$overlay.fadeIn();
		 				setTimeout(function() {
		 					this._$el.addClass("is-modal");
		 					this._$el.css({
		 						left: this._mobile
		 						? 0
		 						: "50%",
		 						top: this._mobile
		 						? 0
		 						: "50%",
		 						width: this._mobile
		 						? "100%"
		 						: desiredWidth,
		 						height: this._mobile
		 						? "100%"
		 						: "auto",
		 						"max-height": jQuery(window).height(),
		 						margin:this._mobile
		 						? ""
		 						: "-" + this._$el.height() / 2 + "px 0 0 -" + desiredWidth / 2 + "px",
		 						"padding-bottom": this._mobile
		 						? 0
		 						: 80
		 					});
		 				}.bind(this), 5);
		 			}
		 			this._currentPosition = "modal";
		 		} else {
		 			this._currentPosition = "";
		 			this._$overlay.hide();
		 			this._$el.css({
		 				"z-index": "",
		 				margin: "",
		 				"padding-bottom": "",
		 				top: "",
		 				left: "",
		 				width: "",
		 				height: "",
		 				position: "static"
		 			});
		 			this._$el.removeClass("is-modal");
		 			this.setWidth();
		 			this.reposition();
		 		}
		 	}
		 });


		/***************************************************
		 * Vertical steps/tabs (CS-8751)
		 **************************************************/
		 jQuery.UVOformPotatoUI.RozaSteps = function RozaSteps(o) {

		 	if (o.steps instanceof Array == false) {
		 		throw new Error("RozaSteps requres `steps: [object]` option")
		 	}
		 	if (o.checkIfVisibleIsValid != null && typeof o.checkIfVisibleIsValid != "function") {
		 		throw new Error("RozaSteps: `checkIfVisibleIsValid` option must be a function, returning boolean")
		 	}
		 	if (o.stepChanged != null && typeof o.stepChanged != "function") {
		 		throw new Error("RozaSteps: `stepChanged` option must be a function")
		 	}
		 	if (o.currentStepValidationChanged != null && typeof o.currentStepValidationChanged != "function") {
		 		throw new Error("RozaSteps: `currentStepValidationChangedd` option must be a function")
		 	}
		 	if (o.finalValidationChanged != null && typeof o.finalValidationChanged != "function") {
		 		throw new Error("RozaSteps: `finalValidationChanged` option must be a function")
		 	}
		 	if (typeof o.enabled != "boolean") {
		 		throw new Error("RozaSteps requres `enabled: boolen` option")
		 	}

		 	this._steps                                  = [];
		 	this._checkIfVisibleIsValid                  = o.checkIfVisibleIsValid || function() { return true };
		 	this._notifyThatCurrentStepChanged           = o.stepChanged || jQuery.noop;
		 	this._notifyThatCurrentStepValidationChanged = o.currentStepValidationChanged || jQuery.noop;
		 	this._notifyThatFinalValidationChanged       = o.finalValidationChanged || jQuery.noop;
		 	this._currentIndex                           = 0;
		 	this._everythingIsValid                      = false;
		 	this._amimationTime                          = 200

		 	for (var i = 0; i < o.steps.length; i++) {
		 		var stepOptions = o.steps[i];

		 		if (stepOptions.$el instanceof $ == false) {
		 			throw new Error("RozaSteps: `step[" + i + "].$el` option must be a jQuery object");
		 		}

		 		var step = {
		 			_$el       : stepOptions.$el,
		 			_$headingEl: jQuery(".step-title", stepOptions.$el),
		 			_$bodyEl   : jQuery(".step-body", stepOptions.$el)
		 		};

		 		if (step._$el.length === 0) {
		 			break;
		 		}

		 		if (i === 0) {
		 			step._$el.addClass("first-step");
		 		} else if (i === o.steps.length - 1) {
		 			step._$el.addClass("last-step");
		 		}

		 		step._$headingEl.on("click", function(index) {
		 			if (this._enabled) {
		 				this.showStepWithIndex(index, false);
		 			}
		 		}.bind(this, i));

		 		step._$headingEl.on("click", ".edit-step-button", function(index) {
		 			if (this._enabled) {
		 				this.showStepWithIndex(index, false)
		 			}
		 		}.bind(this, i));

		 		step._$bodyEl.on("click", ".roza-next-step-button", function() {
		 			if (this._enabled) {
		 				this.stepFuther();
		 			}
		 		}.bind(this));

		 		this._steps.push(step);

		 	}

		 	o.enabled
		 	? this.enable()
		 	: this.disable();
		 };

		 jQuery.extend(jQuery.UVOformPotatoUI.RozaSteps.prototype, {
		 	enable: function() {
		 		this._currentIndex = this._everythingIsValid
		 		? this._currentIndex
		 		: 0;
		 		if (this._enabled) {
		 			return;
		 		}
		 		for (var i = 0; i < this._steps.length; i++) {
		 			var step = this._steps[i];
		 			if (step._isValid == null) {
		 				this._setStepWithIndexIsValid(i, i < this._currentIndex);
		 			}
		 			step._$bodyEl.toggle(i === this._currentIndex);
		 			step._$el.toggleClass("step-active", i === this._currentIndex);
		 			step._$el.toggleClass("collapsed", i !== this._currentIndex);
		 			step._$el.toggleClass("valid", step._isValid);
		 			jQuery(".roza-next-step-button", step._$bodyEl).css("display", "");
		 			jQuery(".edit-step-button", step._$headingEl).css("display", "");
		 		}

		 		this._notifyThatCurrentStepChanged(this._currentIndex);
		 		this._notifyThatCurrentStepValidationChanged(this._steps[this._currentIndex]._isValid);
		 		this._enabled = true;

		 		this.revalidateVisible(false);
		 	},
		 	disable: function() {
		 		if (this._enabled === false) {
		 			return;
		 		}

		 		for (var i = 0; i < this._steps.length; i++) {
		 			var step = this._steps[i];
		 			step._$bodyEl.show();
		 			step._$el
		 			.addClass("step-active")
		 			.removeClass("valid collapsed");
		 			jQuery(".roza-next-step-button", step._$bodyEl).hide();
		 			jQuery(".edit-step-button", step._$headingEl).hide();
		 		}

		 		this._enabled = false;

		 		this.revalidateVisible(false);
		 	},
		 	showStepWithIndex: function(index, verbose) {
		 		index = Math.min(index, this._currentIndex + 1);
		 		if (index === this._currentIndex || index > this._currentIndex && this._checkIfVisibleIsValid(verbose) == false) {
		 			return;
		 		}
		 		this._currentIndex = index;
		 		for (var i = 0; i < this._steps.length; i++) {
		 			if (i != index) {
		 				this._setHiddenStepWIthIndex(i);
		 			}
		 		}
		 		this._setVisibleStepWithIndex(index);
		 	},
		 	stepFuther: function() {
		 		this.showStepWithIndex(this._currentIndex + 1, true);
		 	},
		 	_setVisibleStepWithIndex: function(index) {
		 		var step = this._steps[index];
		 		step._$bodyEl.slideDown(this._amimationTime);
		 		step._$el.addClass("step-active");
		 		step._$el.removeClass("collapsed");
		 		if (this._enabled) {
		 			this._notifyThatCurrentStepChanged(index);
		 			setTimeout(function() {
		 				this.revalidateVisible(false);
		 			}.bind(this), this._amimationTime + 50);
		 		}
		 	},
		 	_setHiddenStepWIthIndex: function(index) {
		 		var step = this._steps[index];
		 		step._$bodyEl.slideUp(this._amimationTime);
		 		step._$el.removeClass("step-active");
		 		step._$el.addClass("collapsed");
		 	},
		 	_setStepWithIndexIsValid: function(index, isValid) {
		 		var step = this._steps[index];
		 		var wasValid = step._isValid;
		 		if (isValid !== wasValid) {
		 			step._isValid = isValid;
		 			if (this._enabled) {
		 				step._$el.toggleClass("valid", isValid);
		 			}
		 			if (this._enabled && index === this._currentIndex) {
		 				this._notifyThatCurrentStepValidationChanged(isValid);
		 			}
		 		}
		 	},
		 	_setAllStepsAreValid: function(areValid) {
		 		for (var i = 0; i < this._steps.length; i++) {
		 			this._setStepWithIndexIsValid(i, areValid);
		 		}
		 	},
		 	revalidateVisible: jQuery.debounce(200, function(verbose, cb) {
		 		var visibleIsValid = this._checkIfVisibleIsValid(verbose);
		 		if (this._enabled) {
		 			this._setStepWithIndexIsValid(this._currentIndex, visibleIsValid);
		 		} else {
		 			this._setAllStepsAreValid(visibleIsValid);
		 		}
		 		var everythingIsValid = this._enabled
		 		? this.getIfAllStepsAreValid()
		 		: visibleIsValid;
		 		if (this._everythingIsValid !== everythingIsValid) {
		 			this._everythingIsValid = everythingIsValid;
		 			this._notifyThatFinalValidationChanged(everythingIsValid);
		 		}
		 		cb && cb(everythingIsValid)
		 	}),
		 	getIfLastStep: function() {
		 		return this._enabled == false || this._currentIndex === this._steps.length - 1
		 	},
		 	getCurrentStep$body: function() {
		 		return this._steps[this._currentIndex].bodyEl;
		 	},
		 	getIfAllStepsAreValid: function() {
		 		return this._enabled
		 		? jQuery.grep(this._steps, function(step) { return step._isValid !== true }).length === 0
		 		: this._everythingIsValid;
		 	}
		 });

}());
</script>

<div class="mainInRgt cabinet">
	<article class="box-orderNow">
		<div class="box-orderNowIn">

			<h1 class="formtitle" data-finder="form.h1.title">Place an order. <span>It's fast, secure, and confidential.</span></h1>
			<div class="uvoform_container clearfix"> <script>

				jQuery(function () {

					var $innerSidebar = jQuery(".inner-sidebar");
					if ($innerSidebar.length) {
						$innerSidebar.show();
						var orderformRightSidebar = new jQuery.UVOformPotatoUI.FloatingRightSidebar({
							$el: $innerSidebar,
							$container: $innerSidebar.closest('.container'),
							$pageContent: jQuery(".uvoform_potato"),
							spacer: 20
						});
					}

				});

			</script>
			<style>

			</style>

			<div id="potato-form-data-restored-notice" class="form-data-restored-notice" style="display:none;"></div>

			<form id="order_form" class="orderform orderform-bordered f uvoform_potato " action="https://essayhave.com/uvoform/potato/submit/" method="POST" data-finder="form.order_form" with-local-storage-keeper="">

				<input type="hidden" name="ci_csrf_token" value="">
				<input type="hidden" name="min_academic_level" id="min_academic_level" value="1">
				<input type="hidden" name="coupon_id" id="coupon_id" value="">
				<input type="hidden" name="percent" id="percent" value="">
				<input type="hidden" name="coupon_code" id="coupon_code" value="">
				<input type="hidden" name="totcharg" id="totcharg" value="32">
				<input type="hidden" name="base_price" id="base_price" value="32">
				<input type="hidden" name="raw_price" id="raw_price" value="32">
				<input type="hidden" name="form_type" value="potato">
				<input type="hidden" name="price_without_pd" id="price_without_pd" value="32">
				<input type="hidden" name="new_user" id="new_user" value="1">
				<input type="hidden" name="typeofservice" id="typeofservice" value="37">
				<select name="gmt" id="gmt" style="display:none">
					<option value="-720">(GMT-12:00)</option>
					<option value="-660">(GMT-11:00)</option>
					<option value="-600">(GMT-10:00)</option>
					<option value="-540">(GMT-09:00)</option>
					<option value="-480">(GMT-08:00)</option>
					<option value="-420">(GMT-07:00)</option>
					<option value="-360">(GMT-06:00)</option>
					<option value="-300">(GMT-05:00)</option>
					<option value="-240">(GMT-04:00)</option>
					<option value="-180">(GMT-03:00)</option>
					<option value="-120">(GMT-02:00)</option>
					<option value="-60">(GMT-01:00)</option>
					<option value="0">(GMT)</option>
					<option value="60">(GMT+01:00)</option>
					<option value="120">(GMT+02:00)</option>
					<option value="180">(GMT+03:00)</option>
					<option value="240">(GMT+04:00)</option>
					<option value="300">(GMT+05:00)</option>
					<option value="360">(GMT+06:00)</option>
					<option value="420">(GMT+07:00)</option>
					<option value="480">(GMT+08:00)</option>
					<option value="540">(GMT+09:00)</option>
					<option value="600">(GMT+10:00)</option>
					<option value="660">(GMT+11:00)</option>
					<option value="720">(GMT+12:00)</option>
					<option value="780">(GMT+13:00)</option>
				</select>

				<div id="uvoform_tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">

					<div id="tab_services" aria-labelledby="ui-id-8" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="true" aria-hidden="false">
						<fieldset id="fieldset_services" class="orderform-step">
							<div class="table">

								<div id="box_paperlanguage" class="row ui-buttonset" style="display: none;">
									<div class="cell cell-left with-mobile-tip">
										<div class="mobile-tip" data-hasqtip="13"></div>
										<label for="" class="left-label">Language of your paper:</label>
									</div>
									<div class="cell cell-right side_tip side_tip_paper_language" data-hasqtip="12"><div class="selects visible-in-mobile" style="display: none;"><select id="mob_paperlanguage" name="mob_paperlanguage" class="dr-down-input" style="position: absolute; top: 0px; left: 0px; visibility: hidden;"><option value="1">English</option></select>

										<div class="uvo_pref-uvoAutocomplete-wrapper"><input class="uvo_pref-uvoAutocomplete-input" name="mob_paperlanguage_input" type="text" readonly="" data-finder="mob_paperlanguage" data-value="1"><span class="ui-icon ui-icon-triangle-1-s" style="position: absolute; cursor: pointer; right: 10px; top: 17px;"></span><div class="dr-down" style="display: none; top: 38px;"><span class="dr-down-search" style="display: none;"><span class="dr-down-zoom"></span><input type="text" class="dr-down-input smaller" placeholder="Search within list"></span><div class="dr-down-list"><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="1">English</a></div></div></div></div>
										<div class="radios visible-in-desktop">

											<input type="radio" title="English" id="radio_paper_language_1" value="1" name="paperlanguage" data-finder="form.input.paperlanguage1" class="ui-helper-hidden-accessible">
											<label for="radio_paper_language_1" id="tip_radio_paper_language_1" data-finder="form.label.paperlanguage1" class="lang_en ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right" role="button" aria-disabled="false"><span class="ui-button-text">
												English</span></label>
											</div>
										</div>
									</div>
									<?php $levels = get_attributes_by_code('levels', 'term_id'); ?>

									<div id="box_academiclevel" class="row ui-buttonset" style="">
										<div class="cell cell-left with-mobile-tip">
											<div class="mobile-tip" data-hasqtip="11"></div>
											<label for="" class="left-label">Academic level:</label>
										</div>
										<div class="cell cell-right side_tip side_tip_academic_level" data-hasqtip="10" aria-describedby="qtip-10"><div class="selects visible-in-mobile" style="display: none;">
											<select id="mob_academiclevel" name="mob_academiclevel" class="dr-down-input" style="position: absolute; top: 0px; left: 0px; visibility: hidden;">
												<?php if(count($levels)): ?>
													<?php foreach($levels as $item): ?>
														<option value="<?php echo $item->term_id ?>"><?php echo $item->name; ?></option>
													<?php endforeach; ?>
												<?php endif;//end if(count($levels)) ?>
											</select>
											<div class="uvo_pref-uvoAutocomplete-wrapper"><input class="uvo_pref-uvoAutocomplete-input" name="mob_academiclevel_input" type="text" readonly="" data-finder="mob_academiclevel" data-value="1"><span class="ui-icon ui-icon-triangle-1-s" style="position: absolute; cursor: pointer; right: 10px; top: 20px;"></span><div class="dr-down" style="display: none; top: 38px;"><span class="dr-down-search" style="display: none;"><span class="dr-down-zoom"></span>
												<input type="text" class="dr-down-input smaller" placeholder="Search within list">
											</span>

											<div class="dr-down-list">

												<?php if(count($levels)): ?>
													<?php foreach($levels as $item): ?>
														<option value="<?php echo $item->term_id ?>"><?php echo $item->name; ?></option>
													<?php endforeach; ?>
												<?php endif;//end if(count($levels)) ?>

											</div></div></div></div>
											<div class="radios visible-in-desktop">

												<?php if(count($levels)): ?>
													<?php foreach($levels as $item): ?>
														<input type="radio" title="High school" id="adio_academic_level_<?php $item->term_id ?>" value="<?php $item->term_id ?>" name="academiclevel" data-finder="form.input.academiclevel<?php $item->term_id ?>" class="ui-helper-hidden-accessible">

														<label for="radio_academic_level_<?php $item->term_id ?>" id="tip_radio_academic_level_<?php $item->term_id ?>" data-finder="form.label.academiclevel<?php $item->term_id ?>" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left" role="button" aria-disabled="false" aria-pressed="true"><span class="ui-button-text"><?php echo $item->name; ?></span>
														</label>
													<?php endforeach; ?>
												<?php endif;//end if(count($levels)) ?>







											</div>
										</div>
									</div>


									<?php
									$paper_types = get_attributes_by_code('paper-types');

									?>
									<div id="box_paper_type_id" class="row">
										<div class="cell cell-left with-mobile-tip">
											<div class="mobile-tip" data-hasqtip="15"></div>
											<label for="paper_type_id" class="left-label">Type of paper needed:</label>
										</div>
										<div class="cell cell-right side_tip side_tip side_tip_type_of_paper" data-hasqtip="14" aria-describedby="qtip-14">
											<select name="paper_type_id" id="paper_type_id" class="paper-type-id f_sz_400 validate[required] dr-down-input" data-finder="form.select.papertypeid" style="position: absolute; top: 0px; left: 0px; visibility: hidden;">
												<option class="hidden empty" value="">Please select</option>
												<?php if(count($paper_types)): ?>
													<?php foreach($paper_types as $item): ?>
														<option value="<?php echo $item->term_id; ?>"><?php echo $item->name; ?></option>
													<?php endforeach; ?>
													<!-- <option value="other">Other (enter below)</option> -->
												<?php endif; //end count($paper_typers); ?>
											</select><div class="uvo_pref-uvoAutocomplete-wrapper ui-state-active"><input class="uvo_pref-uvoAutocomplete-input" name="paper_type_id_input" type="text" readonly="" data-finder="paper_type_id" data-value="10"><span class="ui-icon ui-icon-triangle-1-s" style="position: absolute; cursor: pointer; right: 10px; top: 20px;"></span>
											<div class="dr-down" style="display: block; top: 38px;"><span class="dr-down-search"><span class="dr-down-zoom"></span><input type="text" class="dr-down-input smaller" placeholder="Search within list"></span>
												<div class="dr-down-list">
													<!-- <a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="other">Other (enter below)</a> -->
													<?php if(count($paper_types)): ?>
														<?php foreach($paper_types as $item): ?>
															<a class="dr-down-list-item" href="#" data-value="<?php echo $item->term_id; ?>"><?php echo $item->name; ?></option>
															<?php endforeach; ?>
															<!-- <option value="other">Other (enter below)</option> -->
														<?php endif; //end count($paper_typers); ?>

													</div><!--end dr-down-list-->

												</div>
											</div>
											<div class="field_tip field_tip_paper_type_id hidden" style="display: none;">Please note that any orders related to thesis and dissertation papers, as well as their parts/chapters, are only available for University (College 3-4) academic level and higher.</div>
											<div id="agree_text_tr" class="hidden"><p>User agreement</p></div>
											<div id="other_box_paper_type_id" style="display: none;">
												<input name="paper_type_option" maxlength="1000" id="paper_type_option" class="f_sz_full validate[required,custom[typeofpaperother]]" type="text" data-finder="form.input.papertypeoption">
											</div>
										</div>
									</div>

									<div id="box_topcat_id" class="row">
										<div class="cell cell-left with-mobile-tip">
											<div class="mobile-tip" data-hasqtip="17"></div>
											<label for="" class="left-label">Subject or discipline:</label>
										</div>
										<div class="cell cell-right side_tip side_tip_discipline" data-hasqtip="16" aria-describedby="qtip-16">
											<select name="topcat_id" id="topcat_id" class="topcat-id f_sz_400 validate[required] dr-down-input" data-finder="form.select.topcatid" style="position: absolute; top: 0px; left: 0px; visibility: hidden;">
												<option class="hidden empty" value="">Please select</option>
												<optgroup label="Humanities">
													<option value="81">Art (Fine arts, Performing arts)</option>
													<option value="10">Classic English Literature</option>
													<option value="57" disabled="true">Composition</option>
													<option value="56">English 101</option>
													<option value="18" disabled="true">Film &amp; Theater studies</option>
													<option value="20">History</option>
													<option value="59" disabled="true">Linguistics</option>
													<option value="41">Literature</option>
													<option value="26">Music</option>
													<option value="27">Philosophy</option>
													<option value="60">Poetry</option>
													<option value="31" disabled="true">Religious studies</option>
													<option value="34">Shakespeare</option>
												</optgroup>
												<optgroup label="Social Sciences">
													<option value="2" disabled="true">Anthropology</option>
													<option value="61" disabled="true">Cultural and Ethnic Studies</option>
													<option value="15">Economics</option>
													<option value="65" disabled="true">Ethics</option>
													<option value="29" disabled="true">Political science</option>
													<option value="30">Psychology</option>
													<option value="54" disabled="true">Social Work and Human Services</option>
													<option value="35" disabled="true">Sociology</option>
													<option value="64" disabled="true">Tourism</option>
													<option value="66" disabled="true">Urban Studies</option>
													<option value="40" disabled="true">Women's &amp; gender studies</option>
												</optgroup>
												<optgroup label="Business and administrative studies">
													<option value="1" disabled="true">Accounting</option>
													<option value="8" disabled="true">Business Studies</option>
													<option value="19" disabled="true">Finance</option>
													<option value="75" disabled="true">Human Resources Management (HRM)</option>
													<option value="62" disabled="true">International Relations</option>
													<option value="63" disabled="true">Logistics</option>
													<option value="22" disabled="true">Management</option>
													<option value="23" disabled="true">Marketing</option>
													<option value="76" disabled="true">Public Relations (PR)</option>
												</optgroup>
												<optgroup label="Natural Sciences">
													<option value="67">Astronomy (and other Space Sciences)</option>
													<option value="42">Biology (and other Life Sciences)</option>
													<option value="9">Chemistry</option>
													<option value="68">Ecology</option>
													<option value="49">Geography</option>
													<option value="69">Geology (and other Earth Sciences)</option>
													<option value="28">Physics</option>
													<option value="45" disabled="true">Zoology</option>
												</optgroup>
												<optgroup label="Formal Sciences">
													<option value="12">Computer science</option>
													<option value="24">Mathematics</option>
													<option value="37">Statistics</option>
												</optgroup>
												<optgroup label="Professions and Applied Sciences">
													<option value="70" disabled="true">Agriculture</option>
													<option value="3" disabled="true">Application Letters</option>
													<option value="5" disabled="true">Architecture, Building and Planning</option>
													<option value="71" disabled="true">Aviation</option>
													<option value="72" disabled="true">Civil Engineering</option>
													<option value="11">Communications</option>
													<option value="73" disabled="true">Criminal Justice</option>
													<option value="48" disabled="true">Criminal law</option>
													<option value="16" disabled="true">Education</option>
													<option value="74" disabled="true">Engineering</option>
													<option value="50" disabled="true">Environmental studies and Forestry</option>
													<option value="17" disabled="true">Family and consumer science</option>
													<option value="53" disabled="true">Health Care</option>
													<option value="44" disabled="true">International Trade</option>
													<option value="39" disabled="true">IT, Web</option>
													<option value="21" disabled="true">Law</option>
													<option value="77">Leadership Studies</option>
													<option value="80" disabled="true">Medical Sciences (Anatomy, Physiology, Pharmacology etc.)</option>
													<option value="25" disabled="true">Medicine</option>
													<option value="47" disabled="true">Nursing</option>
													<option value="78">Nutrition/Dietary</option>
													<option value="79" disabled="true">Public Administration</option>
													<option value="55">Sports</option>
													<option value="38" disabled="true">Technology</option>
												</optgroup>
												<optgroup label="Other">
													<option value="52">Other (enter below)</option>
												</optgroup>
											</select><div class="uvo_pref-uvoAutocomplete-wrapper"><input class="uvo_pref-uvoAutocomplete-input" name="topcat_id_input" type="text" readonly="" data-finder="topcat_id" data-value=""><span class="ui-icon ui-icon-triangle-1-s" style="position: absolute; cursor: pointer; right: 10px; top: 20px;"></span><div class="dr-down" style="display: none; top: 38px;"><span class="dr-down-search"><span class="dr-down-zoom"></span><input type="text" class="dr-down-input smaller" placeholder="Search within list"></span><div class="dr-down-list"><a href="https://essayhave.com/order.html#" class="dr-down-list-item uvo_pref-uvoAutocomplete-default-option" data-value="">Please select</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="1">Accounting</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="70">Agriculture</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="2">Anthropology</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="3">Application Letters</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="5">Architecture, Building and Planning</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="81">Art (Fine arts, Performing arts)</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="67">Astronomy (and other Space Sciences)</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="71">Aviation</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="42">Biology (and other Life Sciences)</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="8">Business Studies</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="9">Chemistry</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="72">Civil Engineering</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="10">Classic English Literature</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="11">Communications</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="57">Composition</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="12">Computer science</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="73">Criminal Justice</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="48">Criminal law</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="61">Cultural and Ethnic Studies</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="68">Ecology</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="15">Economics</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="16">Education</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="74">Engineering</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="56">English 101</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="50">Environmental studies and Forestry</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="65">Ethics</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="17">Family and consumer science</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="18">Film &amp; Theater studies</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="19">Finance</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="49">Geography</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="69">Geology (and other Earth Sciences)</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="53">Health Care</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="20">History</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="75">Human Resources Management (HRM)</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="39">IT, Web</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="62">International Relations</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="44">International Trade</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="21">Law</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="77">Leadership Studies</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="59">Linguistics</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="41">Literature</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="63">Logistics</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="22">Management</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="23">Marketing</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="24">Mathematics</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="80">Medical Sciences (Anatomy, Physiology, Pharmacology etc.)</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="25">Medicine</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="26">Music</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="47">Nursing</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="78">Nutrition/Dietary</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="27">Philosophy</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="28">Physics</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="60">Poetry</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="29">Political science</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="30">Psychology</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="79">Public Administration</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="76">Public Relations (PR)</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="31">Religious studies</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="34">Shakespeare</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="54">Social Work and Human Services</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="35">Sociology</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="55">Sports</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="37">Statistics</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="38">Technology</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="64">Tourism</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="66">Urban Studies</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="40">Women's &amp; gender studies</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item ui-state-disabled" data-value="45">Zoology</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="52">Other (enter below)</a></div></div></div>
											<div id="other_box_topcat_id" style="display: none;">
												<input name="topcat_option" id="topcat_option" class="f_sz_full validate[required,custom[subjectother]]" maxlength="1000" type="text" data-finder="form.input.topcatoption">
											</div>
										</div>
									</div>

									<div id="box_toptitle" class="row">
										<div class="cell cell-left with-mobile-tip">
											<div class="mobile-tip" data-hasqtip="19"></div>
											<label for="" class="left-label">Topic:</label>
										</div>
										<div class="cell cell-right side_tip side_tip_toptitle" data-hasqtip="18" aria-describedby="qtip-18">
											<input name="toptitle" id="toptitle" class="validate[required,custom[topiclength]] f_sz_full" maxlength="1000" type="text" data-finder="form.input.toptitle">
										</div>
									</div>

									<div id="box_paperdets" class="row">
										<div class="cell cell-left with-mobile-tip">
											<div class="mobile-tip" data-hasqtip="21"></div>
											<label for="" class="left-label">Paper instructions:</label>
										</div>
										<div class="cell cell-right side_tip side_tip_paperdets" data-hasqtip="20" aria-describedby="qtip-20">
											<textarea name="paperdets" class="expand50-200 f_sz_full" rows="4" id="paperdets" placeholder="Optional field..." data-finder="form.textarea.paperdets"></textarea>
										</div>
									</div>

									<div id="box_min_source" class="row">
										<div class="cell cell-left">
											<label for="" class="left-label">Sources:</label>
										</div>
										<div class="cell cell-right"><div id="contentspinmin_source" class="contentspin clearfix"><button type="button" class="dec buttonspin" data-finder="spinner.decrease.min_source" title="Decrease">−</button><input name="min_source" id="min_source" type="text" class="f_sz_30 validate[required,custom[sourcesrequired]]" maxlength="5" value="0" data-finder="form.input.minsource"><button type="button" class="inc buttonspin" data-finder="spinner.increase.min_source" title="Increase">+</button></div>

										<div class="field_tip">Please note the number of sources has been predetermined based on our experiences; however, this can be reset at your discretion.</div>
									</div>
								</div>

								<div id="box_addmaterials" class="row">
									<div class="cell cell-left with-mobile-tip">
										<label for="" class="left-label">Additional materials:</label>
									</div>
									<div class="cell cell-right cell-additional-materials side_tip side_tip_additional_materials with-mobile-tip checkbox-wrapper" data-hasqtip="22" aria-describedby="qtip-22">
										<div class="mobile-tip" data-hasqtip="23"></div>

										<div id="order-customer-files-container" class="additional_materials_files_container" data-finder="form.container.order-customer-files-container"></div>
										<a class="button form-files-up-button upload-dialog-triger" href="https://essayhave.com/order.html#" data-finder="form.link.uploadfiles" onclick="ga('send', 'event', 'button', 'click', 'upload_files_orderform');"><i class="form-files-but-icon"></i> Upload files...</a><br>

										<script>
											jQuery(function(){
												jQuery("#customer_upload_dialog").dialog({
												autoOpen: false,
												title:"Upload Additional Materials",
												width:'915px',
												modal: true,
											});

											jQuery('.upload-dialog-triger').click(function(){
												var elem = jQuery("#customer_upload_dialog");
												elem.dialog({
													resizable: false
												});  // end dialog
												elem.dialog('open');

												//jQuery('#customer_upload_dialog').dialog('open');
												return false;
											});


											function expand (e) {
												jQuery(e).find('.ago, .hiddenpart').slideToggle(300);
												jQuery(e).removeClass('collapsed');
												jQuery(e).next('.file').css({'border-top': 'none', 'margin-top':'0'});
											};
											window.expand = expand;

											function collapse (e) {
												jQuery(e).find('.ago, .hiddenpart').slideToggle(300);
												jQuery(e).addClass('collapsed');
												jQuery(e).next('.file').css({'border-top': '1px solid #E0E0E0', 'margin-top':'-1px'});
											};
											window.collapse = collapse;

											jQuery('#addmaterials_dialog_complete').click(function()
											{
												if (jQuery('#addmaterials_dialog_complete').is(":checked"))
												{
													jQuery('#addmaterials_hidden').val("2");
													jQuery('#addmaterials_field_tip_yes').removeClass('hidden');
													jQuery('#addmaterials_field_tip_no').addClass('hidden');
												}
												else{
													jQuery('#addmaterials_hidden').val("0");
													jQuery('#addmaterials_field_tip_yes').addClass('hidden');
													jQuery('#addmaterials_field_tip_no').removeClass('hidden');
												}
											});	
											});	
										</script>

										<input type="hidden" id="addmaterials" name="addmaterials" value="2">
										<!--<label for="addmaterials" class="large">I will upload additional materials later</label>-->

										<input name="addmaterials_hidden" id="addmaterials_hidden" type="hidden" value="0">

					<!--<div class="checkbox-tip field_tip hidden" id="addmaterials_field_tip_default">If you have any additional materials such as articles, diagrams, graphs, or other documents that might help the writer in completion of the paper, please upload them on your personal order page.</div>
					<div class="checkbox-tip field_tip" id="addmaterials_field_tip_no">Please note that in case your paper requires specific information/materials to be used, the writer will need additional time to retrieve them.</div>
					<div class="checkbox-tip field_tip hidden" id="addmaterials_field_tip_yes">Please make sure to upload materials right after you are done with filling out in this order form. It is essential for us to start looking for the writer based on all of your instructions. To upload materials, you need to log in to your personal order page. The link will be provided to you once you submit this form.</div>-->
				</div>
			</div>

			<div id="box_paperformat" class="row ui-buttonset">
				<div class="cell cell-left">
					<label for="" class="left-label">Paper format or citation style:</label>
				</div>
				<div class="cell cell-right"><div class="selects visible-in-mobile" style="display: none;"><select id="mob_paperformat" name="mob_paperformat" class="dr-down-input" style="position: absolute; top: 0px; left: 0px; visibility: hidden;"><option value="0" selected="selected">MLA</option><option value="1">APA</option><option value="2">Chicago / Turabian</option><option value="3">Not applicable</option><option value="4">Other</option></select><div class="uvo_pref-uvoAutocomplete-wrapper"><input class="uvo_pref-uvoAutocomplete-input" name="mob_paperformat_input" type="text" readonly="" data-finder="mob_paperformat" data-value="0"><span class="ui-icon ui-icon-triangle-1-s" style="position: absolute; cursor: pointer; right: 10px; top: 20px;"></span><div class="dr-down" style="display: none; top: 38px;"><span class="dr-down-search" style="display: none;"><span class="dr-down-zoom"></span><input type="text" class="dr-down-input smaller" placeholder="Search within list"></span><div class="dr-down-list"><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="0">MLA</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="1">APA</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="2">Chicago / Turabian</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="3">Not applicable</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="4">Other</a></div></div></div></div>
				<div class="radios visible-in-desktop">
					<input type="radio" title="MLA" id="radio_paper_format_0" value="0" name="paperformat" data-finder="form.input.radiopaperformat0" class="ui-helper-hidden-accessible" checked="checked"><label for="radio_paper_format_0" id="tip_radio_paper_format_0" data-finder="form.label.radiopaperformat0" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-state-active" role="button" aria-disabled="false" aria-pressed="true"><span class="ui-button-text">MLA</span></label>
					<input type="radio" title="APA" id="radio_paper_format_1" value="1" name="paperformat" data-finder="form.input.radiopaperformat1" class="ui-helper-hidden-accessible"><label for="radio_paper_format_1" id="tip_radio_paper_format_1" data-finder="form.label.radiopaperformat1" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false" aria-pressed="false"><span class="ui-button-text">APA</span></label>
					<input type="radio" title="Chicago / Turabian" id="radio_paper_format_2" value="2" name="paperformat" data-finder="form.input.radiopaperformat2" class="ui-helper-hidden-accessible"><label for="radio_paper_format_2" id="tip_radio_paper_format_2" data-finder="form.label.radiopaperformat2" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false" aria-pressed="false"><span class="ui-button-text">Chicago / Turabian</span></label>
					<input type="radio" title="Not applicable" id="radio_paper_format_3" value="3" name="paperformat" data-finder="form.input.radiopaperformat3" class="ui-helper-hidden-accessible"><label for="radio_paper_format_3" id="tip_radio_paper_format_3" data-finder="form.label.radiopaperformat3" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false" aria-pressed="false"><span class="ui-button-text">Not applicable</span></label>
					<input type="radio" title="Other" id="radio_paper_format_4" value="4" name="paperformat" data-finder="form.input.radiopaperformat4" class="ui-helper-hidden-accessible"><label for="radio_paper_format_4" id="tip_radio_paper_format_4" data-finder="form.label.radiopaperformat4" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-right" role="button" aria-disabled="false" aria-pressed="false"><span class="ui-button-text">Other</span></label>
				</div>
				<div id="other_box_paperformat" style="display: none;">
					<input type="text" name="paper_format_type_option" maxlength="50" class="f_sz_full validate[required,custom[paperformatother]]" id="paper_format_type_option" data-finder="form.input.paperformattypeoption">

				</div>
			</div>
		</div>
	</div>
	<div class="orderform-buttons f_submit_box f_submit_box_logged">

		<a href="javascript:void(0)" class="orderform-submit button highlight large to_step2_btn" onclick="ga('send', 'event', 'button', 'click', 'go to price calculation');" data-finder="form.link.tostep2">Go to step 2. Price calculation →</a>

	</div>
</fieldset>
</div>


<div id="tab_price" aria-labelledby="ui-id-9" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;">
	<fieldset id="fieldset_s2" class="orderform-step table no-padding-bottom">

		<div id="box_pagesreq" class="row ui-buttonset">
			<div class="cell cell-left with-mobile-tip">
				<div class="mobile-tip" data-hasqtip="25"></div>
				<label for="" class="left-label">Pages:</label>
			</div>
			<div class="side_tip side_tip_pages cell cell-right" data-hasqtip="24"><div id="contentspinpagesreq" class="contentspin clearfix"><button type="button" class="dec buttonspin" data-finder="spinner.decrease.pagesreq" title="Decrease">−</button><input type="text" id="pagesreq" name="pagesreq" maxlength="5" value="2" class="f_sz_30 validate[required,funcCall[pagesOrSlidesOrChartsRequired]]" minval="0" data-finder="form.input.pagesreq"><button type="button" class="inc buttonspin" data-finder="spinner.increase.pagesreq" title="Increase">+</button></div>

			<span class="words_total"><span id="words_total_qty">550</span> words</span>
			<div class="spacing-inliner"><div class="selects visible-in-mobile" style="display: none;"><select id="mob_spacing" name="mob_spacing" class="dr-down-input" style="position: absolute; top: 0px; left: 0px; visibility: hidden;"><option value="double" selected="selected">Double spaced</option><option value="single">Single spaced</option></select><div class="uvo_pref-uvoAutocomplete-wrapper"><input class="uvo_pref-uvoAutocomplete-input" name="mob_spacing_input" type="text" readonly="" data-finder="mob_spacing" data-value="double"><span class="ui-icon ui-icon-triangle-1-s" style="position: absolute; cursor: pointer; right: 10px; top: 17px;"></span><div class="dr-down" style="display: none; top: 38px;"><span class="dr-down-search" style="display: none;"><span class="dr-down-zoom"></span><input type="text" class="dr-down-input smaller" placeholder="Search within list"></span><div class="dr-down-list"><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="double">Double spaced</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="single">Single spaced</a></div></div></div></div>
			<div class="radios radios-spacing visible-in-desktop">
				<input name="spacing" id="spacing_double" value="double" type="radio" data-finder="form.input.spacingdouble" class="ui-helper-hidden-accessible" checked="checked"><label id="spacing_double_btn" for="spacing_double" data-finder="form.label.spacingdouble" data-hasqtip="4" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-state-active" role="button" aria-disabled="false" aria-pressed="true"><span class="ui-button-text">Double spaced</span></label>
				<input name="spacing" id="spacing_single" value="single" type="radio" data-finder="form.input.spacingsingle" class="ui-helper-hidden-accessible"><label id="spacing_single_btn" for="spacing_single" data-finder="form.label.spacingsingle" data-hasqtip="5" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-right" role="button" aria-disabled="false" aria-pressed="false"><span class="ui-button-text">Single spaced</span></label>
			</div>
		</div>
	</div>
</div>

<div id="box_first_draft_deadline" class="row ui-buttonset">
	<div class="cell cell-left with-mobile-tip">
		<div class="mobile-tip" data-hasqtip="27"></div>
		<label for="" class="left-label">First draft deadline:</label>
	</div>
	<div class="side_tip side_tip_deadlines cell cell-right" data-hasqtip="26"><div class="selects visible-in-mobile" style="display: none;"><select id="mob_deadline" name="mob_deadline" class="dr-down-input" style="position: absolute; top: 0px; left: 0px; visibility: hidden;"><option value="52723">8 hours</option><option value="52721">24 hours</option><option value="68095">48 hours</option><option value="68093" selected="selected">3 days</option><option value="68091">5 days</option><option value="68089">7 days</option><option value="68087">14 days</option></select><div class="uvo_pref-uvoAutocomplete-wrapper"><input class="uvo_pref-uvoAutocomplete-input" name="mob_deadline_input" type="text" readonly="" data-finder="mob_deadline" data-value="68093"><span class="ui-icon ui-icon-triangle-1-s" style="position: absolute; cursor: pointer; right: 10px; top: 17px;"></span><div class="dr-down" style="display: none; top: 38px;"><span class="dr-down-search" style="display: none;"><span class="dr-down-zoom"></span><input type="text" class="dr-down-input smaller" placeholder="Search within list"></span><div class="dr-down-list"><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="52723">8 hours</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="52721">24 hours</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="68095">48 hours</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="68093">3 days</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="68091">5 days</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="68089">7 days</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="68087">14 days</a></div></div></div></div>
	<div class="radios visible-in-desktop" id="deadlines"><input type="radio" id="radio_deadline_52723" name="deadline" hrs="8" value="52723" price_per_page="25" hrs30="10.4" hrs60="12.8" class="ui-helper-hidden-accessible"><label for="radio_deadline_52723" id="tip_radio_deadline_52723" hrs="8" value="52723" data-finder="form.label.radiodeadline8" data-hasqtip="65" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left" role="button" aria-disabled="false" aria-pressed="false"><span class="ui-button-text">8 hours</span></label>
		<input type="radio" id="radio_deadline_52721" name="deadline" hrs="24" value="52721" price_per_page="22" hrs30="31.2" hrs60="38.4" class="ui-helper-hidden-accessible"><label for="radio_deadline_52721" id="tip_radio_deadline_52721" hrs="24" value="52721" data-finder="form.label.radiodeadline24" data-hasqtip="66" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false" aria-pressed="false"><span class="ui-button-text">24 hours</span></label>
		<input type="radio" id="radio_deadline_68095" name="deadline" hrs="48" value="68095" price_per_page="19" hrs30="62.4" hrs60="76.8" class="ui-helper-hidden-accessible"><label for="radio_deadline_68095" id="tip_radio_deadline_68095" hrs="48" value="68095" data-finder="form.label.radiodeadline48" data-hasqtip="67" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false" aria-pressed="false"><span class="ui-button-text">48 hours</span></label>
		<input type="radio" id="radio_deadline_68093" name="deadline" hrs="72" value="68093" price_per_page="16" hrs30="93.6" hrs60="115.2" checked="checked" class="ui-helper-hidden-accessible"><label for="radio_deadline_68093" id="tip_radio_deadline_68093" hrs="72" value="68093" data-finder="form.label.radiodeadline72" data-hasqtip="68" class="ui-state-active ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false" aria-pressed="true"><span class="ui-button-text">3 days</span></label>
		<input type="radio" id="radio_deadline_68091" name="deadline" hrs="120" value="68091" price_per_page="15" hrs30="156.0" hrs60="192.0" class="ui-helper-hidden-accessible"><label for="radio_deadline_68091" id="tip_radio_deadline_68091" hrs="120" value="68091" data-finder="form.label.radiodeadline120" data-hasqtip="69" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false" aria-pressed="false"><span class="ui-button-text">5 days</span></label>
		<input type="radio" id="radio_deadline_68089" name="deadline" hrs="168" value="68089" price_per_page="12" hrs30="218.4" hrs60="268.8" class="ui-helper-hidden-accessible"><label for="radio_deadline_68089" id="tip_radio_deadline_68089" hrs="168" value="68089" data-finder="form.label.radiodeadline168" data-hasqtip="70" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false" aria-pressed="false"><span class="ui-button-text">7 days</span></label>
		<input type="radio" id="radio_deadline_68087" name="deadline" hrs="336" value="68087" price_per_page="10" hrs30="436.8" hrs60="537.6" class="ui-helper-hidden-accessible"><label for="radio_deadline_68087" id="tip_radio_deadline_68087" hrs="336" value="68087" data-finder="form.label.radiodeadline336" data-hasqtip="71" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-right" role="button" aria-disabled="false" aria-pressed="false"><span class="ui-button-text">14 days</span></label>
	</div>
	<div class="field_tip">We estimate that your final submission deadline is <strong class="tip_attention" id="ext_time">7 Jun 16, 1:13 PM (GMT+07)</strong></div>
	<input type="hidden" name="max_ext_time" id="max_ext_time" value="2016-06-07 01:11:12">
</div>
</div>

<div id="box_writer_preferences" class="row">
	<div class="cell cell-left with-mobile-tip">
		<div class="mobile-tip" data-hasqtip="29"></div>
		<label for="" class="left-label">Category of writer:</label>
	</div>
	<div class="side_tip side_tip_category_of_writer cell cell-right" data-hasqtip="28"><div class="selects visible-in-mobile" style="display: none;"><select id="mob_writer_preferences" name="mob_writer_preferences" class="dr-down-input" style="position: absolute; top: 0px; left: 0px; visibility: hidden;"><option value="2" selected="selected">Best availableStandard price</option><option value="3">AdvancedHigh-rank professional writer, proficient in the requested field of study+25%</option><option value="4">ENLEnglish as a native language writer (US, UK, CA, AU writers)+30%</option></select><div class="uvo_pref-uvoAutocomplete-wrapper"><input class="uvo_pref-uvoAutocomplete-input" name="mob_writer_preferences_input" type="text" readonly="" data-finder="mob_writer_preferences" data-value="2"><span class="ui-icon ui-icon-triangle-1-s" style="position: absolute; cursor: pointer; right: 10px; top: 17px;"></span><div class="dr-down" style="display: none; top: 38px;"><span class="dr-down-search" style="display: none;"><span class="dr-down-zoom"></span><input type="text" class="dr-down-input smaller" placeholder="Search within list"></span><div class="dr-down-list"><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="2">Best availableStandard price</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="3">AdvancedHigh-rank professional writer, proficient in the requested field of study+25%</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="4">ENLEnglish as a native language writer (US, UK, CA, AU writers)+30%</a></div></div></div></div>
	<div class="radios writer-category ui-buttonset visible-in-desktop" id="box_category_of_writer_rad">
		<input type="radio" id="radio_writer_preferences_2" name="writer_preferences" value="2" data-finder="form.input.radiowriterpreferences2" class="ui-helper-hidden-accessible" checked="checked"><label for="radio_writer_preferences_2" id="tip_radio_writer_preferences_2" data-finder="form.label.radiowriterpreferences2" data-hasqtip="6" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-state-active" role="button" aria-disabled="false" aria-pressed="true"><span class="ui-button-text">Best available<span class="writer_preferences_text">Standard price</span></span></label>
		<input type="radio" title="Advanced" id="radio_writer_preferences_3" value="3" name="writer_preferences" sample_available="1" percent="25" data-finder="form.input.radiowriterpreferences3" class="ui-helper-hidden-accessible"><label for="radio_writer_preferences_3" id="tip_radio_writer_preferences_3" data-finder="form.label.radiowriterpreferences3" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false" aria-pressed="false"><span class="ui-button-text">Advanced<span class="writer_preferences_text">High-rank professional writer, proficient in the requested field of study<strong>+25%</strong></span></span></label>
		<input type="radio" title="ENL" id="radio_writer_preferences_4" value="4" name="writer_preferences" sample_available="1" percent="30" data-finder="form.input.radiowriterpreferences4" class="ui-helper-hidden-accessible"><label for="radio_writer_preferences_4" id="tip_radio_writer_preferences_4" data-finder="form.label.radiowriterpreferences4" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-right" role="button" aria-disabled="false" aria-pressed="false"><span class="ui-button-text">ENL<span class="writer_preferences_text">English as a native language writer (US, UK, CA, AU writers)<strong>+30%</strong></span></span></label>
	</div>
</div>
</div>



<div id="box_slidesreq" class="row">
	<div class="cell cell-left">
		<div class="mobile-tip with-mobile-tip"></div>
		<label for="" class="left-label">Power Point slides:</label>
	</div>
	<div class="side_tip side_tip_slidesreq cell cell-right" data-hasqtip="30" aria-describedby="qtip-30"><div id="contentspinslidesreq" class="contentspin clearfix"><button type="button" class="dec buttonspin ui-state-disabled" data-finder="spinner.decrease.slidesreq" title="Decrease" disabled="">−</button><input type="text" id="slidesreq" name="slidesreq" value="0" maxlength="5" class="f_sz_30 validate[required,funcCall[pagesOrSlidesOrChartsRequired]]" minval="0" data-finder="form.input.slidesreq"><button type="button" class="inc buttonspin" data-finder="spinner.increase.slidesreq" title="Increase">+</button></div>

</div>
</div>

<div id="box_chartsreq" class="row">
	<div class="cell cell-left">
		<div class="mobile-tip with-mobile-tip"></div>
		<label for="" class="left-label">Сharts:</label>
	</div>
	<div class="side_tip side_tip_chartsreq cell cell-right" data-hasqtip="31" aria-describedby="qtip-31"><div id="contentspinchartsreq" class="contentspin clearfix"><button type="button" class="dec buttonspin ui-state-disabled" data-finder="spinner.decrease.chartsreq" title="Decrease" disabled="">−</button><input type="text" id="chartsreq" name="chartsreq" value="0" maxlength="5" class="f_sz_30 validate[required,funcCall[pagesOrSlidesOrChartsRequired]]" minval="0" data-finder="form.input.chartsreq"><button type="button" class="inc buttonspin" data-finder="spinner.increase.chartsreq" title="Increase">+</button></div>

</div>
</div>


</fieldset>
<div class="features-block">
	<h4 class="step_header features-header">Additional features</h4>

	<div id="yes_no_features" class="features">


		<input value="1" type="checkbox" name="samples_needed" id="samples_needed" data-finder="form.input.samplesneeded">
		<label class="feature large" for="samples_needed" id="tip_order_samples" data-finder="form.label.samplesneeded" data-hasqtip="8">

			<div class="feature-header">
				Get writer's <br>samples			</div>
				<span class="feature-price">$5</span>
			</label>


			<input value="2" type="checkbox" name="usedsources" id="usedsources" data-finder="form.input.usedsources">
			<label class="feature large" for="usedsources" id="tip_used_sources" data-finder="form.label.usedsources" data-hasqtip="9">

				<div class="feature-header">
					Get a copy of <br>sources used			</div>
					<span id="used_sources_cost" class="feature-price"><span class="form-ico form-ico-source">$14<sup>.95</sup></span></span>
				</label>


				<input value="1" type="checkbox" name="progressive_delivery" id="progressive_delivery" data-finder="form.input.progressivedelivery" disabled="">
				<label class="feature large" id="box_progressive_delivery" for="progressive_delivery" data-finder="form.label.progressivedelivery_disabled_nocheck">

					<div id="tip_progressive_delivery" data-hasqtip="7">
						<div class="feature-header">
							Progressive <br>Delivery				</div>
							<span class="feature-price">+10%</span>
							<input type="hidden" name="progressive_delivery_hidden" id="progressive_delivery_hidden" value="0">
							<input type="hidden" name="progressive_delivery_price" id="progressive_delivery_price" value="0">
						</div>
					</label>

				</div><!-- /yes_no_features --></div>
				<div class="step2_total_price">
					<div id="box_total_price" class="step2_total_price_blocks">
						<div class="step2_total_price_block step2_total_price_text">
							<p>The discount information is available on the next step of placing the order.</p>
						</div>
						<div class="step2_total_price_block step2_total_price_place">
							<p>Grand total price: <span class="total-price-sum total_price total_price_box">$32<sup>.00</sup></span></p>
						</div>
					</div>

					<div class="orderform-buttons f_submit_box f_submit_box_logged">

						<a href="javascript:void(0)" class="orderform-submit button highlight large to_step3_btn" onclick="ga('send', 'event', 'button', 'click', 'go to personal info');" data-finder="form.link.tostep3">Go to step 3. Personal information →</a>
						<a href="javascript:void(0)" class="orderform-prev input-model large order_form_repeat_step1" data-finder="form.link.backtostep1">← Previous step</a>

					</div>
				</div>


			</div>

			<div id="tab_personal" aria-labelledby="ui-id-10" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;">
				<fieldset id="fieldset_s3" class="orderform-step clearfix">

					<!--
			<div class="formtitle">Please insert your personal information in the fields below and pass on to check out. Further on you will be redirected to PayPal or Moneybookers site.</div>
		-->
		<div id="tabs_customer_type" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
			<ul class="cabinet-tabs orderform-inside-tabs ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
				<li class="cabinet-tab ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="tabs-1" aria-labelledby="tabs_1" aria-selected="true"><a id="tabs_1" class="cabinet-tab-link orderform-inside-tab-link ui-tabs-anchor" href="https://essayhave.com/order.html#tabs-1" data-finder="form.link.tabcustomer1" role="presentation" tabindex="-1">New customer</a></li>
				<li class="cabinet-tab ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-2" aria-labelledby="tabs_2" aria-selected="false"><a id="tabs_2" class="cabinet-tab-link orderform-inside-tab-link ui-tabs-anchor" href="https://essayhave.com/order.html#tabs-2" data-finder="form.link.tabcustomer2" role="presentation" tabindex="-1">Returning customer</a></li>
			</ul>

			<!-- New User tab -->
			<div id="tabs-1" class="orderform-auth-container ui-tabs-panel ui-widget-content ui-corner-bottom" aria-labelledby="tabs_1" role="tabpanel" aria-expanded="true" aria-hidden="false">
				<!-- User email -->
				<div id="box_email" class="row">
					<div class="cell cell-left">
						<label for="" class="left-label">E-mail:</label>
					</div>
					<div class="cell cell-right">
						<input type="text" name="email" maxlength="100" id="email" class="validate[required,custom[email],funcCall[isDuplicatedEmail]] f_sz_220" data-validation-class="validate[required,custom[email],funcCall[isDuplicatedEmail]]" data-finder="form.input.email">
						<em id="email_ok" class="validation_img hidden">&nbsp;</em>
						<em id="email_err" class="validation_img hidden">&nbsp;</em>
					</div>
				</div>

				<!-- User password -->
				<div id="box_password" class="row">
					<div class="cell cell-left">
						<label for="" class="left-label">Create password:</label>
					</div>
					<div class="cell cell-right">
						<input name="password" id="password" class="f_sz_220 validate[required,custom[passwordrequired],custom[noSpecialCaracters],custom[minPasswordSize]]" data-validation-new="validate[required,custom[passwordrequired],custom[noSpecialCaracters],custom[minPasswordSize]]" data-validation-existing="validate[required,custom[passwordrequired],custom[noSpecialCaracters],custom[minPasswordSize]]" maxlength="30" value="" type="password" data-finder="form.input.password">
						<em id="password_ok" class="validation_img hidden">&nbsp;</em>
						<em id="password_err" class="validation_img hidden">&nbsp;</em>
					</div>
				</div>

				<!-- Password confirmation if needed -->
				<div id="box_confirm_password" class="row">
					<div class="cell cell-left">
						<label for="" class="left-label">Confirm password:</label>
					</div>
					<div class="cell cell-right">
						<input name="confirm_password" id="confirm_password" class="f_sz_220 validate[required,equals[password]]" data-validation-class="validate[required,equals[password]]" maxlength="30" value="" type="password" data-finder="form.input.confirmpassword">
						<em id="confirm_password_ok" class="validation_img hidden">&nbsp;</em>
						<em id="confirm_password_err" class="validation_img hidden">&nbsp;</em>
					</div>
				</div>

				<!-- User name if needed -->
				<div id="box_fname" class="row">
					<div class="cell cell-left">
						<label for="" class="left-label">Name:</label>
					</div>
					<div class="cell cell-right">
						<input name="fname" id="fname" maxlength="100" class="f_sz_220 validate[required,custom[namerequired]]" data-validation-class="validate[required,custom[namerequired]]" type="text" data-finder="form.input.fname">
						<em id="fname_ok" class="validation_img hidden">&nbsp;</em>
						<em id="fname_err" class="validation_img hidden">&nbsp;</em>
					</div>
				</div>
				<!-- Customer country -->
				<div id="box_customer_country" class="row">
					<div class="cell cell-left">
						<label for="" class="left-label">Your country:</label>
					</div>
					<div class="cell cell-right">
						<input type="hidden" name="mobile_phone_country_hidden" id="mobile_phone_country_hidden" value="704">
						<select name="mobile_phone_country" id="mobile_phone_country" class="f_sz_300  dr-down-input uvo_pref-uvoAutocomplete-disabled ui-state-disabled" data-validation-class="validate[required,custom[countryrequired]]" disabled="disabled" data-finder="form.select.mobilephonecountry" aria-disabled="true" style="position: absolute; top: 0px; left: 0px; visibility: hidden;">
							<option selected="selected" phone_code="1">Please select your country</option>
							<option phone_code="376" value="020">Andorra</option>
							<option phone_code="971" value="784">United Arab Emirates</option>
							<option phone_code="93" value="004">Afghanistan</option>
							<option phone_code="1268" value="028">Antigua And Barbuda</option>
							<option phone_code="1264" value="660">Anguilla</option>
							<option phone_code="355" value="008">Albania</option>
							<option phone_code="374" value="051">Armenia</option>
							<option phone_code="599" value="530">Netherlands Antilles</option>
							<option phone_code="244" value="024">Angola</option>
							<option phone_code="672" value="010">Antarctica</option>
							<option phone_code="54" value="032">Argentina</option>
							<option phone_code="684" value="016">American Samoa</option>
							<option phone_code="43" value="040">Austria</option>
							<option phone_code="61" value="036">Australia</option>
							<option phone_code="297" value="533">Aruba</option>
							<option phone_code="222" value="248">Aland Islands</option>
							<option phone_code="994" value="031">Azerbaijan</option>
							<option phone_code="387" value="070">Bosnia And Herzegovina</option>
							<option phone_code="1246" value="052">Barbados</option>
							<option phone_code="880" value="050">Bangladesh</option>
							<option phone_code="32" value="056">Belgium</option>
							<option phone_code="226" value="854">Burkina Faso</option>
							<option phone_code="359" value="100">Bulgaria</option>
							<option phone_code="973" value="048">Bahrain</option>
							<option phone_code="257" value="108">Burundi</option>
							<option phone_code="229" value="204">Benin</option>
							<option phone_code="" value="652">Saint Barthelemy</option>
							<option phone_code="1441" value="060">Bermuda</option>
							<option phone_code="673" value="096">Brunei Darussalam</option>
							<option phone_code="591" value="068">Bolivia</option>
							<option phone_code="55" value="076">Brazil</option>
							<option phone_code="1242" value="044">Bahamas</option>
							<option phone_code="975" value="064">Bhutan</option>
							<option phone_code="" value="074">Bouvet Island</option>
							<option phone_code="267" value="072">Botswana</option>
							<option phone_code="375" value="112">Belarus</option>
							<option phone_code="501" value="084">Belize</option>
							<option phone_code="1" value="124">Canada</option>
							<option phone_code="61" value="166">Cocos (keeling) Islands</option>
							<option phone_code="243" value="180">Congo, The Democratic Republic Of</option>
							<option phone_code="236" value="140">Central African Republic</option>
							<option phone_code="242" value="178">Congo</option>
							<option phone_code="41" value="756">Switzerland</option>
							<option phone_code="225" value="384">Cote D'ivoire</option>
							<option phone_code="682" value="184">Cook Islands</option>
							<option phone_code="56" value="152">Chile</option>
							<option phone_code="237" value="120">Cameroon</option>
							<option phone_code="86" value="156">China</option>
							<option phone_code="57" value="170">Colombia</option>
							<option phone_code="506" value="188">Costa Rica</option>
							<option phone_code="53" value="192">Cuba</option>
							<option phone_code="238" value="132">Cape Verde</option>
							<option phone_code="61" value="162">Christmas Island</option>
							<option phone_code="357" value="196">Cyprus</option>
							<option phone_code="420" value="203">Czech Republic</option>
							<option phone_code="49" value="276">Germany</option>
							<option phone_code="253" value="262">Djibouti</option>
							<option phone_code="45" value="208">Denmark</option>
							<option phone_code="1767" value="212">Dominica</option>
							<option phone_code="809" value="214">Dominican Republic</option>
							<option phone_code="213" value="012">Algeria</option>
							<option phone_code="593" value="218">Ecuador</option>
							<option phone_code="372" value="233">Estonia</option>
							<option phone_code="20" value="818">Egypt</option>
							<option phone_code="" value="732">Western Sahara</option>
							<option phone_code="291" value="232">Eritrea</option>
							<option phone_code="34" value="724">Spain</option>
							<option phone_code="251" value="231">Ethiopia</option>
							<option phone_code="358" value="246">Finland</option>
							<option phone_code="679" value="242">Fiji</option>
							<option phone_code="500" value="238">Falkland Islands (malvinas)</option>
							<option phone_code="691" value="583">Micronesia, Federated States Of</option>
							<option phone_code="298" value="234">Faroe Islands</option>
							<option phone_code="33" value="250">France</option>
							<option phone_code="241" value="266">Gabon</option>
							<option phone_code="44" value="826">United Kingdom</option>
							<option phone_code="1473" value="308">Grenada</option>
							<option phone_code="995" value="268">Georgia</option>
							<option phone_code="594" value="254">French Guiana</option>
							<option phone_code="441481" value="831">Guernsey</option>
							<option phone_code="233" value="288">Ghana</option>
							<option phone_code="350" value="292">Gibraltar</option>
							<option phone_code="299" value="304">Greenland</option>
							<option phone_code="220" value="270">Gambia</option>
							<option phone_code="224" value="324">Guinea</option>
							<option phone_code="590" value="312">Guadeloupe</option>
							<option phone_code="240" value="226">Equatorial Guinea</option>
							<option phone_code="30" value="300">Greece</option>
							<option phone_code="" value="239">South Georgia And The South Sandwich Islands</option>
							<option phone_code="502" value="320">Guatemala</option>
							<option phone_code="1671" value="316">Guam</option>
							<option phone_code="245" value="624">Guinea-bissau</option>
							<option phone_code="592" value="328">Guyana</option>
							<option phone_code="852" value="344">Hong Kong</option>
							<option phone_code="" value="334">Heard Island And Mcdonald Islands</option>
							<option phone_code="504" value="340">Honduras</option>
							<option phone_code="385" value="191">Croatia</option>
							<option phone_code="509" value="332">Haiti</option>
							<option phone_code="36" value="348">Hungary</option>
							<option phone_code="62" value="360">Indonesia</option>
							<option phone_code="353" value="372">Ireland</option>
							<option phone_code="972" value="376">Israel</option>
							<option phone_code="441624" value="833">Isle Of Man</option>
							<option phone_code="91" value="356">India</option>
							<option phone_code="" value="086">British Indian Ocean Territory</option>
							<option phone_code="964" value="368">Iraq</option>
							<option phone_code="98" value="364">Iran, Islamic Republic Of</option>
							<option phone_code="354" value="352">Iceland</option>
							<option phone_code="39" value="380">Italy</option>
							<option phone_code="441534" value="832">Jersey</option>
							<option phone_code="1876" value="388">Jamaica</option>
							<option phone_code="962" value="400">Jordan</option>
							<option phone_code="81" value="392">Japan</option>
							<option phone_code="254" value="404">Kenya</option>
							<option phone_code="996" value="417">Kyrgyzstan</option>
							<option phone_code="855" value="116">Cambodia</option>
							<option phone_code="686" value="296">Kiribati</option>
							<option phone_code="269" value="174">Comoros</option>
							<option phone_code="1869" value="659">Saint Kitts And Nevis</option>
							<option phone_code="850" value="408">Korea, Democratic People's Republic Of</option>
							<option phone_code="82" value="410">Korea, Republic Of</option>
							<option phone_code="965" value="414">Kuwait</option>
							<option phone_code="1345" value="136">Cayman Islands</option>
							<option phone_code="7" value="398">Kazakhstan</option>
							<option phone_code="856" value="418">Lao People's Democratic Republic</option>
							<option phone_code="961" value="422">Lebanon</option>
							<option phone_code="1758" value="662">Saint Lucia</option>
							<option phone_code="423" value="438">Liechtenstein</option>
							<option phone_code="94" value="144">Sri Lanka</option>
							<option phone_code="231" value="430">Liberia</option>
							<option phone_code="266" value="426">Lesotho</option>
							<option phone_code="370" value="440">Lithuania</option>
							<option phone_code="352" value="442">Luxembourg</option>
							<option phone_code="371" value="428">Latvia</option>
							<option phone_code="218" value="434">Libyan Arab Jamahiriya</option>
							<option phone_code="212" value="504">Morocco</option>
							<option phone_code="377" value="492">Monaco</option>
							<option phone_code="373" value="498">Moldova, Republic Of</option>
							<option phone_code="382" value="499">Montenegro</option>
							<option phone_code="590" value="663">Saint Martin (French part)</option>
							<option phone_code="261" value="450">Madagascar</option>
							<option phone_code="692" value="584">Marshall Islands</option>
							<option phone_code="389" value="807">Macedonia, The Former Yugoslav Republic Of</option>
							<option phone_code="223" value="466">Mali</option>
							<option phone_code="95" value="104">Myanmar</option>
							<option phone_code="976" value="496">Mongolia</option>
							<option phone_code="853" value="446">Macao</option>
							<option phone_code="670" value="580">Northern Mariana Islands</option>
							<option phone_code="596" value="474">Martinique</option>
							<option phone_code="222" value="478">Mauritania</option>
							<option phone_code="1664" value="500">Montserrat</option>
							<option phone_code="356" value="470">Malta</option>
							<option phone_code="230" value="480">Mauritius</option>
							<option phone_code="960" value="462">Maldives</option>
							<option phone_code="265" value="454">Malawi</option>
							<option phone_code="52" value="484">Mexico</option>
							<option phone_code="60" value="458">Malaysia</option>
							<option phone_code="258" value="508">Mozambique</option>
							<option phone_code="264" value="516">Namibia</option>
							<option phone_code="687" value="540">New Caledonia</option>
							<option phone_code="227" value="562">Niger</option>
							<option phone_code="672" value="574">Norfolk Island</option>
							<option phone_code="234" value="566">Nigeria</option>
							<option phone_code="505" value="558">Nicaragua</option>
							<option phone_code="31" value="528">Netherlands</option>
							<option phone_code="47" value="578">Norway</option>
							<option phone_code="977" value="524">Nepal</option>
							<option phone_code="674" value="520">Nauru</option>
							<option phone_code="683" value="570">Niue</option>
							<option phone_code="64" value="554">New Zealand</option>
							<option phone_code="968" value="512">Oman</option>
							<option phone_code="507" value="591">Panama</option>
							<option phone_code="51" value="604">Peru</option>
							<option phone_code="689" value="258">French Polynesia</option>
							<option phone_code="675" value="598">Papua New Guinea</option>
							<option phone_code="63" value="608">Philippines</option>
							<option phone_code="92" value="586">Pakistan</option>
							<option phone_code="48" value="616">Poland</option>
							<option phone_code="508" value="666">Saint Pierre And Miquelon</option>
							<option phone_code="872" value="612">Pitcairn</option>
							<option phone_code="1787" value="630">Puerto Rico</option>
							<option phone_code="970" value="275">Palestinian Territory, Occupied</option>
							<option phone_code="351" value="620">Portugal</option>
							<option phone_code="680" value="585">Palau</option>
							<option phone_code="595" value="600">Paraguay</option>
							<option phone_code="974" value="634">Qatar</option>
							<option phone_code="262" value="638">Reunion</option>
							<option phone_code="40" value="642">Romania</option>
							<option phone_code="381" value="688">Serbia</option>
							<option phone_code="7" value="643">Russian Federation</option>
							<option phone_code="250" value="646">Rwanda</option>
							<option phone_code="966" value="682">Saudi Arabia</option>
							<option phone_code="677" value="090">Solomon Islands</option>
							<option phone_code="248" value="690">Seychelles</option>
							<option phone_code="249" value="736">Sudan</option>
							<option phone_code="46" value="752">Sweden</option>
							<option phone_code="65" value="702">Singapore</option>
							<option phone_code="290" value="654">Saint Helena</option>
							<option phone_code="386" value="705">Slovenia</option>
							<option phone_code="" value="744">Svalbard And Jan Mayen</option>
							<option phone_code="421" value="703">Slovakia</option>
							<option phone_code="232" value="694">Sierra Leone</option>
							<option phone_code="378" value="674">San Marino</option>
							<option phone_code="221" value="686">Senegal</option>
							<option phone_code="252" value="706">Somalia</option>
							<option phone_code="597" value="740">Suriname</option>
							<option phone_code="239" value="678">Sao Tome And Principe</option>
							<option phone_code="503" value="222">El Salvador</option>
							<option phone_code="963" value="760">Syrian Arab Republic</option>
							<option phone_code="268" value="748">Swaziland</option>
							<option phone_code="1649" value="796">Turks And Caicos Islands</option>
							<option phone_code="235" value="148">Chad</option>
							<option phone_code="" value="260">French Southern Territories</option>
							<option phone_code="228" value="768">Togo</option>
							<option phone_code="66" value="764">Thailand</option>
							<option phone_code="992" value="762">Tajikistan</option>
							<option phone_code="690" value="772">Tokelau</option>
							<option phone_code="670" value="626">Timor-leste</option>
							<option phone_code="993" value="795">Turkmenistan</option>
							<option phone_code="216" value="788">Tunisia</option>
							<option phone_code="676" value="776">Tonga</option>
							<option phone_code="90" value="792">Turkey</option>
							<option phone_code="1868" value="780">Trinidad And Tobago</option>
							<option phone_code="688" value="798">Tuvalu</option>
							<option phone_code="886" value="158">Taiwan, Province Of China</option>
							<option phone_code="255" value="834">Tanzania, United Republic Of</option>
							<option phone_code="380" value="804">Ukraine</option>
							<option phone_code="256" value="800">Uganda</option>
							<option phone_code="" value="581">United States Minor Outlying Islands</option>
							<option phone_code="1" value="840">United States</option>
							<option phone_code="598" value="858">Uruguay</option>
							<option phone_code="998" value="860">Uzbekistan</option>
							<option phone_code="39" value="336">Holy See (vatican City State)</option>
							<option phone_code="1784" value="670">Saint Vincent And The Grenadines</option>
							<option phone_code="58" value="862">Venezuela</option>
							<option phone_code="1284" value="092">Virgin Islands, British</option>
							<option phone_code="1340" value="850">Virgin Islands, U.s.</option>
							<option phone_code="84" value="704" selected="selected">Viet Nam</option>
							<option phone_code="678" value="548">Vanuatu</option>
							<option phone_code="681" value="876">Wallis And Futuna</option>
							<option phone_code="684" value="882">Samoa</option>
							<option phone_code="967" value="887">Yemen</option>
							<option phone_code="269" value="175">Mayotte</option>
							<option phone_code="27" value="710">South Africa</option>
							<option phone_code="260" value="894">Zambia</option>
							<option phone_code="263" value="716">Zimbabwe</option>
						</select><div class="uvo_pref-uvoAutocomplete-wrapper">
						<input class="uvo_pref-uvoAutocomplete-input" name="mobile_phone_country_input" type="text" readonly="" data-finder="mobile_phone_country" data-value="704" disabled=""><span class="ui-icon ui-icon-triangle-1-s" style="position: absolute; cursor: not-allowed; right: 10px; top: 17px;"></span><div class="dr-down" style="display: none; top: 38px;"><span class="dr-down-search"><span class="dr-down-zoom"></span><input type="text" class="dr-down-input smaller" placeholder="Search within list"></span><div class="dr-down-list"><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="004">Afghanistan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="248">Aland Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="008">Albania</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="012">Algeria</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="016">American Samoa</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="020">Andorra</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="024">Angola</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="660">Anguilla</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="010">Antarctica</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="028">Antigua And Barbuda</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="032">Argentina</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="051">Armenia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="533">Aruba</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="036">Australia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="040">Austria</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="031">Azerbaijan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="044">Bahamas</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="048">Bahrain</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="050">Bangladesh</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="052">Barbados</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="112">Belarus</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="056">Belgium</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="084">Belize</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="204">Benin</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="060">Bermuda</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="064">Bhutan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="068">Bolivia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="070">Bosnia And Herzegovina</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="072">Botswana</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="074">Bouvet Island</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="076">Brazil</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="086">British Indian Ocean Territory</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="096">Brunei Darussalam</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="100">Bulgaria</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="854">Burkina Faso</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="108">Burundi</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="116">Cambodia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="120">Cameroon</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="124">Canada</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="132">Cape Verde</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="136">Cayman Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="140">Central African Republic</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="148">Chad</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="152">Chile</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="156">China</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="162">Christmas Island</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="166">Cocos (keeling) Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="170">Colombia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="174">Comoros</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="178">Congo</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="180">Congo, The Democratic Republic Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="184">Cook Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="188">Costa Rica</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="384">Cote D'ivoire</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="191">Croatia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="192">Cuba</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="196">Cyprus</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="203">Czech Republic</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="208">Denmark</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="262">Djibouti</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="212">Dominica</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="214">Dominican Republic</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="218">Ecuador</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="818">Egypt</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="222">El Salvador</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="226">Equatorial Guinea</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="232">Eritrea</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="233">Estonia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="231">Ethiopia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="238">Falkland Islands (malvinas)</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="234">Faroe Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="242">Fiji</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="246">Finland</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="250">France</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="254">French Guiana</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="258">French Polynesia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="260">French Southern Territories</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="266">Gabon</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="270">Gambia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="268">Georgia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="276">Germany</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="288">Ghana</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="292">Gibraltar</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="300">Greece</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="304">Greenland</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="308">Grenada</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="312">Guadeloupe</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="316">Guam</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="320">Guatemala</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="831">Guernsey</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="324">Guinea</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="624">Guinea-bissau</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="328">Guyana</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="332">Haiti</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="334">Heard Island And Mcdonald Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="336">Holy See (vatican City State)</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="340">Honduras</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="344">Hong Kong</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="348">Hungary</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="352">Iceland</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="356">India</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="360">Indonesia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="364">Iran, Islamic Republic Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="368">Iraq</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="372">Ireland</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="833">Isle Of Man</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="376">Israel</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="380">Italy</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="388">Jamaica</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="392">Japan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="832">Jersey</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="400">Jordan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="398">Kazakhstan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="404">Kenya</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="296">Kiribati</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="408">Korea, Democratic People's Republic Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="410">Korea, Republic Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="414">Kuwait</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="417">Kyrgyzstan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="418">Lao People's Democratic Republic</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="428">Latvia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="422">Lebanon</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="426">Lesotho</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="430">Liberia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="434">Libyan Arab Jamahiriya</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="438">Liechtenstein</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="440">Lithuania</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="442">Luxembourg</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="446">Macao</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="807">Macedonia, The Former Yugoslav Republic Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="450">Madagascar</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="454">Malawi</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="458">Malaysia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="462">Maldives</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="466">Mali</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="470">Malta</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="584">Marshall Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="474">Martinique</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="478">Mauritania</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="480">Mauritius</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="175">Mayotte</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="484">Mexico</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="583">Micronesia, Federated States Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="498">Moldova, Republic Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="492">Monaco</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="496">Mongolia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="499">Montenegro</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="500">Montserrat</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="504">Morocco</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="508">Mozambique</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="104">Myanmar</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="516">Namibia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="520">Nauru</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="524">Nepal</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="528">Netherlands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="530">Netherlands Antilles</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="540">New Caledonia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="554">New Zealand</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="558">Nicaragua</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="562">Niger</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="566">Nigeria</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="570">Niue</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="574">Norfolk Island</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="580">Northern Mariana Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="578">Norway</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="512">Oman</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="586">Pakistan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="585">Palau</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="275">Palestinian Territory, Occupied</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="591">Panama</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="598">Papua New Guinea</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="600">Paraguay</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="604">Peru</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="608">Philippines</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="612">Pitcairn</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="Please select your country">Please select your country</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="616">Poland</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="620">Portugal</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="630">Puerto Rico</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="634">Qatar</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="638">Reunion</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="642">Romania</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="643">Russian Federation</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="646">Rwanda</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="652">Saint Barthelemy</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="654">Saint Helena</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="659">Saint Kitts And Nevis</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="662">Saint Lucia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="663">Saint Martin (French part)</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="666">Saint Pierre And Miquelon</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="670">Saint Vincent And The Grenadines</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="882">Samoa</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="674">San Marino</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="678">Sao Tome And Principe</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="682">Saudi Arabia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="686">Senegal</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="688">Serbia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="690">Seychelles</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="694">Sierra Leone</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="702">Singapore</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="703">Slovakia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="705">Slovenia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="090">Solomon Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="706">Somalia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="710">South Africa</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="239">South Georgia And The South Sandwich Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="724">Spain</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="144">Sri Lanka</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="736">Sudan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="740">Suriname</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="744">Svalbard And Jan Mayen</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="748">Swaziland</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="752">Sweden</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="756">Switzerland</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="760">Syrian Arab Republic</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="158">Taiwan, Province Of China</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="762">Tajikistan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="834">Tanzania, United Republic Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="764">Thailand</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="626">Timor-leste</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="768">Togo</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="772">Tokelau</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="776">Tonga</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="780">Trinidad And Tobago</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="788">Tunisia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="792">Turkey</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="795">Turkmenistan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="796">Turks And Caicos Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="798">Tuvalu</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="800">Uganda</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="804">Ukraine</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="784">United Arab Emirates</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="826">United Kingdom</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="840">United States</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="581">United States Minor Outlying Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="858">Uruguay</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="860">Uzbekistan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="548">Vanuatu</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="862">Venezuela</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="704">Viet Nam</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="092">Virgin Islands, British</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="850">Virgin Islands, U.s.</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="876">Wallis And Futuna</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="732">Western Sahara</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="887">Yemen</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="894">Zambia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="716">Zimbabwe</a></div></div></div>
					</div>
				</div>

				<!-- Customer contact phone -->
				<div class="row my-phone-row ">
					<div class="cell cell-left">
						<label for="area-code" class="left-label">Phone:</label>
					</div>
					<div class="cell cell-right phone-wrapper">
						<div class="cell plus_wrapper">
							<div class="input-model plus">+</div>
						</div>
						<div class="cell country-code-wrapper">
							<label for="country-code" class="small-label">country code</label>
							<input type="tel" value="84" id="mobile_phone_c" name="mobile_phone_c" class="country-code" disabled="" data-finder="form.input.mobilephonec">
						</div>
						<div class="cell area-code-wrapper">
							<div class="absolute-error-wrapper">
								<label for="area-code" class="small-label">state or area code</label>
								<input type="tel" name="mobile_phone_a" id="mobile_phone_a" class="area-code validate[required,custom[phonerequired]]" data-validation-class="validate[required,custom[phonerequired]]" onkeyup="trim_val(this.value,'mobile_phone_a')" data-finder="form.input.mobilephonea">
							</div>
						</div>
						<div class="cell tel-number-wrapper">
							<div class="absolute-error-wrapper">
								<label for="telephone" class="small-label">phone</label>
								<input type="tel" name="mobile_phone_t" class="telephone validate[required,custom[phonerequired]]" data-validation-class="validate[required,custom[phonerequired]]" onkeyup="trim_val(this.value,'mobile_phone_t')" id="mobile_phone_t" maxlength="20" data-finder="form.input.mobilephonet">
							</div>
						</div>
						<input type="hidden" name="mobile_phone_required" value="0">

					</div>
				</div>

			</div>

			<!-- Existing user tab -->
			<div id="tabs-2" class="orderform-auth-container ui-tabs-panel ui-widget-content ui-corner-bottom" aria-labelledby="tabs_2" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;">
				<div id="box_logged_email" class="row">
					<div class="cell cell-left">
						<label for="" class="left-label">Your email:</label>
					</div>
					<div class="cell cell-right">
						<input type="text" name="old_email" maxlength="100" id="old_email" class="validate[required,custom[email]] f_sz_220" data-validation-class="validate[required,custom[email]]" data-finder="form.input.oldemail">
						<em id="old_email_ok" class="validation_img hidden">&nbsp;</em>
						<em id="old_email_err" class="validation_img hidden">&nbsp;</em>
					</div>
					<div class="clearboth"></div>
				</div>

				<!-- User password -->
				<div id="box_old_password" class="row">
					<div class="cell cell-left">
						<label for="" class="left-label">Password:</label>
					</div>
					<div class="cell cell-right">
						<input name="old_password" id="old_password" class="f_sz_220 validate[required,custom[passwordrequired],custom[noSpecialCaracters]]" value="" type="password" data-finder="form.input.oldpassword">
						<em id="old_password_ok" class="validation_img hidden">&nbsp;</em>
						<em id="old_password_err" class="validation_img hidden">&nbsp;</em>
						<div class="wrap-fp">
							<span class="fp-step3 manage">
								<em class="forIE"><em class="forIE2">

									<input id="customer_login" name="customer_login" type="button" value="Sign in" onclick="void(0); ga('send', 'event', 'button', 'click', 'returning user login');" data-finder="form.button.customerlogin" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">

								</em></em>
							</span>
							<div class="input-model"><a href="https://essayhave.com/password_reminder.html" class="forgot-password-link" data-finder="form.link.passwordreminder">Forgot your password?</a></div>
						</div>
					</div>
					<div class="clearboth"></div>
				</div>

				<!-- Customer country -->
				<div id="box_customer_country_old" class="row hidden">
					<div class="cell cell-left">
						<label for="" class="left-label">Your country:</label>
					</div>
					<div class="cell cell-right">
						<input type="hidden" name="customer_country_hidden" id="customer_country_hidden" value="704" disabled="disabled">
						<select name="customer_country" id="customer_country" class="f_sz_300 validate[required,custom[countryrequired]] dr-down-input" data-validation-class="validate[required,custom[countryrequired]]" disabled="disabled" data-finder="form.select.customercountry" style="position: absolute; top: 0px; left: 0px; visibility: hidden;">
							<option selected="selected">Please select your country</option>
							<option value="020">Andorra</option>
							<option value="784">United Arab Emirates</option>
							<option value="004">Afghanistan</option>
							<option value="028">Antigua And Barbuda</option>
							<option value="660">Anguilla</option>
							<option value="008">Albania</option>
							<option value="051">Armenia</option>
							<option value="530">Netherlands Antilles</option>
							<option value="024">Angola</option>
							<option value="010">Antarctica</option>
							<option value="032">Argentina</option>
							<option value="016">American Samoa</option>
							<option value="040">Austria</option>
							<option value="036">Australia</option>
							<option value="533">Aruba</option>
							<option value="248">Aland Islands</option>
							<option value="031">Azerbaijan</option>
							<option value="070">Bosnia And Herzegovina</option>
							<option value="052">Barbados</option>
							<option value="050">Bangladesh</option>
							<option value="056">Belgium</option>
							<option value="854">Burkina Faso</option>
							<option value="100">Bulgaria</option>
							<option value="048">Bahrain</option>
							<option value="108">Burundi</option>
							<option value="204">Benin</option>
							<option value="652">Saint Barthelemy</option>
							<option value="060">Bermuda</option>
							<option value="096">Brunei Darussalam</option>
							<option value="068">Bolivia</option>
							<option value="076">Brazil</option>
							<option value="044">Bahamas</option>
							<option value="064">Bhutan</option>
							<option value="074">Bouvet Island</option>
							<option value="072">Botswana</option>
							<option value="112">Belarus</option>
							<option value="084">Belize</option>
							<option value="124">Canada</option>
							<option value="166">Cocos (keeling) Islands</option>
							<option value="180">Congo, The Democratic Republic Of</option>
							<option value="140">Central African Republic</option>
							<option value="178">Congo</option>
							<option value="756">Switzerland</option>
							<option value="384">Cote D'ivoire</option>
							<option value="184">Cook Islands</option>
							<option value="152">Chile</option>
							<option value="120">Cameroon</option>
							<option value="156">China</option>
							<option value="170">Colombia</option>
							<option value="188">Costa Rica</option>
							<option value="192">Cuba</option>
							<option value="132">Cape Verde</option>
							<option value="162">Christmas Island</option>
							<option value="196">Cyprus</option>
							<option value="203">Czech Republic</option>
							<option value="276">Germany</option>
							<option value="262">Djibouti</option>
							<option value="208">Denmark</option>
							<option value="212">Dominica</option>
							<option value="214">Dominican Republic</option>
							<option value="012">Algeria</option>
							<option value="218">Ecuador</option>
							<option value="233">Estonia</option>
							<option value="818">Egypt</option>
							<option value="732">Western Sahara</option>
							<option value="232">Eritrea</option>
							<option value="724">Spain</option>
							<option value="231">Ethiopia</option>
							<option value="246">Finland</option>
							<option value="242">Fiji</option>
							<option value="238">Falkland Islands (malvinas)</option>
							<option value="583">Micronesia, Federated States Of</option>
							<option value="234">Faroe Islands</option>
							<option value="250">France</option>
							<option value="266">Gabon</option>
							<option value="826">United Kingdom</option>
							<option value="308">Grenada</option>
							<option value="268">Georgia</option>
							<option value="254">French Guiana</option>
							<option value="831">Guernsey</option>
							<option value="288">Ghana</option>
							<option value="292">Gibraltar</option>
							<option value="304">Greenland</option>
							<option value="270">Gambia</option>
							<option value="324">Guinea</option>
							<option value="312">Guadeloupe</option>
							<option value="226">Equatorial Guinea</option>
							<option value="300">Greece</option>
							<option value="239">South Georgia And The South Sandwich Islands</option>
							<option value="320">Guatemala</option>
							<option value="316">Guam</option>
							<option value="624">Guinea-bissau</option>
							<option value="328">Guyana</option>
							<option value="344">Hong Kong</option>
							<option value="334">Heard Island And Mcdonald Islands</option>
							<option value="340">Honduras</option>
							<option value="191">Croatia</option>
							<option value="332">Haiti</option>
							<option value="348">Hungary</option>
							<option value="360">Indonesia</option>
							<option value="372">Ireland</option>
							<option value="376">Israel</option>
							<option value="833">Isle Of Man</option>
							<option value="356">India</option>
							<option value="086">British Indian Ocean Territory</option>
							<option value="368">Iraq</option>
							<option value="364">Iran, Islamic Republic Of</option>
							<option value="352">Iceland</option>
							<option value="380">Italy</option>
							<option value="832">Jersey</option>
							<option value="388">Jamaica</option>
							<option value="400">Jordan</option>
							<option value="392">Japan</option>
							<option value="404">Kenya</option>
							<option value="417">Kyrgyzstan</option>
							<option value="116">Cambodia</option>
							<option value="296">Kiribati</option>
							<option value="174">Comoros</option>
							<option value="659">Saint Kitts And Nevis</option>
							<option value="408">Korea, Democratic People's Republic Of</option>
							<option value="410">Korea, Republic Of</option>
							<option value="414">Kuwait</option>
							<option value="136">Cayman Islands</option>
							<option value="398">Kazakhstan</option>
							<option value="418">Lao People's Democratic Republic</option>
							<option value="422">Lebanon</option>
							<option value="662">Saint Lucia</option>
							<option value="438">Liechtenstein</option>
							<option value="144">Sri Lanka</option>
							<option value="430">Liberia</option>
							<option value="426">Lesotho</option>
							<option value="440">Lithuania</option>
							<option value="442">Luxembourg</option>
							<option value="428">Latvia</option>
							<option value="434">Libyan Arab Jamahiriya</option>
							<option value="504">Morocco</option>
							<option value="492">Monaco</option>
							<option value="498">Moldova, Republic Of</option>
							<option value="499">Montenegro</option>
							<option value="663">Saint Martin (French part)</option>
							<option value="450">Madagascar</option>
							<option value="584">Marshall Islands</option>
							<option value="807">Macedonia, The Former Yugoslav Republic Of</option>
							<option value="466">Mali</option>
							<option value="104">Myanmar</option>
							<option value="496">Mongolia</option>
							<option value="446">Macao</option>
							<option value="580">Northern Mariana Islands</option>
							<option value="474">Martinique</option>
							<option value="478">Mauritania</option>
							<option value="500">Montserrat</option>
							<option value="470">Malta</option>
							<option value="480">Mauritius</option>
							<option value="462">Maldives</option>
							<option value="454">Malawi</option>
							<option value="484">Mexico</option>
							<option value="458">Malaysia</option>
							<option value="508">Mozambique</option>
							<option value="516">Namibia</option>
							<option value="540">New Caledonia</option>
							<option value="562">Niger</option>
							<option value="574">Norfolk Island</option>
							<option value="566">Nigeria</option>
							<option value="558">Nicaragua</option>
							<option value="528">Netherlands</option>
							<option value="578">Norway</option>
							<option value="524">Nepal</option>
							<option value="520">Nauru</option>
							<option value="570">Niue</option>
							<option value="554">New Zealand</option>
							<option value="512">Oman</option>
							<option value="591">Panama</option>
							<option value="604">Peru</option>
							<option value="258">French Polynesia</option>
							<option value="598">Papua New Guinea</option>
							<option value="608">Philippines</option>
							<option value="586">Pakistan</option>
							<option value="616">Poland</option>
							<option value="666">Saint Pierre And Miquelon</option>
							<option value="612">Pitcairn</option>
							<option value="630">Puerto Rico</option>
							<option value="275">Palestinian Territory, Occupied</option>
							<option value="620">Portugal</option>
							<option value="585">Palau</option>
							<option value="600">Paraguay</option>
							<option value="634">Qatar</option>
							<option value="638">Reunion</option>
							<option value="642">Romania</option>
							<option value="688">Serbia</option>
							<option value="643">Russian Federation</option>
							<option value="646">Rwanda</option>
							<option value="682">Saudi Arabia</option>
							<option value="090">Solomon Islands</option>
							<option value="690">Seychelles</option>
							<option value="736">Sudan</option>
							<option value="752">Sweden</option>
							<option value="702">Singapore</option>
							<option value="654">Saint Helena</option>
							<option value="705">Slovenia</option>
							<option value="744">Svalbard And Jan Mayen</option>
							<option value="703">Slovakia</option>
							<option value="694">Sierra Leone</option>
							<option value="674">San Marino</option>
							<option value="686">Senegal</option>
							<option value="706">Somalia</option>
							<option value="740">Suriname</option>
							<option value="678">Sao Tome And Principe</option>
							<option value="222">El Salvador</option>
							<option value="760">Syrian Arab Republic</option>
							<option value="748">Swaziland</option>
							<option value="796">Turks And Caicos Islands</option>
							<option value="148">Chad</option>
							<option value="260">French Southern Territories</option>
							<option value="768">Togo</option>
							<option value="764">Thailand</option>
							<option value="762">Tajikistan</option>
							<option value="772">Tokelau</option>
							<option value="626">Timor-leste</option>
							<option value="795">Turkmenistan</option>
							<option value="788">Tunisia</option>
							<option value="776">Tonga</option>
							<option value="792">Turkey</option>
							<option value="780">Trinidad And Tobago</option>
							<option value="798">Tuvalu</option>
							<option value="158">Taiwan, Province Of China</option>
							<option value="834">Tanzania, United Republic Of</option>
							<option value="804">Ukraine</option>
							<option value="800">Uganda</option>
							<option value="581">United States Minor Outlying Islands</option>
							<option value="840">United States</option>
							<option value="858">Uruguay</option>
							<option value="860">Uzbekistan</option>
							<option value="336">Holy See (vatican City State)</option>
							<option value="670">Saint Vincent And The Grenadines</option>
							<option value="862">Venezuela</option>
							<option value="092">Virgin Islands, British</option>
							<option value="850">Virgin Islands, U.s.</option>
							<option value="704">Viet Nam</option>
							<option value="548">Vanuatu</option>
							<option value="876">Wallis And Futuna</option>
							<option value="882">Samoa</option>
							<option value="887">Yemen</option>
							<option value="175">Mayotte</option>
							<option value="710">South Africa</option>
							<option value="894">Zambia</option>
							<option value="716">Zimbabwe</option>
						</select><div class="uvo_pref-uvoAutocomplete-wrapper"><input class="uvo_pref-uvoAutocomplete-input" name="customer_country_input" type="text" readonly="" data-finder="customer_country" data-value="Please select your country"><span class="ui-icon ui-icon-triangle-1-s" style="position: absolute; cursor: not-allowed; right: 10px; top: 17px;"></span><div class="dr-down" style="display: none; top: 38px;"><span class="dr-down-search"><span class="dr-down-zoom"></span><input type="text" class="dr-down-input smaller" placeholder="Search within list"></span><div class="dr-down-list"><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="004">Afghanistan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="248">Aland Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="008">Albania</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="012">Algeria</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="016">American Samoa</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="020">Andorra</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="024">Angola</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="660">Anguilla</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="010">Antarctica</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="028">Antigua And Barbuda</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="032">Argentina</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="051">Armenia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="533">Aruba</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="036">Australia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="040">Austria</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="031">Azerbaijan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="044">Bahamas</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="048">Bahrain</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="050">Bangladesh</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="052">Barbados</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="112">Belarus</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="056">Belgium</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="084">Belize</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="204">Benin</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="060">Bermuda</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="064">Bhutan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="068">Bolivia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="070">Bosnia And Herzegovina</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="072">Botswana</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="074">Bouvet Island</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="076">Brazil</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="086">British Indian Ocean Territory</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="096">Brunei Darussalam</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="100">Bulgaria</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="854">Burkina Faso</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="108">Burundi</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="116">Cambodia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="120">Cameroon</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="124">Canada</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="132">Cape Verde</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="136">Cayman Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="140">Central African Republic</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="148">Chad</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="152">Chile</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="156">China</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="162">Christmas Island</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="166">Cocos (keeling) Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="170">Colombia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="174">Comoros</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="178">Congo</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="180">Congo, The Democratic Republic Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="184">Cook Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="188">Costa Rica</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="384">Cote D'ivoire</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="191">Croatia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="192">Cuba</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="196">Cyprus</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="203">Czech Republic</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="208">Denmark</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="262">Djibouti</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="212">Dominica</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="214">Dominican Republic</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="218">Ecuador</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="818">Egypt</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="222">El Salvador</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="226">Equatorial Guinea</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="232">Eritrea</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="233">Estonia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="231">Ethiopia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="238">Falkland Islands (malvinas)</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="234">Faroe Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="242">Fiji</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="246">Finland</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="250">France</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="254">French Guiana</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="258">French Polynesia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="260">French Southern Territories</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="266">Gabon</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="270">Gambia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="268">Georgia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="276">Germany</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="288">Ghana</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="292">Gibraltar</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="300">Greece</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="304">Greenland</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="308">Grenada</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="312">Guadeloupe</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="316">Guam</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="320">Guatemala</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="831">Guernsey</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="324">Guinea</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="624">Guinea-bissau</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="328">Guyana</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="332">Haiti</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="334">Heard Island And Mcdonald Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="336">Holy See (vatican City State)</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="340">Honduras</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="344">Hong Kong</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="348">Hungary</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="352">Iceland</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="356">India</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="360">Indonesia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="364">Iran, Islamic Republic Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="368">Iraq</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="372">Ireland</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="833">Isle Of Man</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="376">Israel</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="380">Italy</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="388">Jamaica</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="392">Japan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="832">Jersey</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="400">Jordan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="398">Kazakhstan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="404">Kenya</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="296">Kiribati</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="408">Korea, Democratic People's Republic Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="410">Korea, Republic Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="414">Kuwait</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="417">Kyrgyzstan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="418">Lao People's Democratic Republic</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="428">Latvia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="422">Lebanon</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="426">Lesotho</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="430">Liberia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="434">Libyan Arab Jamahiriya</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="438">Liechtenstein</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="440">Lithuania</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="442">Luxembourg</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="446">Macao</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="807">Macedonia, The Former Yugoslav Republic Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="450">Madagascar</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="454">Malawi</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="458">Malaysia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="462">Maldives</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="466">Mali</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="470">Malta</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="584">Marshall Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="474">Martinique</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="478">Mauritania</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="480">Mauritius</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="175">Mayotte</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="484">Mexico</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="583">Micronesia, Federated States Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="498">Moldova, Republic Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="492">Monaco</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="496">Mongolia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="499">Montenegro</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="500">Montserrat</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="504">Morocco</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="508">Mozambique</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="104">Myanmar</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="516">Namibia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="520">Nauru</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="524">Nepal</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="528">Netherlands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="530">Netherlands Antilles</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="540">New Caledonia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="554">New Zealand</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="558">Nicaragua</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="562">Niger</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="566">Nigeria</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="570">Niue</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="574">Norfolk Island</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="580">Northern Mariana Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="578">Norway</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="512">Oman</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="586">Pakistan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="585">Palau</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="275">Palestinian Territory, Occupied</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="591">Panama</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="598">Papua New Guinea</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="600">Paraguay</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="604">Peru</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="608">Philippines</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="612">Pitcairn</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="Please select your country">Please select your country</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="616">Poland</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="620">Portugal</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="630">Puerto Rico</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="634">Qatar</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="638">Reunion</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="642">Romania</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="643">Russian Federation</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="646">Rwanda</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="652">Saint Barthelemy</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="654">Saint Helena</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="659">Saint Kitts And Nevis</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="662">Saint Lucia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="663">Saint Martin (French part)</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="666">Saint Pierre And Miquelon</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="670">Saint Vincent And The Grenadines</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="882">Samoa</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="674">San Marino</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="678">Sao Tome And Principe</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="682">Saudi Arabia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="686">Senegal</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="688">Serbia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="690">Seychelles</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="694">Sierra Leone</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="702">Singapore</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="703">Slovakia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="705">Slovenia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="090">Solomon Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="706">Somalia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="710">South Africa</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="239">South Georgia And The South Sandwich Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="724">Spain</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="144">Sri Lanka</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="736">Sudan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="740">Suriname</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="744">Svalbard And Jan Mayen</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="748">Swaziland</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="752">Sweden</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="756">Switzerland</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="760">Syrian Arab Republic</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="158">Taiwan, Province Of China</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="762">Tajikistan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="834">Tanzania, United Republic Of</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="764">Thailand</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="626">Timor-leste</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="768">Togo</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="772">Tokelau</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="776">Tonga</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="780">Trinidad And Tobago</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="788">Tunisia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="792">Turkey</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="795">Turkmenistan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="796">Turks And Caicos Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="798">Tuvalu</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="800">Uganda</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="804">Ukraine</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="784">United Arab Emirates</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="826">United Kingdom</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="840">United States</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="581">United States Minor Outlying Islands</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="858">Uruguay</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="860">Uzbekistan</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="548">Vanuatu</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="862">Venezuela</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="704">Viet Nam</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="092">Virgin Islands, British</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="850">Virgin Islands, U.s.</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="876">Wallis And Futuna</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="732">Western Sahara</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="887">Yemen</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="894">Zambia</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="716">Zimbabwe</a></div></div></div>
						<div class="field_tip">We tried to identify the country in which you reside, but we failed to. Please choose your country from the drop down list to proceed to payment. Thank you for your cooperation.</div>
					</div>
				</div>


			</div>
		</div>

		

		<div class="total-price" id="box_total_price">
			<div class="div_invoice"><div class="invoice_item"><span class="invoice_item_description"><span data-hasqtip="72" oldtitle="3 days, double spaced" title="">2 pages <strong>×</strong> $16.00<span class="i"></span></span></span><span class="invoice_item_cost"><b>$32<sup>.00</sup></b> </span></div></div>
			<div class="total_price_container">
				<h3 class="total-price-header light">Grand total price:</h3>
				<div class="total-price-sum total_price total_price_box">$32<sup>.00</sup></div>
			</div>
		</div>

		
		<div id="box_category_of_writer" class="row hidden">
			<div class="cell cell-left">
				<!--label for="" class="left-label">I want a specific writer to complete my order, if possible:</label -->
			</div>
			<div class="cell cell-right" id="box_defined_writer">
                    <!--select id="request_writer" name="request_writer" disabled="disabled">
                        <option selected="selected" value="">First enter your email</option>
                    </select -->
                    <input name="defined_writer" id="defined_writer" value="1" title="I want a specific writer to complete my order, if possible" type="checkbox" data-finder="form.input.definedwriter2">
                    <label for="defined_writer" class="no-padding" data-finder="form.label.definedwriter2"></label>
                    I want the writer                    <select style="width:180px !important;" id="request_writer" name="request_writer" disabled="disabled" data-finder="form.select.requestwriter2">
                    <option selected="selected" value="">select writer</option>
                </select>
                to&nbsp;complete my order, if possible.
            </div>
        </div>

        <div class="box_customer_discounts row hidden">
        	<div class="cell cell-left">
        		<label for="" class="left-label">Discounts:</label>
        	</div>
        	<div class="cell cell-right">
        		<div class="radios customer_discounts"></div>
        	</div>
        </div>

        <div id="box_payment_system" class="row box_payment_system_logged ui-buttonset">
        	<div class="cell cell-left with-mobile-tip">
        		<label for="" class="left-label">Payment system:</label>
        		<div class="mobile-tip" data-hasqtip="33"></div>
        	</div>
        	<div class="cell cell-right side_tip side_tip_payment_system" data-hasqtip="32"><div class="selects visible-in-mobile" style="display: none;"><select id="mob_payment_system_hidden" name="mob_payment_system_hidden" class="dr-down-input" style="position: absolute; top: 0px; left: 0px; visibility: hidden;"><option value="paypal" selected="selected">PayPal</option><option value="moneybookers">MoneyBookers</option></select><div class="uvo_pref-uvoAutocomplete-wrapper"><input class="uvo_pref-uvoAutocomplete-input" name="mob_payment_system_hidden_input" type="text" readonly="" data-finder="mob_payment_system_hidden" data-value="paypal"><span class="ui-icon ui-icon-triangle-1-s" style="position: absolute; cursor: pointer; right: 10px; top: 17px;"></span><div class="dr-down" style="display: none; top: 38px;"><span class="dr-down-search" style="display: none;"><span class="dr-down-zoom"></span><input type="text" class="dr-down-input smaller" placeholder="Search within list"></span><div class="dr-down-list"><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="paypal">PayPal</a><a href="https://essayhave.com/order.html#" class="dr-down-list-item" data-value="moneybookers">MoneyBookers</a></div></div></div></div>
        	<div class="radios form-box-pay visible-in-desktop">
        		<input name="payment_system_hidden" id="ps_paypal" value="paypal" type="radio" checked="checked" data-finder="form.input.pspaypal" class="ui-helper-hidden-accessible">
        		<label id="ps_paypal_btn" for="ps_paypal" data-finder="form.label.pspaypal" class="ui-state-active ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left" role="button" aria-disabled="false"><span class="ui-button-text">PayPal</span></label>
        		<input name="payment_system_hidden" id="ps_moneybookers" value="moneybookers" type="radio" data-finder="form.input.psmoneybookers" class="ui-helper-hidden-accessible">
        		<label id="ps_moneybookers_btn" for="ps_moneybookers" data-finder="form.label.psmoneybookers" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-right" role="button" aria-disabled="false"><span class="ui-button-text">MoneyBookers</span></label>
        	</div>

        </div>
    </div>


    <p class="submit-agreement">By clicking "Proceed to safe payment" button you agree to the <a class="tu" href="https://essayhave.com/terms-conditions.html" target="_blank" rel="nofollow" data-finder="form.link.termsconditions">Terms and Conditions</a>, <a target="_blank" rel="nofollow" class="gu" href="https://essayhave.com/money-back-guarantee.html" data-finder="form.link.moneybackguarantee">MoneyBack Policy</a> and <a target="_blank" rel="nofollow" class="po" href="https://essayhave.com/privacy-policy.html" data-finder="form.link.privacypolicy">Privacy Policy</a>.</p>
    <div class="orderform-buttons f_submit_box f_submit_box_logged">

    	<input type="submit" class="orderform-submit button highlight large to_billing_btn" onclick="ga('send', 'event', 'button', 'click', 'proceed to payment');" data-finder="form.link.tobilling" value="Proceed to safe payment →">

    	<img src="./checkout_files/ajax-loader-circle.gif" alt="" width="24" height="24" class="to_billing_processing hidden">
    	<a href="javascript:void(0)" class="orderform-prev input-model large order_form_repeat_step2" data-finder="form.link.backtostep2">← Previous step</a>		</div>

    </fieldset>
</div>
</div>

</form>

</div>

<div class="uvoform_scripts_container"></div>

</div>
</article>

</div>



<!-- FORM UPLOADER -->
<script type="text/javascript">

	(function ($) {
		jQuery(document).ready(function () {
			if (jQuery(window).innerWidth()<1025){
				jQuery('.lft .rgt').hide();
			}
			jQuery(window).resize(function(){
				if (jQuery(window).innerWidth()<1025){
					jQuery('.lft .rgt').hide();
				}
			});

				// enable transparent overlay on FF/Linux
				jQuery.blockUI.defaults.applyPlatformOpacityRules = false;

				/*jQuery('.uvoform_container').block();

				jQuery.UVObus.listen('Form.Ready', function () {
					//jQuery('.uvoform_container').unblock();
				});*/

				// jQuery('.uvoform_container').UVOload('/uvoform/potato/get_form/logged', {
				// 	dataType: 'json',
				// 	block: false,
				// 	placement: function (elem, data) {
				// 		jQuery(elem)
				// 		.html(data)
				// 		.trigger("potato-form-html-ready");
				// 	}
				// });

			});
	}(jQuery));
</script>

<!-- END FORM UPLOADER -->

<!-- FORM DIALOGS -->

<script type="text/javascript">
	jQuery(function () {
		jQuery('#dialog-bad-discipline').dialog({
			closeOnEscape: false,
			dialogClass: "cabinet no-close user-action",
			autoOpen: false,
			resizable: false,
			modal: true,
			buttons:[
			{
				html: "<span class='btn-yes' data-finder='order.dialog.badadiscipline.change'>Change level</span>",
				click: function() {
					jQuery('#radio_academic_level_2').prop('checked', true).trigger('change');
					jQuery('#topcat_id').val(jQuery(this).data('topcat_id')).trigger('change');
					jQuery( this ).dialog( "close" );
				},
			},
			{
				html: "<span class='btn-yes' data-finder='order.dialog.badadiscipline.leave'>Leave current level</span>",
				click: function() {
					jQuery( this ).dialog( "close" );
				},
			},
			]
		});
	});
</script>


<script type="text/javascript">
	jQuery(function () {
		jQuery('#dialog-bad-academiclevel').dialog({
			closeOnEscape: false,
			dialogClass: "cabinet no-close user-action",
			autoOpen: false,
			resizable: false,
			modal: true,
			buttons:[
			{
				html: "<span class='btn-yes' data-finder='order.dialog.badacademiclevel.ok'>Ok</span>",
				click: function() {
					jQuery( this ).dialog( "close" );
				},


			}
			]
		});
	});
</script>

<script type="text/javascript">
	jQuery(function () {
		jQuery('#dialog-bad-academiclevel-pages').dialog({
			closeOnEscape: false,
			dialogClass: "cabinet no-close user-action",
			autoOpen: false,
			resizable: false,
			modal: true,
			buttons: [
			{
				html: "<span class='btn-yes' data-finder='order.dialog.badacademiclevelpages.yes'>Yes</span> <span>Change it</span>",
				click: function() {
					jQuery('#radio_academic_level_2').prop('checked', true).trigger('change');
					jQuery('#topcat_id').val(jQuery(this).data('topcat_id')).trigger('change');
					jQuery( this ).dialog( "close" );
				}
			},
			{
				html: "<span class='btn-no' data-finder='order.dialog.badacademiclevelpages.no'>No</span> <span>Leave the count at 4 pages</span>",
				click: function () {
					jQuery('#pagesreq').val(4).trigger('change');
					jQuery('#topcat_id').val(jQuery(this).data('topcat_id')).trigger('change');
					jQuery( this ).dialog( "close" );
				}
			}
			],
                close: function (e) {  //For (x) button
                	if (jQuery(e.currentTarget).hasClass('ui-dialog-titlebar-close')) {
                		jQuery('#pagesreq').val(4).trigger('change');
                		jQuery('#topcat_id').val(jQuery(this).data('topcat_id')).trigger('change');
                	}
                }
            });
	});
</script>

<script type="text/javascript">
	jQuery(function () {
		jQuery('#dialog-form-potato-little-time').dialog({
			closeOnEscape: false,
			dialogClass: "cabinet no-close user-action",
			autoOpen: false,
			resizable: false,
			modal: true,
			buttons: [{
				html: "<span class='btn-yes' data-finder='order.dialog.littletime.ok'>Ok</span>",
				click: function() {
					jQuery( this ).dialog( "close" );
				}
			}]
		});
	});

</script>

<script type="text/javascript">
	jQuery(function () {
		jQuery('#dialog-low-academiclevel').dialog({
			closeOnEscape: false,
			dialogClass: "cabinet user-action",
			autoOpen: false,
			resizable: false,
			modal: true,
			buttons: [
			{
				html: "<span class='btn-yes' data-finder='order.dialog.lowacademiclevel.keep'>Keep</span>",
				click: function() {
					jQuery( this ).dialog( "close" );
				},
				class: 'highlight'
			},
			{
				html: "<span class='btn-no' data-finder='order.dialog.lowacademiclevel.revert'>Revert</span>",
				click: function () {
					var new_academic_level = jQuery('#min_academic_level').val();

					jQuery('[name="academiclevel"][value="' + new_academic_level + '"]').prop('checked', true).trigger('change');
					jQuery( this ).dialog( "close" );
				}
			}
			]
		});
	});
</script>

<script type="text/javascript">
	jQuery(function () {
		jQuery('#dialog-low-academiclevel-step3').dialog({
			closeOnEscape: false,
			dialogClass: "cabinet no-close user-action",
			autoOpen: false,
			resizable: false,
			modal: true,
			buttons: [
			{
				html: "<span class=\"btn-yes\" data-finder='order.dialog.lowacademiclevelstep3.keep'>Keep</span>",
				click: function() {
					jQuery( this ).dialog( "close" );
				},
				class: 'highlight'
			},
			{
				html: "<span class=\"btn-no\" data-finder='order.dialog.lowacademiclevelstep3.revert'>Revert</span>",
				click: function () {
					var new_academic_level = jQuery('#min_academic_level').val();

					jQuery('[name="academiclevel"][value="' + new_academic_level + '"]').prop('checked', true).trigger('change');
					jQuery( this ).dialog( "close" );
				}
			}
			]
		});
	});
</script>

<!-- END FORM DIALOGS -->

<!-- POPUP WITH UNPAID ORDERS -->
<!-- END POPUP WITH UNPAID ORDERS -->


</div>
</div>
</div>
</div>

<style>

	/* See http://code.google.com/p/minify/wiki/CommonProblems#@imports_can_appear_in_invalid_locations_in_combined_CSS_files */
	.cabinet
	p{margin:0}.cabinet
	label{font-weight:400}.cabinet input,
	.cabinet
	button{-moz-box-sizing:initial;box-sizing:initial}.cabinet ol, .cabinet
	ul{list-style:none;margin:0;padding:0}.cabinet sub, .cabinet
	sup{font-size: .8em;line-height:0;position:relative;vertical-align:baseline}.cabinet
	sup{top:-.3em}.cabinet
	sub{bottom:-.25em}.cabinet
	pre{white-space:pre;white-space:pre-wrap;word-wrap:break-word}.cabinet input, .cabinet textarea, .cabinet button, .cabinet
	select{margin:0;font-size:100%;line-height:normal;vertical-align:baseline}.cabinet button, .cabinet html input[type="button"],
	.cabinet input[type="reset"], .cabinet input[type="submit"]{cursor:pointer}.cabinet
	textarea{overflow:auto;resize:vertical}.cabinet
	figure{position:relative}.cabinet figure img, .cabinet figure object, .cabinet figure embed,
	.cabinet figure
	video{max-width:100%;display:block}.cabinet
	img{border:0;-ms-interpolation-mode:bicubic;display:block}.cabinet a,
	.cabinet a u,
	.cabinet
	.link{text-decoration:none}.cabinet .active, .cabinet .active a,
	.cabinet a.active:hover, .cabinet .active a:hover{text-decoration:none;cursor:default;color:inherit}.cabinet
	em{font-style:italic}.cabinet
	strong{font-weight:bold}@import url(//fonts.googleapis.com/css?family=Roboto:400,700);
	.cabinet .clearfix {
		*zoom: 1
	}
	.cabinet .clearfix:before,
	.clearfix:after {
		display: table;
		visibility: hidden;
		height: 0;
		content: " ";
		font-size: 0
	}
	.cabinet .clearfix:after {
		clear: both
	}
	.cabinet .clear {
		clear: both
	}
	.cabinet .nobr {
		white-space: nowrap
	}
	.mainInRgt {
		min-height: 750px
	}
	.cabinet .table {
		display: table;
		width: 100%
	}
	.cabinet .row {
		display: table-row
	}
	.cabinet .row:before,
	.cabinet .row:after {
		display: none
	}
	.cabinet .cell {
		display: table-cell;
		vertical-align: top
	}
	html.js .cabinet .hidden {
		display: none
	}
	html.js .cabinet .visible {
		display: block
	}
	.cabinet .visible-in-mobile {
		display: none
	}
	.cabinet .validation_img {
		display: none !important
	}
	.cabinet .uvoform_potato {
		-webkit-animation: loading-forms 1.5s 1 cubic-bezier(0, 1.07, 1, 1) forwards;
		animation: loading-forms 1.5s 1 cubic-bezier(0, 1.07, 1, 1) forwards
	}
	.cabinet .bubbles {
		display: none
	}
	.cabinet .loading+.bubbles {
		display: block;
		margin: 0 -30px;
		width: 60px;
		height: 60px;
		position: absolute;
		top: 100px;
		left: 50%;
		text-align: center;
		border-radius: 50%;
		-webkit-animation: rotate-bubbles 2.0s infinite linear;
		animation: rotate-bubbles 2.0s infinite linear;
		z-index: 5
	}
	.cabinet .blockUI {
		padding: 0 !important;
		border: 0 !important;
		background: none !important
	}
	.ie8 .blockUI {
		background: #fff !important
	}
	.cabinet .loader-text {
		color: transparent
	}
	.ie8 .loader-text {
		color: #aaa
	}
	.cabinet .spinner-bubbles {
		margin: -30px;
		width: 60px;
		height: 60px;
		position: absolute;
		top: 50%;
		left: 50%;
		text-align: center;
		border-radius: 50%;
		-webkit-animation: rotate-bubbles 2.0s infinite linear;
		animation: rotate-bubbles 2.0s infinite linear
	}
	.cabinet .spinner-bubbles:before,
	.cabinet .spinner-bubbles:after,
	.cabinet .loading + .bubbles:before,
	.cabinet .loading+.bubbles:after {
		content: ' ';
		width: 14px;
		height: 14px;
		border: 2px solid white;
		display: inline-block;
		box-sizing: content-box;
		-mox-box-sizing: content-box;
		position: absolute;
		top: 10px;
		left: 10px;
		background-color: #3998ce;
		border-radius: 100%;
		-webkit-animation: bounce-bubbles 2.0s infinite ease-in-out;
		animation: bounce-bubbles 2.0s infinite ease-in-out
	}
	.cabinet .spinner-bubbles:after,
	.cabinet .loading+.bubbles:after {
		top: auto;
		bottom: 10px;
		-webkit-animation-delay: -1.0s;
		animation-delay: -1.0s
	}
	@-webkit-keyframes rotate-bubbles {
		100% {
			-webkit-transform: rotate(360deg)
		}
	}
	@keyframes rotate-bubbles {
		100% {
			transform: rotate(360deg);
			-webkit-transform: rotate(360deg)
		}
	}
	@-webkit-keyframes bounce-bubbles {
		0%, 100% {
			-webkit-transform: scale(0.0)
		}
		50% {
			-webkit-transform: scale(1.0)
		}
	}
	@keyframes bounce-bubbles {
		0%, 100% {
			transform: scale(0.0);
			-webkit-transform: scale(0.0)
		}
		50% {
			transform: scale(1.0);
			-webkit-transform: scale(1.0)
		}
	}
	@-webkit-keyframes loading-forms {
		0% {
			opacity: 0.05
		}
		100% {
			opacity: 1
		}
	}
	@keyframes loading-forms {
		0% {
			opacity: 0.05
		}
		100% {
			opacity: 1
		}
	}
	.ui-dialog[aria-describedby="unpaid_popup"] {
		opacity: 1;
		-webkit-transition: opacity .3s ease;
		transition: opacity .3s ease
	}
	.cabinet .ui-dialog [aria-describedby="unpaid_popup"] {
		-webkit-transition: top 0.2s linear, left 0.2s linear, opacity .3s ease;
		transition: top 0.2s linear, left 0.2s linear, opacity .3s ease
	}
	.ui-widget-overlay {
		-webkit-transition: opacity .3s ease;
		transition: opacity .3s ease
	}
	.ui-dialog[aria-describedby="unpaid_popup"].loading,
	.ui-widget-overlay.loading-overlay {
		opacity: 0;
		visibility: hidden
	}
	label {
		display: inline-block
	}
	.cabinet select,
	.cabinet textarea,
	.cabinet input[type="text"],
	.cabinet input[type="password"],
	.cabinet input[type="datetime"],
	.cabinet input[type="datetime-local"],
	.cabinet input[type="date"],
	.cabinet input[type="month"],
	.cabinet input[type="time"],
	.cabinet input[type="week"],
	.cabinet input[type="number"],
	.cabinet input[type="email"],
	.cabinet input[type="url"],
	.cabinet input[type="search"],
	.cabinet input[type="tel"],
	.cabinet input[type="file"],
	.cabinet .button,
	.cabinet .ui-button,
	.cabinet button,
	.cabinet input[type="submit"],
	.cabinet input[type="checkbox"] + label,
	.cabinet .input-model,
	#addmaterials_dialog_complete + label,
	.fileupload-buttons .upload-file {
		display: -moz-inline-stack;
		display: inline-block;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		vertical-align: top;
		font-size: 100%;
		line-height: 17px;
		zoom: 1;
		*height: auto;
		*display: inline
	}
	.cabinet .button,
	.cabinet .ui-button,
	.cabinet button,
	.cabinet input[type="submit"],
	.fileupload-buttons .upload-file {
		text-align: center;
		text-decoration: none;
		white-space: nowrap
	}
	.cabinet .input-inliner {
		display: block;
		overflow: hidden
	}
	@-moz-document url-prefix() {
		.cabinet input:-moz-focus-inner {
			padding: 0;
			border: 0
		}
	}
	.cabinet .button.attachleft,
	.cabinet .ui-button.attachleft,
	.cabinet .input-model.attachleft,
	.cabinet button.attachleft,
	.cabinet input.attachleft {
		margin-left: -1px;
		-webkit-border-top-left-radius: 0;
		-moz-border-radius-topleft: 0;
		border-top-left-radius: 0;
		-webkit-border-bottom-left-radius: 0;
		-moz-border-radius-bottomleft: 0;
		border-bottom-left-radius: 0
	}
	.cabinet .button.attachright,
	.cabinet .ui-button.attachright,
	.cabinet .input-model.attachright,
	.cabinet button.attachright,
	.cabinet input.attachright {
		margin-right: -1px;
		-webkit-border-top-right-radius: 0;
		-moz-border-radius-topright: 0;
		border-top-right-radius: 0;
		-webkit-border-bottom-right-radius: 0;
		-moz-border-radius-bottomright: 0;
		border-bottom-right-radius: 0
	}
	.cabinet select,
	.cabinet textarea,
	.cabinet input[type="text"],
	.cabinet input[type="password"],
	.cabinet input[type="number"],
	.cabinet input[type="email"],
	.cabinet input[type="url"],
	.cabinet input[type="search"],
	.cabinet input[type="tel"] {
		max-width: 100%;
		width: 100%;
		vertical-align: middle
	}
	.cabinet select,
	.cabinet textarea,
	.cabinet input[type="text"],
	.cabinet input[type="password"],
	.cabinet input[type="number"],
	.cabinet input[type="email"],
	.cabinet input[type="url"],
	.cabinet input[type="search"],
	.cabinet input[type="tel"],
	.cabinet input[type="file"],
	.cabinet .button,
	.cabinet .ui-button,
	.cabinet button,
	.cabinet input[type="submit"],
	.cabinet input[type="checkbox"] + label,
	.cabinet .input-model,
	#addmaterials_dialog_complete + label,
	.fileupload-buttons .upload-file {
		margin-bottom: 12px;
		padding: 9px 10px;
		height: 38px
	}
	.cabinet select {
		height: 38px
	}
	.cabinet .button,
	.cabinet .ui-button,
	.cabinet button,
	.cabinet input[type="submit"],
	.cabinet input[type="checkbox"] + label,
	.cabinet .input-model,
	#addmaterials_dialog_complete + label,
	.fileupload-buttons .upload-file {
		padding-right: 15px;
		padding-left: 15px
	}
	.cabinet .button:active,
	.cabinet .ui-button:active,
	.cabinet .ui-button.ui-state-active,
	.cabinet button:active,
	.cabinet input[type="submit"]:active,
	.fileupload-buttons .upload-file:active {
		padding-top: 10px;
		padding-bottom: 9px
	}
	.cabinet select.smaller,
	.cabinet textarea.smaller,
	.cabinet input[type="text"].smaller,
	.cabinet input[type="password"].smaller,
	.cabinet input[type="number"].smaller,
	.cabinet input[type="email"].smaller,
	.cabinet input[type="url"].smaller,
	.cabinet input[type="search"].smaller,
	.cabinet input[type="tel"].smaller,
	.cabinet input[type="file"].smaller,
	.cabinet .button.smaller,
	.cabinet .ui-button.smaller,
	.cabinet button.smaller,
	.cabinet input[type="submit"].smaller,
	.cabinet input[type="checkbox"] + label.smaller,
	.cabinet .input-model.smaller {
		margin-bottom: 12px;
		padding: 6px 5px 7px;
		height: 30px
	}
	.cabinet .button.smaller,
	.cabinet .ui-button.smaller,
	.cabinet button.smaller,
	.cabinet input[type="submit"].smaller,
	.cabinet input[type="checkbox"] + label.smaller,
	.cabinet .input-model.smaller {
		padding: 6px 15px 7px 15px
	}
	.cabinet .button.smaller:active,
	.cabinet .ui-button.smaller:active,
	.cabinet .ui-button.smaller.ui-state-active,
	.cabinet button.smaller:active,
	.cabinet input[type="submit"].smaller:active {
		padding-top: 7px;
		padding-bottom: 6px
	}
	.cabinet select.large,
	.cabinet textarea.large,
	.cabinet input[type="text"].large,
	.cabinet input[type="password"].large,
	.cabinet input[type="number"].large,
	.cabinet input[type="email"].large,
	.cabinet input[type="url"].large,
	.cabinet input[type="search"].large,
	.cabinet input[type="tel"].large,
	.cabinet input[type="file"].large,
	.cabinet .button.large,
	.cabinet .ui-button.large,
	.cabinet button.large,
	.cabinet input[type="submit"].large,
	.cabinet input[type="checkbox"] + label.large,
	.cabinet .input-model.large,
	#addmaterials_dialog_complete+label.large {
		margin-bottom: 0;
		padding: 15px 10px;
		height: 48px
	}
	.cabinet .button.large,
	.cabinet .ui-button.large,
	.cabinet button.large,
	.cabinet input[type="submit"].large,
	.cabinet input[type="checkbox"] + label.large,
	.cabinet .input-model.large,
	#addmaterials_dialog_complete+label.large {
		padding-right: 20px;
		padding-left: 20px
	}
	.cabinet .button.large,
	.cabinet .ui-button.large,
	.cabinet button.large,
	.cabinet input[type="submit"].large {
		font-weight: bold
	}
	.cabinet .button.large:active,
	.cabinet .ui-button.large:active,
	.cabinet .ui-button.large.ui-state-active,
	.cabinet button.large:active,
	.cabinet input[type="submit"].large:active {
		padding-top: 16px;
		padding-bottom: 14px
	}
	.cabinet textarea[rows="1"],
	.cabinet select[size="1"] {
		height: 30px
	}
	.cabinet textarea[rows="2"],
	.cabinet select[size="2"] {
		height: 54px
	}
	.cabinet textarea[rows="3"],
	.cabinet select[size="3"] {
		height: 78px
	}
	.cabinet textarea[rows="4"],
	.cabinet select[size="4"] {
		height: 102px
	}
	.cabinet textarea[rows="5"],
	.cabinet select[size="5"] {
		height: 126px
	}
	.cabinet textarea[rows="6"],
	.cabinet select[size="6"] {
		height: 150px
	}
	.cabinet textarea[rows="7"],
	.cabinet select[size="7"] {
		height: 174px
	}
	.cabinet textarea[rows="8"],
	.cabinet select[size="8"] {
		height: 198px
	}
	.cabinet textarea[rows="9"],
	.cabinet select[size="9"] {
		height: 222px
	}
	.cabinet textarea,
	.cabinet select[multiple] {
		height: 102px
	}
	.cabinet .left-label,
	.cabinet .large-label {
		color: #5e676b;
		font-size: 15px;
		font-family: 'roboto', 'arial', sans-serif
	}
	.cabinet .left-label {
		display: table-cell;
		padding-right: 10px;
		width: 500px;
		height: 36px;
		vertical-align: middle;
		text-align: right;
		line-height: 16px
	}
	.cabinet .left-label .optional {
		font-size: 12px;
		opacity: 0.6
	}
	.cabinet .large-label {
		margin-bottom: 5px
	}
	.cabinet input,
	.cabinet .button,
	.cabinet .ui-button,
	.cabinet button,
	.cabinet select,
	.cabinet textarea,
	.fileupload-buttons .upload-file {
		font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important
	}
	.cabinet input:-webkit-input-placeholder,
	.cabinet textarea:-webkit-input-placeholder,
	.cabinet input.placeholder,
	.cabinet textarea.placeholder {
		color: grey !important;
		font-style: italic
	}
	.cabinet select,
	.cabinet textarea,
	.cabinet input[type="text"],
	.cabinet input[type="password"],
	.cabinet input[type="number"],
	.cabinet input[type="email"],
	.cabinet input[type="url"],
	.cabinet input[type="search"],
	.cabinet input[type="tel"],
	.cabinet input[type="file"],
	.cabinet input[type="checkbox"] + label:before,
	.cabinet input[type="checkbox"] + label .feature-header:before,
	.cabinet .input-style,
	#addmaterials_dialog_complete+label:before {
		border: 1px solid #ccc;
		border-radius: 3px;
		background-color: #fff;
		box-shadow: inset 0 2px 2px rgba(0, 0, 0, .05);
		color: #555;
		-webkit-transition: border-color linear .2s, box-shadow linear .2s;
		-moz-transition: border-color linear .2s, box-shadow linear .2s;
		transition: border-color linear .2s, box-shadow linear .2s
	}
	.cabinet select:hover,
	.cabinet textarea:hover,
	.cabinet input[type="text"]:hover,
	.cabinet input[type="password"]:hover,
	.cabinet input[type="number"]:hover,
	.cabinet input[type="email"]:hover,
	.cabinet input[type="url"]:hover,
	.cabinet input[type="search"]:hover,
	.cabinet input[type="tel"]:hover,
	.cabinet .button:hover,
	.cabinet button:hover,
	.cabinet input[type=submit]:hover,
	.cabinet input[type="checkbox"] + label:hover:before,
	.cabinet input[type="checkbox"] + label:hover .feature-header:before,
	.cabinet .input-style:hover,
	#addmaterials_dialog_complete + label:hover:before,
	.fileupload-buttons .upload-file:hover {
		background-color: #fafdff;
		box-shadow: inset 0 2px 2px rgba(0, 44, 60, .07)
	}
	.cabinet select:focus,
	.cabinet textarea:focus,
	.cabinet input[type="text"]:focus,
	.cabinet input[type="password"]:focus,
	.cabinet input[type="number"]:focus,
	.cabinet input[type="email"]:focus,
	.cabinet input[type="url"]:focus,
	.cabinet input[type="search"]:focus,
	.cabinet input[type="tel"]:focus,
	.cabinet .button:focus,
	.cabinet button:focus,
	.cabinet input[type=submit]:focus,
	.cabinet input[type="checkbox"]:focus + label:before,
	.cabinet .input-style.focus,
	#addmaterials_dialog_complete:focus + label:before,
	.fileupload-buttons .upload-file:focus {
		outline: 0;
		outline: thin dotted \9;
		border-color: rgba(82, 168, 236, .8);
		background-color: white;
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgba(82, 168, 236, .6)
	}
	.cabinet select:focus,
	.cabinet input[type="file"]:focus,
	.cabinet input[type="radio"]:focus,
	.cabinet input[type="checkbox"]:focus,
	#addmaterials_dialog_complete:before {
		outline: none;
		outline: 1px auto rgba(82, 168, 236, 1)
	}
	.cabinet select:disabled,
	.cabinet textarea:disabled,
	.cabinet input[type="text"]:disabled,
	.cabinet input[type="password"]:disabled,
	.cabinet input[type="number"]:disabled,
	.cabinet input[type="email"]:disabled,
	.cabinet input[type="url"]:disabled,
	.cabinet input[type="search"]:disabled,
	.cabinet input[type="tel"]:disabled,
	.cabinet input[type="file"]:disabled,
	.cabinet input[type="checkbox"] + label:disabled:before,
	.cabinet input[type="checkbox"] + label .feature-header:disabled:before,
	.cabinet .input-style:disabled,
	#addmaterials_dialog_complete+label:disabled:before {
		background-color: #eee !important;
		background-image: none;
		border-color: #ccc !important;
		cursor: not-allowed
	}
	.cabinet .ui-button .ui-button-text {
		display: inline
	}
	.cabinet .ui-button-text-only .ui-button-text,
	.cabinet .ui-button-text-icon-primary .ui-button-text,
	.cabinet .ui-button-text-icons .ui-button-text,
	.cabinet .ui-button-text-icon-secondary .ui-button-text,
	.cabinet .ui-button-text-icons .ui-button-text,
	.cabinet .ui-button-text-icons .ui-button-text {
		padding: 0
	}
	.cabinet .ui-button-icon-only .ui-button-text,
	.cabinet .ui-button-icons-only .ui-button-text {
		padding: 0;
		text-indent: -9999999px
	}
	.cabinet .button,
	.cabinet .ui-button,
	.cabinet button,
	.cabinet input[type="submit"],
	.cabinet .button-style,
	.fileupload-buttons .upload-file {
		border: 1px solid #e4e4e4;
		border-color: rgba(0, 0, 0, .18);
		border-radius: 3px;
		background-color: #f6f7f7;
		background-image: -webkit-linear-gradient(top, #fbfbfb, #ebeded);
		background-image: -moz-linear-gradient(top, #fbfbfb, #ebeded);
		background-image: linear-gradient(to bottom, #fbfbfb, #ebeded);
		background-position: 0 -1px;
		background-repeat: repeat-x;
		color: #538395;
		text-shadow: 0 1px 1px rgba(255, 255, 255, .4);
		cursor: pointer;
		-webkit-transition: color .1s linear;
		-moz-transition: color .1s linear;
		transition: color .1s linear
	}
	.cabinet .button:hover,
	.cabinet .ui-button:hover,
	.cabinet a:hover .button,
	.cabinet a:hover .button,
	.cabinet button:hover,
	.cabinet input[type="submit"]:hover,
	.cabinet .button-style:hover,
	.fileupload-buttons .upload-file:hover {
		border-color: rgba(0, 0, 0, .18);
		background-color: #fbfbfb;
		background-image: -webkit-linear-gradient(top, #fbfbfb, #dcf2f8);
		background-image: -moz-linear-gradient(top, #fbfbfb, #dcf2f8);
		background-image: linear-gradient(to bottom, #fbfbfb, #dcf2f8);
		background-position: 0 7px;
		box-shadow: inset 0 -1px 2px rgba(0, 44, 60, .07);
		color: #14789e;
		text-decoration: none;
		-webkit-transition: color .1s linear, background-position .1s linear, padding .1s linear, box-shadow .1s linear;
		-moz-transition: color .1s linear, background-position .1s linear, padding .1s linear, box-shadow .1s linear;
		transition: color .1s linear, background-position .1s linear, padding .1s linear, box-shadow .1s linear
	}
	.cabinet .button:active,
	.cabinet .ui-button:active,
	.cabinet .ui-button.ui-state-active,
	.cabinet a:active .button,
	.cabinet button:active,
	.cabinet input[type="submit"]:active,
	.cabinet .button-style:active,
	.fileupload-buttons .upload-file:active {
		outline: 0;
		background-image: none;
		box-shadow: inset 0 2px 4px rgba(0, 0, 0, .15), 0 1px 2px rgba(0, 0, 0, .05)
	}
	.cabinet .ui-dialog .button + .button,
	.cabinet .ui-dialog .ui-button + .ui-button,
	.cabinet .ui-dialog button + button,
	.cabinet .ui-dialog .button-style+.button-style {
		margin-left: 5px
	}
	.cabinet .ui-button.ui-state-active {
		background-color: #fbfbfb;
		color: #58696e
	}
	.cabinet .input-model:hover {
		border-color: transparent
	}
	.cabinet .button.highlight,
	.cabinet button.highlight,
	.cabinet input[type="submit"].highlight,
	.cabinet .ui-button.highlight:hover,
	.fileupload-buttons .upload-file.highlight {
		border: 1px solid #2177a3;
		border-color: rgba(0, 29, 45, .2);
		background-color: #1a77ac;
		background-image: -webkit-linear-gradient(top, #44b2db, #1a77ac);
		background-image: -moz-linear-gradient(top, #44b2db, #1a77ac);
		background-image: linear-gradient(to bottom, #44b2db, #1a77ac);
		color: white;
		text-shadow: 0 -1px 1px rgba(0, 29, 45, .4)
	}
	.cabinet .button.highlight:hover,
	.cabinet button.highlight:hover,
	.cabinet input[type="submit"].highlight:hover,
	.cabinet .ui-button.highlight:hover,
	.fileupload-buttons .upload-file.highlight:hover {
		border-color: rgba(0, 29, 45, .2);
		background-color: #44b2db;
		color: #dcf8ff
	}
	.cabinet .button.grey-button,
	.cabinet button.grey-button,
	.cabinet input[type="submit"].grey-button,
	.cabinet .ui-button.grey-button {
		background-color: #e1e1e1;
		background-image: -webkit-linear-gradient(top, #f4f4f4, #e1e1e1);
		background-image: -moz-linear-gradient(top, #f4f4f4, #e1e1e1);
		background-image: linear-gradient(to bottom, #f4f4f4, #e1e1e1);
		color: #1471a7;
		text-shadow: 0 1px 1px rgba(255, 255, 255, .7);
		font-size: 13px
	}
	.cabinet .button.grey-button:hover,
	.cabinet button.grey-button:hover,
	.cabinet input[type="submit"].grey-button:hover,
	.cabinet .ui-button.grey-button:hover {
		background-color: #f4f4f4;
		color: #198cce
	}
	.cabinet .button.white-button,
	.cabinet button.white-button,
	.cabinet input[type="submit"].white-button,
	.cabinet .ui-button.white-button:hover {
		background-color: white;
		background-image: -webkit-linear-gradient(top, white, #f5fcfd);
		background-image: -moz-linear-gradient(top, white, #f5fcfd);
		background-image: linear-gradient(to bottom, white, #f5fcfd);
		box-shadow: 0 1px 1px rgba(0, 31, 49, .21);
		color: #1471a7;
		text-shadow: 0 1px 1px rgba(255, 255, 255, .7);
		font-size: 13px
	}
	.cabinet .button.white-button:hover,
	.cabinet button.white-button:hover,
	.cabinet input[type="submit"].white-button:hover,
	.cabinet .ui-button.white-button:hover {
		background-color: #f5fcfd;
		background-position: 0 -20px;
		color: #198cce
	}
	.cabinet .button.lightblue,
	.cabinet button.lightblue,
	.cabinet input[type="submit"].lightblue,
	.cabinet .ui-button.lightblue {
		border: 1px solid #cfdbe1;
		border-color: rgba(0, 48, 74, .1);
		border-radius: 3px;
		background-color: #dee9ee;
		background-image: -moz-linear-gradient(top, #f4f8fa, #dee9ee);
		background-image: -webkit-linear-gradient(top, #f4f8fa, #dee9ee);
		background-image: linear-gradient(to bottom, #f4f8fa, #dee9ee);
		background-repeat: repeat-x;
		color: #1471a7;
		text-shadow: 0 1px 1px rgba(255, 255, 255, .7)
	}
	.cabinet .button.lightblue:hover,
	.cabinet button.lightblue:hover,
	.cabinet input[type="submit"].lightblue:hover,
	.cabinet .ui-button.lightblue:hover {
		background-color: #f4f8fa;
		background-position: 0 10px;
		color: #198cce;
		text-decoration: none;
		-webkit-transition: background-position .1s linear;
		-moz-transition: background-position .1s linear;
		transition: background-position .1s linear
	}
	.cabinet .button[disabled],
	.cabinet .ui-button[disabled],
	.cabinet button[disabled],
	.cabinet input[type="submit"][disabled],
	.fileupload-buttons .upload-file[disabled],
	.cabinet .button[disabled]:hover,
	.cabinet .ui-button[disabled]:hover,
	.cabinet button[disabled]:hover,
	.cabinet input[type="submit"][disabled]:hover,
	.fileupload-buttons .upload-file[disabled]:hover,
	.cabinet .button[disabled]:active,
	.cabinet .ui-button[disabled]:active,
	.cabinet button[disabled]:active,
	.cabinet input[type="submit"][disabled]:active,
	.fileupload-buttons .upload-file[disabled]:active,
	.cabinet .button.disabled,
	.cabinet .ui-button.disabled,
	.cabinet button.disabled,
	.cabinet input[type="submit"].disabled,
	.cabinet body .ui-button.ui-button-disabled.ui-state-disabled,
	.cabinet .button.disabled:hover,
	.cabinet .ui-button.disabled:hover,
	.cabinet button.disabled:hover,
	.cabinet input[type="submit"].disabled:hover,
	.cabinet .ui-button.ui-button-disabled.ui-state-disabled:hover,
	.cabinet .button.disabled:active,
	.cabinet .ui-button.disabled:active,
	.cabinet button.disabled:active,
	.cabinet input[type="submit"].disabled:active,
	.cabinet .ui-button.ui-button-disabled.ui-state-disabled:active,
	.cabinet input[type="radio"][disabled] + label,
	.cabinet input[type="radio"][disabled] + label:hover,
	.cabinet .ui-button.ui-button-disabled.ui-state-disabled,
	.cabinet .ui-button.ui-button-disabled.ui-state-disabled:hover {
		border-color: rgba(0, 29, 45, .05);
		background: #eee none;
		box-shadow: none;
		color: #a9a9a9;
		text-shadow: 0 1px 1px rgba(255, 255, 255, .7);
		cursor: default
	}
	.cabinet .nobutton,
	.cabinet .nobutton:hover,
	.cabinet .nobutton:active {
		border-color: transparent;
		background: transparent none;
		box-shadow: none
	}
	.cabinet .white-icon-approve.approve-button:before,
	.cabinet .white-icon-approve.approve-button:hover:before {
		background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6Q0U4QkNCQjlGRDM4MTFFMzlGRjdBMzA2NTNDNTlDMzEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6Q0U4QkNCQkFGRDM4MTFFMzlGRjdBMzA2NTNDNTlDMzEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpDRThCQ0JCN0ZEMzgxMUUzOUZGN0EzMDY1M0M1OUMzMSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpDRThCQ0JCOEZEMzgxMUUzOUZGN0EzMDY1M0M1OUMzMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PpNDT/kAAAD5SURBVHjaYvz//z8DNQATA5UApQZZA3EFmAXyGhmYBYg7gPg4EKuCxFjIcAUXEK8C4ntAbAvEf0CCjCQGNhsQbwDiB0CchSxBikGMQLwUiOWA2BGIf6PIkhAuNUD8GYgVsMkTa4grEP8B4kJcamAMTyAuBmImLIr4gfgxEN8EYlZCBuUD8Tcg3gDEnGiKZv+HgGB8rkbm2APxVyDeBsRsUDFnIP4HxCeBmJFYgxigtoI0zgVidiC+BXVNAKFwxCY4Gap5K5S+Ssg1uAziBuIH/xEglZiYxSURATXkNZbAJ8kgkFdOAHE/sQkWXxZxAeIXQHyFmPwDEGAARk+Dwzn7jGUAAAAASUVORK5CYII=) center center
	}
	.cabinet label.absolute-error,
	.cabinet label.validation_fail,
	.cabinet label.errors,
	.cabinet div.error:not(:empty),
	.cabinet span.absolute-error,
	.cabinet p.validation_fail,
	.cabinet div.validation_fail,
	.cabinet .formError,
	.cabinet #box_professor_email .professor_emailformError,
	.cabinet #other_box_paperformat .paper_format_type_optionformError {
		z-index: 15;
		position: relative;
		display: inline-block;
		margin-bottom: 12px;
		padding: 2px 10px;
		border-radius: 3px;
		background-color: #dc6868;
		color: white;
		text-shadow: 0 1px 0 rgba(0, 0, 0, .3)
	}
	.cabinet .formError,
	.cabinet #box_professor_email .professor_emailformError,
	.cabinet #other_box_paperformat .paper_format_type_optionformError {
		top: 0 !important;
		left: 0 !important;
		margin: -25px 0 0 !important;
		z-index: 2
	}
	.cabinet #contentspinpagesreq .pagesreqformError {
		position: absolute !important;
		top: -4px !important;
		left: 44px !important;
		margin: -24px 0 0 !important
	}
	.cabinet .cell-right:hover .formError,
	.cabinet .cell-right:hover #box_professor_email .professor_emailformError,
	.cabinet .cell-right:hover #other_box_paperformat .paper_format_type_optionformError {
		z-index: 4
	}
	.cabinet label.absolute-error:before,
	.cabinet label.errors:before,
	.cabinet div.error:not(:empty):before,
	.cabinet label.validation_fail:before,
	.cabinet span.validation_fail:before,
	.cabinet p.validation_fail:before,
	.cabinet div.validation_fail:before,
	.cabinet .formError:before,
	.cabinet #box_professor_email .professor_emailformError:before,
	.cabinet #other_box_paperformat .paper_format_type_optionformError:before {
		position: absolute;
		bottom: -7px;
		left: 10px;
		width: 0px;
		height: 0px;
		border-width: 7px 5px 0;
		border-style: solid;
		border-color: #dc6868 transparent transparent;
		content: " "
	}
	.cabinet .validation_fail>.validation_fail:before {
		content: none !important
	}
	.cabinet label.errors:before,
	.cabinet div.error:not(:empty):before,
	.cabinet span.validation_fail:before,
	.cabinet p.validation_fail:before,
	.cabinet div.validation_fail:before {
		top: -7px;
		bottom: auto;
		left: 10px;
		border-width: 0 5px 7px;
		border-color: transparent transparent #dc6868
	}
	.cabinet .td .validation_fail:before {
		display: none
	}
	.cabinet .td span.validation_fail:before {
		display: block
	}
	.cabinet .td .validation_fail {
		margin-bottom: 32px
	}
	.cabinet .td span.validation_fail {
		background-color: #dc6868;
		border-radius: 5px;
		color: #fff;
		display: block;
		font-size: 11px;
		line-height: 120%;
		padding: 5px;
		position: absolute;
		margin-top: -20px;
		margin-left: -5px;
		width: 150px
	}
	.cabinet .absolute-error-wrapper {
		position: relative;
		display: block
	}
	.cabinet .absolute-error-wrapper .formError,
	.cabinet .absolute-error-wrapper .absolute-error {
		top: auto !important;
		bottom: 100%;
		margin-top: -7px !important
	}
	.cabinet label.absolute-error {
		position: absolute !important;
		box-sizing: border-box;
		margin-bottom: 2px;
		max-width: 100%;
		-moz: border-box
	}
	.cabinet select.absolute-error,
	.cabinet textarea.absolute-error,
	.cabinet input[type="text"].absolute-error,
	.cabinet input[type="password"].absolute-error,
	.cabinet input[type="number"].absolute-error,
	.cabinet input[type="email"].absolute-error,
	.cabinet input[type="url"].absolute-error,
	.cabinet input[type="search"].absolute-error,
	.cabinet input[type="tel"].absolute-error,
	.cabinet .button.absolute-error,
	.cabinet button.absolute-error,
	.cabinet input[type=submit].absolute-error,
	.cabinet select.errors,
	.cabinet textarea.errors,
	.cabinet input[type="text"].errors,
	.cabinet input[type="password"].errors,
	.cabinet input[type="number"].errors,
	.cabinet input[type="email"].errors,
	.cabinet input[type="url"].errors,
	.cabinet input[type="search"].errors,
	.cabinet input[type="tel"].errors,
	.cabinet .button.errors,
	.cabinet button.errors,
	.cabinet input[type=submit].errors,
	.cabinet select.validation_fail,
	.cabinet textarea.validation_fail,
	.cabinet input[type="text"].validation_fail,
	.cabinet input[type="password"].validation_fail,
	.cabinet input[type="number"].validation_fail,
	.cabinet input[type="email"].validation_fail,
	.cabinet input[type="url"].validation_fail,
	.cabinet input[type="search"].validation_fail,
	.cabinet input[type="tel"].validation_fail,
	.cabinet .button.validation_fail,
	.cabinet button.validation_fail,
	.cabinet input[type=submit].validation_fail {
		border-color: rgba(236, 82, 82, .8);
		background-color: #fff9f9
	}
	.cabinet input.absolute-error:-webkit-input-placeholder,
	.cabinet textarea.absolute-error:-webkit-input-placeholder,
	.cabinet input.absolute-error.placeholder,
	.cabinet textarea.absolute-error.placeholder,
	.cabinet input.errors:-webkit-input-placeholder,
	.cabinet textarea.errors:-webkit-input-placeholder,
	.cabinet input.errors.placeholder,
	.cabinet textarea.errors.placeholder,
	.cabinet input.validation_fail:-webkit-input-placeholder,
	.cabinet textarea.validation_fail:-webkit-input-placeholder,
	.cabinet input.validation_fail.placeholder,
	.cabinet textarea.validation_fail.placeholder {
		color: rgba(236, 82, 82, .8) !important;
		font-style: italic
	}
	.cabinet select.absolute-error:focus,
	.cabinet textarea.absolute-error:focus,
	.cabinet input[type="text"].absolute-error:focus,
	.cabinet input[type="password"].absolute-error:focus,
	.cabinet input[type="number"].absolute-error:focus,
	.cabinet input[type="email"].absolute-error:focus,
	.cabinet input[type="url"].absolute-error:focus,
	.cabinet input[type="search"].absolute-error:focus,
	.cabinet input[type="tel"].absolute-error:focus,
	.cabinet .button.absolute-error:focus,
	.cabinet button.absolute-error:focus,
	.cabinet input[type=submit].absolute-error:focus,
	.cabinet select.errors:focus,
	.cabinet textarea.errors:focus,
	.cabinet input[type="text"].errors:focus,
	.cabinet input[type="password"].errors:focus,
	.cabinet input[type="number"].errors:focus,
	.cabinet input[type="email"].errors:focus,
	.cabinet input[type="url"].errors:focus,
	.cabinet input[type="search"].errors:focus,
	.cabinet input[type="tel"].errors:focus,
	.cabinet .button.errors:focus,
	.cabinet button.errors:focus,
	.cabinet input[type=submit].errors:focus,
	.cabinet select.validation_fail:focus,
	.cabinet textarea.validation_fail:focus,
	.cabinet input[type="text"].validation_fail:focus,
	.cabinet input[type="password"].validation_fail:focus,
	.cabinet input[type="number"].validation_fail:focus,
	.cabinet input[type="email"].validation_fail:focus,
	.cabinet input[type="url"].validation_fail:focus,
	.cabinet input[type="search"].validation_fail:focus,
	.cabinet input[type="tel"].validation_fail:focus,
	.cabinet .button.validation_fail:focus,
	.cabinet button.validation_fail:focus,
	.cabinet input[type=submit].validation_fail:focus {
		outline: 0;
		outline: thin dotted \9;
		border-color: rgba(236, 82, 82, .8);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgba(236, 82, 82, .6)
	}
	.cabinet select.valid,
	.cabinet textarea.valid,
	.cabinet input[type="text"].valid,
	.cabinet input[type="password"].valid,
	.cabinet input[type="number"].valid,
	.cabinet input[type="email"].valid,
	.cabinet input[type="url"].valid,
	.cabinet input[type="search"].valid,
	.cabinet input[type="tel"].valid,
	.cabinet .button.valid,
	.cabinet button.valid,
	.cabinet input[type=submit].valid,
	.cabinet select.validation_success,
	.cabinet textarea.validation_success,
	.cabinet input[type="text"].validation_success,
	.cabinet input[type="password"].validation_success,
	.cabinet input[type="number"].validation_success,
	.cabinet input[type="email"].validation_success,
	.cabinet input[type="url"].validation_success,
	.cabinet input[type="search"].validation_success,
	.cabinet input[type="tel"].validation_success,
	.cabinet .button.validation_success,
	.cabinet button.validation_success,
	.cabinet input[type=submit].validation_success {
		border-color: rgba(130, 225, 145, .8);
		background-color: #f9fffa
	}
	.cabinet select.valid:focus,
	.cabinet textarea.valid:focus,
	.cabinet input[type="text"].valid:focus,
	.cabinet input[type="password"].valid:focus,
	.cabinet input[type="number"].valid:focus,
	.cabinet input[type="email"].valid:focus,
	.cabinet input[type="url"].valid:focus,
	.cabinet input[type="search"].valid:focus,
	.cabinet input[type="tel"].valid:focus,
	.cabinet .button.valid:focus,
	.cabinet button.valid:focus,
	.cabinet input[type=submit].valid:focus,
	.cabinet select.validation_success:focus,
	.cabinet textarea.validation_success:focus,
	.cabinet input[type="text"].validation_success:focus,
	.cabinet input[type="password"].validation_success:focus,
	.cabinet input[type="number"].validation_success:focus,
	.cabinet input[type="email"].validation_success:focus,
	.cabinet input[type="url"].validation_success:focus,
	.cabinet input[type="search"].validation_success:focus,
	.cabinet input[type="tel"].validation_success:focus,
	.cabinet .button.validation_success:focus,
	.cabinet button.validation_success:focus,
	.cabinet input[type=submit].validation_success:focus {
		outline: 0;
		outline: thin dotted \9;
		border-color: rgba(130, 225, 145, .8);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgba(75, 209, 94, .4)
	}
	.cabinet #error_sample_comment {
		display: block
	}
	html,
	body {
		height: 100%
	}
	.cabinet {
		color: #51575d;
		font-size: 14px;
		font-family: 'arial', sans-serif;
		line-height: 21px
	}
	body.cabinet {
		min-width: 800px;
		margin: 0;
		padding: 0;
		background-color: white
	}
	.cabinet .container {
		margin: 0 auto;
		max-width: 960px
	}
	.cabinet .cabinet-content {
		position: relative;
		padding: 21px 20px
	}
	.notification {
		background-color: #f2eec3;
		border-bottom: 1px solid #d3edfb;
		font-size: 12px;
		padding: 5px 15px;
		text-align: center
	}
	.main-wrapper {
		min-height: 100%
	}
	.cabinet #main-wrapper {
		margin-bottom: -42px;
		min-height: 100%;
		height: auto !important;
		height: 100%
	}
	.cabinet #main-wrapper:after {
		display: block;
		clear: both;
		height: 42px;
		content: " "
	}
	.cabinet p,
	.cabinet ol,
	.cabinet ul,
	.cabinet dl,
	.cabinet table,
	.cabinet blockquote,
	.cabinet form,
	.cabinet hr,
	.cabinet .form-section {
		margin-bottom: 21px
	}
	.cabinet h1,
	.cabinet h2,
	.cabinet h3,
	.cabinet h4,
	.cabinet h5,
	.cabinet h6 {
		margin-bottom: 21px;
		color: #3e454c;
		font-weight: normal;
		font-family: 'roboto', 'arial', sans-serif;
		line-height: 21px
	}
	.cabinet h1 {
		margin-top: 42px;
		margin-bottom: 13px;
		font-size: 30px;
		line-height: 42px
	}
	.cabinet h2 {
		font-size: 26px
	}
	.cabinet h3 {
		color: #5e676b;
		font-size: 21px
	}
	.cabinet h4 {
		color: #5e676b;
		font-size: 19px
	}
	.cabinet h5 {
		color: #5e676b;
		font-size: 17px
	}
	.cabinet h6 {
		margin-bottom: 0;
		color: #5e676b;
		font-size: 16px
	}
	.cabinet h1:first-child,
	.cabinet h2:first-child,
	.cabinet h3:first-child,
	.cabinet h4:first-child,
	.cabinet h5:first-child,
	.cabinet h6:first-child {
		margin-top: 0
	}
	.cabinet .grey-box {
		margin-bottom: 21px;
		padding: 21px 30px;
		border: 1px solid #d0d0d0;
		border-radius: 3px;
		background-color: #fafafa
	}
	.cabinet a,
	.cabinet .link {
		border-bottom: 1px solid;
		border-bottom-color: transparent;
		color: #2a8ebe;
		font-weight: normal;
		text-decoration: none;
		cursor: pointer;
		outline: 0;
		-webkit-transition: border-color linear .2s, color linear .2s;
		-moz-transition: border-color linear .2s, color linear .2s;
		transition: border-color linear .2s, color linear .2s
	}
	.cabinet a:hover,
	.cabinet .link:hover {
		border-bottom-color: #77c8ee;
		text-decoration: none;
		color: #0099e2
	}
	.cabinet a:focus,
	.cabinet .link:focus {
		color: #2a8ebe;
		text-decoration: none
	}
	a[href^=tel] {
		border-bottom: 0 none
	}
	.cabinet .submit-wrapper {
		text-align: right
	}
	.cabinet .lighter {
		color: #373737;
		color: rgba(0, 0, 0, .78)
	}
	.cabinet .light {
		color: #777;
		color: rgba(0, 0, 0, .53)
	}
	.cabinet .mobile-tip {
		display: none;
		padding: 3px;
		width: 19px;
		height: 19px;
		border-radius: 999px;
		background-color: #e8f6fa;
		color: #538395;
		text-align: center;
		font: normal 16px/16px monospace;
		cursor: pointer
	}
	.cabinet .mobile-tip:after {
		display: inline-block;
		width: 11px;
		height: 13px;
		background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAaCAYAAABhJqYYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpDQkVFRDFGOTQ4OTUxMUUzQTE5MDgxNTI5OUJBRThGMyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpDQkVFRDFGQTQ4OTUxMUUzQTE5MDgxNTI5OUJBRThGMyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkNCRUVEMUY3NDg5NTExRTNBMTkwODE1Mjk5QkFFOEYzIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkNCRUVEMUY4NDg5NTExRTNBMTkwODE1Mjk5QkFFOEYzIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+mNF4RwAAANZJREFUeNpi+P//PwMyDm6eqgTEoejiIMzEgAnSgHhVSMs0QXQJbIrPAnHnmpqs9+gSjCDjiQUsyByg1eVACmw90OQKYpwRCsTlBN0MNK0TSK3G5QwmBhLAIFashEZjVwwMrjPQoAOBM0B+KN4YRA5f9CgnKbqHSziTpJgfiCcDMTehnAJSuBOIeYH4LRB7AvFXbCbDFJ4GYl0gvgHE24GYB64Cms35gfgEEE8GYkaoGIieBcSHgJgH7AKowuNAPAlJIQM2DSABEyDuxKIQWUMfEOsDBBgAPsyz8cba20MAAAAASUVORK5CYII=');
		background-position: 0 0;
		background-repeat: no-repeat;
		content: " ";
		vertical-align: -2px
	}
	.cabinet .mobile-tip:before {
		display: inline-block;
		margin-left: -1000px;
		width: 1000px;
		height: 1px;
		background-color: #e8f6fa;
		content: " ";
		vertical-align: 3px
	}
	.cabinet .mobile-tip:hover {
		background-color: #a5cad4
	}
	.cabinet .mobile-tip:hover:after {
		background-position: 0 -17px
	}
	.cabinet .mobile-tip:hover:before {
		background-color: #a5cad4
	}
	.cabinet-menu {
		padding: 0 20px;
		border-bottom: 1px solid #d0d0d0;
		background-color: #fafafa;
		background-image: -moz-linear-gradient(top, rgba(255, 255, 255, 1) 0%, rgba(244, 244, 244, 1) 100%);
		background-image: -webkit-linear-gradient(top, rgba(255, 255, 255, 1) 0%, rgba(244, 244, 244, 1) 100%);
		background-image: linear-gradient(to bottom, rgba(255, 255, 255, 1) 0%, rgba(244, 244, 244, 1) 100%)
	}
	.time-zone-notice {
		float: right;
		padding-top: 12px;
		width: 180px;
		color: gray;
		font-size: 12px;
		line-height: 14px
	}
	.cabinet .cabinet-tabs {
		margin-bottom: 0;
		white-space: nowrap
	}
	.cabinet .order-actions {
		position: relative;
		z-index: 4
	}
	.cabinet .cabinet-tab {
		display: inline-block;
		vertical-align: top;
		margin: 0;
		zoom: 1;
		*display: inline
	}
	.cabinet .cabinet-tab-link {
		position: relative;
		display: block;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		margin-top: 7px;
		margin-bottom: -1px;
		padding: 10px;
		border: 1px solid #d0d0d0;
		border-radius: 3px 3px 0 0;
		background-color: #e8eced;
		background-image: -moz-linear-gradient(top, white 0%, #e8eced 100%);
		background-image: -webkit-linear-gradient(top, white 0%, #e8eced 100%);
		background-image: linear-gradient(to bottom, white 0%, #e8eced 100%);
		box-shadow: inset 0 -2px 1px rgba(0, 48, 74, .07);
		color: #2f7cad;
		font: normal 17px/21px 'Roboto', sans-serif;
		font-family: 'roboto', 'arial', sans-serif;
		-webkit-transition: border-bottom-color linear .1s, box-shadow linear .1s, color linear .1s;
		-moz-transition: border-bottom-color linear .1s, box-shadow linear .1s, color linear .1s;
		transition: border-bottom-color linear .1s, box-shadow linear .1s, color linear .1s
	}
	.cabinet .cabinet-tab-link:before {
		display: inline-block;
		margin: -5px 10px -5px 0;
		width: 28px;
		height: 30px;
		background-image: url(/css/common/cs/images/cabinet_spr.png);
		background-repeat: no-repeat;
		content: " ";
		vertical-align: -4px
	}
	.cabinet .cabinet-tab-link:hover,
	.cabinet .active .cabinet-tab-link,
	.cabinet .active .cabinet-tab-link:hover,
	.cabinet .active .new.cabinet-tab-link,
	.cabinet .active .new.cabinet-tab-link:hover,
	.cabinet .ui-state-active .cabinet-tab-link,
	.cabinet .ui-state-active .cabinet-tab-link:hover,
	.cabinet .ui-state-active .new.cabinet-tab-link,
	.cabinet .ui-state-active .new.cabinet-tab-link:hover {
		border-color: #d0d0d0;
		background: white none;
		color: #239ae5;
		text-shadow: none;
		text-decoration: none
	}
	.cabinet .cabinet-tab-link:active,
	.cabinet .active .cabinet-tab-link,
	.cabinet .active .cabinet-tab-link:active,
	.cabinet .active .new.cabinet-tab-link,
	.cabinet .active .new.cabinet-tab-link:active,
	.cabinet .ui-state-active .cabinet-tab-link,
	.cabinet .ui-state-active .cabinet-tab-link:active,
	.cabinet .ui-state-active .new.cabinet-tab-link,
	.cabinet .ui-state-active .new.cabinet-tab-link:active {
		border-bottom-color: white;
		box-shadow: none
	}
	.cabinet .active .cabinet-tab-link,
	.cabinet .active .cabinet-tab-link:hover,
	.cabinet .active .new.cabinet-tab-link,
	.cabinet .active .new.cabinet-tab-link:hover,
	.cabinet .ui-state-active .cabinet-tab-link,
	.cabinet .ui-state-active .cabinet-tab-link:hover,
	.cabinet .ui-state-active .new.cabinet-tab-link,
	.cabinet .ui-state-active .new.cabinet-tab-link:hover {
		border-bottom-color: white;
		box-shadow: none;
		color: #3e454c
	}
	.cabinet .order-page-tab-link,
	.cabinet .orderform-inside-tab-link {
		border-color: #e6e6e6;
		font-weight: bold
	}
	.cabinet .order-page-tab-link:hover,
	.cabinet .active .order-page-tab-link,
	.cabinet .active .order-page-tab-link:hover,
	.cabinet .active .new.order-page-tab-link,
	.cabinet .active .new.order-page-tab-link:hover,
	.cabinet .orderform-inside-tab-link:hover,
	.cabinet .ui-state-active .orderform-inside-tab-link,
	.cabinet .ui-state-active .orderform-inside-tab-link:hover {
		border-color: #e6e6e6;
		background: #fafafa none;
		background-image: -moz-linear-gradient(top, white 0%, #fafafa 100%);
		background-image: -webkit-linear-gradient(top, white 0%, #fafafa 100%);
		background-image: linear-gradient(to bottom, white 0%, #fafafa 100%)
	}
	.cabinet .order-page-tab-link:active,
	.cabinet .active .order-page-tab-link,
	.cabinet .active .order-page-tab-link:hover,
	.cabinet .active .new.order-page-tab-link,
	.cabinet .active .new.order-page-tab-link:hover,
	.cabinet .orderform-inside-tab-link:active,
	.cabinet .ui-state-active .orderform-inside-tab-link,
	.cabinet .ui-state-active .orderform-inside-tab-link:hover {
		border-bottom-color: #fafafa
	}
	.cabinet .active .cabinet-tab-link.messages-button,
	.cabinet .active .cabinet-tab-link.messages-button:hover {
		border-bottom-color: #f2f8fa;
		background-color: #f2f8fa;
		background-image: -moz-linear-gradient(top, white 0%, #f2f8fa 100%);
		background-image: -webkit-linear-gradient(top, white 0%, #f2f8fa 100%);
		background-image: linear-gradient(to bottom, white 0%, #f2f8fa 100%)
	}
	.cabinet .tab-new-order .cabinet-tab-link {
		background-image: -moz-linear-gradient(top, #f0ffe5 0%, #dbecd5 100%);
		background-image: -webkit-linear-gradient(top, #f0ffe5 0%, #dbecd5 100%);
		background-image: linear-gradient(to bottom, #f0ffe5 0%, #dbecd5 100%);
		color: #579e27
	}
	.cabinet .tab-new-order .cabinet-tab-link:hover,
	.cabinet .tab-new-order.active .cabinet-tab-link {
		background-image: -moz-linear-gradient(top, #eeffec 0%, white 100%);
		background-image: -webkit-linear-gradient(top, #eeffec 0%, white 100%);
		background-image: linear-gradient(to bottom, #eeffec 0%, white 100%);
		color: #39c347
	}
	.cabinet .cabinet-tab .new-count,
	.cabinet .cabinet-tab:hover .new-count {
		background-color: #3998ce;
		box-shadow: 0 1px 2px rgba(0, 48, 67, .3);
		color: white
	}
	.cabinet .tab-orders a:before {
		background-position: 0 0
	}
	.cabinet .tab-profile a:before {
		background-position: -28px 0
	}
	.cabinet .tab-discounts a:before {
		background-position: -57px 0
	}
	.cabinet .tab-feedback a:before {
		background-position: -87px 0
	}
	.cabinet .tab-new-order a:before {
		background-position: -114px 0
	}
	.cabinet .tab-orders a:hover:before {
		background-position: 0 -30px
	}
	.cabinet .tab-profile a:hover:before {
		background-position: -28px -30px
	}
	.cabinet .tab-discounts a:hover:before {
		background-position: -57px -30px
	}
	.cabinet .tab-feedback a:hover:before {
		background-position: -87px -30px
	}
	.cabinet .tab-new-order a:hover:before {
		background-position: -114px -30px
	}
	.cabinet .active.tab-orders a:before {
		background-position: 0 -60px
	}
	.cabinet .active.tab-profile a:before {
		background-position: -28px -60px
	}
	.cabinet .active.tab-discounts a:before {
		background-position: -57px -60px
	}
	.cabinet .active.tab-feedback a:before {
		background-position: -87px -60px
	}
	.cabinet .active.tab-new-order a:before {
		background-position: -114px -60px
	}
	.cabinet .tab-plagcheck a:before {
		display: none
	}
	.cabinet .new-count {
		margin-left: 3px;
		padding: 1px 4px;
		border-radius: 3px;
		background-color: white;
		box-shadow: 0 1px 2px rgba(0, 48, 67, .5);
		color: #085783;
		vertical-align: 1px;
		text-shadow: none;
		font: 12px/13px 'Arial', sans-serif;
		font-size: 12px
	}
	.cabinet .active .new-count,
	.cabinet .active:hover .new-count,
	.cabinet .active:active .new-count {
		background-color: #e7ebee;
		box-shadow: none;
		color: #41484f
	}
	.cabinet .new-count:after {
		content: " <?= __("new",[],"cabinet/orders","orders_new");?>";
		vertical-align: 1px;
		font-size: 10px
	}
	.cabinet .order-page-tab-link:before,
	.cabinet .messages-button:before,
	.cabinet .files-button:before,
	.cabinet .files-up-button:before,
	.fileupload-buttons .files-up-button:before,
	.cabinet .files-dl-button:before,
	.cabinet .pay-button:before,
	.cabinet .delete-button:before,
	.cabinet .cancel-button:before,
	.cabinet .revision-button:before,
	.cabinet .writer-button:before,
	.cabinet .support-button:before,
	.cabinet .approve-button:before,
	.cabinet .refund-button:before,
	.cabinet .reorder-button:before {
		display: inline-block;
		margin: 0 8px 0 0;
		width: 18px;
		height: 16px;
		background-image: url(/css/common/cs/images/cabinet_spr.png);
		background-position: 100% 0;
		background-repeat: no-repeat;
		content: " ";
		vertical-align: top
	}
	.cabinet .messages-button:before {
		background-position: -17px -90px
	}
	.cabinet .files-button:before {
		background-position: -34px -90px
	}
	.cabinet .files-up-button:before,
	.fileupload-buttons .files-up-button:before {
		background-position: -51px -90px
	}
	.cabinet .files-dl-button:before {
		background-position: -68px -90px
	}
	.cabinet .pay-button:before {
		background-position: -86px -90px
	}
	.cabinet .delete-button:before {
		background-position: -104px -122px
	}
	.cabinet .cancel-button:before {
		background-position: -122px -122px
	}
	.cabinet .revision-button:before {
		background-position: -122px -90px
	}
	.cabinet .writer-button:before {
		width: 21px;
		background-position: -140px -90px
	}
	.cabinet .support-button:before {
		background-position: -161px -90px
	}
	.cabinet .reorder-button:before {
		background-position: -140px 0
	}
	.cabinet .approve-button:before {
		background-position: -106px -138px
	}
	.cabinet .refund-button:before {
		background-position: -156px 0
	}
	.cabinet .messages-button:hover:before {
		background-position: -17px -106px
	}
	.cabinet .files-button:hover:before {
		background-position: -34px -106px
	}
	.cabinet .files-up-button:hover:before,
	.fileupload-buttons .files-up-button:hover:before {
		background-position: -51px -106px
	}
	.cabinet .files-dl-button:hover:before {
		background-position: -68px -106px
	}
	.cabinet .pay-button:hover:before {
		background-position: -86px -106px
	}
	.cabinet .delete-button:hover:before {
		background-position: -104px -106px
	}
	.cabinet .cancel-button:hover:before {
		background-position: -122px -138px
	}
	.cabinet .revision-button:hover:before {
		background-position: -122px -106px
	}
	.cabinet .writer-button:hover:before {
		width: 21px;
		background-position: -140px -106px
	}
	.cabinet .support-button:hover:before {
		background-position: -161px -106px
	}
	.cabinet .reorder-button:hover:before {
		background-position: -140px -16px
	}
	.cabinet .approve-button:hover:before {
		background-position: -106px -154px
	}
	.cabinet .refund-button:hover:before {
		background-position: -156px -16px
	}
	.cabinet .highlight.messages-button:before {
		background-position: 0 -138px
	}
	.cabinet .highlight.files-button:before {
		background-position: -34px -138px
	}
	.cabinet .highlight.files-up-button:before,
	.fileupload-buttons .highlight.files-up-button:before {
		background-position: -51px -138px
	}
	.cabinet .highlight.files-dl-button:before {
		background-position: -68px -138px
	}
	.cabinet .highlight.pay-button:before {
		background-position: -86px -138px
	}
	.cabinet .highlight.messages-button:hover:before {
		background-position: -17px -154px
	}
	.cabinet .highlight.files-button:hover:before {
		background-position: -34px -154px
	}
	.cabinet .highlight.files-up-button:hover:before,
	.fileupload-buttons .highlight.files-up-button:hover:before {
		background-position: -51px -154px
	}
	.cabinet .highlight.files-dl-button:hover:before {
		background-position: -68px -154px
	}
	.cabinet .highlight.pay-button:hover:before {
		background-position: -86px -154px
	}
	.cabinet .light.delete-button:before {
		background-position: -104px -90px
	}
	.cabinet .light.delete-button:hover:before {
		background-position: -104px -106px
	}
	.active .new.messages-button:before,
	.active .new.messages-button:hover:before {
		background-position: 0 -122px
	}
	.cabinet .active .messages-button:before,
	.cabinet .active .messages-button:hover:before {
		background-position: -17px -122px
	}
	.cabinet .active .files-button:before,
	.cabinet .active .files-button:hover:before {
		background-position: -34px -122px
	}
	.cabinet .active .files-up-button:before,
	.cabinet .active .files-up-button:hover:before {
		background-position: -51px -122px
	}
	.cabinet .active .files-dl-button:before,
	.cabinet .active .files-dl-button:hover:before {
		background-position: -68px -122px
	}
	.cabinet .active .pay-button:before,
	.cabinet .active .pay-button:hover:before {
		background-position: -86px -122px
	}
	.cabinet .active .delete-button:before,
	.cabinet .active .delete-button:hover:before {
		background-position: -104px -122px
	}
	.cabinet .active .revision-button:before,
	.cabinet .active .revision-button:hover:before {
		background-position: -122px -122px
	}
	.cabinet .active .writer-button:before,
	.cabinet .active .writer-button:hover:before {
		width: 21px;
		background-position: -140px -122px
	}
	.cabinet .active .support-button:before,
	.cabinet .active .support-button:hover:before {
		background-position: -161px -122px
	}
	.cabinet .cabinet .cancel-button:hover,
	.cabinet .cabinet .delete-button:hover,
	.cabinet .cabinet .light.delete-button:hover,
	.cabinet .cabinet .revision-button:hover {
		border-bottom-width: 0
	}
	.cabinet .ui-button.cancel-button:hover,
	.cabinet .sample-reject.cancel-button,
	.cabinet .sample-cancel.cancel-button {
		border-bottom-width: 1px
	}
	.cabinet .cancel-button,
	.cabinet .delete-button {
		color: #cd5148
	}
	.cabinet .light.delete-button {
		color: #aaa3a3
	}
	.cabinet .cancel-button:hover,
	.cabinet .delete-button:hover,
	.cabinet .light.delete-button:hover {
		color: #b22119
	}
	.cabinet .revision-button {
		color: #1471a7
	}
	.cabinet .revision-button:hover {
		color: #198cce
	}
	.cabinet .order-page-tab-link:before {
		position: relative;
		top: 2px;
		display: none
	}
	.cabinet .pic-tab.order-page-tab-link:before {
		display: inline-block
	}
	.cabinet .orderform-inside-tab-link:before {
		display: none
	}
	.cabinet .radios {
		display: table;
		margin-bottom: 13px;
		width: 100%
	}
	.cabinet .radios .ui-button {
		display: table-cell;
		-moz-box-sizing: content-box;
		box-sizing: content-box;
		margin: 0;
		padding: 9px 7px;
		height: 18px;
		border: 1px solid #d0d0d0;
		border-width: 1px 0 1px 1px;
		border-radius: 0;
		box-shadow: none;
		font-size: 13px
	}
	.cabinet .ui-button:active,
	.cabinet .ui-button.ui-state-active {
		padding-top: 10px;
		padding-bottom: 8px;
		background-color: #e0e9ed;
		background-image: -webkit-linear-gradient(top, #eceeee, #ecf1f1);
		background-image: -moz-linear-gradient(top, #eceeee, #ecf1f1);
		background-image: linear-gradient(to bottom, #eceeee, #ecf1f1);
		background-position: 0 0;
		box-shadow: inset 0 3px 5px rgba(0, 70, 106, .28), inset 0 1px 3px rgba(0, 50, 76, .05);
		text-shadow: 0 1px 0 white
	}
	.cabinet .radios .ui-corner-right {
		border-width: 1px;
		border-radius: 0 3px 3px 0
	}
	#tip_radio_academic_level_1_clone,
	#tip_radio_academic_level_1,
	.cabinet .radios .ui-corner-left {
		border-radius: 3px 0 0 3px
	}
	.cabinet .radios .ui-corner-left {
		border-top-left-radius: 3px;
		border-bottom-left-radius: 3px
	}
	.cabinet .radios.form-box-pay .ui-button:last-of-type {
		border-top-right-radius: 3px;
		border-bottom-right-radius: 3px
	}
	.cabinet .radios .ui-button .ui-button-text {
		padding: 0
	}
	.cabinet .ui-helper-hidden {
		display: none
	}
	.cabinet .ui-helper-hidden-accessible {
		display: none
	}
	.ui-helper-hidden-accessible {
		display: none
	}
	.cabinet .ui-helper-zfix {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		opacity: 0;
		filter: Alpha(Opacity=0)
	}
	.cabinet input[type="checkbox"],
	#addmaterials_dialog_complete {
		display: none
	}
	.ie8 input[type="checkbox"] {
		position: absolute;
		left: -9999px;
		display: inline-block;
		width: 0;
		height: 0;
		-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
		filter: alpha(opacity=0)
	}
	.cabinet input[type="checkbox"] + label,
	.cabinet input[type="checkbox"] + label.large,
	.cabinet input[type="checkbox"]+label.smaller,
	#addmaterials_dialog_complete+label.large {
		position: relative;
		padding-left: 28px;
		cursor: pointer
	}
	.cabinet input[type="checkbox"][disabled] + label,
	.cabinet input[type="checkbox"][disabled]+label:hover,
	#addmaterials_dialog_complete[disabled]+label,
	#addmaterials_dialog_complete[disabled]+label:hover {
		cursor: not-allowed
	}
	.cabinet input[type="checkbox"]+label:before,
	#addmaterials_dialog_complete+label:before {
		position: absolute;
		top: 49%;
		left: 0;
		display: block;
		margin-top: -9px;
		width: 18px;
		height: 18px;
		content: " "
	}
	.cabinet input[type="checkbox"]+label:hover:before,
	#addmaterials_dialog_complete+label:hover:before {
		background-color: #f5fbff
	}
	.cabinet input[type="checkbox"]+label:after,
	#addmaterials_dialog_complete+label:after {
		background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo5QzlEQjI0NzNGQzMxMUUzOEQ1NTg4QzVBRTNEN0EzOCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo5QzlEQjI0ODNGQzMxMUUzOEQ1NTg4QzVBRTNEN0EzOCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjlDOURCMjQ1M0ZDMzExRTM4RDU1ODhDNUFFM0Q3QTM4IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjlDOURCMjQ2M0ZDMzExRTM4RDU1ODhDNUFFM0Q3QTM4Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+ZT0f2gAAAZxJREFUeNpi/P//PwMlILeh2w5IWbFQYAAHkJoAxJ5A7M5CpiGiQGoLEF9hZWHR6Ksp/M5IqteAhkgAqQOMDAzbJzWUFsLEWUg0hB9I7WJiZLzGy8NThCxHtIuAhjADqZ1MjEwafLzc2s1FGR+R5UlxUQcjA6Mz0BA/dEOIdhHQNYFAai0fD8+y1pLMGGxqmN8ycAdsP3AsE4iPejlY/8IRuNu5OTmZ2NnZ/B0sjD9hM4gJiCWBOAWIjwI1KWJRM5ONlVWEg4N9QkN+6hNcrmaa3FA6nZuL0w3ofzkg/zDQMBUk14QCA9ePh5vrHZDbic/78DCq7Zvh8PnL1z1///27D+RaAvEPIL7By80tzcbGWgd0TTM+g5hgDGBMHODj5algZmICuWgOEJeys7GBDPkCtGwyoQhhQuEwMfUBDTsCTHD+QK9WcXGCshPDnMaCtA8kGQR0/j+gYRncXFx/ODnYWYDsf0DXTCUmkWFNRw0TZ88AUulAvBlouB8xBjHhEK8DYlB6mU1sssdqENAVr4BU9e/fv7cTaxBAgAEAajCA6d8TjLsAAAAASUVORK5CYII=');
		position: absolute;
		top: 49%;
		left: 0;
		display: none;
		margin: -11px 0 0 4px;
		width: 18px;
		height: 18px;
		content: " "
	}
	.cabinet input[type="checkbox"]:checked+label:after,
	input[type="checkbox"].checked+label:after,
	#addmaterials_dialog_complete:checked+label:after,
	#addmaterials_dialog_complete.checked+label:after {
		display: block
	}
	.cabinet input[type="checkbox"]+label.small {
		font-size: 13px
	}
	.cabinet input[type="checkbox"]+label.large,
	#addmaterials_dialog_complete+label.large {
		font-size: 16px
	}
	.cabinet input[type="checkbox"],
	#addmaterials_dialog_complete {
		width: 18px \0;
		height: 18px \0;
		vertical-align: middle \0;
		display: inline \0
	}
	.cabinet input[type="checkbox"]+label.no-padding {
		margin: -4px 0 0;
		padding: 0 0 0 28px
	}
	#request_writer {
		margin: -3px 0 0
	}
	#box_defined_writer {
		padding-bottom: 10px;
		vertical-align: middle;
		font-size: 13px
	}
	.cabinet .field_tip.definder_writer_tip {
		display: none;
		margin-top: 5px
	}
	.cabinet #defined_writer:checked~.definder_writer_tip {
		display: block
	}
	.cabinet input.uvo_pref-uvoAutocomplete-input {
		margin-bottom: 0;
		cursor: pointer;
		padding-right: 30px;
		text-overflow: ellipsis
	}
	.cabinet .uvo_pref-uvoAutocomplete-wrapper {
		position: relative;
		margin-bottom: 13px;
		text-align: left
	}
	.cabinet .uvo_pref-uvoAutocomplete-wrapper.ui-state-active,
	.cabinet .uvo_pref-uvoAutocomplete-wrapper:hover {
		z-index: 3
	}
	.cabinet .ui-icon-triangle-1-s {
		margin-top: -2px;
		width: 0;
		height: 0;
		border-width: 4px 4px 0;
		border-style: solid;
		border-color: #8f9ea4 transparent transparent
	}
	.cabinet .ui-icon-triangle-1-s:before {
		position: absolute;
		top: -15px;
		left: -14px;
		display: block;
		width: 1px;
		height: 26px;
		background-color: #f0f1f1;
		content: " "
	}
	.cabinet .uvo_pref-uvoAutocomplete-wrapper:hover .ui-icon-triangle-1-s {
		border-top-color: #4ca5ed
	}
	.cabinet .ui-state-active .ui-icon-triangle-1-s {
		z-index: 11;
		border-width: 0 4px 5px;
		border-color: transparent transparent #c28480
	}
	.cabinet .uvo_pref-uvoAutocomplete-wrapper.ui-state-active .uvo_pref-uvoAutocomplete-input {
		border: 1px solid #ccc;
		border-radius: 3px 3px 0 0;
		box-shadow: 0 3px 10px rgba(15, 30, 36, .2)
	}
	.cabinet .dr-down {
		position: absolute;
		z-index: 3;
		overflow: hidden;
		overflow: auto;
		overflow-x: hidden;
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		max-height: 263px;
		width: 100%;
		border: 1px solid #d0d0d0;
		border-top-width: 0;
		border-radius: 0 0 3px 3px;
		background: white;
		box-shadow: 0 3px 10px rgba(15, 30, 36, .2)
	}
	.cabinet .dr-down-search {
		position: relative;
		display: block;
		border: 4px solid #f0f3f4
	}
	.cabinet .dr-down-search .dr-down-zoom {
		position: absolute;
		background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAYAAAAfSC3RAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo5QzlEQjI0QjNGQzMxMUUzOEQ1NTg4QzVBRTNEN0EzOCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo5QzlEQjI0QzNGQzMxMUUzOEQ1NTg4QzVBRTNEN0EzOCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjlDOURCMjQ5M0ZDMzExRTM4RDU1ODhDNUFFM0Q3QTM4IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjlDOURCMjRBM0ZDMzExRTM4RDU1ODhDNUFFM0Q3QTM4Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+MziO3gAAANhJREFUeNqM0k0KglAUBWANN9BWojYhFQVNGhntoIYRVBANk+ZhDoJGRfa3h6ilBDVu1LlwHsjzVh74SHte7/OqG0Zrh6lBDyo8v8EcTo6SAn+nkMCLxeIJB65l4kEVBtCFOLW2hA5EcIWj3bHPbrFyY/lvzx1ktlqGs/M9F16jPuO/uFrhHfwfRT4nnCkMoQGBUhRwLdSmKtOawQrqfCbTqcltlrSpSoa8qAgLkuMW7GAMI7ujSUJ25MvZsFgyyTvVN7Ttzl7O12GKTedH3veYLpYPf/sRYABhxi4fqLOUgwAAAABJRU5ErkJggg==');
		top: 50%;
		left: 8px;
		display: block;
		margin-top: -7px;
		width: 14px;
		height: 14px;
		background-repeat: no-repeat;
		content: " "
	}
	.cabinet .dr-down-search input.dr-down-input {
		margin-bottom: 0;
		padding-left: 26px;
		border-color: #e4e8e9;
		border-radius: 0
	}
	.cabinet .dr-down-search input.dr-down-input:focus {
		outline: 0;
		outline: thin dotted \9;
		border-color: rgba(82, 168, 236, .6);
		box-shadow: inset 0 2px 2px rgba(0, 44, 60, .07), 0 0 4px rgba(82, 168, 236, .3)
	}
	.cabinet .dr-down-list {}.cabinet .dr-down-list a {
		display: block;
		padding: 7px 10px;
		border-top: 1px solid #e7e7e7;
		color: #58696e;
		-webkit-transition: none;
		-moz-transition: none;
		transition: none
	}
	.cabinet .dr-down-list a.uvo_pref-uvoAutocomplete-default-option {
		display: none
	}
	.cabinet #box_defined_writer .dr-down-list a.uvo_pref-uvoAutocomplete-default-option {
		display: block
	}
	.cabinet .dr-down-list a:hover {
		background-color: #53afe8;
		color: white;
		text-decoration: none
	}
	.cabinet .uvo_pref-uvoAutocomplete-hightlight {
		color: black
	}
	.cabinet .dr-down-list .ui-state-disabled,
	.cabinet .dr-down-list .ui-state-disabled:hover {
		border-bottom: #e7e7e7;
		background-color: #fff;
		color: #ccc
	}
	.cabinet .contentspin {
		display: inline-block;
		white-space: nowrap
	}
	.cabinet .buttonspin,
	.cabinet .contentspin input {
		border-radius: 0
	}
	.cabinet .contentspin input {
		position: relative;
		z-index: 1;
		width: 3.5em;
		text-align: center
	}
	.cabinet .buttonspin {
		position: relative;
		width: 36px;
		text-indent: -9999px
	}
	.ie8 .buttonspin:active,
	.ie8 .buttonspin:focus {
		background-color: #e0e9ed
	}
	.cabinet .buttonspin:after {
		position: absolute;
		top: 50%;
		left: 50%;
		display: inline-block;
		margin-top: -7px;
		margin-left: -7px;
		width: 14px;
		height: 14px;
		background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAyCAYAAABs8OoHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpCQjJDQTdGOTQwQUUxMUUzOTBEM0MxMkU0MEM0NTE0MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpCQjJDQTdGQTQwQUUxMUUzOTBEM0MxMkU0MEM0NTE0MiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkJCMkNBN0Y3NDBBRTExRTM5MEQzQzEyRTQwQzQ1MTQyIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkJCMkNBN0Y4NDBBRTExRTM5MEQzQzEyRTQwQzQ1MTQyIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+k33MQgAAAMBJREFUeNpiTKtuZ8AB/kNpRmySTLh0SYoKgzEugFMjITCENLIghR6h0EXViC/kYKGLy0ZGcuKR5fnrt3htwiU/lKLj/afPeP2IS54Fl4nX7j7wI5QAsAItZYXPUANGdO7wxSEHiwes8iyw0CMUuthsPIBDjwOUxirPgiuegDYxjMYjbnCALI24QhMGGP///z9UAie9poNmdQcDWXUHPhsZydKIq24gBIZQPI5qpEnuCMurpnrdgV8joboDFwAIMABRJTa3ankougAAAABJRU5ErkJggg==');
		background-repeat: no-repeat;
		content: " "
	}
	.cabinet .buttonspin:active:after {
		margin-top: -6px
	}
	.cabinet .dec.buttonspin {
		margin-right: -1px;
		border-radius: 3px 0 0 3px
	}
	.cabinet .dec.buttonspin:after {
		background-position: 0 -28px
	}
	.cabinet .dec.buttonspin.disabled:after {
		background-position: 0 -32px
	}
	.cabinet .inc.buttonspin {
		margin-left: -1px;
		border-radius: 0 3px 3px 0
	}
	.cabinet .inc.buttonspin:after {
		background-position: 0 0
	}
	.cabinet .inc.buttonspin.disabled:after {
		background-position: 0 -14px
	}
	.cabinet .dec.buttonspin.ui-state-disabled:after,
	.cabinet .inc.buttonspin.ui-state-disabled:after {
		opacity: .4
	}
	.ui-tooltip,
	.qtip {
		position: absolute;
		z-index: 50;
		display: none
	}
	.qtip {
		padding: 4px 5px;
		min-width: 50px;
		max-width: 170px;
		border: 1px solid #d1cdc5;
		border-radius: 3px 3px 3px 2px;
		background-color: #fcfaf3;
		box-shadow: 0 1px 5px rgba(45, 31, 0, .1);
		color: #a79d8b;
		font: 11px/14px 'Arial', sans-serif
	}
	.tip-progressibe-delivery-unavailable {
		font-weight: bold;
		color: #86505A
	}
	.qtip-pos-bl {
		margin-left: -5px
	}
	.qtip-pos-bl:before,
	.qtip-pos-bl:after {
		position: absolute;
		bottom: -7px;
		left: 0;
		margin-left: 2px;
		width: 0;
		height: 0;
		border-width: 6px 6px 0;
		border-style: solid;
		border-color: #c8c2b6 transparent transparent;
		content: " ";
		text-align: center;
		-moz-transform: scale(.999)
	}
	.qtip-pos-bl:after {
		bottom: -6px;
		border-top-color: #fcfaf3
	}
	.qtip-pos-tl {
		margin-top: -6px;
		margin-left: 10px;
		padding: 10px;
		width: 170px;
		border: 1px solid #d4cbb8;
		background-color: #fffbea;
		box-shadow: 0 2px 7px rgba(45, 31, 0, .2);
		color: #9d9387;
		font: 12px/16px 'Arial', sans-serif
	}
	.qtip-pos-tl:before,
	.qtip-pos-tl:after {
		position: absolute;
		top: 10px;
		left: -9px;
		width: 0;
		height: 0;
		border-width: 8px 8px 8px 0;
		border-style: solid;
		border-color: transparent #d4cbb8 transparent transparent;
		content: " ";
		-moz-transform: scale(.999)
	}
	.qtip-pos-tl:after {
		left: -8px;
		border-right-color: #fffbea
	}
	.qtip-pos-bc:before,
	.qtip-pos-bc:after {
		position: absolute;
		bottom: -7px;
		left: 50%;
		margin-left: -6px;
		width: 0;
		height: 0;
		border-width: 6px 6px 0;
		border-style: solid;
		border-color: #c8c2b6 transparent transparent;
		content: " ";
		text-align: center;
		-moz-transform: scale(.999)
	}
	.qtip-pos-bc:after {
		bottom: -6px;
		border-top-color: #fcfaf3
	}
	.qtip-tip {
		display: none !important
	}
	#qtip-26 {
		margin-top: -10px
	}
	#tip_progressive_delivery {
		min-height: 36px
	}
	@media (max-width: 800px) {
		#tip_progressive_delivery {
			min-height: 18px
		}
	}
	.cabinet #footer {
		clear: both;
		overflow: hidden;
		padding: 0 20px;
		height: 42px;
		background-color: #3e454c;
		color: #8e8e8e;
		font-size: 12px
	}
	.cabinet #footer a {
		color: #8e8e8e
	}
	.cabinet #footer a:hover {
		border-color: #5f6367;
		color: #aeaeae
	}
	.cabinet .footer-menu {
		float: right;
		margin: 10px 0
	}
	.cabinet .footer-menu li {
		display: inline;
		padding-left: 20px
	}
	.cabinet .footer-copyright {
		float: left;
		margin: 10px 0
	}
	.cabinet #back-top {
		position: fixed;
		right: 10px;
		bottom: 0;
		display: block;
		padding: 23px 0 7px;
		width: 50px;
		height: 20px;
		border-bottom-width: 0;
		border-radius: 50%;
		background-color: white;
		color: #006db2;
		text-align: center;
		font: bold 12px/20px sans-serif;
		opacity: .4;
		-webkit-transition: all 200ms ease-out;
		-moz-transition: all 200ms ease-out;
		transition: all 200ms ease-out
	}
	.cabinet #back-top:after {
		position: absolute;
		top: 8px;
		left: 50%;
		margin-left: -10px;
		width: 0;
		height: 0;
		border-width: 0 10px 12px;
		border-style: solid;
		border-color: transparent transparent #006db2;
		content: " ";
		-moz-transform: scale(.95)
	}
	.cabinet #back-top:hover {
		border-bottom-right-radius: 0;
		border-bottom-left-radius: 0;
		box-shadow: 0 1px 3px rgba(0, 45, 73, .2);
		color: #239ae5;
		opacity: 1
	}
	.cabinet #back-top:hover:after {
		border-bottom-color: #239ae5
	}
	.cabinet .order {
		position: relative;
		margin-bottom: 18px;
		padding-top: 15px;
		border: 1px solid #e6e6e6;
		border-color: rgba(0, 0, 0, .08);
		border-radius: 3px;
		background-color: #fafafa
	}
	.cabinet .order .empty-message {
		text-align: center
	}
	.cabinet .order .button,
	.cabinet .order .input-model {
		margin-bottom: 0
	}
	.cabinet .order-title,
	.cabinet .order-description,
	.cabinet .order-status {
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		padding: 0 15px
	}
	.cabinet .order-title {
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
		font-weight: bold
	}
	.cabinet .order-title a {
		font-weight: bold
	}
	.cabinet .order-title,
	.cabinet .order-description {
		width: 42%
	}
	.cabinet .order-status {
		width: 57%
	}
	.cabinet .order-description {
		float: left;
		margin-bottom: 5px;
		font-size: 12px
	}
	.cabinet .order-general-info {
		display: block;
		overflow: hidden;
		padding-top: 10px;
		text-overflow: ellipsis;
		white-space: nowrap;
		line-height: 150%
	}
	.cabinet .order-deadline {
		font-weight: normal;
		font-style: italic
	}
	.cabinet .time-remaining {
		white-space: nowrap
	}
	.cabinet .order-delete-n-cost {
		position: absolute;
		right: 15px;
		bottom: 12px
	}
	.cabinet .order-delete-n-cost .input-model,
	.cabinet .order-total-price,
	.cabinet .order-discount {
		display: inline-block;
		vertical-align: bottom
	}
	.cabinet .order-delete-n-cost .input-model {
		margin-bottom: -7px
	}
	.cabinet .order-total-price {
		font-size: 20px
	}
	.cabinet .order-total-price:before {
		margin-right: 2px;
		content: "$";
		font-size: 17px
	}
	.cabinet .order-discount {
		position: relative;
		margin: 0 0 -1px 14px;
		vertical-align: top;
		font-size: 11px;
		line-height: 12px
	}
	.cabinet .order-discount:before,
	.cabinet .order-discount:after {
		position: absolute;
		top: 50%;
		left: -9px;
		display: block;
		margin-top: -9px;
		width: 0;
		width: 0;
		height: 0;
		border-width: 0 1px 12px;
		border-style: solid;
		border-color: transparent transparent #838383;
		content: " ";
		-webkit-transform: rotate(47deg) scale(.95);
		-moz-transform: rotate(47deg) scale(.95);
		-ms-transform: rotate(47deg) scale(.95);
		transform: rotate(47deg) scale(.95)
	}
	.cabinet .order-discount:before {
		margin-top: -2px;
		-webkit-transform: rotate(133deg) scale(.95);
		-moz-transform: rotate(133deg) scale(.95);
		-ms-transform: rotate(133deg) scale(.95);
		transform: rotate(133deg) scale(.95)
	}
	.cabinet .ie8 .order-discount:after {
		display: none
	}
	.cabinet .ie8 .order-discount:before {
		top: 5px;
		left: -16px;
		margin: 0;
		width: 14px;
		height: 12px;
		border: 0;
		background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJhJREFUeNpiYCASWFpaGCDzGYnQoACk+oHYAYgdjx8/cQEkzkRAUwOQOg/EH4BYEaYJBFhwaACZPh+IHyDbgtOpUGeBNID8UwjUsACXa1igGgSAVAEQ1wPxBCAOBGr6gM8b6H78QGwoM+IJQbxOZSQicAoJBg6O6MgHYpDNjcj+xhuPQIUgjYZADPLCfahXSANQ58MBQIABAJieNGhjzaW4AAAAAElFTkSuQmCC) center center no-repeat
	}
	.cabinet .order-status {
		float: right;
		margin-top: -21px;
		font-size: 12px
	}
	.cabinet .order-status-bar {
		display: table;
		margin-bottom: 4px;
		width: 100%;
		border-radius: 6px
	}
	.cabinet .order-status-bar .segment {
		border-width: 1px 0;
		border-style: solid
	}
	.cabinet .order-status-bar .segment:first-child {
		border-left-width: 1px;
		border-top-left-radius: 3px;
		border-bottom-left-radius: 3px
	}
	.cabinet .order-status-bar .segment:last-child {
		border-right-width: 1px;
		border-top-right-radius: 3px;
		border-bottom-right-radius: 3px
	}
	.cabinet .finished-mark {
		display: inline-block;
		margin: 0 4px 0 -9px;
		width: 12px;
		height: 12px;
		background: transparent url(/css/common/cs/images/cabinet_spr.png) -125px -155px no-repeat;
		vertical-align: baseline
	}
	.cabinet .order-status-info {
		margin-bottom: 10px;
		font-size: 12px;
		line-height: 16px;
		margin-top: 7px
	}
	.cabinet .order-bottom {
		clear: both;
		padding: 7px 15px 10px;
		border-radius: 0 0 3px 3px
	}
	.cabinet .order-id {
		position: relative;
		margin-right: 20px;
		border-width: 0;
		font-size: 12px
	}
	.cabinet .order-bottom .order-id {
		vertical-align: -5px
	}
	.cabinet .order-id:before {
		display: inline;
		content: "#"
	}
	.cabinet .order-id:after {
		position: absolute;
		top: 50%;
		right: -12px;
		display: block;
		margin-top: -22px;
		width: 1px;
		height: 42px;
		background: #cfcfcf;
		background: -moz-linear-gradient(top, rgba(0, 0, 0, .03), rgba(0, 0, 0, .15));
		background: -webkit-linear-gradient(top, rgba(0, 0, 0, .03), rgba(0, 0, 0, .15));
		background: linear-gradient(to bottom, rgba(0, 0, 0, .03), rgba(0, 0, 0, .15));
		content: " "
	}
	.ie8 .order-id:after {
		margin-top: -19px;
		height: 38px
	}
	@-webkit-keyframes inprogress {
		from {
			background-position: 32px 0
		}
		to {
			background-position: 0 0
		}
	}
	@-moz-keyframes inprogress {
		from {
			background-position: 32px 0
		}
		to {
			background-position: 0 0
		}
	}
	@-ms-keyframes inprogress {
		from {
			background-position: 32px 0
		}
		to {
			background-position: 0 0
		}
	}
	@keyframes inprogress {
		from {
			background-position: 32px 0
		}
		to {
			background-position: 0 0
		}
	}
	.cabinet .order-status-bar {
		background-color: white;
		background-image: -webkit-linear-gradient(-58deg, black 25%, white 25%, white 50%, black 50%, black 75%, white 75%, white);
		background-image: -moz-linear-gradient(-58deg, black 25%, white 25%, white 50%, black 50%, black 75%, white 75%, white);
		background-image: linear-gradient(-58deg, black 25%, white 25%, white 50%, black 50%, black 75%, white 75%, white);
		background-size: 16px 30px;
		-webkit-animation: inprogress 2s linear infinite;
		-moz-animation: inprogress 2s linear infinite;
		-ms-animation: inprogress 2s linear infinite;
		animation: inprogress 2s linear infinite
	}
	.cabinet .finished .order-status-bar {
		background-image: none
	}
	.cabinet .segment {
		position: relative;
		display: table-cell;
		padding: 3px 10px 2px 20px;
		border-color: #cfcfcf;
		background-color: #ececec;
		color: #898989;
		text-align: center;
		text-shadow: 0 1px 0 rgba(255, 255, 255, .8);
		white-space: nowrap
	}
	.cabinet .segment:before,
	.cabinet .segment:after {
		position: relative;
		top: 0;
		z-index: 1;
		display: block;
		float: right;
		margin: -3px -18px -2px 0;
		width: 0;
		height: 0;
		border-width: 13px 0 13px 8px;
		border-style: solid;
		border-color: transparent transparent transparent rgba(0, 0, 0, .15);
		content: " ";
		-webkit-transform: scale(1.1);
		-moz-transform: scale(1.1);
		transform: scale(1.1)
	}
	.ie8 .segment:before {
		border-color: transparent transparent transparent #cfcfcf
	}
	.ie8 .segment:after {
		border-color: transparent transparent transparent #ececec
	}
	.cabinet .segment:after {
		right: 1px;
		z-index: 2;
		border-left-color: #ececec
	}
	.cabinet .segment:last-child:before,
	.cabinet .segment:last-child:after {
		display: none !important
	}
	.cabinet .order-not-paid .order-pay-segment,
	.cabinet .order-placed .order-assign-segment,
	.cabinet .in-progress .order-progress-segment,
	.cabinet .review .order-review-segment,
	.cabinet .in-review .order-review-segment,
	.cabinet .order-completed .order-approve-segment,
	.cabinet .order-dispute .order-approve-segment,
	.cabinet .approved .order-approve-segment,
	.cabinet .finished .segment,
	.cabinet .order-finished .segment {
		font-weight: bold
	}
	.cabinet .order-not-paid .single-segment,
	.cabinet .order-not-paid .order-pay-segment {
		border-color: #d5d1a4;
		background-color: #f2eec3;
		color: #ac7d00
	}
	.cabinet .order-placed .single-segment,
	.cabinet .order-placed .order-assign-segment {
		border-color: #a5cecf;
		background-color: #d1f3f4;
		color: #0e8b90
	}
	.cabinet .in-progress .single-segment,
	.cabinet .in-progress .order-progress-segment {
		border-color: #97c3c4;
		background-color: #c4e9f6;
		background-color: rgba(196, 233, 246, .97);
		color: #156384
	}
	.cabinet .review .single-segment,
	.cabinet .review .order-review-segment {
		border-color: #bdaad3;
		background-color: #decdf2;
		color: #75189c
	}
	.cabinet .in-review .single-segment,
	.cabinet .in-review .order-review-segment {
		border-color: #bdaad3;
		background-color: #eebdf0;
		background-color: rgba(238, 189, 240, .97);
		color: #8a028c
	}
	.cabinet .order-completed .single-segment,
	.cabinet .order-completed .order-approve-segment {
		border-color: #e6c092;
		background-color: #fbd4a5;
		color: #a97000
	}
	.cabinet .order-dispute .single-segment,
	.cabinet .order-dispute .order-approve-segment {
		border-color: #dfaa95;
		background-color: #fbc7b2;
		background-color: rgba(251, 199, 178, .97);
		color: #a73300
	}
	.cabinet .order-finished .single-segment,
	.cabinet .finished .single-segment,
	.cabinet .finished .order-cancelled-segment {
		border-color: transparent;
		background-color: #f3f3f3;
		color: #cf4545
	}
	.cabinet .order-not-paid .order-pay-segment:after {
		border-left-color: #f2eec3
	}
	.cabinet .order-placed .order-assign-segment:after {
		border-left-color: #d1f3f4
	}
	.cabinet .in-progress .order-progress-segment:after {
		border-left-color: #c4e9f6
	}
	.cabinet .review .order-review-segment:after {
		border-left-color: #decdf2
	}
	.cabinet .in-review .order-review-segment:after {
		border-left-color: #eebdf0
	}
	.cabinet .order-completed .order-approve-segment:after {
		border-left-color: #fbd4a5
	}
	.cabinet .order-dispute .order-approve-segment:after {
		border-left-color: #fbc7b2
	}
	.ie8 .order-not-paid .order-status-bar .single-segment {
		border-right: 1px solid #d5d1a4
	}
	.ie8 .order-placed .order-status-bar .single-segment {
		border-right: 1px solid #a5cecf
	}
	.ie8 .in-progress .order-status-bar .single-segment {
		border-right: 1px solid #97c3c4
	}
	.ie8 .review .order-status-bar .single-segment {
		border-right: 1px solid #bdaad3
	}
	.ie8 .in-review .order-status-bar .single-segment {
		border-right: 1px solid #bdaad3
	}
	.ie8 .order-dispute .order-status-bar .single-segment {
		border-right: 1px solid #dfaa95
	}
	.ie8 .order-completed .order-status-bar .single-segment {
		border-right: 1px solid #e6c092
	}
	.cabinet .finished .order-status-bar {
		display: block;
		float: right;
		width: auto
	}
	.cabinet .finished .order-status-bar .segment {
		padding-right: 20px
	}
	.cabinet .finished .order-status-info {
		clear: right;
		text-align: right
	}
	.cabinet .finished .order-status .button {
		float: right;
		margin: -1px 0 -1px 20px
	}
	.ie8 .finished .order-cancelled-segment:before,
	.ie8 .finished .order-cancelled-segment:after {
		display: none
	}
	.ie8 .finished .order-cancelled-segment {
		border-right: 1px solid transparent
	}
	.cabinet .single-segment:before,
	.cabinet .single-segment:after {
		display: none
	}
	.cabinet .order-header {
		overflow: hidden
	}
	.cabinet .order-header-title {
		overflow: hidden;
		margin-top: 5px;
		text-overflow: ellipsis;
		white-space: nowrap;
		font-weight: bold;
		margin-top: 5px;
		cursor: default
	}
	.cabinet .order-page-id {
		float: right;
		padding: 21px 10px 21px 30px;
		white-space: nowrap
	}
	.cabinet .order-header-status {
		clear: both;
		margin-bottom: 21px;
		border-radius: 4px
	}
	.cabinet .order-header-status-bar {
		float: left;
		margin: 2px 20px 2px 2px;
		width: 200px;
		border-width: 0
	}
	.cabinet .order-header-deadline {
		float: right;
		padding: 6px 10px 5px 20px;
		font-size: 12px
	}
	.cabinet .order-header-status-info {
		overflow: hidden;
		overflow: hidden;
		padding: 6px 0 5px;
		text-overflow: ellipsis;
		white-space: nowrap;
		font-size: 12px;
		cursor: default
	}
	.cabinet .order-actions .button,
	.cabinet .order-actions .input-model {
		float: right;
		margin: 13px 0 0 5px
	}
	.cabinet .order-content {
		clear: both;
		padding: 0 10px;
		border: 1px solid #e6e6e6;
		border-radius: 3px;
		border-top-left-radius: 0;
		background-color: #fafafa
	}
	.order-content-section {
		min-height: 100px;
		min-width: 100%
	}
	.cabinet .additional-files {
		padding: 21px 0 0;
		border-top: 1px solid #e6e6e6
	}
	.cabinet .order-review-pay {
		margin-top: 10px;
		text-align: center;
		margin-right: 500px
	}
	.cabinet .order-review-pay-button,
	.cabinet .order-review-pay-button:active {
		margin: 0;
		height: auto;
		padding: 15px 20px;
		font-size: 15px;
		font-weight: bold;
		line-height: 15px
	}
	.cabinet .section-order-info {
		display: table;
		margin: 0 -10px
	}
	.cabinet .order-info {
		padding: 21px 0 0
	}
	.cabinet .order-tabs-title,
	.cabinet .order-files-title {
		margin-bottom: 15px;
		padding: 21px 10px 10px;
		border-bottom: 1px solid #e7e7e7
	}
	.cabinet .order-info .row {
		padding: 0 10px;
		width: 100%
	}
	.cabinet .order-info .cell {
		border-bottom: 1px solid #e7e7e7;
		line-height: 1.3;
		transition: color .3s ease
	}
	.cabinet .order-info .row:last-child .cell {
		border-bottom-width: 0
	}
	.cabinet .order-info .additional-payment .cell,
	.cabinet .order-info .additional-payment:last-child .cell {
		border-width: 1px 0;
		border-color: #e1dfd2;
		background-color: #f4f2e4
	}
	.cabinet .order-info .cell-left {
		padding: 10px 10px 10px 20px;
		width: 400px;
		text-align: right
	}
	.cabinet .order-info .cell-right {
		padding: 10px 20px 10px 10px;
		width: 1000px;
		font-weight: bold
	}
	.cabinet .order-info .cell-right .discounted-price {
		text-align: left;
		text-decoration: line-through;
		font-weight: normal
	}
	.cabinet .order-info .cell-right .discounted-price:after {
		display: inline-block;
		padding-left: 5px;
		color: #008000;
		content: "FREE";
		font-weight: bold
	}
	.cabinet .order-info .cell-right .free-pages {
		margin-right: 0px;
		width: auto;
		color: #008000;
		text-align: left
	}
	.cabinet .order-info .cell-right .free-pages-price {
		width: auto;
		text-align: left;
		text-decoration: line-through;
		font-weight: normal
	}
	.cabinet .order-info .remove {
		float: right;
		color: #b37772;
		font-size: 11px;
		line-height: 16px;
		transition: color .3s ease, border-color .4s ease
	}
	.cabinet .order-info .remove:hover {
		border-bottom: 1px solid rgba(178, 33, 25, .6);
		color: #b22119
	}
	.cabinet .order-info .hide-text .cell,
	.cabinet .order-info .hide-text .remove {
		color: transparent
	}
	.cabinet .order-info-right {
		padding: 7px 30px 0;
		width: 400px;
		border-left: 1px solid #e7e7e7;
		background-color: white;
		position: relative
	}
	.cabinet .prog-delivery .denied {
		color: #BF5050;
		font-size: 16px
	}
	.cabinet .prog-delivery .accepted {
		color: #7cb149;
		font-size: 16px
	}
	.cabinet .prog-delivery-buttons {
		max-width: 16em
	}
	.cabinet .block-thank {
		margin: 21px -20px;
		padding: 21px 20px;
		background-color: #f9f8f2;
		font-size: 13px;
		line-height: 1.3
	}
	.cabinet .order-info-right label {
		margin-right: 10px;
		font-size: 15px
	}
	.cabinet .order-info-right .td {
		margin-bottom: 21px
	}
	.cabinet .order-info-right input[type="radio"] {
		margin-right: 3px
	}
	.cabinet .prog-delivery-part {
		display: block;
		margin: 30px 0
	}
	.cabinet .prog-delivery-part:first-child {
		margin-top: 21px
	}
	.cabinet .approved.prog-delivery-part:after {
		color: #7cb149;
		content: "Approved"
	}
	.cabinet .part-title {
		position: relative
	}
	.cabinet .approved .part-title:before {
		position: absolute;
		top: 50%;
		left: -20px;
		display: block;
		margin-top: -10px;
		width: 18px;
		height: 18px;
		background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpFRUZFMEM3RDQyRkUxMUUzQTRDNkFBMTA5NEM3OERBMCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpFRUZFMEM3RTQyRkUxMUUzQTRDNkFBMTA5NEM3OERBMCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkVFRkUwQzdCNDJGRTExRTNBNEM2QUExMDk0Qzc4REEwIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkVFRkUwQzdDNDJGRTExRTNBNEM2QUExMDk0Qzc4REEwIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+VeKh6wAAASxJREFUeNqk0j9Lw0AYx/HaKjhUCqKgS6FYaV+DQxZBkaK2Q6GOooOzLgXFWReNg4haR0cHkVLoJtK+A4uDg4OTQ2mlaAZRvwdPIIYk5s/BB5K78Mvd89zQ3u1SLOLQMDccIWAUOtROFuMhQyZxjxHk8RQmaAoPaGMDn2oyaFAKTXSwbV0IEpTADcaxjh/rYpBiH2AeK+jbF/3uqIQdXOPO6QMVVMQJxjyKewEDVbc/qaBpbKKFjMM355iQO/PqFXSGBaSlrVnLellq0sWh19nNGrXkiOoYDdlBEseyrjsV+E9LtbVZ8/kFH6gghxksYyBzhleQvf1HWBUFmauh919b7e3/xha+5Cfq/dTP/XC6R4+4kuc6nsMGqbGPd1z6vfZuQW/YlQ76Gr8CDADsMDL/ol7t7QAAAABJRU5ErkJggg==');
		content: " "
	}
	.cabinet .part-title:after,
	.cabinet .part-pages:after,
	.cabinet .approved .part-deadline:after {
		color: #e7e7e7;
		content: " — "
	}
	.cabinet .part-pages-count,
	.cabinet .part-deadline-count {
		color: #81868b
	}
	.cabinet .part-pages-count {
		position: relative;
		padding: 3px 4px;
		border: 1px solid #c4c7c9;
		background-color: #fafafa;
		box-shadow: -1px -1px 0 #f4f4f1, -2px -2px 0 #dbdddd
	}
	.cabinet .part-pages-count:before {
		position: absolute;
		bottom: 1px;
		left: -2px;
		display: block;
		width: 1px;
		height: 1px;
		background-color: #dbdddd;
		content: " "
	}
	.cabinet .part-pages-count:after {
		position: absolute;
		right: -1px;
		bottom: -1px;
		display: block;
		width: 5px;
		height: 4px;
		background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAECAIAAADJUWIXAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpFRUZFMEM3OTQyRkUxMUUzQTRDNkFBMTA5NEM3OERBMCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpFRUZFMEM3QTQyRkUxMUUzQTRDNkFBMTA5NEM3OERBMCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkVFRkUwQzc3NDJGRTExRTNBNEM2QUExMDk0Qzc4REEwIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkVFRkUwQzc4NDJGRTExRTNBNEM2QUExMDk0Qzc4REEwIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+oWGz0wAAAEVJREFUeNpi/PHjx/v37zg5Of/+/fvp82fG9+/fv3z5QkhI4MuX74qKiowvX758/vwZHx8/kMMABIeOHr97795/GAAIMAAR0Ch6U51SXwAAAABJRU5ErkJggg==');
		content: " "
	}
	.cabinet .part-deadline-count {
		position: relative;
		padding: 2px 3px;
		border: 1px solid #c4c7c9;
		box-shadow: inset 0 0 1px #c4c7c9
	}
	.cabinet .part-deadline-count:before,
	.cabinet .part-deadline-count:after {
		position: absolute;
		top: -3px;
		left: 4px;
		display: block;
		width: 3px;
		height: 3px;
		border-width: 0 1px;
		border-style: solid;
		border-color: white;
		background-color: #c4c7c9;
		content: " "
	}
	.cabinet .part-deadline-count:after {
		right: 4px;
		left: auto
	}
	.cabinet .prog-delivery {
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		padding: 0 8px
	}
	.cabinet .prog-delivery-buttons {
		text-align: center;
		margin-left: auto;
		margin-right: auto
	}
	.cabinet .payment_system_btns {
		width: auto
	}
	.cabinet .payment_system_btns .ui-button:last-of-type {
		border-top-right-radius: 3px;
		border-bottom-right-radius: 3px
	}
	.cabinet .order-inforeorder {
		text-align: center;
		padding: 15px 20px
	}
	.cabinet .order-inforeorder .reorder-button {
		margin: 0
	}
	.cabinet .order-info-right.order-info-has-quest {
		display: none
	}
	.cabinet .add-services {
		overflow: hidden;
		margin: -5px;
		width: 450px
	}
	.cabinet .add-services:last-of-type {
		margin-bottom: 20px
	}
	.cabinet .add-services:after {
		display: table;
		clear: both;
		content: ' '
	}
	.cabinet .add-service {
		display: block;
		float: left;
		overflow: hidden;
		margin: 5px;
		padding: 0 10px;
		width: 120px;
		height: 125px;
		border-radius: 4px;
		color: #1e6b9c;
		text-align: center;
		font-size: 12px;
		line-height: 16px;
		transition: background-color .5s ease, color .5s ease, opacity .5s ease,
	}
	.cabinet .add-service:before {
		display: block;
		margin: 10px auto 5px;
		width: 86px;
		height: 68px;
		background-position: center center;
		background-size: 86px 68px;
		background-repeat: none;
		content: ' '
	}
	.cabinet .add-service[data-add-service="shorten"]:before {
		background-image: url('/img/common/add_icons_shorten.svg')
	}
	.cabinet .add-service[data-add-service="slide"]:before {
		background-image: url('/img/common/add_icons_slides.svg')
	}
	.cabinet .add-service[data-add-service="samples"]:before {
		background-image: url('/img/common/add_icons_samples.svg')
	}
	.cabinet .add-service[data-add-service="sources"]:before {
		background-image: url('/img/common/add_icons_sources.svg')
	}
	.cabinet .add-service[data-add-service="boost"]:before {
		background-image: url('/img/common/add_icons_boost.svg')
	}
	.cabinet .add-service[data-add-service="page"]:before {
		background-image: url('/img/common/add_icons_pages.svg')
	}
	.cabinet .add-service[data-add-service="progressive"]:before {
		background-image: url('/img/common/add_icons_progressive.svg')
	}
	.cabinet .add-service[data-add-service="chart"]:before {
		background-image: url('/img/common/add_icons_graph.svg')
	}
	.cabinet .add-service .title {
		display: block;
		min-height: 37px;
		white-space: nowrap;
		font-size: 12px
	}
	.cabinet .add-service[data-choosen="true"] .title,
	.cabinet .add-service:hover .title {
		min-height: 24px
	}
	.cabinet .add-service[data-add-service="sources"][data-choosen="true"] .title,
	.cabinet .add-service[data-add-service="sources"]:hover .title {
		min-height: 0
	}
	.cabinet .add-service[data-add-service="progressive"] .title,
	.cabinet .add-service[data-add-service="boost"] .title {
		white-space: normal
	}
	.cabinet .add-service .description {
		margin: 0
	}
	.cabinet .add-service[data-disabled="true"] .description p {
		color: #794b4b
	}
	.cabinet .add-service .price {
		font-size: 12px;
		line-height: 13px;
		display: block
	}
	.cabinet .add-service .title,
	.cabinet .add-service .description p {
		margin-bottom: 5px
	}
	.cabinet .add-service .description p {
		font-size: 11px;
		line-height: 120%
	}
	.cabinet .add-service[data-disabled="true"] {
		opacity: .7;
		color: #747575;
		filter: grayscale(80%)
	}
	.cabinet .add-service[data-disabled="true" data-choosen="false"]:before {
		-webkit-filter: grayscale(80%);
		filter: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale")
	}
	.cabinet .add-service[data-disabled="true" data-choosen="false"]:hover {
		cursor: default;
		opacity: 1;
		background-color: #f2f5f5
	}
	.cabinet .add-service[data-choosen="true"],
	.cabinet .add-service:hover {
		background-color: #f8f7eb;
		color: #827651;
		cursor: pointer
	}
	.cabinet .add-service[data-choosen="true"]:hover {}.cabinet .add-service:before,
	.cabinet .add-service .title,
	.cabinet .add-service .description {
		position: relative;
		top: 0;
		transition: top .5s ease .1s, min-height .5s ease
	}
	.cabinet .add-service[data-choosen="true"]:before,
	.cabinet .add-service[data-choosen="true"] .title,
	.cabinet .add-service[data-choosen="true"] .description,
	.cabinet .add-service:hover:before,
	.cabinet .add-service:hover .title,
	.cabinet .add-service:hover .description {
		top: -72px
	}
	.cabinet .add-service[data-disabled="false"],
	.cabinet .add-service[data-choosen="false"] {
		cursor: pointer
	}
	.cabinet .add-service[data-choosen="true"] {
		border-bottom-right-radius: 0;
		border-bottom-left-radius: 0;
		box-shadow: inset 0 -1px 1px #dcdac0, inset 0 -2px 0px #7cbe7f;
		-moz-transform: scale(1.04);
		-ms-transform: scale(1.04);
		transform: scale(1.04);
		-webkit-transform: scale(1.04)
	}
	.cabinet .ui-effects-transfer {
		border: 1px solid rgba(130, 118, 95, .1);
		background-color: rgba(164, 151, 0, .1)
	}
	.cabinet .services-dialog {}.cabinet .services-dialog-group {
		margin-bottom: 20px;
		text-align: center
	}
	.cabinet .services-dialog-message-group {
		overflow: hidden;
		padding: 7px 10px 0;
		border-left: 2px solid #cfbe9c;
		background-color: #f8f5ea;
		color: #4e4838;
		text-align: left;
		font-size: 13px;
		line-height: 1.35
	}
	.cabinet .services-dialog-message-group p {
		margin-bottom: 7px
	}
	.cabinet .services-dialog .ui-dialog-buttonset {
		text-align: center
	}
	.cabinet .services-dialog-ui-inliner {
		display: inline-block;
		vertical-align: middle
	}
	.cabinet .services-dialog-ui-inliner .dec.buttonspin,
	.cabinet .services-dialog-ui-inliner .contentspin input,
	.cabinet .services-dialog-ui-inliner .inc.buttonspin,
	.cabinet .services-dialog-ui-inliner .radios {
		margin-bottom: 0
	}
	.cabinet .services-dialog-deadline-group {
		margin-bottom: 0
	}
	.cabinet .services-dialog-deadline-group .radios {
		margin-left: -3px
	}
	@media (max-width: 767px) {
		.cabinet .services-dialog-deadline-group .radios {
			margin-left: 0
		}
	}
	.cabinet .services-dialog-deadline-group .radios .ui-button {
		padding: 9px 5px
	}
	.cabinet .services-dialog-deadline-group .title {
		margin-bottom: .2em
	}
	.cabinet .new-deadline {
		margin-bottom: 20px;
		display: inline-block
	}
	.cabinet .subtabs {
		margin: 0 -10px;
		padding: 10px;
		border-bottom: 1px solid #dfe5e6;
		background-color: #f2f8fa
	}
	.cabinet .order-messages-container,
	.cabinet .order-service-files-container,
	.cabinet .order-customer-files-container {
		margin-bottom: 0;
		-webkit-transition: width linear 1s;
		-moz-transition: width linear 1s;
		transition: width linear 1s
	}
	.cabinet .slimScrollDiv .order-customer-files-container {
		padding-right: 15px
	}
	.cabinet .order-item {
		margin: 0 -10px;
		border-bottom: 1px dotted #dedede
	}
	.cabinet .order-messages-container li:last-of-type {
		border-bottom: none
	}
	.cabinet .order-item.message {
		margin: 0 -10px
	}
	.cabinet .order-item-header {
		display: block;
		overflow: hidden;
		padding: 10px;
		text-overflow: ellipsis;
		white-space: nowrap;
		cursor: pointer
	}
	.cabinet .order-item-header.new {
		font-weight: bold
	}
	.cabinet .order-item-header:hover {
		border-color: transparent;
		background-color: #f5f9fa
	}
	.cabinet .order-item-type {
		position: relative;
		float: left;
		margin: 0 5px 0 15px;
		text-align: right;
		font-weight: inherit;
		font-size: 13px
	}
	.cabinet .message .order-item-type {
		margin: 0 20px 0 30px;
		width: 70px;
		border-left: 1px solid #e7e7e7
	}
	.cabinet .message .order-item-type:before {
		position: absolute;
		top: 3px;
		left: -28px;
		display: block;
		width: 22px;
		height: 16px;
		background-image: url(/css/common/cs/images/cabinet_spr.png);
		background-repeat: no-repeat;
		content: " "
	}
	.cabinet .message .with-support.order-item-type:before {
		background-position: -162px -138px
	}
	.cabinet .message .new .with-support.order-item-type:before {
		background-position: -162px -122px
	}
	.cabinet .message .with-writer.order-item-type:before {
		background-position: -140px -138px
	}
	.cabinet .message .new .with-writer.order-item-type:before {
		background-position: -140px -122px
	}
	.cabinet .message .order-item-type:after {
		content: ":"
	}
	.cabinet .file .order-item-type:before {
		position: absolute;
		top: 6px;
		left: -13px;
		display: block;
		width: 0px;
		height: 0px;
		border-width: 4px 0 4px 4px;
		border-style: solid;
		border-color: transparent transparent transparent #c1c0c0;
		content: " ";
		-moz-transform: scale(.999)
	}
	.cabinet .file .item-open .order-item-type:before {
		margin: 2px 0 0 -2px;
		border-width: 4px 4px 0;
		border-color: #c1c0c0 transparent transparent
	}
	.cabinet .order-customer-files .file .order-item-type {
		margin-left: 0
	}
	.cabinet .order-customer-files .file .order-item-type:before {
		display: none
	}
	.cabinet .file .order-item-type:after {
		display: inline-block;
		margin: -4px 5px -7px 5px;
		width: 20px;
		height: 26px;
		background: transparent url(/css/common/cs/images/cabinet_spr.png) 0 -170px no-repeat;
		content: " ";
		vertical-align: baseline
	}
	.cabinet .file .order-item-type.pdf:after {
		background-position: -20px -170px
	}
	.cabinet .file .order-item-type.doc:after {
		background-position: -40px -170px
	}
	.cabinet .file .order-item-type.ppt:after {
		background-position: -60px -170px
	}
	.cabinet .new .order-item-type {
		color: inherit
	}
	.cabinet .order-item-subject {
		display: block;
		overflow: hidden;
		text-overflow: ellipsis;
		font-weight: inherit
	}
	.cabinet .new .new-lable {
		display: inline;
		margin-right: 3px;
		padding: 1px 4px;
		border-radius: 3px;
		background-color: #3998ce;
		color: white;
		vertical-align: 1px;
		font-size: 11px
	}
	.cabinet .order-item-date {
		float: right;
		margin-left: 10px;
		font-size: 12px
	}
	.cabinet .order-item-body {
		margin: 0 -10px 0 -10px;
		padding: 10px 20px 20px 70px
	}
	.cabinet .order-item-additional-info {
		margin-top: -21px;
		margin-bottom: 10px;
		font-size: 12px
	}
	.cabinet .message .order-item-body {
		margin: -10px 130px 10px 40px;
		padding: 20px 0 0 90px;
		border-left: 1px solid #e7e7e7
	}
	.cabinet .reply-container {
		margin: 21px 0 0 0
	}
	.cabinet .reply-container form {
		margin-bottom: 0
	}
	.cabinet .reply-container .cancel-button {
		font-weight: normal
	}
	.cabinet .section-order-files {
		display: table;
		margin: 0 -10px
	}
	.cabinet .order-files-title {
		margin: 0 -8px;
		padding: 21px 10px 10px;
		border-bottom: 1px solid #e7e7e7
	}
	.cabinet .order-service-files {
		display: table-cell;
		overflow: hidden;
		padding: 0 5% 0 20px;
		width: 700px;
		border-right: 1px solid #e6e6e6
	}
	.cabinet .order-customer-files {
		display: table-cell;
		padding: 0 20px 0 5%;
		width: 500px;
		background-color: #f6f6f6
	}
	.cabinet .smaller-comment {
		margin: -1px -10px 6px;
		padding: 5px 10px;
		font-size: 13px;
		line-height: 1.35
	}
	.cabinet .small-comment {
		margin: -1px -10px 6px;
		padding: 5px 10px;
		border-top: 1px solid #ececec;
		border-radius: 2px;
		background-color: rgba(0, 0, 0, .04);
		font-size: 12px;
		line-height: 1.3
	}
	.cabinet .big-comment {
		margin-top: 10px
	}
	.cabinet .no-plagiarism {
		margin: 10px -10px;
		padding: 10px;
		border-radius: 2px;
		background-color: #f0f5ea
	}
	.cabinet .files-count {
		float: right;
		margin-top: 1px;
		padding: 21px 0 10px 10px;
		color: #80858a;
		font-size: 16px
	}
	.cabinet .file-description {
		margin-top: 5px;
		margin-bottom: 10px;
		font-size: 12px;
		line-height: 130%;
		white-space: normal
	}
	.cabinet .file-description strong {
		display: inline-block;
		max-width: 260px;
		vertical-align: top;
		width: 100%;
		word-wrap: break-word
	}
	.cabinet .order-customer-files .button {
		margin-top: 21px
	}
	.cabinet .action-block .button {
		margin-bottom: 4px;
		min-width: 13em;
		text-align: left
	}
	.message .order-item-body table,
	.message .order-item-body ul {
		margin-bottom: 0
	}
	.message .order-item-body ul {
		list-style: inside disc
	}
	.message .order-item-body div {
		border-width: 0 !important;
		box-shadow: none !important
	}
	.message table {
		width: auto !important;
		border-collapse: collapse !important;
		font-size: 13px !important
	}
	.message table td td {
		padding: 5px 10px !important;
		border: 1px solid #dedede !important
	}
	.message table td:first-child {
		border-right-width: 0 !important
	}
	.message table td:last-child {
		border-left-width: 0 !important
	}
	.message .order-item-body div + br,
	.message .order-item-body ul+br {
		display: none
	}
	.message .order-item-body div a[href*="/my_order/view/"] {
		display: inline-block !important;
		-moz-box-sizing: border-box !important;
		box-sizing: border-box !important;
		border: 1px solid #e4e4e4 !important;
		border-color: rgba(0, 0, 0, .18) !important;
		border-radius: 3px !important;
		background-color: #f6f7f7 !important;
		background-image: -webkit-linear-gradient(top, #fbfbfb, #f6f7f7) !important;
		background-image: -moz-linear-gradient(top, #fbfbfb, #f6f7f7) !important;
		background-image: linear-gradient(to bottom, #fbfbfb, #f6f7f7) !important;
		background-position: 0 -1px !important;
		background-repeat: repeat-x !important;
		color: #538395 !important;
		vertical-align: top !important;
		text-align: center !important;
		text-decoration: none !important;
		text-shadow: 0 1px 1px rgba(255, 255, 255, .4) !important;
		white-space: nowrap !important;
		cursor: pointer !important;
		-webkit-transition: color .1s linear !important;
		-moz-transition: color .1s linear !important;
		transition: color .1s linear !important
	}
	.message .order-item-body div a[href*="/my_order/view/"]:hover {
		border-color: rgba(0, 0, 0, .18) !important;
		background-color: #fbfbfb !important;
		background-image: -webkit-linear-gradient(top, #fbfbfb, #dcf2f8) !important;
		background-image: -moz-linear-gradient(top, #fbfbfb, #dcf2f8) !important;
		background-image: linear-gradient(to bottom, #fbfbfb, #dcf2f8) !important;
		background-position: 0 7px !important;
		box-shadow: inset 0 -1px 2px rgba(0, 44, 60, .07) !important;
		color: #14789e !important;
		text-decoration: none !important;
		-webkit-transition: color .1s linear, background-position .1s linear, padding .1s linear, box-shadow .1s linear !important;
		-moz-transition: color .1s linear, background-position .1s linear, padding .1s linear, box-shadow .1s linear !important;
		transition: color .1s linear, background-position .1s linear, padding .1s linear, box-shadow .1s linear !important
	}
	.message .order-item-body div a[href*="/my_order/view/"]:active {
		background-image: none !important;
		box-shadow: inset 0 2px 4px rgba(0, 0, 0, .15), 0 1px 2px rgba(0, 0, 0, .05) !important
	}
	.ui-dialog[aria-describedby='customer_upload_dialog'] {
		position: absolute !important
	}
	#customer_files_upload_drag_block {
		border-radius: 3px;
		background-color: #f9f8f1;
		border: 2px dashed #dbd9c9;
		padding: 30px 0;
		margin: 10px 0;
		box-shadow: none;
		-webkit-transition: all 0.3s ease-out;
		-moz-transition: all 0.3s ease-out;
		-ms-transition: all 0.3s ease-out;
		-o-transition: all 0.3s ease-out;
		transition: all 0.3s ease-out
	}
	.customer_files_upload_text,
	.customer_files_upload_or {
		color: #6b695e;
		font-size: 22px;
		line-height: 120%;
		text-align: center;
		width: 100%;
		-webkit-transition: all 0.3s ease-out;
		-moz-transition: all 0.3s ease-out;
		-ms-transition: all 0.3s ease-out;
		-o-transition: all 0.3s ease-out;
		transition: all 0.3s ease-out
	}
	.customer_files_upload_or {
		font-size: 18px;
		margin: 15px 0;
		color: #777
	}
	.customer_files_upload_button {
		background-color: #f6f7f7;
		background-image: -webkit-linear-gradient(#fbfbfb, #ebeded);
		background-image: linear-gradient(#fbfbfb, #ebeded);
		background-position: 0 -1px;
		background-repeat: repeat-x;
		border: 1px solid rgba(0, 0, 0, 0.18);
		border-radius: 3px;
		cursor: pointer;
		height: 38px;
		width: 165px;
		margin: 15px auto 0;
		overflow: hidden;
		position: relative
	}
	.customer_files_upload_button:hover {
		background-color: #fbfbfb;
		background-image: -webkit-linear-gradient(#fbfbfb, #dcf2f8);
		background-image: linear-gradient(#fbfbfb, #dcf2f8);
		background-position: 0 7px;
		border-color: rgba(0, 0, 0, 0.18);
		box-shadow: 0 -1px 2px rgba(0, 44, 60, 0.07) inset;
		text-decoration: none;
		-webkit-transition: color 0.1s linear, background-position 0.1s linear, padding 0.1s linear, box-shadow 0.1s linear;
		-moz-transition: color 0.1s linear, background-position 0.1s linear, padding 0.1s linear, box-shadow 0.1s linear;
		-ms-transition: color 0.1s linear, background-position 0.1s linear, padding 0.1s linear, box-shadow 0.1s linear;
		-o-transition: color 0.1s linear, background-position 0.1s linear, padding 0.1s linear, box-shadow 0.1s linear;
		transition: color 0.1s linear, background-position 0.1s linear, padding 0.1s linear, box-shadow 0.1s linear
	}
	.customer_files_upload_button:active {
		background-color: #e0e9ed;
		background-image: -webkit-linear-gradient(#eceeee, #ecf1f1);
		background-image: linear-gradient(#eceeee, #ecf1f1);
		background-position: 0 0;
		box-shadow: 0 3px 5px rgba(0, 70, 106, 0.28) inset, 0 1px 3px rgba(0, 50, 76, 0.05) inset
	}
	.customer_files_upload_button input[type=file] {
		text-indent: 300px;
		position: absolute;
		top: 0;
		left: 0;
		cursor: pointer;
		height: 30px;
		width: 165px;
		opacity: 0;
		background: none;
		border: 0;
		white-space: nowrap;
		box-shadow: none
	}
	.ie8 .customer_files_upload_button input[type=file] {
		width: 260px;
		-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
		filter: alpha(opacity=0)
	}
	.customer_files_upload_button span {
		font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;
		display: inline-block;
		vertical-align: top;
		color: #538395;
		text-shadow: 0 1px 1px rgba(255, 255, 255, 0.4);
		font-size: 13px;
		line-height: 38px;
		text-align: center;
		width: 100%;
		-webkit-transition: color .1s linear;
		-moz-transition: color .1s linear;
		-ms-transition: color .1s linear;
		-o-transition: color .1s linear;
		transition: color .1s linear
	}
	.customer_files_upload_button:hover span {
		color: #14789e
	}
	.customer_files_upload_button:active span {
		line-height: 40px;
		text-shadow: 0 1px 0 white
	}
	#customer_files_upload_drag_block.in {
		border-color: #b8d49f
	}
	#customer_files_upload_drag_block.in .customer_files_upload_text,
	#customer_files_upload_drag_block.in .customer_files_upload_or {
		color: #dbd9c9
	}
	div.ui-widget-content .btn.btn_all,
	div.ui-widget-content button.cancel_btn,
	div.ui-widget-content button.cancel_btn:hover {
		width: auto;
		font-size: 13px;
		vertical-align: top;
		top: 0
	}
	div.ui-widget-content .btn.btn_all:before {
		content: '';
		display: block;
		height: 15px;
		left: 7px;
		position: absolute;
		top: 5px;
		width: 20px
	}
	div.ui-widget-content button.cancel_btn,
	div.ui-widget-content button.cancel_btn:hover {
		padding-top: 6px;
		padding-bottom: 6px
	}
	.fileupload-buttons {
		float: right;
		position: relative;
		z-index: 2;
		margin-left: 15px
	}
	div.ui-widget-content button.btn_all .ui-button-text,
	div.ui-widget-content button.cancel_btn .ui-button-text {
		padding: 1px 5px;
		background: none
	}
	div.ui-widget-content button.btn_all .ui-icon,
	.upload-file .ui-icon,
	div.ui-widget-content button.cancel_btn .ui-icon {
		display: none
	}
	#customer_upload_dialog .upload_label_agree {
		color: #777;
		font-size: 12px;
		display: block;
		position: relative;
		overflow: visible;
		line-height: 17px;
		padding-right: 20px
	}
	.form-files-but-icon {
		display: inline-block;
		width: 28px;
		height: 19px;
		vertical-align: middle;
		background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0i0KjQsNGAXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjdweCIgaGVpZ2h0PSIxOXB4IiB2aWV3Qm94PSIwIDAgMjcgMTkiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDI3IDE5IiB4bWw6c3BhY2U9InByZXNlcnZlIj48Zz48ZGVmcz48cmVjdCBpZD0iU1ZHSURfMV8iIHg9IjAuNDQ0IiB5PSIwLjc0NiIgd2lkdGg9IjI2LjExMiIgaGVpZ2h0PSIxNy41MDgiLz48L2RlZnM+PGNsaXBQYXRoIGlkPSJTVkdJRF8yXyI+PHVzZSB4bGluazpocmVmPSIjU1ZHSURfMV8iICBvdmVyZmxvdz0idmlzaWJsZSIvPjwvY2xpcFBhdGg+PHBhdGggY2xpcC1wYXRoPSJ1cmwoI1NWR0lEXzJfKSIgZmlsbD0iIzUzODM5NSIgZD0iTTE3Ljc1NywxMS4zNjhsLTMuNDM4LTMuNDM5Yy0wLjE0OS0wLjE1LTAuMzU4LTAuMjQ0LTAuNTg4LTAuMjQ0cy0wLjQzOCwwLjA5My0wLjU4OSwwLjI0NGwtMy40MzgsMy40MzljLTAuMzI1LDAuMzI1LTAuMzI1LDAuODUxLDAsMS4xNzVjMC4zMjUsMC4zMjUsMC44NTEsMC4zMjUsMS4xNzUsMGwyLjAyLTIuMDIxdjYuOWMwLDAuNDU5LDAuMzczLDAuODMxLDAuODMyLDAuODMxczAuODMtMC4zNzIsMC44My0wLjgzMXYtNi45bDIuMDIxLDIuMDIxYzAuMTYzLDAuMTYzLDAuMzc1LDAuMjQ0LDAuNTg4LDAuMjQ0YzAuMjEyLDAsMC40MjYtMC4wODEsMC41ODctMC4yNDRDMTguMDgzLDEyLjIxOSwxOC4wODMsMTEuNjkzLDE3Ljc1NywxMS4zNjgiLz48cGF0aCBjbGlwLXBhdGg9InVybCgjU1ZHSURfMl8pIiBmaWxsPSIjNTM4Mzk1IiBkPSJNNC41MjQsMTcuMzE5Yy0yLjMxNi0wLjM3Ny00LjA4LTIuMzMzLTQuMDgtNC42ODhjMC0xLjg2OSwxLjExMS0zLjQ4NiwyLjcyNy00LjI2MkMyLjk5Miw3Ljk3NiwyLjg5Myw3LjU0MiwyLjg5Myw3LjA4NWMwLTEuNzUxLDEuNDYxLTMuMTY5LDMuMjY0LTMuMTY5YzAuOTYsMCwxLjgyNCwwLjQwMywyLjQyMSwxLjA0NGMxLjI2My0yLjQ5NiwzLjkwNi00LjIxNCw2Ljk2Mi00LjIxNGM0LjIzNywwLDcuNjgzLDMuMzAyLDcuNzUxLDcuNDAxYzEuOTAyLDAuNjUyLDMuMjY2LDIuNDE0LDMuMjY2LDQuNDg0YzAsMi4zNTUtMS43NjUsNC4zMTItNC4wOCw0LjY4OGwtMC44MTYsMC4wNjVoLTQuNTE4di0xLjE3OWg0LjQ2MWgwLjcxN2MxLjU2NC0wLjI0OSwzLjExNi0yLjE0NywzLjExNi0zLjY2NGMwLTEuMzA4LTAuODY4LTIuNDc5LTIuMTYxLTIuOTE1bC0xLjE0Ny0wLjMwOFY4LjIwN2MtMC4wNTMtMy4xNDEtMy4zMDYtNi4yODMtNi42MDMtNi4yODNjLTIuMzM2LDAtNC40MDcsMS4yNTktNS42MzcsMy4wOTFMOC44MDQsNi40NjJMNy41MjcsNS41NzZjLTAuMzEyLTAuMzMtMC44NTUtMC41NjEtMS4zMTktMC41NjFjLTEuMzUzLDAtMi4yMTQsMS4yNjYtMi4yMTQsMi4xMTljMCwwLjIxOSwwLjA0NiwwLjQyOSwwLjEzOCwwLjYyNmwwLjM5MywxLjAyNmMtMS45NTEsMC42NzMtMi45NjEsMi41NzMtMi45NjEsMy43NTZjMCwxLjUxNywxLjcyOCwzLjM3MSwzLjI5MiwzLjYyMWwwLjU0MiwwLjA0M2g0LjQ5MXYxLjE3OUg1LjM0TDQuNTI0LDE3LjMxOXoiLz48L2c+PC9zdmc+) center center no-repeat
	}
	.upload_files_table .files {
		background: none;
		position: relative;
		z-index: 3
	}
	.template-upload,
	.template-download {
		display: block;
		width: 100%;
		margin-bottom: 10px
	}
	.template-upload,
	.template-upload *,
	.template-download,
	.template-download * {
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important
	}
	.template-upload-box {
		width: 100%;
		border: 1px solid #dee3e5;
		border-radius: 3px;
		background-color: #f4f6f7;
		padding: 8px 10px 0;
		position: relative;
		white-space: nowrap
	}
	#samples_container {
		display: table;
		width: 100%
	}
	.files .template-upload-box .name {
		display: inline-block;
		color: #6a7178;
		font-size: 13px;
		padding: 0 5px 0 30px;
		position: relative;
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
		line-height: 30px;
		width: 45%;
		margin: 0 -4px 0 0
	}
	.template-upload-box .name i {
		display: block;
		position: absolute;
		width: 20px;
		height: 27px;
		left: 0;
		top: 0;
		background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAbCAYAAAB836/YAAAAZklEQVRIx+3SMQrAMAhA0dy8V3UQoaCjONqp0CEm1XZU+OtDwQFInuwYqwEkN7NXqeoezYBm5oDkJ0uMVkBVjdEKeJ8/RatgiH4Bp2gFXL5UFhSRaczsgORpcLd9gw022GCDD/DPLtbKrKjuXRiiAAAAAElFTkSuQmCC) no-repeat center center
	}
	.template-upload-box .name i:before {
		content: '';
		display: block;
		position: absolute;
		width: 100%;
		left: 0;
		top: 50%;
		margin-top: -3px;
		box-shadow: 0 1px 0 #dfe3e5;
		font-family: Helvetica, Arial, sans-serif;
		font-style: normal;
		font-size: 8px;
		line-height: 11px;
		color: #fff;
		letter-spacing: 1px;
		text-align: center
	}
	.template-upload-box .name i[data-title*='.jpg']:before {
		content: '.jpg';
		background-color: #9f590f;
		background-image: linear-gradient(0deg, #9f590f 0%, #c68622 100%)
	}
	.template-upload-box .name i[data-title*='.png']:before {
		content: '.png';
		background-color: #9f590f;
		background-image: linear-gradient(0deg, #9f590f 0%, #c68622 100%)
	}
	.template-upload-box .name i[data-title*='.gif']:before {
		content: '.gif';
		background-color: #9f590f;
		background-image: linear-gradient(0deg, #9f590f 0%, #c68622 100%)
	}
	.template-upload-box .name i[data-title*='.tiff']:before {
		content: '.tiff';
		background-color: #9f590f;
		background-image: linear-gradient(0deg, #9f590f 0%, #c68622 100%)
	}
	.template-upload-box .name i[data-title*='.bmp']:before {
		content: '.bmp';
		background-color: #9f590f;
		background-image: linear-gradient(0deg, #9f590f 0%, #c68622 100%)
	}
	.template-upload-box .name i[data-title*='.doc']:before {
		content: '.doc';
		background-color: #3775a0;
		background-image: linear-gradient(0deg, #3775a0 0%, #4b9cbb 100%)
	}
	.template-upload-box .name i[data-title*='.odt']:before {
		content: '.odt';
		background-color: #3775a0;
		background-image: linear-gradient(0deg, #3775a0 0%, #4b9cbb 100%)
	}
	.template-upload-box .name i[data-title*='.rtf']:before {
		content: '.rtf';
		background-color: #3775a0;
		background-image: linear-gradient(0deg, #3775a0 0%, #4b9cbb 100%)
	}
	.template-upload-box .name i[data-title*='.txt']:before {
		content: '.txt';
		background-color: #3775a0;
		background-image: linear-gradient(0deg, #3775a0 0%, #4b9cbb 100%)
	}
	.template-upload-box .name i[data-title*='.xls']:before {
		content: '.xls';
		background-color: #0f9e12;
		background-image: linear-gradient(0deg, #0f9e12 0%, #22c53d 100%)
	}
	.template-upload-box .name i[data-title*='.pdf']:before {
		content: '.pdf';
		background-color: #9e0f0f;
		background-image: linear-gradient(0deg, #9e0f0f 0%, #c53222 100%)
	}
	.template-upload-box .name i[data-title*='.rar']:before {
		content: '.rar';
		background-color: #9e0f93;
		background-image: linear-gradient(0deg, #9e0f93 0%, #a122c5 100%)
	}
	.template-upload-box .name i[data-title*='.zip']:before {
		content: '.zip';
		background-color: #9e0f93;
		background-image: linear-gradient(0deg, #9e0f93 0%, #a122c5 100%)
	}
	.template-upload-box .size {
		font-size: 11px;
		color: #999;
		line-height: 12px
	}
	.template-upload-box .progress {
		height: auto
	}
	.progress-extended {
		color: #999;
		font-size: 12px;
		line-height: 100%
	}
	.template-upload .error,
	.template-download .error {
		display: block;
		font-size: 11px;
		margin: 3px 0 1px;
		padding: 0 5px;
		position: relative;
		color: #b15a5a;
		line-height: 1.4
	}
	.template-download .error {
		margin: 1px 0
	}
	.template-upload .error.error-animation,
	.template-download .error.error-animation {
		-webkit-animation: pulsate 1s ease-out;
		-webkit-animation-iteration-count: 2;
		animation: pulsate 1s ease-out;
		animation-iteration-count: 2
	}
	@-webkit-keyframes pulsate {
		0% {
			-webkit-transform: scale(1)
		}
		50% {
			-webkit-transform: scale(1.1)
		}
		100% {
			-webkit-transform: scale(1)
		}
	}
	@keyframes pulsate {
		0% {
			transform: scale(1)
		}
		50% {
			transform: scale(1.1)
		}
		100% {
			transform: scale(1)
		}
	}
	.files .template-upload-box .description-select,
	.template-download .download-text-danger {
		width: 55%;
		padding-right: 25px;
		display: inline-block;
		vertical-align: top
	}
	.template-download .download-text-danger {
		line-height: 30px
	}
	#customer_upload_dialog .ui-custom-dropdown {
		padding: 0 !important;
		margin: 0;
		position: relative
	}
	#customer_upload_dialog .control-wrapper {
		width: 100%;
		height: 32px;
		overflow: hidden;
		background-color: #fff;
		border: 1px solid #ccc;
		border-radius: 3px;
		box-shadow: 0 2px 2px rgba(0, 0, 0, 0.05) inset;
		color: #555;
		transition: border-color 0.2s linear 0s, box-shadow 0.2s linear 0s
	}
	#customer_upload_dialog .control-wrapper:hover {
		background-color: #fafdff;
		box-shadow: 0 2px 2px rgba(0, 44, 60, 0.07) inset
	}
	div.ui-widget-content .control-wrapper .innertext {
		font-style: normal;
		line-height: 30px;
		height: 28px;
		padding: 0 33px 0 10px;
		color: #697177;
		font-size: 13px;
		cursor: pointer;
		display: inline-block;
		margin-right: -31px;
		width: 100%
	}
	div.ui-widget-content .control-wrapper .ui-dropdown-button {
		height: 30px;
		background: none;
		border: 0 !important;
		cursor: pointer;
		width: 30px;
		margin: 0;
		padding: 0;
		box-shadow: none;
		position: relative;
		vertical-align: top
	}
	div.ui-widget-content .control-wrapper .ui-dropdown-button:focus,
	div.ui-widget-content .control-wrapper .ui-dropdown-button:active {
		box-shadow: none
	}
	div.ui-widget-content .control-wrapper .ui-dropdown-button:before {
		background-color: #f0f1f1;
		content: " ";
		display: block;
		height: 26px;
		left: 0;
		position: absolute;
		top: 2px;
		width: 1px
	}
	div.ui-widget-content .control-wrapper .ui-icon-triangle-1-s,
	div.ui-widget-content .control-wrapper .ui-icon-triangle-1-s:hover {
		border-color: #8f9ea4 transparent transparent;
		border-style: solid;
		border-width: 4px 4px 0;
		height: 0;
		margin-left: -4px;
		margin-top: -2px;
		left: 50%;
		position: absolute;
		top: 50%;
		width: 0
	}
	.ui-icon-triangle-1-s:before {
		display: none
	}
	.item-container {
		background-color: #fff;
		border: 1px solid #ccc;
		margin-top: -3px;
		border-radius: 0 0 3px 3px;
		height: 120px;
		overflow-x: hidden;
		overflow-y: auto;
		width: 100%;
		z-index: 1
	}
	.item-container dl {
		margin: 0
	}
	.item-container dl dt {
		cursor: pointer;
		width: 100%;
		font-size: 12px;
		line-height: 130%;
		border-bottom: 1px solid #f0f1f1
	}
	.item-container dl dt:last-child {
		border: none !important
	}
	.item-container .item-value {
		padding: 5px
	}
	.item-container .item-value-active {
		background-color: #53afe8;
		border-color: transparent !important;
		color: #fff;
		cursor: pointer;
		padding: 5px
	}
	div.ui-widget-content .btn-warning,
	div.ui-widget-content .btn-warning span,
	div.ui-widget-content .btn-primary,
	div.ui-widget-content .btn-primary span {
		background: none;
		border: 0;
		box-shadow: none;
		padding: 0;
		margin: 0
	}
	div.ui-widget-content .btn-warning span.ui-icon-cancel,
	div.ui-widget-content .btn-warning span.ui-button-icon-primary {
		display: none
	}
	div.ui-widget-content .btn-warning {
		width: 18px;
		height: 18px;
		overflow: hidden;
		border-radius: 0;
		position: absolute;
		top: 14px;
		right: 10px;
		cursor: pointer
	}
	div.ui-widget-content .btn-warning:hover,
	div.ui-widget-content .btn-warning:active {
		background: none !important;
		box-shadow: none !important
	}
	div.ui-widget-content .btn-warning:active {
		padding: 0
	}
	div.ui-widget-content .btn-warning span.ui-button-text {
		display: block;
		width: 18px;
		height: 18px;
		overflow: hidden;
		text-indent: 20px;
		background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAALCAYAAACprHcmAAAAVElEQVQoz2Pon7ckHYjnADEjAxYAEofKpzNAGf+BeC66BqjCuVD5OegCcA1YxJkYcEgwY1WIw8rbOBUiaWBCUgiiWRjw+JqwyUS7maTQIDWciY5BAK83xB9wCFNHAAAAAElFTkSuQmCC) no-repeat center center
	}
	div.ui-widget-content .btn-warning:hover span.ui-button-text {
		background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAALCAYAAACprHcmAAAAVElEQVQoz2MITc5IB+I5QMzIgAWAxKHy6QxQxn8gnouuAapwLlR+DroAXAMWcSYGHBLMWBXisPI2ToVIGpiQFIJoFgY8viZsMtFuJik0SA1nomMQAL2yiVr1MCZVAAAAAElFTkSuQmCC)
	}
	div.ui-widget-content .btn-primary {
		display: none
	}
	div.ui-widget-content .fileupload-progress {
		margin: 0 0 5px
	}
	div.ui-widget-content .progress {
		height: 12px;
		border-radius: 2px;
		border: 1px solid #CFCFCF;
		margin: 5px 0;
		background: #fff
	}
	div.ui-widget-content .template-upload .progress {
		position: absolute;
		top: 0;
		left: 0;
		height: 100%;
		width: 100%;
		border-radius: 3px;
		margin: 0;
		border: none;
		background: transparent;
		z-index: 0
	}
	div.ui-widget-content .progress .ui-progressbar-value {
		background: rgba(196, 233, 246, 0.97);
		margin: 0;
		box-shadow: 1px 0 0 #CFCFCF;
		height: 100%;
		border-radius: 0;
		position: relative
	}
	div.ui-widget-content .progress .ui-progressbar-value:before {
		content: '';
		display: block;
		position: absolute;
		width: 100%;
		height: 100%;
		opacity: .05;
		left: 0;
		top: 0;
		background: url(data:image/gif;base64,R0lGODlhKAAoAIABAAAAAP///yH/C05FVFNDQVBFMi4wAwEAAAAh+QQJAQABACwAAAAAKAAoAAACkYwNqXrdC52DS06a7MFZI+4FHBCKoDeWKXqymPqGqxvJrXZbMx7Ttc+w9XgU2FB3lOyQRWET2IFGiU9m1frDVpxZZc6bfHwv4c1YXP6k1Vdy292Fb6UkuvFtXpvWSzA+HycXJHUXiGYIiMg2R6W459gnWGfHNdjIqDWVqemH2ekpObkpOlppWUqZiqr6edqqWQAAIfkECQEAAQAsAAAAACgAKAAAApSMgZnGfaqcg1E2uuzDmmHUBR8Qil95hiPKqWn3aqtLsS18y7G1SzNeowWBENtQd+T1JktP05nzPTdJZlR6vUxNWWjV+vUWhWNkWFwxl9VpZRedYcflIOLafaa28XdsH/ynlcc1uPVDZxQIR0K25+cICCmoqCe5mGhZOfeYSUh5yJcJyrkZWWpaR8doJ2o4NYq62lAAACH5BAkBAAEALAAAAAAoACgAAAKVDI4Yy22ZnINRNqosw0Bv7i1gyHUkFj7oSaWlu3ovC8GxNso5fluz3qLVhBVeT/Lz7ZTHyxL5dDalQWPVOsQWtRnuwXaFTj9jVVh8pma9JjZ4zYSj5ZOyma7uuolffh+IR5aW97cHuBUXKGKXlKjn+DiHWMcYJah4N0lYCMlJOXipGRr5qdgoSTrqWSq6WFl2ypoaUAAAIfkECQEAAQAsAAAAACgAKAAAApaEb6HLgd/iO7FNWtcFWe+ufODGjRfoiJ2akShbueb0wtI50zm02pbvwfWEMWBQ1zKGlLIhskiEPm9R6vRXxV4ZzWT2yHOGpWMyorblKlNp8HmHEb/lCXjcW7bmtXP8Xt229OVWR1fod2eWqNfHuMjXCPkIGNileOiImVmCOEmoSfn3yXlJWmoHGhqp6ilYuWYpmTqKUgAAIfkECQEAAQAsAAAAACgAKAAAApiEH6kb58biQ3FNWtMFWW3eNVcojuFGfqnZqSebuS06w5V80/X02pKe8zFwP6EFWOT1lDFk8rGERh1TTNOocQ61Hm4Xm2VexUHpzjymViHrFbiELsefVrn6XKfnt2Q9G/+Xdie499XHd2g4h7ioOGhXGJboGAnXSBnoBwKYyfioubZJ2Hn0RuRZaflZOil56Zp6iioKSXpUAAAh+QQJAQABACwAAAAAKAAoAAACkoQRqRvnxuI7kU1a1UU5bd5tnSeOZXhmn5lWK3qNTWvRdQxP8qvaC+/yaYQzXO7BMvaUEmJRd3TsiMAgswmNYrSgZdYrTX6tSHGZO73ezuAw2uxuQ+BbeZfMxsexY35+/Qe4J1inV0g4x3WHuMhIl2jXOKT2Q+VU5fgoSUI52VfZyfkJGkha6jmY+aaYdirq+lQAACH5BAkBAAEALAAAAAAoACgAAAKWBIKpYe0L3YNKToqswUlvznigd4wiR4KhZrKt9Upqip61i9E3vMvxRdHlbEFiEXfk9YARYxOZZD6VQ2pUunBmtRXo1Lf8hMVVcNl8JafV38aM2/Fu5V16Bn63r6xt97j09+MXSFi4BniGFae3hzbH9+hYBzkpuUh5aZmHuanZOZgIuvbGiNeomCnaxxap2upaCZsq+1kAACH5BAkBAAEALAAAAAAoACgAAAKXjI8By5zf4kOxTVrXNVlv1X0d8IGZGKLnNpYtm8Lr9cqVeuOSvfOW79D9aDHizNhDJidFZhNydEahOaDH6nomtJjp1tutKoNWkvA6JqfRVLHU/QUfau9l2x7G54d1fl995xcIGAdXqMfBNadoYrhH+Mg2KBlpVpbluCiXmMnZ2Sh4GBqJ+ckIOqqJ6LmKSllZmsoq6wpQAAAh+QQJAQABACwAAAAAKAAoAAAClYx/oLvoxuJDkU1a1YUZbJ59nSd2ZXhWqbRa2/gF8Gu2DY3iqs7yrq+xBYEkYvFSM8aSSObE+ZgRl1BHFZNr7pRCavZ5BW2142hY3AN/zWtsmf12p9XxxFl2lpLn1rseztfXZjdIWIf2s5dItwjYKBgo9yg5pHgzJXTEeGlZuenpyPmpGQoKOWkYmSpaSnqKileI2FAAACH5BAkBAAEALAAAAAAoACgAAAKVjB+gu+jG4kORTVrVhRlsnn2dJ3ZleFaptFrb+CXmO9OozeL5VfP99HvAWhpiUdcwkpBH3825AwYdU8xTqlLGhtCosArKMpvfa1mMRae9VvWZfeB2XfPkeLmm18lUcBj+p5dnN8jXZ3YIGEhYuOUn45aoCDkp16hl5IjYJvjWKcnoGQpqyPlpOhr3aElaqrq56Bq7VAAAOw==)
	}
	.cabinet .additional_materials_files_container .order-item {
		margin: 0;
		padding: 10px 5px;
		border: 0
	}
	.cabinet .additional_materials_files_container .order-item:last-child {
		margin-bottom: 10px
	}
	.cabinet .additional_materials_files_container .order-item+.order-item {
		border-top: 1px dotted #dedede
	}
	.cabinet .additional_materials_files_container .file .order-item-type:before {
		display: none
	}
	.cabinet .additional_materials_files_container .order-item-type {
		margin: 0 5px 0 0
	}
	.cabinet .additional_materials_files_container .order-item-subject em {
		color: #888;
		font-size: 12px
	}
	@media (max-width: 800px) {
		.cabinet .additional_materials_files_container .order-item:first-child {
			margin-top: 30px
		}
	}
	@media (max-width: 700px) {
		.customer_files_upload_text {
			display: none
		}
		#customer_files_upload_drag_block {
			padding: 0
		}
		.customer_files_upload_button {
			margin: 10px auto
		}
	}
	@media (max-width: 479px) {
		.template-upload,
		.template-download {
			width: 100%;
			margin-right: 0
		}
		.template-upload-box {
			padding: 8px 10px 5px;
			white-space: normal
		}
		.files .template-upload-box .name {
			width: 100%;
			margin: 0;
			padding-right: 25px
		}
		.files .template-upload-box .description-select,
		.template-download .download-text-danger {
			width: 100%;
			padding-right: 0
		}
		.additional_materials_style .field_tip {
			margin: 10px 0 13px
		}
		.fileupload-buttons {
			float: none;
			margin: 0;
			text-align: center
		}
		#customer_upload_dialog .upload_label_agree {
			padding-right: 0
		}
	}
	.cabinet .dialog {
		clear: both;
		overflow: hidden;
		padding: 21px 20px 0;
		border: 1px solid #e6e6e6;
		border-radius: 3px;
		background-color: #fafafa
	}
	.cabinet .dialog h2 {}.cabinet .dialog label {
		display: inline
	}
	.cabinet .dialog .clearfix label {
		display: inline-block
	}
	.cabinet .dialog .large-label {
		display: block
	}
	.cabinet .dialog-action-block {
		text-align: center
	}
	.cabinet .dialog .small-comment {
		margin: 0 0 10px
	}
	.cabinet .request-revision-dialog,
	.cabinet .request-refund-dialog {
		background-color: #f9f5f2
	}
	.cabinet .read-policy {
		margin-top: -11px;
		font-size: 12px
	}
	.cabinet .revision-notes,
	.cabinet .refund-notes {
		float: right;
		margin-left: 10%;
		width: 45%;
		-webkit-transition: width .5s linear;
		-moz-transition: width .5s linear;
		transition: width .5s linear
	}
	.cabinet .revision-notes.narrow,
	.cabinet .refund-notes.narrow {
		width: 27%
	}
	.cabinet .revision-note,
	.cabinet .refund-note {
		position: relative;
		margin-bottom: 21px;
		color: #543c29;
		font-size: 13px;
		line-height: 1.3
	}
	.cabinet .revision-note:before,
	.cabinet .refund-note:before {
		position: absolute;
		top: -2px;
		left: -30px;
		width: 22px;
		height: 23px;
		background: transparent url(/css/common/cs/images/cabinet_spr.png) -156px -170px no-repeat;
		content: " "
	}
	.cabinet .request-revision-form {}.cabinet .revision-refund-options {
		overflow: hidden;
		margin-bottom: 21px
	}
	.cabinet .revision-refund-option {
		margin-bottom: 5px;
		padding: 0
	}
	.cabinet .revision-refund-options .caption {
		position: relative
	}
	.cabinet .revision-refund-options .revision-refund-option label {
		margin-bottom: 0;
		padding-left: 35px;
		width: 100%;
		cursor: pointer
	}
	.cabinet .revision-refund-options .revision-refund-option label:before,
	.cabinet .revision-refund-options .revision-refund-option label:after {
		left: 7px
	}
	.cabinet .revision-refund-options .revision-refund-option-checkbox {
		margin: 0 -10px 0 10px
	}
	.cabinet .revision-refund-option-body {
		margin-top: -3px;
		padding: 21px 35px 8px;
		border: 1px solid #e4e4e4;
		border-color: rgba(0, 0, 0, .18);
		border-radius: 3px;
		background-color: #f6f7f7;
		color: #3e454c;
		text-shadow: 0 1px 1px rgba(255, 255, 255, .4)
	}
	.cabinet .revision-refund-option-body p {
		margin-bottom: 13px;
		font-size: 13px;
		line-height: 1.4
	}
	.cabinet .revision-deadline-label {
		display: block;
		float: left;
		clear: both;
		font-weight: bold
	}
	.cabinet .revision-deadline-wrapper {
		clear: right;
		overflow: hidden
	}
	.cabinet .revision-deadline-wrapper .field_tip {
		margin: -5px 0 13px;
		color: #6a6764;
		font-size: 12px
	}
	.cabinet .refund-type-item {
		position: relative;
		padding-left: 35px
	}
	.cabinet .refund-type-lable {
		margin: 0 0 5px -35px;
		padding: 10px 10px 10px 35px
	}
	.cabinet .refund-type-item input[type="radio"] {
		position: absolute;
		top: 12px;
		left: 12px;
		width: 15px;
		height: 15px
	}
	.cabinet .full-refund-note p {
		margin-bottom: 10px;
		font-size: 13px
	}
	.cabinet .approve-order-dialog,
	.cabinet .approve-part-dialog {
		background-color: #f3f7f8
	}
	.cabinet .approve-feedback {
		display: table;
		margin-bottom: 21px;
		padding: 21px 20px 0;
		background-color: #fafcfd;
		box-shadow: 0 1px 6px rgba(0, 42, 53, .07)
	}
	.cabinet .feedback-title {
		margin-bottom: 42px
	}
	.cabinet .approve-feedback .cell {
		width: 500px
	}
	.cabinet .stars_rate,
	.cabinet .result_rate {
		display: table-cell;
		overflow: visible !important;
		padding-right: 5px;
		vertical-align: middle;
		white-space: nowrap
	}
	.cabinet .result_rate {
		font-size: 27px;
		opacity: 0;
		-webkit-transition: opacity .5s linear;
		-moz-transition: opacity .5s linear;
		transition: opacity .5s linear
	}
	.cabinet .result_rate i {
		font-size: 16px
	}
	.cabinet .jquery-ratings-star {
		position: relative;
		display: inline-block;
		width: 29px;
		height: 27px;
		background: transparent url(/css/common/cs/images/cabinet_spr.png) -127px -170px no-repeat;
		cursor: pointer
	}
	.cabinet .jquery-ratings-star.jquery-ratings-full {
		background-position: -98px -170px
	}
	.cabinet .jquery-ratings-star:after {
		position: absolute;
		bottom: -21px;
		left: 50%;
		display: block;
		margin-left: -30px;
		width: 60px;
		color: #babdbe;
		text-align: center;
		font-size: 11px
	}
	.cabinet .jquery-ratings-star:first-child:after {
		margin-left: -20px;
		content: "Very Poor"
	}
	.cabinet .jquery-ratings-star:last-child:after {
		margin-left: -33px;
		content: "Perfect"
	}
	.cabinet .stars_rate:hover .jquery-ratings-star:after {
		content: " "
	}
	.cabinet .stars_rate:hover .jquery-ratings-star:nth-child(1):hover:after {
		content: "Very Poor"
	}
	.cabinet .stars_rate:hover .jquery-ratings-star:nth-child(2):hover:after {
		content: "Poor"
	}
	.cabinet .stars_rate:hover .jquery-ratings-star:nth-child(3):hover:after {
		content: "Not that Bad"
	}
	.cabinet .stars_rate:hover .jquery-ratings-star:nth-child(4):hover:after {
		content: "Fair"
	}
	.cabinet .stars_rate:hover .jquery-ratings-star:nth-child(5):hover:after {
		content: "Average"
	}
	.cabinet .stars_rate:hover .jquery-ratings-star:nth-child(6):hover:after {
		content: "Almost good"
	}
	.cabinet .stars_rate:hover .jquery-ratings-star:nth-child(7):hover:after {
		content: "Good"
	}
	.cabinet .stars_rate:hover .jquery-ratings-star:nth-child(8):hover:after {
		content: "Very Good"
	}
	.cabinet .stars_rate:hover .jquery-ratings-star:nth-child(9):hover:after {
		content: "Excellent"
	}
	.cabinet .stars_rate:hover .jquery-ratings-star:nth-child(10):hover:after {
		content: "Perfect"
	}
	.cabinet .title-rate.large-label {
		display: inline-block;
		margin-right: 10px
	}
	.cabinet .approve-feedback input[type="radio"] {
		width: 15px;
		height: 15px
	}
	.cabinet .approve-order-dialog label.agree-checkbox-label {
		display: block;
		margin: 0 auto;
		margin-bottom: 21px;
		max-width: 715px;
		height: auto;
		font-size: 12px
	}
	.cabinet .agree-checkbox {
		display: block;
		margin-bottom: -21px
	}
	.cabinet .approve-order-dialog label.agree-checkbox-label:before,
	.cabinet .approve-order-dialog label.agree-checkbox-label:after {
		top: 14px
	}
	.cabinet .approve-order-dialog .dialog-action-block {
		padding-top: 20px;
		border-top: 1px solid #e1e7ea
	}
	.cabinet .download-doc-tip {
		display: inline-block;
		margin-right: -18em;
		padding: 1px 10px;
		width: 18em;
		text-align: left;
		font-size: 12px;
		line-height: 20px
	}
	.cabinet .approve-form .doc {
		white-space: nowrap
	}
	.cabinet .approve-form .doc:before {
		display: inline-block;
		margin: 0 5px -7px 0;
		width: 20px;
		height: 26px;
		background: transparent url(/css/common/cs/images/cabinet_spr.png) -40px -170px no-repeat;
		content: " ";
		vertical-align: baseline
	}
	.cabinet .select_rate {
		position: absolute;
		left: -9999px;
		visibility: hidden;
		width: 0;
		height: 0
	}
	.cabinet .refund-type-wrapper {
		border-bottom: 1px solid #ccc
	}
	.cabinet .refund-type-wrapper:after {
		display: table;
		clear: both;
		content: ""
	}
	#refund_form_rating_errors {
		margin-top: -20px
	}
	.cabinet .rate_margin {
		display: block;
		margin: 10px 0 40px
	}
	.cabinet .title-rate {
		color: #ab7f4a;
		font-weight: bold;
		font-size: 16px
	}
	.cabinet p.approve-conscious-notice {
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		display: block;
		width: 85%;
		padding: 10px 20px;
		margin: 15px auto;
		border: 1px solid #f9b5b6;
		border-radius: 2px;
		background: #faf7f7
	}
	@media (max-width: 767px) {
		.cabinet p.approve-conscious-notice {
			width: 100%
		}
	}
	.ui-dialog[aria-describedby='prices_vat_rates'] {
		max-width: 480px;
		max-height: 360px
	}
	#prices_vat_rates.hidden {
		display: none
	}
	#prices_vat_rates {
		height: 250px !important;
		overflow-y: scroll
	}
	.prices_vat_rates_table {
		width: 100%
	}
	.prices_vat_rates_table tr:nth-of-type(2n) {
		background-color: rgba(0, 0, 0, 0.04)
	}
	.prices_vat_rates_table td {
		padding: 5px 10px
	}
	.cabinet .orderform-bordered {
		min-width: 720px;
		width: 75%;
		border: 4px solid #f5f5f5;
		border-radius: 4px
	}
	.inquiry-page {
		margin: 0 auto;
		padding: 0 15px;
		max-width: 780px
	}
	@media (max-width: 580px) {
		.inquiry-page {
			padding: 0
		}
	}
	.inner-content .cabinet .orderform-bordered {
		min-width: 100%;
		max-width: 100%
	}
	.roza-form-page .orderform {
		width: 68%;
		float: left;
		position: relative;
		z-index: 1
	}
	.cabinet .orderform sup {
		top: 0;
		vertical-align: baseline;
		font-size: inherit
	}
	.cabinet fieldset {
		border: 0 none
	}
	.cabinet .is_logged .orderform-step>.step_header {
		display: none
	}
	.cabinet .orderform-tabs,
	.cabinet .orderform-step {
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		width: 100%;
		border: 1px solid #dedede;
		min-width: 0
	}
	.cabinet .orderform-title {
		margin-bottom: 10px
	}
	.cabinet .orderform-disclamer {
		font-size: 13px
	}
	.cabinet .orderform-tabs {
		position: relative;
		z-index: 2;
		display: table;
		margin-bottom: 0;
		background-color: white;
		padding: 0
	}
	.cabinet .orderform-tabs li {
		display: table-cell
	}
	.cabinet .orderform-tabs a {
		position: relative;
		padding-left: 26px;
		width: 100%;
		color: #828485;
		text-align: center;
		text-decoration: none;
		font-size: 20px;
		line-height: 17px;
		-webkit-transition: color .1s linear, background-color .1s linear;
		-moz-transition: color .1s linear, background-color .1s linear;
		transition: color .1s linear, background-color .1s linear
	}
	.cabinet .orderform-tabs a.large {
		padding-left: 28px
	}
	.cabinet .orderform-tabs a .step-number {
		color: #d3d3d3;
		font-size: 26px
	}
	.cabinet .orderform-tabs a:before,
	.cabinet .orderform-tabs a:after {
		position: absolute;
		top: 0;
		left: 1px;
		width: 0;
		height: 0;
		border-width: 24px 0 24px 14px;
		border-style: solid;
		border-color: transparent transparent transparent #dcdcdc;
		content: " ";
		-webkit-transition: border-color .1s linear;
		-moz-transition: border-color .1s linear;
		transition: border-color .1s linear;
		transform: scale(1.01)
	}
	.cabinet .orderform-tabs a:after {
		right: -14px;
		left: auto;
		z-index: 2;
		border-left-color: white
	}
	.cabinet .orderform-tabs a:hover,
	.cabinet .orderform-tabs .ui-state-active a {
		background-color: #7cb149;
		color: white
	}
	.cabinet .orderform-tabs a:hover:after,
	.cabinet .orderform-tabs .ui-state-active a:after {
		border-left-color: #7cb149
	}
	.cabinet .orderform-tabs li:first-child a:before,
	.cabinet .orderform-tabs li:last-child a:after {
		display: none
	}
	.cabinet .orderform-step {
		margin: -1px 0 0;
		padding: 21px 20px 0;
		padding-bottom: 42px;
		background-color: #f8f8f8
	}
	.cabinet .orderform-step .cell-left,
	.cabinet .orderform-step .cell-left {
		min-width: 150px;
		width: 25%
	}
	.cabinet .cell-right {
		position: relative;
		width: 75%
	}
	.cabinet .cell-right div[id*="other_"] {
		position: relative;
		z-index: 1
	}
	.cabinet .is_logged .orderform-step {
		padding-bottom: 10px;
		border-bottom: 0
	}
	.cabinet .is_logged #tab_personal .orderform-step {
		padding-top: 0;
		padding-bottom: 25px;
		border-top: 0;
		border-bottom: 1px solid #dedede
	}
	.cabinet .is_logged #tab_personal .orderform-step {
		margin-top: 0;
		padding-top: 20px
	}
	.cabinet .field_tip,
	.cabinet .words_total,
	.additional_materials_style .field_tip {
		margin: -10px 0 13px;
		color: #999ca0;
		font-style: italic;
		font-size: 12px;
		line-height: 1.4
	}
	.cabinet .checkbox-tip,
	.additional_materials_style .checkbox-tip {
		padding-left: 25px
	}
	#contentspinpagesreq {
		float: left;
		margin-right: 10px
	}
	.cabinet .cell-additional-materials {
		padding-bottom: 15px
	}
	.cabinet .spacing-inliner {
		overflow: hidden
	}
	@media (max-width: 580px) {
		.cabinet .spacing-inliner {
			overflow: visible;
			clear: both
		}
	}
	.cabinet .words_total {
		clear: both;
		float: left;
		min-width: 120px;
		text-align: center
	}
	.cabinet .features-block {
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		padding: 15px 20px 5px;
		width: 100%;
		border: 1px solid #dedede;
		background-color: #fafafa;
		margin-top: -1px
	}
	.cabinet .features {
		display: table;
		margin: -15px 0 0 -15px;
		width: 105%;
		border-spacing: 10px
	}
	.roza-form-page .cabinet .features {
		margin: -15px 0 0 -5px;
		border-spacing: 8px
	}
	.cabinet .feature,
	.cabinet input[type="checkbox"]+label.feature {
		display: table-cell;
		width: 500px;
		height: auto;
		border: 1px solid #d1e2ea;
		border-radius: 3px;
		background-color: #e8f3f6
	}
	.cabinet input[type="checkbox"]+label.large.feature {
		padding: 10px 10px 2px
	}
	@media (max-width: 800px) {
		.cabinet input[type="checkbox"]+label.large.feature {
			padding: 10px
		}
	}
	.cabinet input[type="checkbox"][disabled] + label.feature,
	.cabinet input[type="checkbox"][disabled] + label.feature:hover,
	.cabinet input[type="checkbox"][disabled] + label.feature .feature-header,
	.cabinet input[type="checkbox"][disabled] + label.feature:hover .feature-header,
	.cabinet input[type="checkbox"][disabled] + label.feature .feature-price,
	.cabinet input[type="checkbox"][disabled] + label.feature .feature-description,
	.cabinet input[type="checkbox"][disabled] + label.feature .why-not-available {
		border-color: #E8E8E8;
		background-color: #F4F4F4;
		color: #939B9E
	}
	.cabinet input[type="checkbox"] + label.feature:before,
	.cabinet input[type="checkbox"]+label.feature:after {
		display: none
	}
	.cabinet input[type="checkbox"]:checked + label.feature:after,
	.cabinet input[type="checkbox"].checked+label.feature:after {
		display: none
	}
	.cabinet input[type="checkbox"] + label .feature-header:before {
		position: absolute;
		top: 9px;
		left: 0;
		display: block;
		margin-top: -9px;
		width: 18px;
		height: 18px;
		content: " "
	}
	.cabinet input[type="checkbox"] + label .feature-header:hover:before {
		background-color: #f5fbff
	}
	.cabinet input[type="checkbox"] + label .feature-header:after {
		position: absolute;
		top: 9px;
		left: 0;
		display: none;
		margin: -11px 0 0 4px;
		width: 18px;
		height: 18px;
		background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo5QzlEQjI0NzNGQzMxMUUzOEQ1NTg4QzVBRTNEN0EzOCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo5QzlEQjI0ODNGQzMxMUUzOEQ1NTg4QzVBRTNEN0EzOCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjlDOURCMjQ1M0ZDMzExRTM4RDU1ODhDNUFFM0Q3QTM4IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjlDOURCMjQ2M0ZDMzExRTM4RDU1ODhDNUFFM0Q3QTM4Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+ZT0f2gAAAZxJREFUeNpi/P//PwMlILeh2w5IWbFQYAAHkJoAxJ5A7M5CpiGiQGoLEF9hZWHR6Ksp/M5IqteAhkgAqQOMDAzbJzWUFsLEWUg0hB9I7WJiZLzGy8NThCxHtIuAhjADqZ1MjEwafLzc2s1FGR+R5UlxUQcjA6Mz0BA/dEOIdhHQNYFAai0fD8+y1pLMGGxqmN8ycAdsP3AsE4iPejlY/8IRuNu5OTmZ2NnZ/B0sjD9hM4gJiCWBOAWIjwI1KWJRM5ONlVWEg4N9QkN+6hNcrmaa3FA6nZuL0w3ofzkg/zDQMBUk14QCA9ePh5vrHZDbic/78DCq7Zvh8PnL1z1///27D+RaAvEPIL7By80tzcbGWgd0TTM+g5hgDGBMHODj5algZmICuWgOEJeys7GBDPkCtGwyoQhhQuEwMfUBDTsCTHD+QK9WcXGCshPDnMaCtA8kGQR0/j+gYRncXFx/ODnYWYDsf0DXTCUmkWFNRw0TZ88AUulAvBlouB8xBjHhEK8DYlB6mU1sssdqENAVr4BU9e/fv7cTaxBAgAEAajCA6d8TjLsAAAAASUVORK5CYII=');
		content: " "
	}
	input[type="checkbox"]:checked + label .feature-header:after,
	input[type="checkbox"].checked + label .feature-header:after {
		display: block
	}
	.cabinet .feature:hover,
	input[type="checkbox"]+label.feature:hover {
		background-color: #e6f6fb;
		cursor: pointer
	}
	.cabinet .feature-header {
		position: relative;
		float: left;
		padding-bottom: 10px;
		padding-left: 25px;
		color: #0076a3;
		font-size: 15px;
		font-family: 'roboto', 'arial', sans-serif
	}
	.cabinet .feature:hover .feature-header {
		color: #009bd6
	}
	.cabinet .feature-price {
		float: right;
		margin-left: 5px;
		padding-bottom: 10px;
		font-size: 18px
	}
	.cabinet .feature-price sup {
		top: -6px;
		font-size: 12px
	}
	.cabinet .feature-description,
	.cabinet .feature .why-not-available {
		clear: both;
		padding-top: 10px;
		border-top: 1px solid #afd9e7;
		font-size: 13px;
		line-height: 1.38;
		-webkit-transition: color linear .4s, max-height linear .4s, padding linear .2s;
		-moz-transition: color linear .4s, max-height linear .4s, padding linear .2s;
		transition: color linear .4s, max-height linear .4s, padding linear .2s
	}
	.cabinet .feature .why-not-available {
		display: none;
		overflow: hidden;
		padding: 0;
		max-height: 0
	}
	input[type="checkbox"][disabled] + label.feature .why-not-available {
		display: block;
		border-color: transparent;
		color: transparent;
		font-style: italic
	}
	input[type="checkbox"][disabled] + label.feature:hover .why-not-available {
		padding: 10px 0;
		max-height: 200px;
		border-color: #E8E8E8;
		color: black
	}
	.cabinet .feature-price,
	.cabinet .feature-description {
		color: #5c676a
	}
	.cabinet .feature:hover .feature-price,
	.cabinet .feature:hover .feature-description {
		color: #4d7285
	}
	.cabinet .total-price {
		display: table;
		margin-bottom: 21px;
		width: 100%;
		border: 1px solid #d6decf;
		border-spacing: 15px;
		border-radius: 3px;
		background-color: #eff6e9;
		color: #63695e
	}
	.cabinet .invoice_item_description .i {
		content: "";
		width: 16px;
		height: 16px;
		display: inline-block;
		vertical-align: -2px;
		margin-left: 4px;
		background: url('data:image/svg+xml,<svg baseProfile="basic" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16"><path class="st0" d="M8 0C3.6 0 0 3.6 0 8s3.6 8 8 8 8-3.6 8-8-3.6-8-8-8zm0 15c-3.9 0-7-3.1-7-7s3.1-7 7-7 7 3.1 7 7-3.1 7-7 7zm-.7-2.3h1.3v-1.3H7.3v1.3zM8 3.3c-.8 0-1.4.2-1.9.6-.5.4-.7 1-.7 1.6h1.2c0-.4.1-.7.4-.9.3-.2.6-.3 1-.3s.8.1 1 .4c.2.2.4.6.4 1s-.1.7-.3 1c-.1.3-.4.7-.8 1.2-.5.4-.7.7-.8.9s-.2.7-.2 1.3h1.3c0-.4 0-.7.1-.9 0-.2.2-.4.4-.6.5-.5.9-.9 1.2-1.4.3-.5.4-1 .4-1.5 0-.7-.3-1.3-.7-1.7-.5-.5-1.1-.7-2-.7z" fill="%2373C2DA"/></svg>')
	}
	.cabinet .div_invoice {
		display: table-header-group
	}
	.cabinet .total-price .invoice_item,
	.cabinet .total-price .total_price_container {
		display: table-row;
		font-size: 13px
	}
	.cabinet .total-price .invoice_item_description,
	.cabinet .total-price .invoice_item_cost,
	.cabinet .total-price .total-price-header,
	.cabinet .total-price .total-price-sum {
		display: table-cell;
		width: 50%
	}
	.cabinet .total-price .invoice_item_description,
	.cabinet .total-price .total-price-header {
		text-align: right
	}
	.cabinet .total-price .invoice_item_cost:before {
		margin-left: -10px;
		content: " = "
	}
	.cabinet .total-price .invoice_item_cost {
		font-size: 12px
	}
	.cabinet .total-price .invoice_item_cost b {
		font-weight: bold;
		font-size: 13px
	}
	.cabinet .total-price .total-price-sum {
		padding-bottom: 15px;
		color: #3e454c;
		font-size: 24px
	}
	.cabinet .form-box-pay.radios {
		width: auto;
		table-layout: fixed
	}
	.cabinet .form-box-pay label {
		text-indent: -9999px
	}
	.cabinet .form-box-pay label .ui-button-text {
		display: none
	}
	.cabinet .form-box-pay #ps_paypal_btn:before,
	.cabinet .form-box-pay #ps_moneybookers_btn:before,
	.cabinet .form-box-pay #ps_realex_btn:before,
	.cabinet .form-box-pay #ps_realex_btn:after {
		position: relative;
		display: block;
		float: left;
		margin: 0 5px;
		height: 16px;
		background: transparent url(/css/common/cs/images/cabinet_spr.png) 999px 999px no-repeat;
		content: " ";
		text-indent: 0
	}
	.cabinet .form-box-pay #ps_paypal_btn:before {
		top: 10px;
		left: 50%;
		margin-left: -30px;
		width: 61px;
		background-position: 0 -197px
	}
	.cabinet .form-box-pay #ps_moneybookers_btn:before {
		top: 5px;
		left: 50%;
		margin-left: -30px;
		width: 60px;
		height: 32px;
		background-position: -120px -202px
	}
	.cabinet .form-box-pay #ps_realex_btn {
		padding-left: 5%;
		width: 140px
	}
	.cabinet .form-box-pay #ps_paypal_btn {
		width: 100px;
		height: 32px
	}
	.cabinet .form-box-pay #ps_moneybookers_btn {
		width: 100px
	}
	.cabinet .form-box-pay #ps_realex_btn:before {
		margin-right: 0;
		content: "Credit Card";
		font-weight: bold
	}
	.cabinet .form-box-pay #ps_realex_btn:after {
		color: #373737;
		color: rgba(0, 0, 0, .78);
		content: "by Realex Payments";
		font-size: 12px;
		line-height: 19px
	}
	.cabinet .submit-agreement {
		float: right;
		margin-bottom: 13px;
		max-width: 400px;
		text-align: right;
		font-size: 11px;
		line-height: 16px
	}
	.cabinet .orderform-buttons {
		clear: both
	}
	.cabinet .orderform-submit {
		float: right;
		clear: right
	}
	.cabinet .to_billing_processing {
		float: right;
		margin: 12px
	}
	.orderform h4.features-header {
		position: relative;
		top: 0;
		left: 0
	}
	.formtitle {
		margin-bottom: 15px
	}
	.form-data-restored-notice {
		background: #fdf6e0;
		padding: 10px 20px;
		display: flex;
		justify-content: space-between;
		align-items: center;
		box-shadow: 0 3px 16px rgba(90, 63, 0, 0.15), inset 0 0 0 1px #f1e1af, 0 0 7px rgba(0, 0, 0, 0.09);
		z-index: 5;
		position: fixed;
		bottom: 20px;
		left: 50%;
		width: 300px;
		margin-left: -150px
	}
	.form-data-restored-notice .date-time {
		white-space: nowrap
	}
	.form-data-restored-notice .close {
		position: absolute;
		top: 2px;
		right: 2px;
		width: 22px;
		height: 22px;
		text-align: center;
		color: #A05151;
		font-size: 22px;
		font-weight: 100;
		opacity: 0.7;
		cursor: pointer
	}
	.form-data-restored-notice .close:hover {
		opacity: 1
	}
	.form-data-restored-notice .close:after {
		content: "×";
		line-height: 22px
	}
	.step2_total_price {
		background-color: #f8f8f8;
		padding: 15px 20px 32px;
		border: 1px solid #dedede;
		border-top: 0
	}
	.step2_total_price_blocks {
		margin-bottom: 10px
	}
	.step2_total_price_blocks:after {
		content: '';
		clear: both;
		display: table
	}
	.step2_total_price_block {
		width: 70%;
		float: left
	}
	.step2_total_price_place {
		width: 30%;
		text-align: right;
		white-space: nowrap
	}
	.step2_total_price_block p {
		font-family: "Roboto", Arial, sans-serif;
		color: #999ca0;
		font-size: 12px;
		line-height: 26px
	}
	.step2_total_price_place p {
		font-size: 14px;
		color: inherit
	}
	.step2_total_price_place p .total-price-sum {
		font-size: 20px;
		color: #5e8c31
	}
	@media (max-width: 992px) {
		.step2_total_price_block,
		.step2_total_price_place {
			width: 50%
		}
		.step2_total_price_text p {
			line-height: 120%
		}
	}
	@media (max-width: 580px) {
		.step2_total_price {
			background: none;
			padding: 15px 0 32px;
			border: 0
		}
	}
	@media (max-width: 460px) {
		.step2_total_price_block,
		.step2_total_price_place {
			float: none;
			width: 100%
		}
		.step2_total_price_place {
			text-align: center;
			margin-top: 5px
		}
	}
	#ext_time {
		color: #5e8c31
	}
	@media (min-width: 400px) {
		#fieldset_s2 {
			padding-bottom: 10px;
			position: relative
		}
		#box_slidesreq,
		#box_chartsreq {
			margin-top: 15px
		}
		#box_chartsreq {
			position: absolute;
			bottom: 10px;
			right: 20px;
			width: 50%
		}
	}
	.cabinet .orderform-auth-container {
		clear: both;
		margin-bottom: 20px;
		padding: 20px;
		border: 1px solid #e6e6e6;
		border-radius: 3px;
		border-top-left-radius: 0;
		background-color: #fafafa
	}
	.cabinet .forgot-password-link {
		font-size: 12px
	}
	.cabinet .price-free:after {
		display: inline-block;
		padding-left: 5px;
		color: #008000;
		content: "FREE";
		font-weight: bold
	}
	.cabinet .invoice_item .price-free:after {
		position: absolute
	}
	.cabinet .price-free {
		text-decoration: line-through
	}
	.cabinet .total_price_no_discounts_box {
		margin-left: 5px;
		text-decoration: line-through
	}
	.cabinet .more-button {
		text-align: center
	}
	.cabinet .more-button .blockOverlay {
		background-color: white !important;
		opacity: 1 !important
	}
	.cabinet .more-button .button {
		margin: 0
	}
	.cabinet .price-value {
		color: #538521;
		font-weight: bold;
		font-size: 16px;
		line-height: 18px
	}
	.cabinet .prog-delivery-buttons input.large[type=submit],
	.cabinet .prog-delivery-buttons .large {
		padding: 6px 20px;
		height: auto
	}
	.cabinet .prog-delivery-buttons .cancel-button {
		line-height: 23px
	}
	.cabinet .prog-delivery-buttons .cancel-button:before {
		margin-top: 3px
	}
	.cabinet .writer-category .ui-button {
		padding: 9px 0 8px;
		width: 100px;
		box-shadow: inset 0 0 0 rgba(0, 0, 0, 0), inset 0 -3px 10px 2px rgba(236, 241, 241, .9), inset 0 0px 30px rgba(0, 80, 120, .13);
		font-size: 16px
	}
	.cabinet .writer-category .ui-button.ui-state-active {
		box-shadow: inset 0 3px 7px rgba(0, 70, 106, .28), inset 0 1px 3px rgba(0, 50, 76, .05)
	}
	.cabinet .writer-category .ui-button .ui-button-text {
		position: relative;
		display: block
	}
	.cabinet .writer-category .ui-button .writer_preferences_text {
		top: 100%;
		display: block;
		margin: 8px 10px 0;
		padding-top: 5px;
		border-top: 1px solid #dcd8d8;
		border-right: 0;
		border-radius: 0 0 3px 3px;
		color: #978485;
		white-space: normal;
		font-size: 12px
	}
	.cabinet .writer-category .ui-button .writer_preferences_text strong {
		display: block;
		margin-top: 5px;
		font-weight: normal;
		font-size: 20px;
		line-height: 20px
	}
	.cabinet .writer-category .ui-state-active .writer_preferences_text {
		border-color: #dce2e4;
		background: none;
		color: #8c9aa1
	}
	.cabinet .writer-category .ui-state-active + input + .ui-button .writer_preferences_text {
		border-left-color: #dce2e4
	}
	.cabinet .writer-category input[type="radio"][disabled] + label .writer_preferences_text,
	.cabinet .writer-category input[type="radio"][disabled] + label:hover .writer_preferences_text {
		border-color: rgba(0, 29, 45, 0.05);
		background: #efefef;
		box-shadow: none;
		color: #a9a9a9;
		text-shadow: 0 1px 1px rgba(255, 255, 255, 0.7);
		cursor: default
	}
	.cabinet .notice_p {
		width: 75%;
		color: #888;
		text-align: center;
		font-size: 12px
	}
	.cabinet .subtext-discounts {
		margin-left: 3px;
		color: #888;
		font-style: italic
	}
	.cabinet .realex_payment_notification {
		visibility: visible;
		padding: 0 0 10px;
		font-size: 12px;
		line-height: 120%
	}
	.cabinet .errors a {
		color: #fff;
		border-color: #fff
	}
	.cabinet .sent {
		background-color: #f0ffe5;
		border-bottom: 1px solid #dfe5e6;
		color: #579e27;
		margin: 0 -10px;
		padding: 10px;
		text-align: center
	}
	.cabinet .sent i {
		display: inline-block;
		position: relative;
		top: 1px;
		width: 14px;
		height: 14px;
		background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0i0KjQsNGAXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMTRweCIgaGVpZ2h0PSIxNHB4IiB2aWV3Qm94PSI0NC4yOTYgMjcuNDQ0IDE0IDE0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDQ0LjI5NiAyNy40NDQgMTQgMTQiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnIGlkPSJZb3VyX0ljb24iPjwvZz48ZyBpZD0iTGF5ZXJfMiI+PHBhdGggZmlsbD0iIzU3OUUyNyIgZD0iTTQ4LjU4NiwzNi40MzNjMCwwLDUuMzQtNy43MjksOS4zMzQtOC40OTJsMC4yMzQsMC4yODFjMCwwLTMuMTY5LDIuODc5LTUuNTI2LDUuOTkxYy0yLjQ2NSwzLjI1Mi0zLjYxNSw2Ljc3My00LjI5OSw2LjczNWMtMC45MzEtMC4wNjctMi4wMzYtNi4xNzYtMy44MTMtNS40NTFjLTAuMzc1LTAuMDk0LDAuNjQxLTEuNTc0LDIuMDU5LTEuMjRDNDcuNDY0LDM0LjQ2Nyw0OC41ODYsMzYuNDMzLDQ4LjU4NiwzNi40MzN6Ii8+PC9nPjwvc3ZnPg==)
	}
	.cabinet .ui-dialog {
		position: absolute;
		z-index: 102;
		overflow: hidden;
		padding: 20px;
		border-radius: 8px;
		background: white;
		box-shadow: 0 5px 20px rgba(0, 0, 0, 0.5);
		-webkit-transition: top linear .2s, left linear .2s;
		-moz-transition: top linear .2s, left linear .2s;
		transition: top linear .2s, left linear .2s
	}
	.cabinet .ui-dialog form {
		margin-bottom: 0
	}
	.cabinet .ui-dialog-titlebar {
		margin: 0 0 21px;
		padding-right: 21px;
		font: normal 16px/21px 'Roboto', sans-serif;
		cursor: move
	}
	.cabinet .send-button {
		float: right
	}
	.cabinet .ui-dialog-titlebar-close {
		position: absolute;
		top: 5px;
		right: 5px;
		display: inline-block;
		padding: 0 10px;
		width: 40px;
		height: 40px;
		border: 1px solid transparent;
		border-radius: 999px;
		background: transparent none;
		vertical-align: top;
		text-align: center;
		transition: transform 0.3s linear
	}
	.cabinet .ui-dialog-titlebar-close.ui-state-active,
	.cabinet .ui-dialog-titlebar-close:hover {
		border-color: rgba(178, 33, 25, .3);
		background: transparent none;
		box-shadow: none;
		transition: border-color .5s ease, transform 0.3s linear
	}
	.cabinet .ui-button.ui-dialog-titlebar-close:active {
		padding: 0 10px;
		box-shadow: none
	}
	.cabinet .ui-icon-closethick {
		position: absolute;
		top: 50%;
		left: 50%;
		display: inline-block;
		overflow: hidden;
		margin: -8px 0 0 -8px;
		width: 17px;
		height: 16px;
		background: transparent url(/css/common/cs/images/cabinet_spr.png) -122px -122px no-repeat;
		text-indent: -9999px
	}
	.cabinet .ie8 .ui-icon-closethick {
		margin: 1px 0 0 -8px
	}
	.cabinet .ui-icon-closethick+.ui-button-text {
		display: none
	}
	.cabinet .ui-icon-closethick:hover {
		background-position: -122px -138px
	}
	.cabinet .ui-widget-overlay {
		position: fixed;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		z-index: 101;
		background-color: #00070a;
		opacity: .45
	}
	.cabinet .ie8 .ui-widget-overlay {
		-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=45)";
		filter: alpha(opacity=45)
	}
	.ui-dialog[aria-describedby='dialog-bad-discipline'],
	.ui-dialog[aria-describedby='dialog-bad-academiclevel'],
	.ui-dialog[aria-describedby='dialog-bad-academiclevel-pages'],
	.ui-dialog[aria-describedby='unpaid_popup'] {
		padding: 25px 20px
	}
	.ui-dialog[aria-describedby='unpaid_popup'] {
		max-width: none
	}
	.ui-dialog[aria-describedby='dialog-low-academiclevel-step3'] .ui-dialog-titlebar .ui-dialog-title,
	.ui-dialog[aria-describedby='dialog-low-academiclevel'] .ui-dialog-titlebar .ui-dialog-title,
	.ui-dialog[aria-describedby='dialog-bad-discipline'] .ui-dialog-titlebar .ui-dialog-title,
	.ui-dialog[aria-describedby='dialog-bad-academiclevel'] .ui-dialog-titlebar .ui-dialog-title,
	.ui-dialog[aria-describedby='dialog-bad-academiclevel-pages'] .ui-dialog-titlebar .ui-dialog-title,
	.ui-dialog[aria-describedby='unpaid_popup'] .ui-dialog-titlebar .ui-dialog-title {
		padding: 0
	}
	.ui-dialog[aria-describedby='dialog-low-academiclevel-step3'] .ui-dialog-titlebar .ui-dialog-title:before,
	.ui-dialog[aria-describedby='dialog-low-academiclevel'] .ui-dialog-titlebar .ui-dialog-title:before,
	.ui-dialog[aria-describedby='dialog-bad-discipline'] .ui-dialog-titlebar .ui-dialog-title:before,
	.ui-dialog[aria-describedby='dialog-bad-academiclevel'] .ui-dialog-titlebar .ui-dialog-title:before,
	.ui-dialog[aria-describedby='dialog-bad-academiclevel-pages'] .ui-dialog-titlebar .ui-dialog-title:before,
	.ui-dialog[aria-describedby='unpaid_popup'] .ui-dialog-titlebar .ui-dialog-title:before {
		display: none
	}
	.ui-dialog[aria-describedby='dialog-bad-discipline'] .ui-dialog-buttonset,
	.ui-dialog[aria-describedby='dialog-bad-academiclevel'] .ui-dialog-buttonset,
	.ui-dialog[aria-describedby='dialog-low-academiclevel-step3'] .ui-dialog-buttonset,
	.ui-dialog[aria-describedby='dialog-low-academiclevel'] .ui-dialog-buttonset {
		text-align: center
	}
	.ui-dialog[aria-describedby='dialog-low-academiclevel-step3'] .ui-dialog-buttonset .ui-button,
	.ui-dialog[aria-describedby='dialog-low-academiclevel'] .ui-dialog-buttonset .ui-button {
		margin: 0 4px
	}
	.ui-dialog[aria-describedby='dialog-bad-discipline'] .ui-dialog-buttonset .ui-button,
	.ui-dialog[aria-describedby='dialog-bad-academiclevel'] .ui-dialog-buttonset .ui-button {
		padding-left: 8px;
		padding-right: 8px;
		margin: 0 2px
	}
	.ui-dialog[aria-describedby='dialog-bad-academiclevel-pages'] .ui-dialog-buttonset {
		margin-top: 25px
	}
	.ui-dialog[aria-describedby='dialog-bad-academiclevel-pages'] .ui-dialog-buttonset .ui-button {
		padding-left: 5px;
		padding-right: 5px;
		width: 100%
	}
	.ui-dialog-titlebar-close:hover {
		-moz-transform: rotate(-90deg);
		-webkit-transform: rotate(-90deg);
		-ms-transform: rotate(-90deg);
		transform: rotate(-90deg);
		zoom: 1;
		border: 0;
		box-shadow: none;
		-webkit-transition: all .3s linear;
		-moz-transition: all .3s linear;
		-ms-transition: all .3s linear;
		transition: all .3s linear
	}
	.ui-dialog-titlebar-close * {
		border: 0;
		padding: 0;
		margin: 0;
		background: none;
		width: auto;
		height: auto;
		position: static
	}
	.ui-dialog[aria-describedby="customer_upload_dialog"] {
		max-width: 600px;
		overflow: visible;
		display: none;
		overflow: visible
	}
	.additional_materials_style,
	.additional_materials_style .hidden {
		display: none
	}
	.ui-dialog[aria-describedby="customer_upload_dialog"] .additional_materials_style {
		display: block
	}
	.cabinet .unpaid-popup-orders {
		display: table;
		margin: -13px 0;
		width: 100%;
		border-spacing: 0 13px;
		border-collapse: separate;
		table-layout: fixed
	}
	.cabinet .unpaid-popup-row {
		white-space: nowrap
	}
	.cabinet .unpaid-popup-info,
	.cabinet .unpaid-popup-buttons {
		min-width: 5em;
		border: 1px solid #e6e6e6;
		background-color: #f4f4f4;
		vertical-align: middle
	}
	.cabinet .unpaid-popup-buttons .pay-button {
		min-width: 9em;
		max-width: 9em;
		text-align: left
	}
	.cabinet .unpaid-popup-buttons .pay-button span {
		font-size: 12px
	}
	.cabinet .unpaid-popup-info {
		padding: 8px 15px;
		border-right-width: 0;
		border-radius: 3px 0 0 3px
	}
	.cabinet .unpaid-popup-info h5 {
		margin-bottom: 0
	}
	.cabinet .unpaid-popup-info .order-id {
		display: inline-block;
		float: left
	}
	.cabinet .unpaid-popup-info .order-id:hover {
		border: 0
	}
	.cabinet .unpaid-popup-title {
		display: block;
		overflow: hidden;
		text-overflow: ellipsis
	}
	.cabinet .unpaid-popup-buttons {
		padding-right: 4px;
		padding-left: 0;
		width: 230px;
		border-left-width: 0;
		border-radius: 0 3px 3px 0;
		text-align: right
	}
	.cabinet .unpaid-popup-buttons .button,
	.cabinet .unpaid-popup-buttons .input-model {
		margin-bottom: 0;
		font-size: 14px
	}
	.cabinet .change-forms {
		display: table;
		margin: -20px;
		width: auto;
		border-spacing: 20px
	}
	.cabinet .change-profile-box,
	.cabinet .change-password-box {
		width: 60%;
		border-spacing: 0
	}
	.cabinet .change-profile-box > .row > .cell-left,
	.cabinet .change-password-box>.row>.cel-left {
		white-space: nowrap;
		width: 1px
	}
	.cabinet .change-profile-box>.row>.cell-left {
		width: 100px
	}
	.cabinet .change-password-box>.row>.cell-right {
		width: 180px
	}
	.cabinet .change-profile-box > .row > .cell-left .left-label {
		white-space: nowrap
	}
	.cabinet .change-password-box > .row > .cell-right input {
		min-width: 100px
	}
	.cabinet .change-profile-box .input-model {
		padding-left: 0
	}
	.cabinet .my-phone-row .cell-left,
	.cabinet .my-phone-row .cell-right {
		border-top: 15px solid transparent
	}
	.cabinet .phone-wrapper {
		margin-top: 21px
	}
	.cabinet .phone-wrapper .cell {
		padding-left: 10px
	}
	.cabinet .phone-wrapper .cell:first-child {
		padding-left: 0
	}
	.cabinet .small-label {
		position: relative;
		display: block;
		margin-top: -15px;
		height: 15px;
		white-space: nowrap;
		font-size: 11px;
		line-height: 15px
	}
	.cabinet .country-wrapper {
		min-width: 100px
	}
	.cabinet .plus-wrapper {
		width: 10px
	}
	.cabinet .plus {
		position: relative;
		right: -4px;
		padding-right: 0;
		padding-left: 0
	}
	.cabinet .tel-number-wrapper {
		min-width: 90px
	}
	.cabinet .phone-required-false {
		display: none
	}
	.cabinet .change-password-box {
		width: 40%
	}
	.cabinet .new-password-row .cell-left,
	.cabinet .new-password-row .cell-right {
		border-top: 15px solid transparent
	}
	.cabinet .disclamer-box {
		margin-top: 21px;
		padding-bottom: 0;
		text-align: center
	}
	.cabinet .disclamer-box h4 {
		margin-bottom: 5px
	}
	.cabinet .disclamer-box p {
		font-size: 12px
	}
	.discount-badges {
		display: table;
		margin: 0 -20px;
		width: 100%;
		border-spacing: 20px
	}
	.discount-badge {
		display: table-cell;
		border-spacing: 0;
		vertical-align: top
	}
	.discount-badge dt {
		display: table-cell;
		margin: -1px 5px -1px -1px;
		padding: 5px 2px;
		min-width: 33px;
		border: 1px solid #83b034;
		border-radius: 3px 0 0 3px;
		background-color: #8dc42d;
		color: white;
		vertical-align: top;
		text-align: center;
		text-shadow: 0 -1px 1px #6c9328;
		font-weight: bold
	}
	.discount-badge dd {
		display: table-cell;
		padding: 5px;
		width: 100%;
		border: 1px solid #ede9e5;
		border-radius: 0 3px 3px 0;
		background-color: #faf9f5;
		vertical-align: top
	}
	@media screen and (min-width: 600px) {
		.discount-table {
			position: relative;
			height: 50px;
			border-top: 3px solid #484848
		}
		.disc-cell {
			position: relative;
			z-index: 1;
			float: left;
			height: 50px;
			color: #fff;
			text-align: center;
			font-weight: bold
		}
		.disc-cell em.disc-cell-abs,
		.pay-bg em.disc-cell-abs {
			position: absolute;
			top: -24px;
			left: -23px;
			z-index: 2;
			display: block;
			padding: 0 0 10px;
			width: 50px;
			height: 20px;
			background: url(/css/common/img/sprite2.png) -183px -1047px no-repeat;
			color: #000;
			text-align: center;
			font-weight: bold;
			font-style: normal;
			font-size: 14px
		}
		.pay-bg em.disc-cell-abs {
			right: -25px;
			left: auto;
			background: url(/css/common/img/sprite2.png) -180px -1083px no-repeat
		}
		.disc-cell em {
			display: inline-block;
			padding-top: 15px;
			vertical-align: middle;
			font-style: italic;
			font-size: 16px
		}
		.disc-cell0 {
			width: 15%;
			background-color: #a7a7a7
		}
		.disc_0 {
			color: #a7a7a7
		}
		.disc-cell5 {
			width: 15%;
			background-color: #639dd0
		}
		.disc_5 {
			color: #639dd0
		}
		.disc-cell10 {
			width: 30%;
			background-color: #90c13b
		}
		.disc_10 {
			color: #90c13b
		}
		.disc-cell15 {
			width: 30%;
			background: #d94d4d
		}
		.disc-cell15>em:first-child {
			padding-left: 10%
		}
		.disc_15 {
			color: #d94d4d
		}
		.disc-cell-more {
			width: 10%;
			background: #d94d4d
		}
		.pay-bg {
			position: absolute;
			top: 0;
			left: 0;
			z-index: 9;
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
			width: 50%;
			height: 50px;
			border: 1px solid #484848;
			border-top: 0;
			background: url(/css/common/img/pay-bg.png) 0 0
		}
		.my_dicont_line_status {
			position: relative;
			top: auto;
			left: auto;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
			margin: 0;
			padding: 0;
			font-size: 13px;
			font-family: Arial, Helvetica, sans-serif
		}
		.revert .my_dicont_line_status {
			right: -350px
		}
		.my_dicont_line_status_new {
			-moz-box-sizing: border-box;
			box-sizing: border-box;
			margin: 0;
			padding: 0;
			padding-right: 57px;
			height: 55px;
			background: url(/css/common/img/sprite2.png) 100% -1406px no-repeat
		}
		.my_dicont_line_status_ {
			display: none
		}
		.my_dicont_line_status_new>b {
			display: block;
			height: 2px
		}
		.my_dicont_newTotal {
			position: absolute;
			top: -43px;
			right: 57px;
			padding-bottom: 5px;
			height: auto;
			border-bottom: 1px solid #979292
		}
		.revert .my_dicont_line_status_new {
			padding-right: 0;
			padding-left: 57px;
			background: url(/css/common/img/sprite2.png) 0 -1463px no-repeat
		}
		.revert .my_dicont_newTotal {
			right: auto;
			left: 57px
		}
		.my_dicont_newTotal em {
			font-style: normal
		}
		.discounts-trees .clear {
			position: relative;
			z-index: 1
		}
		.discounts-trees .birds {
			position: relative;
			z-index: 2
		}
		.discounts-trees {
			margin: 0 0 70px;
			padding: 15px 0 0;
			border-radius: 4px;
			background: #E7F6FE
		}
		.discounts-trees .my_dicont_line {
			position: relative;
			margin: 0;
			padding: 0 0 0 75px;
			border-bottom: 6px solid #534741;
			background: #e7f6fe
		}
		.discounts-trees .pay-bg {
			border: none;
			background: none
		}
		.discounts-trees .pay-bg span.disc-cell-abs {
			position: absolute;
			top: -65px;
			right: -3px;
			bottom: auto;
			display: block;
			width: 50px;
			height: 48px;
			background: url(/css/common/img/sprite-discount.png) 0 -446px no-repeat
		}
		.discounts-trees .pay-bg span.disc-cell-abs:after {
			position: absolute;
			top: 7px;
			left: 9px;
			display: block;
			width: auto;
			height: auto;
			color: #fff;
			content: "you here";
			text-transform: uppercase;
			font-size: 13px;
			font-family: Arial, Helvetica, sans-serif;
			line-height: 100%
		}
		.discounts-trees .pay-bg span.disc-cell-abs10 {
			top: -60px;
			width: 44px;
			height: 52px;
			background-position: -50px -447px
		}
		.discounts-trees .pay-bg span.disc-cell-abs10:after {
			font-size: 12px
		}
		.discounts-trees .pay-bg span.disc-cell-abs5 {
			top: -57px;
			width: 41px;
			height: 49px;
			background-position: -96px -447px
		}
		.discounts-trees .pay-bg span.disc-cell-abs5:after {
			font-size: 11px
		}
		.discounts-trees .pay-bg span.disc-cell-abs0 {
			top: -45px;
			width: 38px;
			height: 28px;
			background-position: -139px -448px
		}
		.discounts-trees .pay-bg span.disc-cell-abs0:after {
			top: 6px;
			font-size: 10px
		}
		.discounts-trees .pay-bg span.disc-cell-abs0.disc-cell-abs0-revert {
			top: -55px;
			right: -40px;
			height: 50px;
			background-position: -178px -448px
		}
		.discounts-trees .pay-bg span.disc-cell-abs0.disc-cell-abs0-revert:after {
			left: 3px
		}
		.discounts-trees .pay-bg span.disc-cell-abs0.disc-cell-abs0-small {
			right: -48px
		}
		.discounts-trees .disc-cell em.disc-cell-abs {
			position: absolute;
			top: auto;
			right: auto;
			bottom: -40px;
			left: 0;
			z-index: 9;
			display: block;
			padding: 0;
			width: 100%;
			height: 38px;
			border-bottom: 2px solid #fff;
			background: url(/css/common/img/sprite-discount.png) 0 -692px no-repeat;
			color: #fff;
			font-weight: normal;
			font-style: normal;
			font-size: 22px;
			line-height: 215%
		}
		.discounts-trees .disc-cell15 {
			width: 40%
		}
		.discounts-trees .disc-cell0 em.disc-cell-abs {
			background: none
		}
		.discounts-trees .disc-cell em:first-child {
			position: absolute;
			top: 0;
			left: 0;
			z-index: 5;
			display: block;
			padding: 50px 0 5px;
			width: 100%;
			height: auto;
			color: #4D4D4D;
			text-align: center;
			text-transform: uppercase;
			font-style: normal;
			font-size: 47px;
			font-family: Arial, Helvetica, sans-serif;
			line-height: 85%
		}
		.discounts-trees .disc-cell15.disc-active em:first-child {
			padding: 38px 0 5px
		}
		.discounts-trees .disc-cell em:first-child b {
			position: relative;
			display: block;
			margin-top: -17px;
			font-size: 22px;
			line-height: 23px
		}
		.discounts-trees .disc-cell10 em:first-child {
			padding-top: 85px;
			font-size: 43px
		}
		.discounts-trees .disc-cell10 em:first-child b {
			font-size: 16px
		}
		.discounts-trees .disc-cell10.disc-active em:first-child b {
			line-height: 100%
		}
		.discounts-trees .disc-cell5 em:first-child {
			padding-top: 107px;
			font-size: 33px
		}
		.discounts-trees .disc-cell5 em:first-child b {
			font-size: 14px
		}
		.discounts-trees .disc-cell5.disc-active em:first-child b {
			padding-top: 5px;
			line-height: 100%
		}
		.discounts-trees .disc-cell0 em:first-child {
			padding-top: 147px;
			font-size: 20px
		}
		.discounts-trees .disc-cell0 em:first-child b {
			font-size: 11px
		}
		.discounts-trees .disc-cell.disc-active em:first-child {
			color: #fff
		}
		.discounts-trees .speech {
			position: relative;
			float: right;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
			margin-right: 75px;
			margin-bottom: -40px;
			padding: 20px 0;
			min-height: 206px;
			width: 380px;
			border: 2px solid #b9e5fb;
			border-radius: 190px / 103px;
			background-color: #fff;
			text-align: center;
			line-height: 130%
		}
		.discounts-trees .speech:first-child {
			float: left;
			margin-right: 0;
			margin-left: 75px
		}
		.discounts-trees .speech:before,
		.bird .wing,
		.discounts-trees .disc-cell:after {
			position: absolute;
			right: -1px;
			bottom: 40px;
			display: block;
			width: 30px;
			height: 26px;
			background: url(/css/common/img/sprite-discount.png) -289px -409px no-repeat;
			content: ' '
		}
		.discounts-trees .speech:first-child:before {
			right: auto;
			left: -1px;
			background-position: -320px -409px
		}
		.discounts-trees .c-title-green,
		.discounts-trees .c-title-black,
		.discounts-trees .c-text {
			display: block;
			color: #808080;
			text-align: center;
			font-weight: normal;
			font-size: 12px
		}
		.discounts-trees .c-title-green,
		.discounts-trees .c-title-black {
			color: #484848;
			text-transform: uppercase;
			font-weight: bold;
			font-size: 24px;
			line-height: 110%
		}
		.discounts-trees .c-title-green {
			color: #8CC63F
		}
		.discounts-trees .c-text {
			display: table-cell;
			padding-top: 0;
			width: 377px;
			height: 85px;
			vertical-align: middle;
			text-align: center;
			line-height: 130%
		}
		.discounts-trees .discount-t,
		.discounts-trees .discount-per {
			display: inline-block;
			margin-right: 5px;
			color: #484848;
			vertical-align: middle;
			font-weight: normal;
			font-size: 20px;
			line-height: 150%
		}
		.discounts-trees .discount-per {
			margin-right: -3px;
			color: #77ab2e;
			font-weight: bold
		}
		.discounts-trees .disc-code {
			display: none;
			color: #a8725c
		}
		.discounts-trees .per-code {
			display: none;
			padding: 2px 7px;
			background: #f3f3f3
		}
		.discounts-trees .disc-title {
			display: none
		}
		.discounts-trees .disc-title,
		.discounts-trees .per-per {}.infinity {
			position: relative;
			display: block;
			margin: 15px auto 0;
			width: 37px;
			height: 30px
		}
		.infinity:before,
		.infinity:after {
			position: absolute;
			top: 2px;
			left: 0;
			width: 8px;
			height: 8px;
			border: 4px solid #8e8e8e;
			border-radius: 50px 50px 0 50px;
			content: "";
			-webkit-transform: rotate(-45deg);
			-moz-transform: rotate(-45deg);
			-ms-transform: rotate(-45deg);
			transform: rotate(-45deg)
		}
		.infinity:after {
			top: 0;
			right: 0;
			left: auto;
			width: 11px;
			height: 11px;
			border-radius: 50px 50px 50px 0;
			-webkit-transform: rotate(45deg);
			-moz-transform: rotate(45deg);
			-ms-transform: rotate(45deg);
			transform: rotate(45deg)
		}
		.bird {
			position: absolute;
			right: -77px;
			bottom: -40px;
			display: block;
			width: 84px;
			height: 74px;
			background: url(/css/common/img/sprite-discount.png) -280px -192px no-repeat
		}
		.discounts-trees .speech:first-child .bird {
			right: auto;
			left: -78px;
			background-position: -280px -266px
		}
		.bird .wing {
			top: 15px;
			right: 19px;
			bottom: auto;
			left: auto;
			width: 31px;
			height: 25px;
			background-position: -226px -409px
		}
		.speech:first-child .bird .wing {
			right: auto;
			left: 19px;
			background-position: -258px -409px
		}
		.bird_cupon .wing {
			width: 40px;
			height: 34px;
			background-position: -266px -434px
		}
		.speech:first-child .bird_cupon .wing {
			background-position: -226px -434px
		}
		.discounts-trees .disc-table,
		.discounts-trees .disc-cell {
			height: 200px
		}
		.discounts-trees .disc-cell {
			position: relative;
			border-bottom: 7px solid #B3B3B3;
			background: none
		}
		.discounts-trees .disc-cell.disc-active {
			border-bottom-color: #8CC63F
		}
		.discounts-trees .disc-cell-more {
			display: none
		}
		.discounts-trees .disc-cell:after,
		.discounts-trees .disc-cell15:after {
			top: auto;
			right: auto;
			bottom: 0;
			left: 50%;
			margin-left: -91px;
			width: 182px;
			height: 191px;
			background-position: -183px 0
		}
		.discounts-trees .disc-cell15.disc-active:after {
			background-position: 0 0
		}
		.discounts-trees .disc-cell10:after {
			margin-left: -70px;
			width: 140px;
			height: 140px;
			background-position: -140px -192px
		}
		.discounts-trees .disc-cell10.disc-active:after {
			background-position: 0 -192px
		}
		.discounts-trees .disc-cell5:after {
			margin-left: -57px;
			width: 113px;
			height: 113px;
			background-position: -113px -332px
		}
		.discounts-trees .disc-cell5.disc-active:after {
			background-position: 0 -332px
		}
		.discounts-trees .disc-cell0:after {
			margin-left: -33px;
			width: 67px;
			height: 67px;
			background-position: -294px -341px
		}
		.discounts-trees .disc-cell0.disc-active:after {
			background-position: -226px -340px
		}
		.discounts-trees .my_dicont_line>div {
			position: relative;
			z-index: 2
		}
		.discounts-trees .my_dicont_line>div.clr {
			display: none
		}
		.discounts-trees .cloud01,
		.discounts-trees .cloud02,
		.discounts-trees .cloud03,
		.discounts-trees .cloud04,
		.discounts-trees .cloud05 {
			position: absolute;
			top: -165px;
			left: 15px;
			z-index: 1;
			display: block;
			width: 154px;
			height: 94px;
			background: url(/css/common/img/sprite-discount.png) 0 -514px no-repeat
		}
		.discounts-trees .cloud02 {
			top: -115px;
			left: 250px;
			width: 210px;
			height: 122px;
			background-position: -154px -504px
		}
		.discounts-trees .cloud03 {
			top: -40px;
			left: 550px
		}
		.discounts-trees .cloud04,
		.discounts-trees .cloud05 {
			top: -170px;
			right: -15px;
			left: auto;
			width: 113px;
			height: 73px;
			background-position: 0 -620px
		}
		.discounts-trees .cloud05 {
			top: -175px;
			right: 360px
		}
		.discounts-trees .pay-bg {
			top: auto;
			bottom: -43px;
			height: 33px
		}
		.discounts-trees .discount-table {
			height: 207px;
			border: none;
			border-bottom: 33px solid #998675
		}
		.discounts-trees .my_dicont_newTotal {
			top: auto;
			right: auto;
			bottom: -95px;
			left: 50%;
			margin: 0 0 0 -100px;
			padding: 15px 0 0;
			width: 200px;
			height: 35px;
			border: none;
			color: #534741;
			font-size: 18px
		}
		.discounts-trees .my_dicont_newTotal:after {
			position: absolute;
			top: 0;
			left: 50%;
			display: block;
			margin-left: -8px;
			width: 16px;
			height: 15px;
			background: url(/css/common/img/sprite-discount.png) -113px -625px no-repeat;
			content: ""
		}
		.discounts-trees .my_dicont_newTotal,
		.discounts-trees .my_dicont_newTotal:after {
			display: none
		}
		.discounts-trees .disc-active .my_dicont_newTotal,
		.discounts-trees .disc-active .my_dicont_newTotal:after {
			display: block
		}
		.discounts-trees .my_dicont_newTotal b {
			color: #8CC63F
		}
		.discounts-trees .my_dicont_line_status {
			position: absolute;
			top: 50px;
			right: 0;
			width: auto
		}
		.discounts-trees .cloud-text {
			position: absolute;
			top: -100px;
			right: 11%;
			z-index: 9;
			margin: 0;
			padding: 0;
			width: 250px;
			color: #4D4D4D;
			text-transform: uppercase;
			font-weight: bold;
			font-size: 24px
		}
		.discounts-trees .cloud-text a,
		.discounts-trees .cloud-text a:hover,
		.discounts-trees .cloud-text a:visited,
		.discounts-trees .green_t {
			color: #8CC63F
		}
		.discounts-trees div.flags {
			position: absolute;
			bottom: 0;
			left: 0;
			z-index: 10;
			display: block;
			width: 75px;
			height: auto
		}
		.discounts-trees .flag-g,
		.discounts-trees .flag-w {
			display: block;
			padding: 5px;
			padding-left: 10px;
			background: #fff;
			color: #998675;
			text-align: left;
			text-transform: uppercase;
			font-weight: normal;
			font-style: normal;
			font-size: 15px;
			font-family: Arial, Helvetica, sans-serif;
			line-height: 100%
		}
		.discounts-trees .flag-g {
			background: #8CC63F;
			color: #fff
		}
		.discounts-trees .flag-g b,
		.discounts-trees .flag-w b {
			display: block;
			font-weight: normal;
			font-size: 18px
		}
		.discounts-trees .flag-g:after,
		.discounts-trees .flag-w:after {
			position: absolute;
			right: -10px;
			bottom: 0;
			display: block;
			width: 0;
			height: 0;
			border-top: 20px solid transparent;
			border-bottom: 20px solid transparent;
			border-left: 10px solid #fff;
			content: ' '
		}
		.discounts-trees .flag-g:after {
			top: 0;
			border-left-color: #8CC63F
		}
		.discounts-trees .c-cupons {
			display: block;
			margin-bottom: 6px;
			color: #484848;
			line-height: 95%
		}
		.disc-until {
			display: block;
			color: #8e8e8e
		}
	}
	.cabinet form.f.additional_payment_form {
		background-color: #f8f8f8;
		border: 4px solid #f5f5f5;
		border-radius: 4px;
		box-shadow: 0 0 0 1px #dedede inset;
		padding: 21px 20px
	}
	.cabinet form.additional_payment_form:after {
		clear: both;
		content: "";
		display: table
	}
	.cabinet form.additional_payment_form .totalprice .left-label {
		color: #5e676b;
		color: rgba(0, 0, 0, 0.53);
		font-size: 18px;
		font-weight: bold;
		height: 21px;
		line-height: 21px;
		margin-bottom: 21px;
		padding: 0
	}
	.cabinet form.additional_payment_form .totalprice .cell-left {
		border-spacing: 15px 5px;
		padding-bottom: 15px
	}
	.cabinet form.additional_payment_form .totalprice .cell-right {
		vertical-align: middle;
		font-size: 24px;
		border-spacing: 15px 5px;
		padding-bottom: 15px
	}
	.cabinet #order-call-container .order-item {
		border-bottom: 1px dotted #dedede
	}
	.cabinet .message .with-feedback {
		width: 115px
	}
	.cabinet .message .with-feedback:before {
		background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAUCAMAAABoB+dnAAAAtFBMVEUAAACXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqKXnqISHj1UAAAAO3RSTlMAAQMECg0XHiAmJygtQ0hJVFVcXWBscHJzdHeGjZWdnqCipLDMzc7P0NPV19ja3OPk6uvs7u/y9Pf7/gir3VUAAAB9SURBVHjaPctFEsJAAETRRoNrcB8IGixAkH//ezFkqvirt+iWlJ89DpeuRTaEgLnVCNgwkHJxoprUAl4hnmTgNBz/DntoKukJZSfA+6vidIO6UwA9JwPGqQPnVKIS0G9M2m5IfLymJR9gTVXKrIDdeyqpGMFne5etsIhY+l/WfRQmaeFJOQAAAABJRU5ErkJggg==) no-repeat center center;
		height: 20px;
		left: -30px;
		top: 0;
		width: 19px
	}
	.cabinet .message .new .with-feedback:before {
		background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAUCAMAAABoB+dnAAAAtFBMVEX///9DSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlFDSlF81uZIAAAAO3RSTlMAAQMECg0XHiAmJygtQ0hJVFVcXWBscHJzdHeGjZWdnqCipLDMzc7P0NPV19ja3OPk6uvs7u/y9Pf7/gir3VUAAAB9SURBVHjaPctFEsJAAETRRoNrcB8IGixAkH//ezFkqvirt+iWlJ89DpeuRTaEgLnVCNgwkHJxoprUAl4hnmTgNBz/DntoKukJZSfA+6vidIO6UwA9JwPGqQPnVKIS0G9M2m5IfLymJR9gTVXKrIDdeyqpGMFne5etsIhY+l/WfRQmaeFJOQAAAABJRU5ErkJggg==)
	}
	.cabinet .message .feedback_ft {
		float: left
	}
	#feedback_support .login-block .input_text_dig {
		width: 100%;
		margin: 0
	}
	#feedback_support .rating-box-container {
		position: relative
	}
	#feedback_support .rating-text {
		color: #7cb149;
		display: block;
		font-size: 14px;
		font-weight: 500;
		margin: 0 auto 10px;
		opacity: 0;
		padding: 0 10%;
		position: absolute;
		text-align: center;
		top: 0;
		left: 0;
		-webkit-transition: all 0.3s ease;
		transition: all 0.3s ease;
		width: 100%;
		z-index: 0
	}
	#feedback_support .rating-text.r-text-show {
		top: 68px;
		opacity: 1;
		line-height: 125%
	}
	#feedback_support .rating-box {
		padding: 15px 70px;
		background-color: #fff;
		border-radius: 2px;
		display: inline-block;
		margin-bottom: 50px;
		position: relative;
		text-align: center;
		width: 100%;
		z-index: 1
	}
	#feedback_support .rating-box:before,
	#feedback_support .rating-box:after {
		content: 'Poor';
		position: absolute;
		color: #aaa;
		font-size: 11px;
		line-height: 11px;
		height: 11px;
		left: 20px;
		right: auto;
		top: 0;
		bottom: 0;
		margin: auto
	}
	#feedback_support .rating-box:after {
		content: 'Excellent';
		left: auto;
		right: 20px
	}
	#feedback_dialog_support {
		max-width: 420px;
		margin: 0 auto
	}
	#feedback_support {
		max-height: 1000px;
		position: relative;
		margin: 0
	}
	#feedback_support,
	#feedback_support * {
		box-sizing: border-box
	}
	#feedback_support.success-feedback {
		max-height: 0 !important;
		overflow: hidden;
		margin: 0;
		transition: max-height 0.3s ease;
		-moz-transition: max-height 0.3s ease;
		-webkit-transition: max-height 0.3s ease
	}
	#feedback_support .login-submit.feed_block {
		display: inline-block;
		vertical-align: top;
		width: 100%;
		text-align: right;
		margin-top: 10px
	}
	#feedback_support.success-feedback+.success-mes {
		opacity: 1;
		transform: scale(1);
		-moz-transform: scale(1);
		-webkit-transform: scale(1);
		filter: alpha(opacity=100);
		transition: all 0.3s ease 350ms;
		-moz-transition: all 0.3s ease 350ms;
		-webkit-transition: all 0.3s ease 350ms
	}
	#reviewStars-input_support input:checked ~ label,
	#reviewStars-input_support label,
	#reviewStars-input_support label:hover,
	#reviewStars-input_support label:hover~label {
		background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPjxzdmcgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iNTJweCIgaGVpZ2h0PSIyNXB4IiB2aWV3Qm94PSIwIDAgNTIgMjUiPjxwYXRoIGlkPSJwYXRoNDA2NyIgZmlsbD0iI0JEQzNDNyIgZD0iTTIxLjA0MSwyNC45OTRsLTguMDM2LTQuMjY3TDQuOTcyLDI1bDEuNTMxLTkuMDQ1TDAsOS41NTNMOC45ODIsOC4yM0wxMi45OTYsMGw0LjAyMSw4LjIyN0wyNiw5LjU0M2wtNi40OTgsNi40MDZMMjEuMDQxLDI0Ljk5NHoiLz48cGF0aCBpZD0icGF0aDQwNjctMSIgZmlsbD0iI0YzOUMxMiIgZD0iTTQ3LjA0MSwyNC45OTRsLTguMDM2LTQuMjY3TDMwLjk3MiwyNWwxLjUzMS05LjA0NUwyNiw5LjU1M2w4Ljk4Mi0xLjMyM0wzOC45OTYsMGw0LjAyMSw4LjIyN0w1Miw5LjU0M2wtNi40OTcsNi40MDZMNDcuMDQxLDI0Ljk5NHoiLz48L3N2Zz4=) no-repeat
	}
	.no-svg #reviewStars-input_support label,
	.no-svg #reviewStars-input_support label:hover,
	.no-svg #reviewStars-input_support label:hover~label {
		background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAAZCAMAAABJlP0xAAAAXVBMVEUAAAC9w8fznBK9w8fznBK9w8fznBK9w8fznBK9w8fznBK9w8fznBK9w8fznBK9w8fznBK9w8fznBK9w8fznBK9w8fznBK9w8fznBK9w8fznBK9w8fznBK9w8fznBLAyJJ9AAAAHXRSTlMAEBAgIDAwQEBQUGBgcHCAgI+Pr6+/v8/P39/v7xbfjfEAAAEGSURBVHgBdZD/boQgEIRXW4V20ftxB3fgwfs/ZhswuhPd+Ytk8uVjh2SYSYlzpCVGrUlJazhnVkSlaKqYc1REpSRVBCoQgQpFoAIRqlAEKhCBCkSgAtFB1Znp4l95zctfJvPVmt7O1/Aua97hOttv+s/D59P4xzOU04QncVbCrihxpFFM5FTmnFpYzAb5rCMOy4EZWjN+DsxIBBQwQAGDFDJIIdMCu3vZBFibZNAkGzTJpsuQbm/6AukFZBAye2MRsgKaEJr2ZkZoFtCtDTAMbZDb3tzbAOPYBrkTjudN/Wh94njB1o/WJ4wXf7b7Yl7keOl3uy/J+brIJMKx28ZLTjYu1fn+AJJzQzPXLTYgAAAAAElFTkSuQmCC);
		background-repeat: no-repeat
	}
	#reviewStars-input_support {
		overflow: hidden;
		*zoom: 1;
		position: relative;
		float: left;
		display: inline-block;
		width: 100%;
		height: 27px
	}
	#reviewStars-input_support input {
		filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
		opacity: 0;
		width: 26px;
		height: 25px;
		position: absolute;
		top: 0;
		z-index: 0;
		outline: none
	}
	#reviewStars-input_support input:checked~label {
		background-position: -26px 0
	}
	#reviewStars-input_support label {
		background-position: 0 0;
		height: 25px;
		width: 26px;
		float: right;
		cursor: pointer;
		margin-left: -13px;
		position: absolute;
		z-index: 1
	}
	#reviewStars-input_support label:hover,
	#reviewStars-input_support label:hover ~ label,
	.no-svg #reviewStars-input_support label.active,
	.no-svg #reviewStars-input_support label.active~label {
		background-position: -26px 0
	}
	#reviewStars-input_support label[for=star-0_support] {
		left: 12.5%
	}
	#reviewStars-input_support label[for=star-1_support] {
		left: 37.5%
	}
	#reviewStars-input_support label[for=star-2_support] {
		left: 62.5%
	}
	#reviewStars-input_support label[for=star-3_support] {
		left: 87.5%
	}
	#support_options {
		margin-bottom: 15px
	}
	#feedback_quest {
		width: 56%;
		margin-bottom: 10px
	}
	@media (max-width: 580px) {
		#feedback_quest {
			width: 100%
		}
		#feedback_quest .radios .ui-button {
			height: 38px
		}
	}
	.feedback_quest_caption {
		font-size: 16px;
		margin-bottom: 10px
	}
	.cabinet #feedback_support .radios {
		margin-bottom: 5px
	}
	#feedback_support .button {
		line-height: 100%
	}
	#feedback_support .mes_error,
	#feedback_support .mes_com_error {
		color: #e74c3c;
		font-size: 12px;
		width: 100%;
		z-index: 2;
		margin-bottom: 5px;
		text-align: center
	}
	#feedback_dialog_support .feedback-thankyou-block {
		text-align: center;
		margin: -10px 0 20px !important;
		color: #666;
		font-size: 16px
	}
	@media screen and (max-width: 800px) {
		body.cabinet {
			min-width: 320px
		}
		@media screen and (max-width: 400px) {
			.cabinet .cabinet-menu, .cabinet .cabinet-content, .cabinet #footer {
				padding-left: 10px;
				padding-right: 10px
			}
		}
		.cabinet .time-zone-notice {
			display: none
		}
		@media screen and (max-width: 600px) {
			.cabinet .cabinet-menu .cabinet-tab-link {
				font-size: 15px
			}
		}
		@media screen and (max-width: 550px) {
			.cabinet .cabinet-menu .cabinet-tab-link:before {
				display: none
			}
		}
		@media screen and (max-width: 460px) {
			.cabinet .order-page-tabs .new-count {
				position: absolute;
				top: -7px;
				right: 0
			}
			.cabinet .order-page-tabs .new-count:before {
				content: " ";
				display: block;
				width: 0;
				height: 0;
				border-style: solid;
				border-width: 4px 3px 0;
				border-color: #3998ce transparent transparent;
				position: absolute;
				bottom: -4px;
				left: 2px
			}
			.cabinet .order-page-tabs .active .new-count {
				display: none
			}
			.cabinet .order-page-tabs .active .new-count:before {
				border-top-color: #e7ebee
			}
		}
		@media screen and (max-width: 390px) {
			.cabinet .cabinet-menu .cabinet-tab-link {
				font-size: 13px;
				font-weight: bold
			}
		}
		@media screen and (max-width: 370px) {
			.cabinet .cabinet-menu .cabinet-tab-link, .cabinet .order-page-tabs .cabinet-tab-link {
				padding: 15px 5px;
				margin-right: -2px;
				font-size: 16px
			}
		}
		@media screen and (max-width: 390px) {
			.cabinet .new-count:after {
				content: none
			}
		}
		.cabinet .order {
			padding-top: 0
		}
		.cabinet .order .button,
		.cabinet .order .input-model {
			height: 36px;
			padding: 9px 15px 10px;
			padding-top: 10px;
			padding-bottom: 9px
		}
		.cabinet .order .button:active,
		.cabinet .order .input-model:active {
			padding: 10px 15px 9px
		}
		@media screen and (max-width: 450px) {
			.cabinet .order .messages-button, .cabinet .order .files-button, .cabinet .order .files-up-button, .cabinet .order .files-dl-button, .cabinet .order .delete-button {
				font-size: 0;
				color: transparent
			}
			.cabinet .order .messages-button:before,
			.cabinet .order .files-button:before,
			.cabinet .order .files-up-button:before,
			.cabinet .order .files-dl-button:before,
			.cabinet .order .delete-button:before {
				margin-right: 0
			}
		}
		.cabinet .order .delete-button,
		.cabinet .order .delete-button:hover,
		.cabinet .order .delete-button:active {
			width: 36px;
			height: 36px;
			padding-left: 0;
			padding-right: 0;
			text-align: center;
			font-size: 0;
			color: transparent;
			position: absolute;
			top: -1px;
			right: -16px;
			border-radius: 999px;
			background-image: -moz-linear-gradient(top, #f8f8f8, #ebebeb);
			background-image: -webkit-linear-gradient(top, #f8f8f8, #ebebeb);
			background-image: linear-gradient(to bottom, #f8f8f8, #ebebeb);
			border: 1px solid #e5e5e5
		}
		.cabinet .order .delete-button::before {
			margin-right: 0;
			background-position: -104px -122px
		}
		.cabinet .finished .order-status,
		.cabinet .finished .order-status-bar,
		.cabinet .finished .order-title,
		.cabinet .finished .order-description {
			float: none;
			width: auto
		}
		.cabinet .finished .order-status-bar {
			display: table;
			width: 100%
		}
		.cabinet .finished .order-status .button {
			position: absolute;
			margin: 0;
			bottom: 10px;
			right: 15px;
			z-index: 2
		}
		.cabinet .order-title,
		.cabinet .order-description,
		.cabinet .order-status {
			padding: 0 15px;
			width: auto;
			float: none
		}
		.cabinet .order-title {
			padding: 0
		}
		.cabinet .order-title a {
			display: block;
			padding: 7px 15px;
			height: 20px;
			-moz-box-sizing: content-box;
			box-sizing: content-box;
			background-color: #f2f2f2;
			border-bottom: 1px solid #e3e3e3;
			margin-bottom: 5px;
			overflow: hidden;
			text-overflow: ellipsis
		}
		@media screen and (min-width: 650px) {
			.cabinet .order-general-info, .cabinet .order-deadline {
				display: inline-block;
				white-space: nowrap
			}
		}
		.cabinet .order-status {
			width: 700px
		}
		@media screen and (max-width: 650px) {
			.cabinet .order-status .segment {
				display: none;
				border-radius: 3px;
				padding-right: 20px;
				border: 1px solid
			}
			.cabinet .order-status .segment:before,
			.cabinet .order-status .segment:after {
				display: none
			}
			.cabinet .order-not-paid .order-pay-segment,
			.cabinet .order-placed .order-assign-segment,
			.cabinet .in-progress .order-progress-segment,
			.cabinet .review .order-review-segment,
			.cabinet .in-review .order-review-segment,
			.cabinet .order-completed .order-approve-segment,
			.cabinet .order-dispute .order-approve-segment,
			.cabinet .approved .order-approve-segment,
			.cabinet .finished .segment {
				display: table-cell
			}
		}
		.cabinet .order-delete-n-cost {
			position: static;
			right: auto;
			bottom: auto;
			display: table-cell;
			min-width: 75px;
			padding-bottom: 25px
		}
		.cabinet .order-total-price,
		.cabinet .order-discount {
			display: block;
			margin: 0;
			padding: 0 15px
		}
		.cabinet .order-discount {
			color: #8c8c8c
		}
		.cabinet .order-discount:before,
		.cabinet .order-discount:after {
			display: none
		}
		.cabinet .order-status {
			margin-top: 0;
			display: table-cell;
			border-left: 1px solid #e6e6e6
		}
		.cabinet .order-bottom {
			margin-top: 5px;
			position: relative
		}
		.cabinet .order-id {}.cabinet .order-bottom .order-id {
			position: absolute;
			top: -31px;
			color: #51575d
		}
		.cabinet .order-bottom .order-id:after {
			display: none
		}
		.cabinet .order-header {}.cabinet .order-header-title {
			padding-right: 20px
		}
		.cabinet .order-page-id {
			float: none;
			padding: 0;
			white-space: nowrap;
			font-weight: bold;
			margin-bottom: -10px
		}
		.cabinet .order-header .order-header-status {
			margin-bottom: 10px;
			background-color: transparent
		}
		.cabinet .order-header-status-info {
			white-space: normal;
			padding-right: 10px
		}
		@media screen and (max-width: 500px) {
			.cabinet .order-header-status-info {
				clear: both
			}
		}
		.cabinet .order-header-deadline {
			display: none
		}
		.cabinet .order-actions {
			margin-bottom: 12px
		}
		.cabinet .order-actions .button,
		.cabinet .order-actions .input-model {
			height: 36px;
			padding: 9px 15px 10px;
			padding-top: 10px;
			padding-bottom: 9px
		}
		.cabinet .order-actions .button:active,
		.cabinet .order-actions .input-model:active {
			padding: 10px 15px 9px
		}
		.cabinet .order-actions .button,
		.cabinet .order-actions .input-model {
			float: none;
			margin: 0 5px 5px 0
		}
		.cabinet .order-actions .delete-button,
		.cabinet .order-actions .delete-button:hover,
		.cabinet .order-actions .delete-button:active {
			width: 36px;
			height: 36px;
			padding-left: 0;
			padding-right: 0;
			text-align: center;
			font-size: 0;
			color: transparent;
			position: absolute;
			top: 40px;
			right: 20px;
			border-radius: 999px;
			background-image: -moz-linear-gradient(top, #f8f8f8, #ebebeb);
			background-image: -webkit-linear-gradient(top, #f8f8f8, #ebebeb);
			background-image: linear-gradient(to bottom, #f8f8f8, #ebebeb);
			border: 1px solid #e5e5e5;
			box-shadow: -10px 1px 7px white
		}
		.cabinet .order-actions .delete-button::before {
			margin-right: 0
		}
		.cabinet .order-content-section {
			display: block
		}
		@media screen and (max-width: 450px) {
			.cabinet .subtabs .button {
				display: block
			}
		}
		.cabinet .order-info,
		.cabinet .prog-delivery,
		.cabinet .order-info-right,
		.cabinet .order-service-files,
		.cabinet .order-customer-files,
		.cabinet .add-services {
			display: block;
			border-left-width: 0;
			width: 100%;
			box-sizing: border-box
		}
		.cabinet .order-customer-files {
			box-shadow: inset 0 8px 7px rgba(0, 0, 0, .03)
		}
		.cabinet .message .order-item-body {
			padding-left: 20px;
			margin-right: 20px
		}
		.cabinet .order-review-pay {
			margin-right: 0
		}
		.cabinet .revision-notes,
		.cabinet .refund-notes {
			width: auto;
			float: none;
			margin-left: 40px
		}
		.cabinet .revision-deadline-label {
			float: none;
			margin-bottom: 0;
			padding-left: 0
		}
		.cabinet .revision-deadline-wrapper {
			overflow: visible
		}
		.cabinet .approve-feedback {
			padding: 15px 10px 0;
			margin: 0 -10px 21px;
			display: block
		}
		.cabinet .approve-feedback > .row,
		.cabinet .approve-feedback>.row>.cell {
			display: block;
			width: auto
		}
		.cabinet .download-doc-tip {
			margin-right: -20px
		}
		.cabinet .download-doc-tip .nobr {
			white-space: normal
		}
		@media screen and (max-width: 530px) {
			.cabinet .download-doc-tip {
				display: block;
				width: auto;
				text-align: left;
				padding-left: 50%;
				margin-left: 10px
			}
		}
		@media screen and (max-width: 450px) {
			.cabinet .title-rate.large-label {
				display: block
			}
		}
		@media screen and (max-width: 400px) {
			.cabinet .result_rate {
				display: none
			}
		}
		.cabinet .change-forms {
			display: block;
			border-spacing: 0;
			margin: 0
		}
		.cabinet .change-profile-box,
		.cabinet .change-password-box {
			display: block;
			width: 100%;
			box-sizing: border-box
		}
		.cabinet .change-profile-box>.row>.cell-left,
		.cabinet .change-password-box>.row>.cell-left {
			width: 100px
		}
		@media screen and (max-width: 550px) {
			.cabinet .change-profile-box .country-wrapper {
				display: none
			}
			.cabinet .change-profile-box,
			.cabinet .change-password-box,
			.cabinet .disclamer-box {
				padding-left: 10px;
				padding-right: 10px
			}
			.cabinet .features-block {
				padding: 15px 5px 5px
			}
		}
		@media screen and (max-width: 480px) {
			.cabinet .change-profile-box .plus_wrapper, .cabinet .change-profile-box .country-code-wrapper {
				display: none
			}
		}
		@media screen and (max-width: 400px) {
			.cabinet .change-profile-box>.row {
				display: block
			}
			.cabinet .change-profile-box>.row>.cell {
				display: block;
				width: auto
			}
			.cabinet .change-profile-box .left-label {
				display: block;
				padding: 10px 0 0;
				text-align: left;
				font-weight: bold
			}
		}
		.cabinet .orderform-tabs,
		.cabinet .orderform-prev {
			display: none
		}
		.cabinet .orderform {
			width: auto;
			min-width: 0;
			max-width: 630px;
			float: none
		}
		.cabinet .orderform .row,
		.cabinet .orderform .cell-left,
		.cabinet .orderform .cell-right {
			display: block;
			text-align: left;
			width: auto;
			clear: both
		}
		.cabinet .orderform .left-label {
			display: block;
			width: auto;
			height: auto;
			line-height: 1.5;
			padding: 0;
			text-align: left;
			margin-bottom: 4px
		}
		.cabinet .with-mobile-tip {
			overflow: hidden
		}
		.cabinet .orderform .with-mobile-tip label,
		.cabinet .orderform .with-mobile-tip .checkbox {
			background-color: #f8f8f8;
			display: inline-block;
			padding-right: 5px
		}
		.cabinet .with-mobile-tip .mobile-tip {
			display: block;
			float: right
		}
		.cabinet .feature-header,
		.cabinet .feature-price {
			float: none;
			padding: 0 0 0 25px
		}
		.cabinet .feature-price {
			padding: 0;
			margin: 10px 0 0;
			display: block;
			text-align: center
		}
		.cabinet .submit-agreement {
			max-width: none;
			float: none;
			text-align: center
		}
		.cabinet .orderform-buttons {
			text-align: center
		}
		.cabinet .orderform-submit {
			float: none
		}
		@media screen and (max-width: 580px) {
			.cabinet .visible-in-mobile {
				display: block
			}
			.cabinet .orderform {
				border-width: 0
			}
			.cabinet .orderform-step {
				border-width: 0;
				background-color: transparent;
				margin: 0;
				padding: 0
			}
			.cabinet .orderform .with-mobile-tip label,
			.cabinet .orderform .with-mobile-tip .checkbox {
				background-color: white
			}
			.cabinet .radios {
				display: block
			}
			.cabinet .radios .ui-button {
				display: block;
				max-width: 255px;
				width: 100%;
				border-width: 1px 1px 0
			}
			.cabinet .radios .ui-corner-left {
				border-radius: 3px 3px 0 0
			}
			.cabinet .radios .ui-corner-right {
				border-radius: 0 0 3px 3px;
				border-width: 1px
			}
			.cabinet .radios.writer-category .ui-button {
				width: 195px
			}
			.cabinet .radios.form-box-pay .ui-button {
				width: 100%
			}
			.cabinet .radios.form-box-pay .ui-button img {
				display: inline
			}
			.cabinet .features-header,
			.cabinet .features {
				display: none
			}
		}
		@media screen and (max-width: 650px) {
			.cabinet #footer {
				height: auto
			}
			.cabinet .footer-menu,
			.cabinet .footer-copyright {
				float: none;
				text-align: center
			}
		}
		.cabinet .unpaid-popup-orders {
			display: block;
			text-align: center
		}
		.cabinet .unpaid-popup-row {
			display: block;
			text-align: top;
			white-space: normal;
			border: 1px solid #e6e6e6;
			border-radius: 3px;
			margin: 0 0 15px;
			text-align: left;
			padding: 0 !important
		}
		.cabinet .unpaid-popup-info,
		.cabinet .unpaid-popup-buttons {
			display: block;
			vertical-align: middle;
			background-color: #f4f4f4;
			border: 0 none;
			border-radius: 0;
			width: 100%;
			text-align: left
		}
		.cabinet .unpaid-popup-buttons .pay-button {
			min-width: 9em;
			max-width: none;
			text-align: left
		}
		.cabinet .unpaid-popup-info {
			width: auto;
			border-bottom: 1px solid #e6e6e6;
			padding: 8px 15px
		}
		.cabinet .unpaid-popup-info .order-id {
			float: none;
			margin-right: 0
		}
		.cabinet .unpaid-popup-info .order-id:after {
			display: none
		}
		.cabinet .unpaid-popup-buttons {
			padding: 5px 0
		}
		@media screen and (max-width: 599px) {
			.speech {
				display: block;
				margin: 0 20px 40px;
				padding: 20px;
				vertical-align: top;
				text-align: center;
				border: 1px solid #ede9e5;
				border-radius: 0 3px 3px 0;
				background-color: #faf9f5
			}
			.speech .c-title-green,
			.speech .c-title-black {
				font-weight: 18px;
				font-weight: bold;
				text-transform: uppercase
			}
			.speech .c-title-green {
				color: #8CC63F
			}
			.speech .c-text {
				display: block
			}
			.speech .disc-code,
			.speech .disc-title,
			.speech .per-code {
				display: none
			}
			.speech .discount-per.per-per {
				font-size: 19px;
				display: block;
				margin-bottom: -10px
			}
			.flag-g,
			.flag-w,
			.disc-cell em,
			.disc-cell-abs {
				display: block;
				width: 50%;
				float: right;
				text-align: center;
				background-color: #998675;
				color: white;
				border: 2px solid white;
				font-size: 18px;
				-moz-box-sizing: border-box;
				box-sizing: border-box;
				padding: 10px
			}
			.disc-cell em,
			.disc-cell-abs {
				background-color: #91b166
			}
			.disc-cell-abs:empty {
				display: none
			}
			.disc-cell {
				display: none
			}
			.disc-cell.disc-active {
				display: block;
				overflow: hidden;
				clear: both
			}
			.my_dicont_newTotal {
				color: #534741;
				font-size: 18px;
				text-align: right;
				padding-top: 20px;
				display: block;
				clear: both
			}
			.my_dicont_newTotal b {
				color: #8CC63F
			}
			.cloud-text {
				display: none
			}
		}
		.discount-badges {
			display: block;
			border-spacing: 20px;
			margin: 0 0 21px
		}
		.discount-badge {
			display: block;
			margin-bottom: 5px
		}
		@media screen and (max-width: 760px) {
			body>div.ui-widget-content {
				width: 500px !important
			}
		}
		@media screen and (max-width: 600px) {
			body>div.ui-widget-content {
				width: 400px !important
			}
		}
		@media screen and (max-width: 480px) {
			body>div.ui-widget-content {
				width: 300px !important
			}
		}
		@media screen and (max-width: 360px) {
			body>div.ui-widget-content {
				width: 280px !important
			}
		}
	}
	@media screen and (max-width: 992px) {
		.orderform .features {
			display: block;
			margin: 0 0 21px;
			min-height: 0;
			width: 100%
		}
		.orderform input[type="checkbox"]+label.feature.large {
			display: block;
			margin-bottom: 15px;
			width: auto;
			min-height: 0
		}
		.orderform .feature-header {
			float: none;
			display: inline-block
		}
		.orderform .feature-header br {
			display: none
		}
		.orderform .feature-price {
			float: right;
			margin: 0
		}
		.orderform-bordered .qtip-pos-tl {
			display: none !important;
			opacity: 0 !important
		}
		.orderform-bordered .qtip-pos-tr {
			background-color: #e8f6fa;
			border-color: transparent;
			box-shadow: 0 0 0 1px rgba(111, 137, 147, 0.2), 0 3px 5px rgba(0, 0, 0, .1);
			color: #6f8993;
			font-size: 13px;
			padding: 10px;
			width: 40%
		}
	}
</style> 



<?php get_footer();?>