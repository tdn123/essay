<?php
CONST ATTRIBUTE_PREFIX = 'order_pa_';
CONST SUBJECTS_CODE = 'subjects';
CONST LEVELS_CODE = 'levels';


// var_dump(ATTRIBUTE_PREFIX);die;
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
	die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<?php

/*-----------------------------------------------------------------------------------*/
/* Start WooThemes Functions - Please refrain from editing this section */
/*-----------------------------------------------------------------------------------*/

// Define the theme-specific key to be sent to PressTrends.
define( 'WOO_PRESSTRENDS_THEMEKEY', 'zdmv5lp26tfbp7jcwiw51ix9sj389e712' );

// WooFramework init
require_once ( get_template_directory() . '/functions/admin-init.php' );

/*-----------------------------------------------------------------------------------*/
/* Load the theme-specific files, with support for overriding via a child theme.
/*-----------------------------------------------------------------------------------*/

$includes = array(
				'includes/theme-options.php', 			// Options panel settings and custom settings
				'includes/theme-functions.php', 		// Custom theme functions
				'includes/theme-actions.php', 			// Theme actions & user defined hooks
				'includes/theme-comments.php', 			// Custom comments/pingback loop
				'includes/theme-js.php', 				// Load JavaScript via wp_enqueue_script
				'includes/sidebar-init.php', 			// Initialize widgetized areas
				'includes/theme-widgets.php',			// Theme widgets
				'includes/theme-install.php',			// Theme installation
				'includes/theme-woocommerce.php',		// WooCommerce options
				'includes/theme-plugin-integrations.php'	// Plugin integrations
				);

// Allow child themes/plugins to add widgets to be loaded.
$includes = apply_filters( 'woo_includes', $includes );

foreach ( $includes as $i ) {
	locate_template( $i, true );
}

/*-----------------------------------------------------------------------------------*/
/* You can add custom functions below */
/*-----------------------------------------------------------------------------------*/

$new_general_setting = new new_general_setting();

class new_general_setting {
	function new_general_setting( ) {
		add_filter( 'admin_init' , array( &$this , 'register_fields' ) );
	}
	function register_fields() {
		register_setting( 'general', 'favorite_color', 'esc_attr' );
		add_settings_field('fav_color', '<label for="favorite_color">'.__('Favorite Color?' , 'favorite_color' ).'</label>' , array(&$this, 'fields_html') , 'general' );
	}
	function fields_html() {
		echo "<form></form><form  action='' method='POST' enctype='multipart/form-data' ><input type='file' name='price_table' ><input type='hidden' value='1' name='save_csv'/><input type='submit' /></form>";
	}
}

if($_POST['save_csv']){
	$file = fopen($_FILES['price_table']['tmp_name'], 'r');
	$csv_array = array();
	while (!feof($file)){
		
		array_push($csv_array, fgetcsv($file));
	}
	if(get_option('price_table')){
		update_option('price_table', json_encode($csv_array));
	}else{
		add_option('price_table', json_encode($csv_array));	
	}
	
}

add_action('init', 'register_shortcodes');

function register_shortcodes(){
	add_shortcode('price_table', 'get_price_table');
}

function get_price_table(){
	$price_table = json_decode(get_option('price_table'), true);

	$content = "<div id='price_table'><table><tr>";
	foreach($price_table[0] as $item){
		$content .= sprintf("<td>%s</td>", $item);
	}

	$content .= "</tr>";
	$count = 1; $row_count = count($price_table); 
	for($i = 1; $i < $row_count -1 ; $i++):
		$content .= "<tr>";
	foreach($price_table[$i] as $item):
		$content .= sprintf("<td>%s</td>", $item);
	endforeach;
	$content .= "</tr>	";
	endfor;
	$content .= "</table></div>";

	return $content;

}

function get_attributes_by_code($code = '', $order_by = 'name'){
	global $wpdb;
	// var_dump($wpdb->prepare("SELECT DISTINCT * FROM {$wpdb->prefix}terms as t INNER JOIN {$wpdb->prefix}woocommerce_termmeta AS tm ON t.term_id = tm.woocommerce_term_id WHERE tm.meta_key = %s ", ATTRIBUTE_PREFIX.$code ) . "   ORDER BY t.name" );

	$attributes = $wpdb->get_results($wpdb->prepare("SELECT DISTINCT * FROM {$wpdb->prefix}terms as t INNER JOIN {$wpdb->prefix}woocommerce_termmeta AS tm ON t.term_id = tm.woocommerce_term_id WHERE tm.meta_key = %s ", ATTRIBUTE_PREFIX.$code ) . "   ORDER BY t." . $order_by );

	return $attributes;
}

/**
 * Proper way to enqueue scripts and styles
 */
function wpdocs_theme_name_scripts() {


	wp_enqueue_style( 'common-css',  get_template_directory_uri() . '/css/essay/common.css', array(), '1.0.0', true);

	wp_enqueue_script('jquery_ui', get_template_directory_uri() . '/scripts/jquery_ui.js', array(), '1.0.0', true);


	wp_enqueue_script('order-form-js', get_template_directory_uri() . '/scripts/order_form.js', array(), '1.0.0', true);

	wp_enqueue_script('price-calculator-js', get_template_directory_uri() . '/scripts/price_calculator.js', array(), '1.0.0', true);

	wp_enqueue_script('form-potato-js', get_template_directory_uri() . '/scripts/form_potato.js', array(), '1.0.0', true);

}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

add_filter( 'woocommerce_enqueue_styles', '__return_false');

define( 'WOOCOMMERCE_USE_CSS', false );

/*-----------------------------------------------------------------------------------*/
/* Don't add any code below here or the sky will fall down */
/*-----------------------------------------------------------------------------------*/
?>