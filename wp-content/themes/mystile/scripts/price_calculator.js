"use strict";

/* ************************************************************** *
 *                                                                *
 *                          IMPORTANT!                            *
 *                                                                *
 *              This file is used by iOS native app.              *
 *                Please don't use jQuery and DOM.                *
 *                                                                *
 * ************************************************************** */


/**
 ******************************************************************
 * Winback Coupon
 ******************************************************************
 * @typedef {{
 *     type_id:            number,
 *     value:              number|null,
 *     quantity:           number,
 *     service_type_id:    number,
 *     writer_category_id: number|null,
 * }} WinbackCoupon
 *
 * @type {Object}
 */

/**
 ******************************************************************
 * FORM STATE
 ******************************************************************
 * @typedef {{
 *     pages:                    number,
 *     slides:                   number,
 *     discountPercent:          number,
 *     charts:                   number,
 *     winbackCoupons:           Array<WinbackCoupon>,
 *     writerCategoryId:         number|null,
 *     deadlinePricePerPage:     number,
 *     deadlineHrs:              number,
 *     spacing:                  string|null,
 *     getSamplesOn:             boolean,
 *     getProgressiveDeliveryOn: boolean,
 *     getUsedSourcesOn:         boolean,
 *     writerPercent:            number,
 * }} FormState
 *
 * @type {Object}
 */

/**
 ******************************************************************
 * FREE THINGS
 ******************************************************************
 * @typedef {{
 *     pages:               number,
 *     slides:              number,
 *     charts:              number,
 *     progressiveDelivery: boolean,
 *     copyOfSources:       boolean,
 *     writerSamples:       boolean,
 *     categoriesOfWriter:  Array<number>,
 * }} FreeThings
 *
 * @type {Object}
 */

/**
 ******************************************************************
 * COST INFO
 ******************************************************************
 * @typedef {{
 *     basePagesCost:                      number,
 *     baseSlidesCost:                     number,
 *     baseChartsCost:                     number,
 *     couponPagesQuantity:                number,
 *     baseCouponsReduction:               number,
 *     secondaryGetSamplesPrice:           number,
 *     secondaryGetSamplesCost:            number,
 *     secondaryProgressiveDeliveryPrice:  number,
 *     secondaryProgressiveDeliveryPercent:number,
 *     secondaryProgressiveDeliveryCost:   number,
 *     secondaryWriterCategoryCost:        number,
 *     secondaryUsedSourcesPrice:          number,
 *     secondaryUsedSourcesCost:           number,
 *     secondaryCost:                      number,
 *     couponGetSamplesReduction:          number,
 *     couponWriterCategoryReduction:      number,
 *     couponUsedSourcesReduction:         number,
 *     couponProgressiveDeliveryReduction: number,
 *     couponsReduction:                   number,
 *     discountPercent:                    number,
 *     discountReduction:                  number,
 *     rawCost:                            number,
 *     baseCost:                           number,
 *     totalCost:                          number,
 *     progressiveDeliveryCost:            number,
 *     pdDisabled:                         string|false,
 *     pdForced:                           string|false,
 *     freeThings:                         FreeThings,
 *     services:                           Array.<Service>
 * }} CostInfo
 * @type {Object}
 */

/**
 ******************************************************************
 * SERVICE
 ******************************************************************
 * @typedef {{
 *     quantity: number,
 *     type_id:  number,
 *     title:    string,
 *     cost:     number,
 *     priority: number,
 *     free:     boolean,
 * }} Service
 * @type {Object}
 */



(function($, jQuery) {

    var global = typeof self !== "undefined" && self != null && self.self === self && self
        || typeof global !== "undefined" && global != null && global.global === global && global
        || window;

    var normalizePrice = function(amount) {
        return parseFloat(parseFloat(amount).toFixed(2));
    };

    var pluralize = function(word, number) {
        return typeof number && (number).toString().substr(-1) === "1" ? word : word + "s";
    };

    var PROVIDE_SAMPLES_PRICE = 5;
    var PROGRESSIVE_DELIVERY_PERCENT = 10;
    var USED_SOURCES_MIN_PRICE = 14.95;
    var USED_SOURCES_PRICE_RATE = 0.1;
    var COUPONS_TYPES_IDS = {
        PERCENT: 1,
        FREE_UNIT: 5,
        CATEGORY_OF_WRITER: 7
    };
    var COMPLEX_ASSIGNMENT_PERCENT = 20;
    var SERVICES_IDS = {
        "WRITING_PAGES":        1,
        "WRITING_SLIDES":       2,
        "WRITING_CHARTS":       30,
        "CHOOSE_WRITER":        3,
        "PROVIDE_ME_SAMPLES":   4,
        "PROGRESSIVE_DELIVERY": 5,
        "DISCOUNT":             6,
        "USED_SOURCES":         21,
        "COMPLEX_ASSIGNMENT":   37,
        "COUPON_WRT_PAGES":     10
    };

    /**
     * Calculate Raw Base Cost
     * @param {FormState} formState
     * @param {CostInfo} cost
     */
    var calculateBase = function(formState, cost) {

        var basePagesCost = (function() {
            var spacing_factor = (formState.spacing == "single") ? 2 : 1;
            if (formState.pages && formState.deadlinePricePerPage) {
                return normalizePrice(formState.deadlinePricePerPage * formState.pages * spacing_factor);
            } else {
                return 0;
            }
        })();

        var baseSlidesCost = (function() {
            if (formState.slides && formState.deadlinePricePerPage) {
                return normalizePrice(formState.deadlinePricePerPage * formState.slides * 0.5);
            } else {
                return 0;
            }
        })();

        var baseChartsCost = (function() {
            if (formState.charts && formState.deadlinePricePerPage) {
                return normalizePrice(formState.deadlinePricePerPage * formState.charts * 0.5);
            } else {
                return 0;
            }
        })();

        cost.basePagesCost = basePagesCost;
        cost.baseSlidesCost = baseSlidesCost;
        cost.baseChartsCost = baseChartsCost;
        cost.baseCost = normalizePrice(cost.basePagesCost + cost.baseSlidesCost + cost.baseChartsCost);
    };

    /**
     * Calculate Coupon for pages
     * @param {FormState} formState
     * @param {CostInfo} cost
     */
    var calculateBaseCoupons = function(formState, cost) {
        var couponPagesReduction = 0;
        var couponPagesQuantity = 0;
        (function() {
            var spacing_factor = (formState.spacing == "single") ? 2 : 1;
            for (var i = 0; i < formState.winbackCoupons.length; i++) {
                var coupon = formState.winbackCoupons[i];
                if (coupon.type_id === COUPONS_TYPES_IDS.FREE_UNIT) {
                    if (coupon["service_type_id"] == SERVICES_IDS.WRITING_PAGES) {
                        couponPagesReduction = normalizePrice(formState.deadlinePricePerPage * coupon.quantity * spacing_factor);
                        couponPagesQuantity = coupon.quantity;
                        break;
                    }
                    break;
                }
            }
        })();
        cost.couponPagesReduction = normalizePrice(couponPagesReduction);
        cost.couponPagesQuantity  = couponPagesQuantity;
        cost.baseCouponsReduction = normalizePrice(couponPagesReduction);
    };

    /**
     * Calculate Raw Secondary Cost
     * @param {FormState} formState
     * @param {CostInfo} cost
     */
    var calculateSecondary = function(formState, cost) {

        var baseCostWithCoupons = cost.baseCost - cost.baseCouponsReduction;

        // Calculate Get Samples Cost
        var secondaryGetSamplesCost = formState.getSamplesOn == true ? PROVIDE_SAMPLES_PRICE : 0;

        // Calculate Progressive Delivery Price & Cost & availability;
        var pdDisabled =
            (cost.baseCost < 200 || formState.deadlineHrs < 120) && "Available only for orders with a deadline of 5 days and longer, and with the value of $200 and more.";
        var pdForced =
            (cost.baseCost > 600 && formState.deadlineHrs >= 168) && "Mandatory for 7 days and longer, with the value of $500 and more.";

        var secondaryProgressiveDeliveryPrice = pdDisabled ? 0 : normalizePrice(baseCostWithCoupons * PROGRESSIVE_DELIVERY_PERCENT / 100);
        var secondaryProgressiveDeliveryCost = (pdForced || formState.getProgressiveDeliveryOn) ? secondaryProgressiveDeliveryPrice : 0;

        // Calculate Writer Category Cost
        var secondaryWriterCategoryCost = normalizePrice(baseCostWithCoupons * formState.writerPercent / 100);

        // Calculate Used Sourse Price & Cost
        var secondaryUsedSourcesPrice =  normalizePrice(
            Math.max(USED_SOURCES_MIN_PRICE, baseCostWithCoupons * USED_SOURCES_PRICE_RATE)
        );
        var secondaryUsedSourcesCost = formState.getUsedSourcesOn ? secondaryUsedSourcesPrice : 0;

        // Calculate Complex Assignment Price & Cost
        var secondaryComplexAssignmentPrice = normalizePrice(
            Math.max((baseCostWithCoupons * COMPLEX_ASSIGNMENT_PERCENT) / 100)
        );
        var secondaryComplexAssignmentCost = formState.complexAssignmentDiscipline ? secondaryComplexAssignmentPrice : 0;

        console.log('secondaryComplexAssignmentCost:'+secondaryComplexAssignmentCost);
        console.log('secondaryComplexAssignmentPrice:'+secondaryComplexAssignmentPrice);
        console.log('baseCostWithCoupons:'+baseCostWithCoupons);
        
        cost.secondaryGetSamplesPrice = PROVIDE_SAMPLES_PRICE;
        cost.secondaryGetSamplesCost = secondaryGetSamplesCost;
        cost.secondaryProgressiveDeliveryPrice = secondaryProgressiveDeliveryPrice;
        cost.secondaryProgressiveDeliveryPercent = PROGRESSIVE_DELIVERY_PERCENT;
        cost.secondaryProgressiveDeliveryCost = secondaryProgressiveDeliveryCost;
        cost.pdDisabled = pdDisabled;
        cost.pdForced = pdForced;
        cost.secondaryWriterCategoryCost = secondaryWriterCategoryCost;
        cost.secondaryUsedSourcesPrice = secondaryUsedSourcesPrice;
        cost.secondaryUsedSourcesCost = secondaryUsedSourcesCost;
        cost.secondaryComplexAssignmentPrice = secondaryComplexAssignmentPrice;
        cost.secondaryComplexAssignmentCost = secondaryComplexAssignmentCost;
        cost.secondaryCost = normalizePrice(
            secondaryUsedSourcesCost + secondaryGetSamplesCost +
            secondaryProgressiveDeliveryCost + secondaryWriterCategoryCost +
            secondaryComplexAssignmentCost
        );

    };


    /**
     * @param {FormState} formState
     * @param {CostInfo} cost
     */
    var calculateCoupons = function(formState, cost) {

        var couponGetSamplesReduction = 0.0;
        var couponWriterCategoryReduction = 0.0;
        var couponUsedSourcesReduction = 0.0;
        var couponProgressiveDeliveryReduction = 0.0;


        for (var i = 0; i < formState.winbackCoupons.length; i++) {
            var coupon = formState.winbackCoupons[i];
            if (coupon.type_id === COUPONS_TYPES_IDS.PERCENT) {
                switch (coupon["service_type_id"]) {
                    case SERVICES_IDS.PROVIDE_ME_SAMPLES:
                        couponGetSamplesReduction += normalizePrice(cost.secondaryGetSamplesCost / 100 * coupon.value);
                        break;

                    case SERVICES_IDS.PROGRESSIVE_DELIVERY:
                        couponProgressiveDeliveryReduction += normalizePrice(cost.secondaryProgressiveDeliveryCost / 100 * coupon.value);
                        break;

                    case SERVICES_IDS.USED_SOURCES:
                        couponUsedSourcesReduction += normalizePrice(cost.secondaryUsedSourcesCost / 100 * coupon.value);
                        break;
                }
            } else if (coupon.type_id === COUPONS_TYPES_IDS.CATEGORY_OF_WRITER) {
                if (coupon["service_type_id"] === SERVICES_IDS.CHOOSE_WRITER) {
                    if (formState.writerCategoryId === coupon.writer_category_id) {
                        couponWriterCategoryReduction += cost.secondaryWriterCategoryCost;
                    }
                }
            }
        }

        cost.couponGetSamplesReduction          = couponGetSamplesReduction;
        cost.couponProgressiveDeliveryReduction = couponProgressiveDeliveryReduction;
        cost.couponUsedSourcesReduction         = couponUsedSourcesReduction;
        cost.couponWriterCategoryReduction      = couponWriterCategoryReduction;
        cost.couponsReduction = normalizePrice(
            cost.baseCouponsReduction + couponGetSamplesReduction + couponProgressiveDeliveryReduction +
            couponUsedSourcesReduction + couponWriterCategoryReduction
        );
    };


    /**
     * @param {FormState} formState
     * @param {CostInfo} cost
     */
    var calculateDiscount = function(formState, cost) {
        cost.discountPercent = formState.discountPercent;
        cost.discountReduction = normalizePrice((cost.rawCost - cost.couponsReduction) * formState.discountPercent / 100);
    };


    /**
     * @param {FormState} formState
     * @returns {FreeThings}
     */
    function calculateFreeThings(formState) {
        var couponsObject = {};
        var categoriesOfWriter = [];
        for (var i = 0; i < formState.winbackCoupons.length; i++) {
            if (formState.winbackCoupons[i].service_type_id === SERVICES_IDS.CHOOSE_WRITER) {
                categoriesOfWriter.push(formState.winbackCoupons[i].writer_category_id);
            } else {
                couponsObject[formState.winbackCoupons[i].service_type_id] = formState.winbackCoupons[i];
            }
        }

        return {
            pages:               couponsObject[SERVICES_IDS.WRITING_PAGES] && couponsObject[SERVICES_IDS.WRITING_PAGES].quantity || 0,
            slides:              couponsObject[SERVICES_IDS.WRITING_SLIDES] && couponsObject[SERVICES_IDS.WRITING_SLIDES].quantity || 0,
            charts:              couponsObject[SERVICES_IDS.WRITING_CHARTS] && couponsObject[SERVICES_IDS.WRITING_CHARTS].quantity || 0,
            progressiveDelivery: couponsObject[SERVICES_IDS.PROGRESSIVE_DELIVERY] !== undefined,
            copyOfSources:       couponsObject[SERVICES_IDS.USED_SOURCES] !== undefined,
            writerSamples:       couponsObject[SERVICES_IDS.PROVIDE_ME_SAMPLES] !== undefined,
            categoriesOfWriter:  categoriesOfWriter
        };
    }


    /**
     * @param {FormState} formState
     * @param {CostInfo} cost
     * @returns {Array.<Service>}
     */
    var calculateServices = function(formState, cost) {

        var services = [];


        /* GET BASE SERVICES */

        if (cost.basePagesCost) {
            services.push({
                "quantity": formState.pages,
                "title":    formState.pages + " " + pluralize("page", formState.pages) + " × $" + normalizePrice(formState.deadlinePricePerPage),
                "type_id":  SERVICES_IDS.WRITING_PAGES,
                "cost":     cost.basePagesCost,
                "priority": 1,
                "free":     false
            });
        }

        if (cost.baseSlidesCost) {
            services.push({
                "quantity": formState.slides,
                "type_id":  SERVICES_IDS.WRITING_SLIDES,
                "title":    "Power Point slides",
                "cost":     cost.baseSlidesCost,
                "priority": 1,
                "free":     false
            });
        }

        if (cost.baseChartsCost) {
            services.push({
                "quantity": formState.charts,
                "type_id":  SERVICES_IDS.WRITING_CHARTS,
                "title":    "Charts",
                "cost":     cost.baseChartsCost,
                "priority": 1,
                "free":     false
            });
        }


        /* GET SECONDARY SERVICES */

        if (cost.secondaryGetSamplesCost) {
            services.push({
                "quantity": 0,
                "type_id":  SERVICES_IDS.PROVIDE_ME_SAMPLES,
                "title":    "Order writer`s samples",
                "cost":     cost.secondaryGetSamplesCost,
                "priority": 3,
                "free":     cost.freeThings.writerSamples && cost.couponGetSamplesReduction > 0
            });
        }

        if (cost.secondaryProgressiveDeliveryCost) {
            services.push({
                "quantity": 0,
                "type_id":  SERVICES_IDS.PROGRESSIVE_DELIVERY,
                "title":    "Progressive delivery",
                "cost":     cost.secondaryProgressiveDeliveryCost,
                "priority": 2,
                "free":     cost.freeThings.progressiveDelivery && cost.couponProgressiveDeliveryReduction > 0
            });
        }

        if (cost.secondaryWriterCategoryCost) {
            services.push({
                "quantity": 0,
                "type_id":  SERVICES_IDS.CHOOSE_WRITER,
                "title":    "Category of the writer",
                "cost":     cost.secondaryWriterCategoryCost,
                "priority": 2,
                "free":     cost.couponWriterCategoryReduction > 0
            });
        }

        if (cost.secondaryUsedSourcesCost) {
            services.push({
                "quantity": 0,
                "type_id":  SERVICES_IDS.USED_SOURCES,
                "title":    "Copy of sources used",
                "cost":     cost.secondaryUsedSourcesCost,
                "priority": 2,
                "free":     cost.freeThings.copyOfSources && cost.couponUsedSourcesReduction > 0
            });
        }
        
        if (cost.secondaryComplexAssignmentCost) {
            services.push({
                "quantity": 0,
                "type_id":  SERVICES_IDS.COMPLEX_ASSIGNMENT,
                "title":    "Complex Assignment",
                "cost":     cost.secondaryComplexAssignmentCost,
                "priority": 2,
                "free":     false,
            });
        }


        /* GET DISCOUNT SERVICES */

        if (cost.discountReduction > 0) {
            services.push({
                "quantity": 0,
                "type_id":  SERVICES_IDS.DISCOUNT,
                "title":    "Discount",
                "cost":     cost.discountReduction,
                "priority": 8,
                "free":     false
            });
        }


        /* GET COUPON SERVICES */

        if (cost.freeThings.pages > 0 && cost.baseCouponsReduction > 0) {
            services.push({
                "quantity": 0,
                "type_id":  SERVICES_IDS.COUPON_WRT_PAGES,
                "title":    cost.freeThings.pages + " " + pluralize("page", cost.freeThings.pages) + " discount",
                "cost":     cost.baseCouponsReduction,
                "priority": 5,
                "free":     false
            });
        }

        return services.sort(function(a, b) { return a.priority - b.priority; });

    };


    /**
     * @param {FormState} formState
     * @returns {CostInfo}
     */
    global.UVOCostCalculator = function(formState) {


        formState = formState || {};

        // Legacy, remove after FEBRUARY 30, 2016:
        if (!formState.winbackCoupons && formState.couponsObject) {
            formState.winbackCoupons = [];
            for (var couponName in formState.couponsObject) {
                if (formState.couponsObject.hasOwnProperty(couponName)) {
                    formState.winbackCoupons.push(formState.couponsObject[couponName]);
                }
            }
            delete formState.couponsObject;
        }
        formState = {
            pages:                              parseInt(formState.pages)                  || 0,
            slides:                             parseInt(formState.slides)                 || 0,
            discountPercent:                    parseInt(formState.discountPercent)        || 0,
            charts:                             parseInt(formState.charts)                 || 0,
            winbackCoupons:                     formState.winbackCoupons                   || [],
            writerCategoryId:                   formState.writerCategoryId                 || null,
            deadlinePricePerPage:               parseFloat(formState.deadlinePricePerPage) || 0,
            deadlineHrs:                        parseInt(formState.deadlineHrs)            || 0,
            spacing:                            formState.spacing                          || null,
            getSamplesOn:                       Boolean(formState.getSamplesOn),
            getProgressiveDeliveryOn:           Boolean(formState.getProgressiveDeliveryOn),
            getUsedSourcesOn:                   Boolean(formState.getUsedSourcesOn),
            complexAssignmentDiscipline:        Boolean(formState.complexAssignmentDiscipline),
            writerPercent:                      parseInt(formState.writerPercent)          || 0
        };

        var cost = {};

        calculateBase(formState, cost);
        calculateBaseCoupons(formState, cost);
        calculateSecondary(formState, cost);
        cost.rawCost = normalizePrice(cost.baseCost + cost.secondaryCost);
        calculateCoupons(formState, cost);
        calculateDiscount(formState, cost);
        cost.totalCost = normalizePrice(cost.rawCost - cost.discountReduction - cost.couponsReduction);

        cost.progressiveDeliveryCost = normalizePrice(cost.secondaryProgressiveDeliveryCost - cost.couponProgressiveDeliveryReduction);

        cost.freeThings = calculateFreeThings(formState);

        cost.services = calculateServices(formState, cost);

        global.___cost___ = cost;

        return cost;
    };

})(null, null);
