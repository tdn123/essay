
/***************    compatibility stuff   ********************/

// ui.tabs events name. remove everything related after update to >= 1.9.0
var _uvo_compatibility = {
        'new-tabs': false
    },
    _jquery_ui_version = jQuery.map(jQuery.ui.version.split('.'), function (ver, index) {
        return parseInt(ver, 10);
    });

if (_jquery_ui_version[0] > 1 || (_jquery_ui_version[0] == 1 && _jquery_ui_version[1] >= 9)) {
    _uvo_compatibility['new-tabs'] = true;
}


/*************** Global Utility functions ********************/
function htmlEncode(value){
    return jQuery('<div/>').text(value).html();
}

function htmlDecode(value){
    return jQuery('<div/>').html(value).text();
}


function trim_val(val, id){
    var new_val = jQuery.trim(val);
    jQuery("#"+id).val(new_val);
}

/*************** Global Validation functions ********************/
window.isDuplicatedEmail = (function() {

    var lastEmail = "";
    var lastError = undefined;

    return function() {

        var email   = jQuery('#email').val();

        if (lastEmail !== email && email.indexOf('@') > 0) {
            jQuery.ajax({
                url: "/uvoform/validate/email/",
                type: 'post',
                dataType: 'json',
                async: false,
                data: {
                    email: email
                },
                success: function (r) {
                    if (r.existing_client) {
                        lastError = "<div>* Your e-mail is already registered</div><div>* Use Returning customer tab</div>";
                    } else {
                        lastError = undefined;
                    }
                }
            });
        }

        lastEmail = email;
        return lastError;
    }
})();

function isCustomerValid(field, rules, i) {
    var f       = undefined;
    var email   = jQuery('#old_email').val();

    var password=jQuery('#password_hidden').val();

    if (!password) {
        password=jQuery('#old_password').val();
    }

    if (password && crypt_string) {
    	password = crypt_string(password);
    }
    if (password != null && password != "" && email != null && email != null) {
        f = __("Password or email you've typed are incorrect");
        jQuery.ajax({
            url: "/uvoform/validate/client/",
            type: 'post',
            dataType: 'json',
            async: false,
            data: {
                email: email,
                password: password
            },
            success: function (r) {
                if ((r.client_id)) {
                    f = undefined;
                }
            }
        });
    }
   return f;
}

/*************** Global functions ********************/
function g_off(ref) {
    var rightNow = new Date();
    var date1 = new Date(rightNow.getFullYear(), 0, 1, 0, 0, 0, 0);
    var date2 = new Date(rightNow.getFullYear(), 6, 1, 0, 0, 0, 0);
    var temp = date1.toGMTString();
    var date3 = new Date(temp.substring(0, temp.lastIndexOf(" ")-1));
    var temp = date2.toGMTString();
    var date4 = new Date(temp.substring(0, temp.lastIndexOf(" ")-1));
    var hoursDiffStdTime = (date1 - date3) / (1000 * 60 * 60);
    var hoursDiffDaylightTime = (date2 - date4) / (1000 * 60 * 60);

    if (!document.getElementById(ref)) return true;
    var off = new Date().getTimezoneOffset() / 60;

    document.getElementById(ref).selectedIndex=12+hoursDiffStdTime;
}

function build_select_from_radio(name) {
    var options = [],
        new_name = 'mob_' + name,
        $select = jQuery('<select>'),
        $container = jQuery('<div class="selects"></div>'),
        $radios = jQuery('[type="radio"][name="' + name + '"]');

    if ($radios.length === 0) {
        return;
    }

    var pseudo_root = $radios.eq(0).parent().parent();


    $radios.each(function (i) {
	var $el = jQuery(this),
            label = jQuery.trim($el.next().text());

        // old design temporary fix ;(
        if (label == "") {
            var $img = $el.next().find('img');
            if ($img.length !== 0) {
                label = jQuery.trim($img.attr('alt'));
            }
        }
	if (false == $el.is(':disabled')) {
		options.push('<option value="' + $el.val() + '"' + ($el.is(':checked')?' selected="selected"':'') + '>' + label + '</option>');
	}
    });

    $select.attr('id', new_name).attr('name', new_name);
    $select.html(options.join(''));
    $container.append($select);
    pseudo_root.prepend($container);

    jQuery('#' + new_name).change(function () {
        var $select = jQuery(this);
	var parentName = jQuery(this).prop("name").replace('mob_','');
        jQuery('input[type=radio][name="'+parentName+'"][value="' + $select.val() + '"]').prop('checked', 'checked').trigger('change');
    });

    if (jQuery.uvo_pref && jQuery.uvo_pref.uvoAutocomplete) {
        jQuery('#' + new_name).uvoAutocomplete({
            sortBy: 'none'
        });
    }

    $radios.change(function () {
        var $this = jQuery(this),
            value = jQuery(this).val();



        if (pseudo_root.find('.radios').is(':visible')) {

            jQuery('#' + new_name).val(value).trigger('rebuild.uvoAutocomplete');
        }
    });

    var selects_container = pseudo_root.find('.selects'),
        radios_container = pseudo_root.find('.radios');

    selects_container.addClass('visible-in-mobile');
    radios_container.addClass('visible-in-desktop');

    var is_mobile = jQuery(window).width() <= 580;
    if (is_mobile) {
        radios_container.hide();
    } else {
        selects_container.hide();
    }
}
function block_submit_button() {
    jQuery('.orderform-submit').prop('disabled', true);
    jQuery('.to_billing_container').block({message: null});
    jQuery('.to_billing_processing').show();
}

function unblock_submit_button() {
    jQuery('.orderform-submit').prop('disabled', false);
    jQuery('.to_billing_container').unblock();
    jQuery('.to_billing_processing').hide();
}
jQuery(document).on("potato-form-html-ready", function () {

    /***************************************************
     * Order Form Ajax Submit Initialization:
     **************************************************/
    jQuery(document).ready(function () {

        jQuery('#order_form').ajaxForm({
            dataType: 'json',
            beforeSubmit: function(arr, $form, options, a ,b) {
                jQuery('.orderform-submit').prop('disabled', true);
                jQuery('.formError.parentFormorder_form').remove(); // not validationEngine('hideAll'); because of animation timeout, added by plugin
                block_submit_button();
                crypt_passwords_before_submit(arr);//("#formID1").validationEngine('detach');
                var validation_result = $form.validationEngine('validate');
                if (!validation_result) {
                    unblock_submit_button();
                }
		if (jQuery('#tabs_customer_type').length != undefined ) {
			if (jQuery('#tabs_customer_type').tabs( "option", "active" ) == 1 && validation_result) { // returning
				jQuery.UVObus.dispatch('Form.Field.OldEmailPasswordBlur');
				unblock_submit_button();
				return jQuery.UVOform.discountLoaded == undefined?false:jQuery.UVOform.discountLoaded;
			}
		}		
                return validation_result;
            },
            error: function() {
                jQuery("#order_form").trigger("uvoform-potato-submition-error");
                unblock_submit_button();
            },
            success: function (responseText, statusText, xhr, $form) {

                if (responseText.hasOwnProperty('error')) {
                    jQuery.each(responseText['data'], function (k, v) {
                        switch (k) {
                            case 'paper_type_id':
                            case 'paper_type_option':
                            case 'professor_email':
                            case 'topcat_id':
                            case 'topcat_option':
                            case 'toptitle':
                            case 'paperdets':
                            case 'min_source':
                            case 'paperformat':
                            case 'paper_format_type_option':
                                if (_uvo_compatibility['new-tabs']) {
                                    jQuery('#uvoform_tabs').tabs('option', 'active', 0);
                                } else {
                                    jQuery('#uvoform_tabs').tabs('select', 0);
                                }
                                jQuery('#' + k).validationEngine("showPrompt", v, "error", "topRight", true);
                                break;

                            case 'academiclevel':
                            case 'paperlanguage':
                            case 'addmaterials':
                            case 'spacing':
                            case 'deadline':
                            case 'writer_preferences':
                            case 'samples_needed':
                            case 'progressive_delivery':
                            case 'ps':
                            case 'old_ps':
                                jQuery('input[name="' + k +'"]').filter(":last").validationEngine("showPrompt", v, "error", "topRight", true);
                                break;

                            case 'pagesreq':
                            case 'slidesreq':
                            case 'chartsreq':
                            case 'fname':
                            case 'email':
                            case 'password':
                            case 'user_code':
                            case 'old_email':
                            case 'old_password':
                            case 'mobile_phone_a':
                            case 'mobile_phone_t':
                            case 'confirm_password':
                            case 'mobile_phone_country':
                            case 'request_writer':
                                jQuery('#' + k).filter(':visible').validationEngine("showPrompt", v, "error", "topRight", true);
                                break;
                        }
                    });
                    jQuery('#refresha').trigger('click'); // refresh_captcha_image();
                    if (jQuery('.formError')) {
                        jQuery.scrollTo(".formError", 800, { offset: { top: -100, left: 0}} );

                    }

                    jQuery("#order_form").trigger("uvoform-potato-submition-error");
		    unblock_submit_button();
                    if ('url' in responseText['data']) {
                        window.location = responseText['data']['url'];
                    }

                } else if (responseText.hasOwnProperty('success')) {
                    jQuery.UVOform.localStorageKeeper.removeState();
                    window.location = responseText['data']['url'];
                }
                else
                    unblock_submit_button();
                return false;
            }
        });
    });


    /***************************************************
     * Order Form Appearence Initialization
     **************************************************/
    jQuery(document).ready(function () {


        jQuery('#box_academiclevel, #box_paperformat, #box_paperlanguage').buttonset();
        jQuery('#box_first_draft_deadline, #box_category_of_writer_rad, #box_pagesreq, .box_coupon').buttonset();
        jQuery('#box_payment_system, #box_old_payment_system, .box_coupon, #box_discount_code').buttonset();
        jQuery('#box_addmaterials.jq-btns').buttonset();
        jQuery('#customer_login').button();

        //jQuery('#min_source').spinner({min: 0, max: 1000, width: 32, mouseWheel: false});
        //jQuery('#pagesreq').spinner({min: jQuery('#pagesreq').attr('minval'), max: 1000, width: 32, mouseWheel: false});
        //jQuery('#slidesreq').spinner({min: jQuery('#slidesreq').attr('minval'), max: 1000, width: 32, mouseWheel: false});
        /* new spinner*/
        jQuery("#min_source").ForceNumericOnly();
        jQuery("#pagesreq").ForceNumericOnly();
        jQuery("#slidesreq").ForceNumericOnly();
	jQuery("#chartsreq").ForceNumericOnly();
	//alert("SAWKA");

        /* end - new spinner*/

        if (_uvo_compatibility['new-tabs']) {
            jQuery('#tabs_customer_type').tabs({
                activate: function(event, ui) {
                    var tab_index = ui.newTab.index();
                    jQuery.UVObus.dispatch('Form.CustomerSteps.StepShowed', { index: tab_index });
                },
                beforeActivate: function(event, ui) {
                    jQuery.UVObus.dispatch('Form.Errors.ClearAll');
                }
            });
        } else {
            jQuery('#tabs_customer_type').tabs({
                show: function(event, ui) {
                    var tab_index = ui.index;
                    jQuery.UVObus.dispatch('Form.CustomerSteps.StepShowed', { index: tab_index });
                },
                select: function(event, ui) {
                    jQuery.UVObus.dispatch('Form.Errors.ClearAll');
                }
            });
        }

        // uvoautocomplete css fix for invisible (form tabs, tab2) selects in show
        jQuery.UVObus.listen('Form.Steps.StepShowed', function (data) {
            if (data.index == 1) {
                jQuery('#fieldset_s2 select').each(function () {
                    if (jQuery.uvo_pref && jQuery.uvo_pref.uvoAutocomplete) {
                        jQuery(this).uvoAutocomplete('corectStyles');
                    }
                });
            }
        });


        if (jQuery('#uvoform_tabs').length) {

            if (_uvo_compatibility['new-tabs']) {
                jQuery('#uvoform_tabs').tabs({
                    activate: function (event, ui) {
                        jQuery.UVObus.dispatch('Form.Steps.StepShowed', {index: ui.newTab.index()});
                    },
                    beforeActivate: function(event, ui) {
                        var currently_selected = jQuery("#uvoform_tabs").tabs("option", "active"),
                            new_selected = ui.newTab.index();

                        //jQuery.UVObus.dispatch('Form.Errors.ClearAll');

                        switch (new_selected) {
                            case 1:
                                if (currently_selected == 2) {
                                    return true;
                                }
                                if (currently_selected == 0 || currently_selected == 1) {
                                    return jQuery('#order_form').validationEngine('validate');
                                }
                                break;

                            case 2:
                                if (currently_selected == 1 || currently_selected == 2) {
                                    return jQuery('#order_form').validationEngine('validate');
                                }
                                break;
                            case 0:
                                if (currently_selected == 2 || currently_selected == 1) {
                                    return true;
                                }
                                if (currently_selected == 0) {
                                    return jQuery('#order_form').validationEngine('validate');
                                }
                                break;

                        }

                        return false;
                    }
                });
            } else {
                jQuery('#uvoform_tabs').tabs({
                    show: function (event, ui) {
                        jQuery.UVObus.dispatch('Form.Steps.StepShowed', {index: ui.index});
                    },
                    select: function(event, ui) {
                        var currently_selected = jQuery("#uvoform_tabs").tabs("option", "selected"),
                            new_selected = ui.index;

                        //jQuery.UVObus.dispatch('Form.Errors.ClearAll');

                        switch (new_selected) {
                            case 1:
                                if (currently_selected == 2) {
                                    return true;
                                }
                                if (currently_selected == 0 || currently_selected == 1) {
                                    return jQuery('#order_form').validationEngine('validate');
                                }
                                break;

                            case 2:
                                if (currently_selected == 1 || currently_selected == 2) {
                                    return jQuery('#order_form').validationEngine('validate');
                                }
                                break;
                            case 0:
                                if (currently_selected == 2 || currently_selected == 1) {
                                    return true;
                                }
                                if (currently_selected == 0) {
                                    return jQuery('#order_form').validationEngine('validate');
                                }
                                break;

                        }

                        return false;
                    }
                });
            }
        }

        jQuery('#paper_type_id').filterSelect();

    });


    /***************************************************
     * Order Form Behavior Initialization:
     **************************************************/
    jQuery(document).ready(function () {

        jQuery('#toptitle').focus(function () {
            this.setSelectionRange(0, this.value.length);
        });

        if (jQuery.uvo_pref && jQuery.uvo_pref.uvoAutocomplete) {

            jQuery('#paper_type_id, #topcat_id, #mobile_phone_country, #customer_country').uvoAutocomplete();

            if (jQuery('#mobile_phone_country').is(':disabled')) {
                jQuery('#mobile_phone_country').uvoAutocomplete('option', 'disabled', true);
            }

            jQuery('#topcat_id').uvoAutocomplete('option', 'alwaysLast', [52]);
            jQuery('#topcat_id').uvoAutocomplete('option', 'alwaysVisible', [52]);

            jQuery('#paper_type_id').uvoAutocomplete('option', 'highlightInList', [2]);
            jQuery('#paper_type_id').uvoAutocomplete('option', 'alwaysFirst', [2]);
            jQuery('#paper_type_id').uvoAutocomplete('option', 'alwaysLast', ['other']);
            jQuery('#paper_type_id').uvoAutocomplete('option', 'alwaysVisible', ['other']);

	    //set manual matches
	    jQuery('#paper_type_id').uvoAutocomplete('option', 'searchMatching', {
		'english':['composition'],
		'essay':['composition','^english'], //english 101
		'writing':['composition','^english'],//english 101
		'summary':['composition','^english'],//english 101
		'compare':['composition','^english'],//english 101
		'contrast':['composition','^english'],//english 101
		'planning':['^management'],
		'culture':['Cultural and Ethnic Studies'],
		});

	    //set ignore search words
	    jQuery('#paper_type_id').uvoAutocomplete('option', 'searchIgnore', [
		'and','or'
		]);

            jQuery('#topcat_id').uvoAutocomplete('option', 'filter', {
                nocollege: {
                    ids: jQuery.UVOform.DisciplinesForFilter['nocollege'],
                    action: function (id) {
                        jQuery('#dialog-bad-discipline').data('topcat_id', id).dialog('open');
                    }
                },
                noborder: {
                    ids: jQuery.UVOform.DisciplinesForFilter['noborder'],
                    action: function (id) {
                        jQuery('#dialog-bad-academiclevel-pages').data('topcat_id', id).dialog('open');
                    }
                }
            });
        }

        /*************** Validation ********************/

        jQuery('#order_form').validationEngine('attach',{
            promptPosition:'topRight',
            focusFirstField: true,
            validationEventTrigger: 'blur',
            maxErrorsPerField: 1,
            'custom_error_messages': {
                '#password': {
                    'required': {
                        'message': __('Letters, numbers, and underscores only please.')
                    }
                },
                '#confirm_password': {
                    'required': {
                        'message': __('Letters, numbers, and underscores only please.')
                    }
                }
            }
        });

        jQuery.UVObus.listen('Form.Field.Changed.PagesReq', function () {
            jQuery('#pagesreq').validationEngine('validate');
        });

        jQuery.UVObus.listen('Form.Field.Changed.SlidesReq', function () {
            jQuery('#slidesreq').validationEngine('validate');
        });

	jQuery.UVObus.listen('Form.Field.Changed.ChartsReq', function () {
            jQuery('#chartsreq').validationEngine('validate');
        });

        jQuery.UVObus.listen('Form.Errors.ClearAll', function () {
            jQuery('#order_form').validationEngine('hideAll');
        });

        jQuery.UVObus.listen('Form.Errors.Clear', function () {
            if (jQuery('div.formError').filter(':visible').length > 1) {
                jQuery('#order_form').validationEngine('hideAll');
            }
        });

        jQuery('input[name="academiclevel"], input[name="paperlanguage"], #paper_type_id, #topcat_id, #paper_type_option, #topcat_option, #toptitle, #paperdets').click(function(){
            jQuery.UVObus.dispatch('Form.Errors.Clear');
        });

        jQuery('#paper_type_option, #topcat_option, #toptitle, #paperdets').keydown(function () {
            jQuery.UVObus.dispatch('Form.Errors.Clear');
        });

        jQuery('#pagesreq, input[name="spacing"], input[name="deadline"], input[name="writer_preferences"], #slidesreq, #chartsreq, #samples_needed, #progressive_delivery').click(function(){
            jQuery.UVObus.dispatch('Form.Errors.Clear');
        });

        jQuery('#usedsources').change(function(e){
            jQuery.UVObus.dispatch('Form.Errors.Clear');
        });

        jQuery('#pagesreq, #slidesreq, #chartsreq').keydown(function () {
            jQuery.UVObus.dispatch('Form.Errors.Clear');
        });

        jQuery('#email, #password, #confirm_password, #fname, #mobile_phone_country, #mobile_phone_a, #mobile_phone_t, input[name="payment_system_hidden"], input[name="old_ps"], #old_email, #old_password, #user_code').click(function () {
            jQuery.UVObus.dispatch('Form.Errors.Clear');
        });

        jQuery('#email, #password, #confirm_password, #fname, #mobile_phone_a, #mobile_phone_t, #old_email, #old_password, #user_code').keydown(function () {
            jQuery.UVObus.dispatch('Form.Errors.Clear');
        });

        jQuery('#old_email, #old_password').blur(function () {
            jQuery.UVObus.dispatch('Form.Field.OldEmailPasswordBlur');
        });

        jQuery(document).on("click", 'input[name="discount_id"], #request_writer', function () {

            jQuery.UVObus.dispatch('Form.Errors.Clear');
        });

        jQuery('#order_form').bind('jqv.field.result', function (event, field, errorFound, prompText) {
            var field_name = field.attr('name');
            var showPrompts = (jQuery('#order_form').data("jqv") || {}).showPrompts !== false;
            if (errorFound && showPrompts) {
                jQuery('#' + field_name + '_ok').hide().removeClass('ok-err-show');
                jQuery('#' + field_name + '_err').show().addClass('ok-err-show');
                jQuery('#' + field_name).addClass('errors');
            } else {
                if ( field_name == 'slidesreq' || field_name == 'pagesreq' ||  field_name == 'chartsreq' ) {
                    jQuery('#pagesreq, #slidesreq, #chartsreq').removeClass('errors');
                }
                jQuery('#' + field_name + '_err').hide().removeClass('ok-err-show');
                jQuery('#' + field_name).removeClass('errors');

                // hack for old_email and old_password fields:
                // we got two types of validation on those,
                // first one - default field validation (require, email)
                // second - email / password server validation,
                // so we should not show "ok" signs till server validation on those fields:
                if (field_name != 'old_email' && field_name != 'old_password') {
                    jQuery('#' + field_name + '_ok').show().addClass('ok-err-show');
                }
            }
        });



        /*************** Steps Movement Listeners  ********************/
        jQuery.UVObus.listen('Form.CustomerSteps.StepShowed', function (argv) {
            var hidden_field_selector = '#new_user',
                old_password_field_selector = '#old_password';

            switch (argv.index) {
                case 0:
                    jQuery(hidden_field_selector).val(1);
                    jQuery(old_password_field_selector).val('').trigger('change');
                    jQuery.UVObus.dispatch('Form.Field.CleanLifetimeDiscounts');
                    break;

                case 1:
                    jQuery(hidden_field_selector).val(0);
                    break;
            }
        });

        jQuery.UVObus.listen('Form.Steps.StepShowed', function (argv) {
            var ftabs = jQuery('#uvoform_tabs');
            var items = ftabs.find('ul li.uvoform_nav_tab');

            switch (argv.index) {
                case 0:
                    jQuery(items[1]).removeClass('ui-state-filled');
                    jQuery(items[0]).removeClass('ui-state-filled');
                    break;

                case 1:
                    jQuery(items[1]).removeClass('ui-state-filled');
                    jQuery(items[0]).addClass('ui-state-filled');
                    break;

                case 2:
                    jQuery(items[0]).addClass('ui-state-filled');
                    jQuery(items[1]).addClass('ui-state-filled');
                    break;
            }
        });

        jQuery.UVObus.listen('Form.Steps.ToStep3', function () {
            if (_uvo_compatibility['new-tabs']) {
                jQuery('#uvoform_tabs').tabs('option', 'active', 2);
            } else {
                jQuery('#uvoform_tabs').tabs('select', 2);
            }

        });

        jQuery.UVObus.listen('Form.Steps.ToStep2', function () {
            if (_uvo_compatibility['new-tabs']) {
                jQuery('#uvoform_tabs').tabs('option', 'active', 1);
            } else {
                jQuery('#uvoform_tabs').tabs('select', 1);
            }
        });

        jQuery.UVObus.listen('Form.Steps.ToStep1', function () {
            if (_uvo_compatibility['new-tabs']) {
                jQuery('#uvoform_tabs').tabs('option', 'active', 0);
            } else {
                jQuery('#uvoform_tabs').tabs('select', 0);
            }

        });


        /*************** Steps Movement Dispatchers  ********************/
        function scrol_to_tabs(){
            jQuery('html, body').animate({
                scrollTop: jQuery("#uvoform_tabs").offset().top
            }, 800);
        }

        jQuery('.to_step2_btn').click(function () {
            jQuery.UVObus.dispatch('Form.Steps.ToStep2');
            scrol_to_tabs();
        });

        jQuery('.to_step3_btn').click(function () {
            jQuery.UVObus.dispatch('Form.Steps.ToStep3');
            scrol_to_tabs();
        });

        jQuery('.order_form_repeat_step1').click(function () {
            jQuery.UVObus.dispatch('Form.Steps.ToStep1');
            scrol_to_tabs();
        });

        jQuery('.order_form_repeat_step2').click(function () {
            jQuery.UVObus.dispatch('Form.Steps.ToStep2');
            scrol_to_tabs();
        });


        /*************** Fields Changes Events ********************/

        jQuery('[name="academiclevel"]').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.AcademicLevel', jQuery(this));
        });

	jQuery('[name="paperlanguage"]').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.PaperLanguage', jQuery(this));
        });

	jQuery('[name="payment_system_hidden"]').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.PaymentSystem', jQuery(this));
        });

        jQuery('#paper_type_id').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.PaperType', jQuery(this));
        });

        jQuery('#paper_type_option').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.PaperTypeOther', jQuery(this));
        });

        jQuery('#topcat_id').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.Subject', jQuery(this));
        });

        jQuery('#topcat_option').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.SubjectOther', jQuery(this));
        });

        jQuery('#toptitle').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.Topic', jQuery(this));
        });

        jQuery('#paperdets').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.PaperInstructions', jQuery(this));
        });

        jQuery('#min_source').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.Sources', jQuery(this));
        });

        jQuery('[name="addmaterials"]').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.AdditionalMaterials', jQuery(this));
        });

        jQuery('[name="paperformat"]').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.PaperFormat', jQuery(this));
        });

        jQuery('#paper_format_type_option').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.PaperFormatOther', jQuery(this));
        });

        jQuery('#pagesreq').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.PagesReq', jQuery(this));
        });

        jQuery('[name="spacing"]').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.Spacing', jQuery(this));
        });

        jQuery(document).on('change', '[name="deadline"]',  function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.Deadline', jQuery(this));
        });

        jQuery('[name="writer_preferences"]').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.WriterPreferences', jQuery(this));
        });

        jQuery('#slidesreq').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.SlidesReq', jQuery(this));
        });
	jQuery('#chartsreq').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.ChartsReq', jQuery(this));
        });

        jQuery('#samples_needed').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.SamplesNeeded', jQuery(this));
        });

        jQuery('#usedsources').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.UsedSources', jQuery(this));
        });

        jQuery('#progressive_delivery').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.ProgressiveDelivery', jQuery(this));
        });

        jQuery('#ext_time').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.DeadlineDate', jQuery(this));
        });

        jQuery('#mobile_phone_country').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.Country', jQuery(this));
        });

        jQuery('#customer_country').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.CustomerCountry', jQuery(this));
        });

        jQuery('#country_code').click(function () {
            jQuery('#mobile_phone_country').focus();
        });

        jQuery(document).on('click', '.show_box_discount_code', function () {
            jQuery.UVObus.dispatch('Form.Visibility.BoxDiscount', true);
        });

        jQuery('#customer_login').click(function () {
            jQuery.UVObus.dispatch('Form.Server.GetClient');
        });

        if (jQuery('#defined_writer').length !== 0) {
            jQuery('#defined_writer').change(function () {
                jQuery.UVObus.dispatch('Form.Availability.RequestWriter', jQuery(this).is(':checked'));
            });
        }

        jQuery('#request_writer').change(function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.RequestWriter', jQuery(this));
        });

        jQuery(document).on("change", 'input[name="discount_id"], #request_writer', function () {
            jQuery.UVObus.dispatch('Form.Field.Changed.LifetimeDiscount');
        });


        /*************** Additional Materials to hidden field ********************/
        function synchronizeAddMaterials($input) {
            jQuery.UVObus.dispatch('Form.Field.UpdateAddMaterialsHidden', $input.val());
        }

        jQuery.UVObus.listen('Form.Field.Changed.AdditionalMaterials', synchronizeAddMaterials);



        /*************** Fixed Additional Materials for specific service types ********************/
        function calculateAdditionalMaterialsText($additional_materials) {
            var version = 'No';

            switch ($additional_materials.attr('type')) {
                case 'radio':
                    if ($additional_materials.val() == 2) {
                        version = 'Yes';
                    }
                    break;

                case 'checkbox':
                    if ($additional_materials.prop('checked')) {
                        version = 'Yes';
                    }
                    break;
            }

            jQuery.UVObus.dispatch('Form.Availability.AdditionalMaterialsText', version);
        }

        jQuery.UVObus.listen('Form.Field.Changed.AdditionalMaterials', calculateAdditionalMaterialsText);



        /*************** Paper Type and Dissertations/Thesis relationships ********************/
        function calculatePaperTypeDissertationsState() {
            var all_enabled = true,
                academic_level = jQuery.UVOform.getAcademicLevelValue();

                if (jQuery.inArray(academic_level, jQuery.UVOform.AcademiLevelsWithoutDissertations) != -1)
                {
                    all_enabled = false;
                }

            jQuery.UVObus.dispatch('Form.State.PaperTypeDissertationsState', all_enabled);

        }

        jQuery.UVObus.listen('Form.Field.Changed.AcademicLevel', calculatePaperTypeDissertationsState);


        /*************** Paper Type Other box  ********************/
        function calculatePaperTypeOtherVisibility($paper_type) {
            var visibility_state = 'hide';

            if ($paper_type.val() == jQuery.UVOform.PaperTypeOtherValue) {
                visibility_state = 'show';
            }

            jQuery.UVObus.dispatch('Form.State.PaperTypeOtherState', visibility_state);
        }

        jQuery.UVObus.listen('Form.Field.Changed.PaperType', calculatePaperTypeOtherVisibility);


        /*************** Subject or Discipline Other box ********************/
        function calculateSubjectOtherVisibility($subject) {
            var visibility_state = 'hide';

            if ($subject.val() == jQuery.UVOform.SubjectOtherValue) {
                visibility_state = 'show';
            }

            jQuery.UVObus.dispatch('Form.State.SubjectOtherState', visibility_state);
        }

        jQuery.UVObus.listen('Form.Field.Changed.Subject', calculateSubjectOtherVisibility);


        /*************** Paper Format Other box ********************/
        function calculatePaperFormatOtherVisibility($paper_format) {
            var visibility_state = 'hide';

            if ($paper_format.val() == jQuery.UVOform.PaperFormatOtherValue) {
                visibility_state = 'show';
            }

            jQuery.UVObus.dispatch('Form.State.PaperFormatOtherState', visibility_state);
        }

        jQuery.UVObus.listen('Form.Field.Changed.PaperFormat', calculatePaperFormatOtherVisibility);


        /*************** Final Deadline calculation ********************/
        function calculateFinalDeadline() {
            var deadline_hrs = jQuery.UVOform.getDeadlineExtendedHrs();

            var integer     = Math.floor(deadline_hrs);
            var fractional  = deadline_hrs - integer;
            fractional      = fractional.toFixed(1)*60;
            var hours_add   = 0;

            if (integer > 24){
                var days_number = Math.floor(integer/24);
                var hours_add   = integer%24;
            } else{
                var hours_add   = integer;
            }
            var myDate = new Date();

            myDate.setHours(myDate.getHours() + hours_add);
            myDate.setMinutes(myDate.getMinutes() + fractional);
            if (integer > 24){
                myDate.setDate(myDate.getDate() + days_number);
            }

            jQuery.UVObus.dispatch('Form.Field.NewFinalDeadline', myDate);
        }

        function updFinalDeadline(newDate) {
            jQuery('#max_ext_time').val(newDate.format('Y-m-d h:i:s')).trigger('change');
            jQuery('#ext_time').html(newDate.format('j M y, g:H A')+ ' (GMT'+newDate.format('P')+')').trigger('change');
        }

        jQuery.UVObus.listen('Form.Field.NewFinalDeadline', updFinalDeadline);

        jQuery.UVObus.listen('Form.Field.Changed.Deadline', calculateFinalDeadline);


        /*************** Total amount of words tooltip ********************/
        function calculateTotalWords() {
            var pages = jQuery.UVOform.getPagesReqValue(),
                spacing = jQuery.UVOform.getSpacingValue(),
                total_words = pages * ((spacing == 'single')?550:275);

            jQuery.UVObus.dispatch('Form.Field.NewTotalWords', total_words);
        }

        jQuery.UVObus.listen('Form.Field.Changed.PagesReq', calculateTotalWords);
        jQuery.UVObus.listen('Form.Field.Changed.Spacing', calculateTotalWords);


        /*************** Price per slide tooltip ********************/
        function calculatePricePerSlide($deadline) {
            var price_attr_name = 'price_per_page',
                price = parseFloat($deadline.attr(price_attr_name)),
                price_per_slide = price / 2;

            jQuery.UVObus.dispatch('Form.Field.NewSlidePrice', price_per_slide.toFixed(2));
        }
	jQuery.UVObus.listen('Form.Field.Changed.Deadline', calculatePricePerSlide);

	/*************** Price per chart tooltip ********************/
        function calculatePricePerChart($deadline) {
            var price_attr_name = 'price_per_page',
                price = parseFloat($deadline.attr(price_attr_name)),
                price_per_chart = price / 2;

            jQuery.UVObus.dispatch('Form.Field.NewChartPrice', price_per_chart.toFixed(2));
        }
        jQuery.UVObus.listen('Form.Field.Changed.Deadline', calculatePricePerChart);


        /*************** Price calculation ********************/
        function calculatePrice() {
            jQuery.UVObus.dispatch('Form.Price.Recalculate');
	    var hrs   = jQuery.UVOform.getDeadlineHrs();
            var price = jQuery.UVOform.getToChargPrice();
	    if (jQuery("#ps_realex_btn").length>0) {
			if (hrs<=48 && price >= 400)
			    jQuery("#ps_realex_btn").hide();
			else
			    jQuery("#ps_realex_btn").show();
		}

        }

        jQuery.UVObus.listen('Form.Field.Changed.PagesReq',            calculatePrice);
        jQuery.UVObus.listen('Form.Field.Changed.Spacing',             calculatePrice);
        jQuery.UVObus.listen('Form.Field.Changed.Deadline',            calculatePrice);
        jQuery.UVObus.listen('Form.Field.Changed.WriterPreferences',   calculatePrice);
        jQuery.UVObus.listen('Form.Field.Changed.SamplesNeeded',       calculatePrice);
        jQuery.UVObus.listen('Form.Field.Changed.SlidesReq',           calculatePrice);
	jQuery.UVObus.listen('Form.Field.Changed.ChartsReq',           calculatePrice);
        jQuery.UVObus.listen('Form.Field.Changed.RequestWriter',       calculatePrice);
        jQuery.UVObus.listen('Form.Field.Changed.ProgressiveDelivery', calculatePrice);
        jQuery.UVObus.listen('Form.Field.Changed.Discount',            calculatePrice);
        jQuery.UVObus.listen('Form.Field.Changed.DefinedWriter',       calculatePrice);
        jQuery.UVObus.listen('Form.Field.Changed.LifetimeDiscount',    calculatePrice);
        jQuery.UVObus.listen('Form.Field.Changed.UsedSources',         calculatePrice);


        /*************** Progressive Delivery State ('disabled', 'user', 'forced') ********************/
        function calculateProgressiveDeliveryState() {
            var hrs   = jQuery.UVOform.getDeadlineHrs(),
                price = jQuery.UVOform.getBasePrice(),
                state = 'disabled';

            if (hrs < 120) {
                state = 'disabled';
            } else if (hrs >= 120 && price < 200) {
                state = 'disabled';
            } else if (hrs >= 168 && price >= 600) {
                state = 'forced';
            } else if (hrs >= 120 && price >=200) {
                state = 'user';
            }

            jQuery.UVObus.dispatch('Form.State.NewProgressiveDelivery', state);
        }

        jQuery.UVObus.listen('Form.Field.Changed.Deadline',          calculateProgressiveDeliveryState);
        jQuery.UVObus.listen('Form.Field.Changed.SamplesNeeded',     calculateProgressiveDeliveryState);
        jQuery.UVObus.listen('Form.Field.Changed.WriterPreferences', calculateProgressiveDeliveryState);
        jQuery.UVObus.listen('Form.Field.Changed.Spacing',           calculateProgressiveDeliveryState);
        jQuery.UVObus.listen('Form.Field.Changed.DefinedWriter',     calculateProgressiveDeliveryState);
        jQuery.UVObus.listen('Form.Field.Changed.SlidesReq',         calculateProgressiveDeliveryState);
	jQuery.UVObus.listen('Form.Field.Changed.ChartsReq',         calculateProgressiveDeliveryState);
        jQuery.UVObus.listen('Form.Field.Changed.RequestWriter',     calculateProgressiveDeliveryState);
        jQuery.UVObus.listen('Form.Field.Changed.PagesReq',          calculateProgressiveDeliveryState);
        jQuery.UVObus.listen('Form.Field.Changed.Discount',          calculateProgressiveDeliveryState);
        jQuery.UVObus.listen('Form.Field.Changed.LifetimeDiscount',  calculateProgressiveDeliveryState);


        /*************** Discipline filter (all, nocollege, noborder) ********************/
        function disciplineFilter() {
            var pages = jQuery.UVOform.getPagesReqValue(),
                academic_level = jQuery.UVOform.getAcademicLevelValue(),
                state = 'all';

            if (academic_level == 1) {
                if (pages < 5) {
                    state = 'nocollege';
                } else {
                    state = 'nocollege,noborder';
                }
            }

            jQuery.UVObus.dispatch('Form.State.NewDisciplineFilter', state);
	
        }

        jQuery.UVObus.listen('Form.Field.Changed.AcademicLevel', disciplineFilter);
        jQuery.UVObus.listen('Form.Field.Changed.PagesReq',      disciplineFilter);

        /*************** academic level by discipline filter ********************/
        function academicLevelByDisciplineFilter() {
            var pages = jQuery.UVOform.getPagesReqValue(),
                academic_level = jQuery.UVOform.getAcademicLevelValue(),
                discipline_id = jQuery.UVOform.getSubjectId(),
                state = 'norestrictions';

            if (jQuery.inArray(discipline_id, jQuery.UVOform.DisciplinesForFilter['nocollege']) != -1) {
                state = 'nohighschool';
            } else if (jQuery.inArray(discipline_id, jQuery.UVOform.DisciplinesForFilter['noborder']) != -1
                        && pages >= 5
                    ) {
                state = 'toomanypages';
            }

            jQuery.UVObus.dispatch('Form.State.NewAcademicLevelBySubjectFilter', state);
        }

        jQuery.UVObus.listen('Form.Field.Changed.Subject', academicLevelByDisciplineFilter);
	jQuery.UVObus.listen('Form.Field.Changed.PagesReq',academicLevelByDisciplineFilter);

        /*************** PagesReq changes -> academic level influence ********************/
        function pagesReq2AcademicLevelInfluence() {
            var pages = jQuery.UVOform.getPagesReqValue(),
                academic_level = jQuery.UVOform.getAcademicLevelValue(),
                discipline_id = jQuery.UVOform.getSubjectId();

            if (pages >= 5
                    && academic_level == 1
                    && jQuery.inArray(discipline_id, jQuery.UVOform.DisciplinesForFilter['noborder']) != -1
            ) {
                jQuery.UVObus.dispatch('Form.State.PagesReq2AcademicLevelAction', {did: discipline_id});
            }
        }

        jQuery.UVObus.listen('Form.Field.Changed.PagesReq', pagesReq2AcademicLevelInfluence);


        /*************** PagesReq >= 8 and Deadline <= 8hrs, notice ********************/
        function pagesDeadlineTooLittleTimeNotice() {
            var hrs = jQuery.UVOform.getDeadlineHrs(),
                spacing = jQuery.UVOform.getSpacingValue(),
                pages = parseInt(jQuery.UVOform.getPagesReqValue(), 10) * (spacing == 'single'?2:1);

            if (pages >= 8 && hrs <= 8) {
                jQuery('#dialog-form-potato-little-time').dialog('open');
            }
        }

        jQuery.UVObus.listen('Form.Field.Changed.PageReq', pagesDeadlineTooLittleTimeNotice);
        jQuery.UVObus.listen('Form.Field.Changed.Deadline', pagesDeadlineTooLittleTimeNotice);
        jQuery.UVObus.listen('Form.Field.Changed.Spacing', pagesDeadlineTooLittleTimeNotice);


        /*************** automatic sources ********************/
        function automactic_sources_handler() {

            var $target = jQuery('#min_source'),
                switch_text_to_auto = false,
                $text_container = jQuery('#box_min_source .field_tip'),
                paper_type_id = jQuery.UVOform.getPaperTypeValue(),
                academic_level = jQuery.UVOform.getAcademicLevelValue(),
                auto_text = __('Please note the number of sources has been predetermined based on our experiences; however, this can be reset at your discretion.');

            if (jQuery.inArray(paper_type_id, jQuery.UVOform.papersWithSources) != -1) {
                switch (academic_level) {
                    case 1:
                    case 2:
                        switch_text_to_auto = true;
                        $target.val(2).trigger('change')
                        break;

                    case 3:
                    case 4:
                    case 5:
                        switch_text_to_auto = true;
                        $target.val(academic_level).trigger('change');
                        break;
                }
            }

            $text_container.text( switch_text_to_auto ? auto_text : "" );
        }

        jQuery.UVObus.listen('Form.Field.Changed.AcademicLevel', automactic_sources_handler);
        jQuery.UVObus.listen('Form.Field.Changed.PaperType', automactic_sources_handler);

        /*************** Min Academic Level check (CS-3657) ********************/
        function check_min_academic_level() {
            var academic_level = jQuery.UVOform.getAcademicLevelValue(),
                min_academic_level = jQuery.UVOform.getMinAcademicLevelValue();

            if (jQuery.UVOform.flags.min_academic_level_popup && min_academic_level && min_academic_level > academic_level) {
                jQuery('#dialog-low-academiclevel').dialog('open');
            }
        }

        jQuery.UVObus.listen('Form.Field.Changed.AcademicLevel', check_min_academic_level);


        /*************** Additional features box visibility ********************/
        jQuery('#box_features_hidden').click(function () {
            jQuery.UVObus.dispatch('Form.Visibility.Features', true);
        });

        /*jQuery('#box_features_shown .title').click(function () {
            jQuery.UVObus.dispatch('Form.Visibility.Features', false);
        });*/

        /*************** Submit! ********************/

        // new design request_writer
        if (jQuery('#defined_writer').length === 0) {
            jQuery.UVObus.listen('Form.Field.Changed.RequestWriter', function () {

                var writer_category_selector = '[name="writer_preferences"]',
                    is_mobile = (jQuery(window).width() <= 580) ? true : false,
                    is_requested = (jQuery('#request_writer').val() != "");
                if (is_requested) {
                    jQuery(writer_category_selector).attr('disabled', 'disabled').button('refresh');
                    if (is_mobile) {
                        jQuery('#mob_writer_preferences').uvoAutocomplete('disable');
                    }
                } else {
                    jQuery(writer_category_selector).removeAttr('disabled').trigger('change').button('refresh');
                    if (is_mobile) {
                        jQuery('#mob_writer_preferences').uvoAutocomplete('enable');
                    }
                }
            })
        }

    });


    /***************************************************
     * Form Steps Description Updater
     **************************************************/
    jQuery(document).ready(function () {
        /**
         *  toTemplate - renders Long Description items into DOM using template.
         *  expects '#service_type_info_side_header' and '#service_type_info_side_content' in template.
         *
         * @return DOM node
         */
        function toTemplate(elements, template) {
            var result = jQuery('<div></div>');
            for (k in elements) {
                var template_filled = template.clone();
                template_filled.find('.service_type_info_side_header').html(k).end().find('.service_type_info_side_content').html(elements[k]);
                result.append(template_filled);
            }
            return result;
        }


        /**
         * update_step1short_description - send signal for update with formed string as parameter.
         *
         * @return undefined
         */
        function update_step1short_description(str_description) {
            var short_step1_desc_container_selector = '#short_service_type_container';

            jQuery(short_step1_desc_container_selector).html(str_description);
        }

        /**
         * update_step1long_description - send signal for step 1 description update with formed from template DOM node as parameter.
         *
         * @return undefined
         */
        function update_step1long_description(obj_description) {
            var template = jQuery('<div></div>').addClass('service_type_info_row'),
                long_step1_desc_container_selector = '#service_type_container';

            template.append(jQuery('<span></span>').addClass('service_type_info_side_header'));
            template.append(jQuery('<span></span>').addClass('service_type_info_side_content'));
            jQuery(long_step1_desc_container_selector).html(toTemplate(obj_description, template));
        }

        /**
         * update_step2short_description - send signal for update with formed string as parameter.
         *
         * @return undefined
         */
        function update_step2short_description(str_description) {
            var short_step2_desc_container_selector = '#step3_short_service_type_container';

            jQuery(short_step2_desc_container_selector).html(str_description);
        }

        /**
         * update_step2long_description - send signal for step 2 description update with formed from template DOM node as parameter.
         *
         * @return undefined
         */
        function update_step2long_description(obj_description) {
            var template = jQuery('<div></div>').addClass('service_type_info_row'),
                long_step2_desc_container_selector = '#step3_service_type_container';

            template.append(jQuery('<span></span>').addClass('service_type_info_side_header'));
            template.append(jQuery('<span></span>').addClass('service_type_info_side_content'));
            jQuery(long_step2_desc_container_selector).html(toTemplate(obj_description, template));
        }

        /**
         * attach_description_updates - binds description updaters to corresponding events.
         *
         * @return undefined
         */
        function attach_description_updates() {
            jQuery.UVObus.listen('Form.Field.Update.Step1ShortDescription', update_step1short_description);
            jQuery.UVObus.listen('Form.Field.Update.Step1LongDescription', update_step1long_description);
            jQuery.UVObus.listen('Form.Field.Update.Step2ShortDescription', update_step2short_description);
            jQuery.UVObus.listen('Form.Field.Update.Step2LongDescription', update_step2long_description);
        }

        jQuery('#show_service_type_info').parent().parent().click(function() {
            jQuery.UVObus.dispatch('Form.Errors.ClearAll');
            jQuery.UVOform.form_element_hide('#box_short_service_type_info');
            jQuery.UVOform.form_element_show('#box_service_type_info');
        });

        jQuery('#show_short_service_type_info').parent().parent().click(function() {
            jQuery.UVObus.dispatch('Form.Errors.ClearAll');
            jQuery.UVOform.form_element_hide('#box_service_type_info');
            jQuery.UVOform.form_element_show('#box_short_service_type_info');
        });

        jQuery('#show_step3_service_type_info').parent().parent().click(function() {
            jQuery.UVObus.dispatch('Form.Errors.ClearAll');
            jQuery.UVOform.form_element_hide('#box_step3_short_service_type_info');
            jQuery.UVOform.form_element_show('#box_step3_service_type_info');
        });

        jQuery('#show_step3_short_service_type_info').parent().parent().click(function() {
            jQuery.UVObus.dispatch('Form.Errors.ClearAll');
            jQuery.UVOform.form_element_hide('#box_step3_service_type_info');
            jQuery.UVOform.form_element_show('#box_step3_short_service_type_info');
        });

        jQuery.UVObus.listen('Form.Ready', attach_description_updates);
    });






    /***************************************************
     * Service Discounts:
     **************************************************/
    jQuery(document).ready(function () {
        function init_service_coupons(coupons) {
            if (coupons.hasOwnProperty('order')) {
                jQuery.UVOform.addServiceCoupons(coupons['order']);
                jQuery.each(coupons['order'], function (i, discount) {
                    switch (discount['service_type_id']) {
                        case 3: // choose writer
                            jQuery('#tip_radio_writer_preferences_' + discount['writer_category_id'] + ' .ui-button-text span').addClass('price-free');
                            break;

                        case 4: // provide me samples
                            jQuery('#samples_needed').parent().siblings('.check-wrap-sum').addClass('price-free');
                            jQuery('#samples_needed').next().find('.feature-price').addClass('price-free'); // TODO: remove another version. after new cabinet will goes to world @date2013-12-04
                            break;

                        case 21: // used sources
                            jQuery('#usedsources').parent().siblings('.check-wrap-sum').addClass('price-free');
                            jQuery('#usedsources').next().find('.feature-price').addClass('price-free'); // TODO: remove another version. after new cabinet will goes to world @date2013-12-04
                            break;

                        case 5: // progressive delivery
                            jQuery('#progressive_delivery').parent().siblings('.check-wrap-sum').addClass('price-free');
                            jQuery('#progressive_delivery').next().find('.feature-price').addClass('price-free'); // TODO: remove another version. after new cabinet will goes to world @date2013-12-04
                            break;

                        case 1: // writing pages
                            jQuery('#pagesreq').data('minPossibleValue', parseInt(discount['quantity']) + 1);
                            jQuery('#contentspinpagesreq').after(__('<span class="price-free-pagesreq">you pay only for <span>1</span> page(s)</span>'));
                            break;
                    }
                })
            } else if (coupons.hasOwnProperty('customer')) {
                jQuery.UVOform.addServiceCoupons(coupons['customer']);
                jQuery.each(coupons['customer'], function (i, discount) {
                    switch (discount['service_type_id']) {
                        case 3: // choose writer
                            jQuery('[name="writer_preferences"]:checked').prop('checked', false);
                            jQuery('#radio_writer_preferences_' + discount['writer_category_id']).prop('checked', true).trigger('change');
                            jQuery('#tip_radio_writer_preferences_' + discount['writer_category_id'] + ' .ui-button-text span').addClass('price-free');
                            break;

                        case 4: // provide me samples
                            jQuery('#samples_needed').prop('checked', true).trigger('change').closest('label').addClass('active');
                            jQuery('#samples_needed').parent().siblings('.check-wrap-sum').addClass('price-free');
                            jQuery('#samples_needed').next().find('.feature-price').addClass('price-free'); // TODO: remove another version. after new cabinet will goes to world @date2013-12-04
                            break;

                        case 21: // used sources
                            jQuery('#usedsources').prop('checked', true).trigger('change').closest('label').addClass('active');
                            jQuery('#usedsources').parent().siblings('.check-wrap-sum').addClass('price-free');
                            jQuery('#usedsources').next().find('.feature-price').addClass('price-free'); // TODO: remove another version. after new cabinet will goes to world @date2013-12-04
                            break;

                        case 5: // progressive delivery
                            var $pd = jQuery('#progressive_delivery:not(:disabled)');
                            if ($pd.length > 0) {
                                $pd.prop('checked', true).trigger('change').closest('label').addClass('active');
                            }
                            jQuery('#progressive_delivery').parent().siblings('.check-wrap-sum').addClass('price-free');
                            jQuery('#progressive_delivery').next().find('.feature-price').addClass('price-free'); // TODO: remove another version. after new cabinet will goes to world @date2013-12-04
                            break;

                        case 1: // writing pages
                            jQuery('#pagesreq').data('minPossibleValue', parseInt(discount['quantity']) + 1);
                            jQuery('#contentspinpagesreq').after(__('<span class="price-free-pagesreq">you pay only for <span>1</span> page(s)</span>'));
                            break;
                    }
                })
            }
        }

        jQuery.UVObus.listen('Server.Retrieved.Deadlines', function (data) {

            //TEST WINBACK
            //data["coupons"] = JSON.parse('{"customer":[{"type_id":8,"value":null,"quantity":0,"service_type_id":1,"writer_category_id":null},{"type_id":1,"value":100,"quantity":0,"service_type_id":4,"writer_category_id":null},{"type_id":1,"value":100,"quantity":0,"service_type_id":5,"writer_category_id":null},{"type_id":1,"value":100,"quantity":0,"service_type_id":21,"writer_category_id":null},{"type_id":5,"value":null,"quantity":1,"service_type_id":1,"writer_category_id":null},{"type_id":7,"value":null,"quantity":0,"service_type_id":3,"writer_category_id":4}]}');

            if (data.hasOwnProperty('coupons')) {
                init_service_coupons(data.coupons);
            }
        })
    });


    /***************************************************
     * Order Form Parts Initialization
     **************************************************/
    jQuery(document).ready(function () {

        /*************** Deadlines ********************/
        function populateDeadlines(r) {
	    var pagesreq = jQuery.UVOform.getPagesReqValue();
	    var sliedsreq = jQuery.UVOform.getSlidesReqValue();
	    var spacing_ = jQuery.UVOform.getSpacingValue();
            var possible_deadlines = {};

            var current_selected_     = jQuery.UVOform.getDeadlineId();
            var current_selected_hrs_ = jQuery.UVOform.getDeadlineHrs();

            var pay_attempt_    = jQuery('#deadline_hidden').val();
            var posted_early_   = jQuery('#deadline_post').val();

            var new_selected_   = false;

	    pagesreq = (spacing_ == 'single' ? pagesreq * 2 : pagesreq);
	    pagesreq += sliedsreq > 0 ? sliedsreq / 2 : 0;


            for (var i = 0; i < r.length; i++) {
                var deadline = r[i];
                var spacing_ = jQuery.UVOform.getSpacingValue();
                var min_pages_flag = false;
                //CS-6254
                if (pagesreq >= deadline.min_pages ) {
                    min_pages_flag = true;
                }
                else if(deadline.min_pages == 0){
                    min_pages_flag = true;
                }
                else {
                    min_pages_flag = false;
                }
                // end CS-6254

                possible_deadlines[deadline.hrs] = {
                    enabled: min_pages_flag,
                    checked: false,
                    deadline: deadline,
                    deadline_name: deadline.deadline_name
                };
                if (!new_selected_ && deadline.hrs == current_selected_hrs_ && possible_deadlines[deadline.hrs].enabled) {
                    new_selected_ = deadline.deadline_id;
                }
            }

            if (!current_selected_ && pay_attempt_) {
                for (var hrs in possible_deadlines) {
                    if (possible_deadlines[hrs].enabled && possible_deadlines[hrs].deadline.deadline_id == pay_attempt_) {
                        new_selected_ = possible_deadlines[hrs].deadline.deadline_id;
                        break;
                    }
                }
            }

            if (!current_selected_ && posted_early_) {
                for (var hrs in possible_deadlines) {
                    if (possible_deadlines[hrs].enabled && possible_deadlines[hrs].deadline.deadline_id == posted_early_) {
                        new_selected_ = possible_deadlines[hrs].deadline.deadline_id;
                        break;
                    }
                }
            }

            if (!new_selected_) {
                for (var i = 0; i < r.length; i++) {
                    if (r[i].hrs === 72) { // 168
                        if (possible_deadlines[r[i].hrs].enabled) {
                            new_selected_ = r[i].deadline_id;
                        }
                        break;
                    }
                }
            }


            for (var hrs in possible_deadlines) {
                if (possible_deadlines[hrs].enabled && possible_deadlines[hrs].deadline.deadline_id == new_selected_ ) {
                    possible_deadlines[hrs].checked = true;
                }
            }

            deadlines_html_ = '';
            fake_deadline_id_ = '';

            for (var hrs in possible_deadlines) {
                var r30_            = hrs * 1.3;
                var r60_            = hrs * 1.6;
                var deadline_id_    = '';
                var price_per_page_ = '';
                var deadline_name_  = possible_deadlines[hrs].deadline_name;

                if (possible_deadlines[hrs].enabled) {
                    deadline_id_    = possible_deadlines[hrs].deadline.deadline_id;
                    price_per_page_ = possible_deadlines[hrs].deadline.price_per_page;
                    deadline_name_  = possible_deadlines[hrs].deadline.deadline_name;
                }

                if (deadline_id_ == '') {
                    deadline_id_ = fake_deadline_id_++;
                }

                var radio_deadline_attrs_ = [];
                radio_deadline_attrs_.push('type="radio"');
                radio_deadline_attrs_.push('id="radio_deadline_' + deadline_id_ + '"');
                radio_deadline_attrs_.push('name="deadline"');
                radio_deadline_attrs_.push('hrs="' + hrs + '"');
                radio_deadline_attrs_.push('value="' + deadline_id_ + '"');
                radio_deadline_attrs_.push('price_per_page="' + price_per_page_ + '"');
                radio_deadline_attrs_.push('hrs30="' + r30_.toFixed(1) + '"');
                radio_deadline_attrs_.push('hrs60="' + r60_.toFixed(1) + '"');

                if (!possible_deadlines[hrs].enabled) {
                    radio_deadline_attrs_.push('disabled="disabled"');
                }

                if (possible_deadlines[hrs].checked) {
                    radio_deadline_attrs_.push('checked="checked"');

                }


                var label_deadline_attrs_ = [];
                label_deadline_attrs_.push('for="radio_deadline_' + deadline_id_ + '"');
                label_deadline_attrs_.push('id="tip_radio_deadline_' + deadline_id_ + '"');
                label_deadline_attrs_.push('hrs="' + hrs + '"');                // really?
                label_deadline_attrs_.push('value="' + deadline_id_ + '"');     // really?
		label_deadline_attrs_.push('data-finder="form.label.radiodeadline' + hrs + '"');

                deadlines_html_ += '<input ' + radio_deadline_attrs_.join(' ') + ' /><label ' + label_deadline_attrs_.join(' ') + '>' + __(deadline_name_) + '</label>' + "\n";

            }


            jQuery('#deadlines').empty().siblings('.selects.visible-in-mobile').remove();
            jQuery('#deadlines').html(deadlines_html_);

            var spacing_ = jQuery.UVOform.getSpacingValue();
            var spacing_factor_ = (spacing_ == 'single')? 2 : 1;

            for (var hrs in possible_deadlines) {
                if (possible_deadlines[hrs].enabled) {
                    jQuery('#tip_radio_deadline_' + possible_deadlines[hrs].deadline.deadline_id).qtip({
                        content:'$<b>' + (possible_deadlines[hrs].deadline.price_per_page * spacing_factor_) + '</b> per page',
                        position: {
                            at: "top center",
                            my: "bottom left"
                        },
                        style: {
                            classes: "ui-tooltip-rounded ui-tooltip-pale-pastel-cyan"
                        }
                    });
                }
            }

            jQuery('#box_first_draft_deadline').buttonset();
            jQuery('[name="deadline"]:checked').trigger('change');
            build_select_from_radio('deadline');

            jQuery.UVObus.dispatch('Form.DeadlinesReady');
        }


        function get_deadlines_data() {

            var xmlhttp,
                url = '/uvoform/get_deadlines_js/',
                fid_element = document.getElementById('fid');

            if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }

            if (null != fid_element) {
                url += '/' + fid_element.value;
            }

            xmlhttp.open('GET', url, false);
            xmlhttp.send();
             jQuery.UVObus.dispatch('Server.Retrieved.Deadlines',JSON.parse(xmlhttp.responseText));
        }

        jQuery.UVObus.listen('Form.Field.NewDeadlines', populateDeadlines);
	jQuery.UVObus.listen('Form.Update.Deadlines', get_deadlines_data);
	get_deadlines_data();
        //jQuery.UVObus.dispatch('Server.Retrieved.Deadlines', get_deadlines_data());



    });

     /***************************************************
     * Get Form Values:
     **************************************************/
    jQuery(document).ready(function () {

        jQuery(document).UVOload('/uvoform/potato/get_values/', {
            async: false,
            dataType: 'json',
            placement: function (elem, data) {
                if (data["form_state_default"] !== true) {
                    jQuery('#order_form').removeAttr("with-local-storage-keeper");
                    jQuery.UVOform.localStorageKeeper.removeState();
                }
                jQuery.each(data, function (k, v) {

		    if(k == 'deadline' && v.hasOwnProperty('custom_properties') && v.custom_properties.hasOwnProperty('hrs'))
		    {
			jQuery('input[name="'+k+'"][hrs="'+v.custom_properties.hrs['value']+'"]').attr('checked', 'checked').button('refresh').trigger('change');
		    }
                    if (v.hasOwnProperty('is_visible')) {
                        if (v['is_visible']) {
                            jQuery('#box_' + k).show();
                        } else {
                            jQuery('#box_' + k).hide();
                        }
                    }

                    if (v.hasOwnProperty('is_other_visible')) {
                        if (v['is_other_visible']) {
                            jQuery('#other_box_' + k).show();
                        } else {
                            jQuery('#other_box_' + k).hide();
                        }
                    }

                    if (v.hasOwnProperty('value') && v.hasOwnProperty('type')) {
                        switch (v['type']) {
                            case 'checkbox':
                                if (v['value']) {
                                    jQuery('#'+k).attr('checked', 'checked').trigger('change');
                                }
                                break;

                            case 'radio':
                                jQuery('input[name="'+k+'"][value="'+v['value']+'"]').attr('checked', 'checked').button('refresh').trigger('change');
                                break;

                            case 'text':
                            case 'select':
                                jQuery('#'+k).val(v['value']).trigger('change');
                                break;

                            case 'new':

				jQuery('<input>').attr({
				    type: 'hidden',
				    id: v['name'],
				    name: v['name'],
				    value: v['value']
				}).appendTo('#order_form').trigger('change');

                                if (v['name'] == 'fid') {
                                    jQuery(document).ready(function () {
                                        jQuery('#uvoform_tabs').tabs('enable', 1);
                                        if (_uvo_compatibility['new-tabs']) {
                                            jQuery('#uvoform_tabs').tabs('option', 'active', 1);
                                        } else {
                                            jQuery('#uvoform_tabs').tabs('select', 1);
                                        }
                                        jQuery('#uvoform_tabs').tabs('enable', 2);
                                        if (_uvo_compatibility['new-tabs']) {
                                            jQuery('#uvoform_tabs').tabs('option', 'active', 2);
                                        } else {
                                            jQuery('#uvoform_tabs').tabs('select', 2);
                                        }
					jQuery.UVObus.dispatch('Form.Update.Deadlines'); // TODO: remove if not really needed
                                        jQuery.UVObus.dispatch('Form.Price.Recalculate'); // TODO: remove if not really needed
                                    })
                                }
                                if (v['name'] == 'deadline_hidden') {
                                    var deadline_input = jQuery('[name="deadline"][value='+v['value']+']');

                                    if (deadline_input.length > 0) {
                                        deadline_input.attr('checked', 'checked').button('refresh').trigger('change');

                                    }
                                    jQuery.UVObus.dispatch('Form.Price.Recalculate'); // TODO: remove if not really needed
                                }
                                break;
                        }
                    }

                    if (v.hasOwnProperty('is_tip_visible')) {
                        if (v['is_tip_visible']) {
                            jQuery('.field_tip_'+k).show();
                        } else {
                            jQuery('.field_tip_'+k).hide();
                        }
                    }

                    if (v.hasOwnProperty('disabled') && v['disabled']['is_active']) {
                        switch (v['type']) {
                            case 'radio':
                                jQuery.each(v['disabled']['values'], function () {
                                    jQuery('input[name="'+k+'"][value="'+this+'"]').filter(':enabled').attr('disabled', 'disabled').button('refresh');
                                });
                                break;
                        }
                    }
                });

                jQuery("#order_form").trigger("uvoform-potato-initialized");

            }
        });
    });



    /***************************************************
     * Order Form radio->select for mobile version
     **************************************************/
    jQuery(document).ready(function () {

        var mob_dependant_radios = ['academiclevel','paperlanguage', 'paperformat', 'spacing', 'writer_preferences', 'payment_system_hidden', 'ps', 'old_ps']; //'discount_id', 'deadline'

        jQuery.each(mob_dependant_radios, function (i, name) {
            build_select_from_radio(name);
        });

        // TODO: Get rid of this pornography:

        function show_mob_inputs(names) {
            jQuery('.visible-in-desktop').hide();
            jQuery('.visible-in-mobile').css('display', '');
        }

        function hide_mob_inputs(names) {
            jQuery('.visible-in-mobile').hide();
            jQuery('.visible-in-desktop').css('display', '');
        }

        var mobile = null;
        function checkScreenType() {
            var current_state;
            jQuery(window).width() <= 580 ? current_state = true : current_state = false;
            if (current_state !== mobile) {
                mobile = current_state;
                if (mobile) {
                    show_mob_inputs(mob_dependant_radios);
                } else {
                    hide_mob_inputs(mob_dependant_radios);
                }
            }
        }

        jQuery(window).resize(function() {
            checkScreenType();
        });

        checkScreenType();
    });



    /***************************************************
     * Form Ready
     **************************************************/
    jQuery(document).ready(function () {

        g_off('gmt');

        jQuery('#mobile_phone_country').trigger('change'); // Country by IP auto-detection hack.

        jQuery.UVObus.dispatch('Form.Ready');
        jQuery.UVOform.flags.min_academic_level_popup = true;

        jQuery.UVObus.listen('Form.Steps.StepShowed', function(i) {
            if(i.index == 2){
                jQuery('.box-orderNow #uvoform_tabs > ul:first-child').removeClass('form-tabs-background').addClass('form-tabs-background-active');
            }else{
                jQuery('.box-orderNow #uvoform_tabs > ul:first-child').removeClass('form-tabs-background-active').addClass('form-tabs-background');
            }
        });

		function f_check_active(obj) {
			if(obj.is(":checked")){
				obj.parents('.check-wrap').addClass('active');
			}else{
				obj.parents('.check-wrap').removeClass('active');
			}
		}

        jQuery(document).on('change', '.check-wrap input[type="checkbox"]', function() {
            f_check_active(jQuery(this));
        });

    });

    jQuery.UVObus.listen('Form.Field.Changed.PagesReq', function ($pagesreq) {
        if (!isNaN($pagesreq.data('minPossibleValue'))) {
            var pages2paid = parseInt($pagesreq.val()) - parseInt($pagesreq.data('minPossibleValue')) + 1;
            if (pages2paid < 0) {
                pages2paid = 0;
            }
            jQuery('.price-free-pagesreq span').text(pages2paid);
        }
    });

	if (jQuery('#ps_realex').length > 0) {
		jQuery('#box_payment_system').on('change', 'input[type="radio"]', function () {
			if (jQuery('#ps_realex').prop('checked') || jQuery('#ps_realex_btn').prop('checked')) {
				jQuery('.realex_payment_notification').show();
			} else {
				jQuery('.realex_payment_notification').hide();
			}
		});
	}
    jQuery('.uvoform_container').removeClass('loading');
    jQuery('.ui-dialog').removeClass('loading');
    jQuery('.loading-overlay').removeClass('loading-overlay');

    jQuery('.bubbles').remove();
});
