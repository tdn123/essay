// qTips for order form

jQuery (function() {

    var disableSWTips = jQuery("#order_form").is("[disable-sw-tooltips]");

    // copypasted from jquery.qtip2
    var BROWSER = {
        /*
         * IE version detection
         *
         * Adapted from: http://ajaxian.com/archives/attack-of-the-ie-conditional-comment
         * Credit to James Padolsey for the original implemntation!
         */
        ie: (function(){
            var v = 3, div = document.createElement('div');
            while ((div.innerHTML = '<!--[if gt IE '+(++v)+']><i></i><![endif]-->')) {
                if(!div.getElementsByTagName('i')[0]) { break; }
            }
            return v > 4 ? v : NaN;
        }()),

        /*
         * iOS version detection
         */
        iOS: parseFloat(
            ('' + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0,''])[1])
                .replace('undefined', '3_2').replace('_', '.').replace('_', '')
        ) || false
    };

    var ppc_shared_options = {
        position: {
            at: "top center",
            my: "bottom left"
        },
        show: {
            event: BROWSER.iOS ? 'click' : 'mouseenter'
        },
        style: {
            classes: "ui-tooltip-rounded ui-tooltip-pale-pastel-cyan"
        }
    },

    sw_shared_options = {
        position: {
            at: "top right",
            my: "top left"
        },
        show: {
            event: BROWSER.iOS ? 'click' : 'mouseenter'
        },
        style: {
            classes: "ui-tooltip-rounded ui-tooltip-simple-white"
        }
    },

	ppc_shared_options_last = {
	        position: {
	            at: "top left",
	            my: "bottom left"
	        },
            show: {
                event: BROWSER.iOS ? 'click' : 'mouseenter'
            },
	        style: {
	            classes: "ui-tooltip-rounded ui-tooltip-pale-pastel-cyan ppc-shared-options-last"
	        }
	},
    
    mob_shared_options = {
        position: {
            at: "center left",
            my: "top right"
        },
        hide: {
            event: 'click mouseleave'
        },
        show: {
            event: 'click'
        },
        style: {
            classes: "ui-tooltip-rounded ui-tooltip-simple-white"
        }
    };
    console.log('asdfsadfsadf');
    //paper types drop-down
    jQuery('[name="paper_type_id_input"]').parent('div').find('.dr-down').hide();


    jQuery('[name="paper_type_id_input"]').click(function(){
        jQuery('[name="paper_type_id_input"]').parent('div').find('.dr-down').toggle();
    });




    // jQuery('#tip_radio_type_service_36').qtip(jQuery.extend({}, ppc_shared_options, {
    //     content:__('<span class="tip_attention">Business writing</span> includes CV, resume, cover letter, content writing and the price for this service does not depend on academic level.')
    // }));

    // jQuery('#tip_radio_type_service_37').qtip($.extend({}, ppc_shared_options, {
    //     content:__('This service implies completion of an original paper as per your instructions.')
    // }));

    // jQuery('#tip_radio_type_service_33').qtip($.extend({}, ppc_shared_options, {
    //     content:__('Rewriting includes editing, proofreading, formatting and change of the paper content. We will rewrite 30-70% of the original content depending on your order\'s instructions. You will have to provide us with your own draft.')
    // }));

    // jQuery('#tip_radio_type_service_30').qtip($.extend({}, ppc_shared_options, {
    //     content:__('Editing includes formatting and proofreading of your paper. We will rewrite only up to 30% of the original content depending on your order\'s specifications. You will have to provide us with your own draft.')
    // }));

    // jQuery('#tip_radio_type_service_31').qtip($.extend({}, ppc_shared_options, {
    //     content:__('Proofreading includes correction of spelling, grammar and punctuation mistakes. No original content will be added. You will have to provide us with your own draft.')
    // }));

    // jQuery('#tip_radio_type_service_32').qtip($.extend({}, ppc_shared_options, {
    //     content:__('<span class="tip_attention">Formatting</span> implies correction of the paper format. You will have to provide us with your own draft.')
    // }));

    // jQuery('#tip_radio_academic_level_2').qtip($.extend({}, ppc_shared_options, {
    //     content:__('Associate Degree Studies (Vocational Technical Institutions, Junior and Community Colleges)')
    // }));

    // jQuery('#tip_radio_academic_level_3').qtip($.extend({}, ppc_shared_options, {
    //     content:__('Bachelor\'s Degree Studies')
    // }));

    // jQuery('#tip_radio_academic_level_4').qtip($.extend({}, ppc_shared_options, {
    //     content:__('Master\'s Degree or Professional Studies')
    // }));

    // jQuery('#tip_radio_academic_level_5').qtip($.extend({}, ppc_shared_options, {
    //     content:__('Ph.D. or Advanced Professional Degree')
    // }));

    // jQuery('#tip_radio_business_writing_38').qtip($.extend({}, ppc_shared_options, {
    //     content:__('Content writing is a service mainly offered to website owners and businesses. It consists in writing promotional SEO-texts, keyword rich articles, and all sorts of accompanying website content.')
    // }));

    // jQuery('#tip_radio_business_writing_39').qtip($.extend({}, ppc_shared_options, {
    //     content:__('Cover letter is a concise presentation of one\'s experience, knowledge about the company/ institution, and motivation to fill a certain position. It usually accompanies a CV or a resume.')
    // }));

    // jQuery('#tip_radio_business_writing_40').qtip($.extend({}, ppc_shared_options, {
    //     content:__('Resume is a brief summary of one\'s education, skills and work/ volunteer experience. It is written to apply for a position or inquire about a vacancy.')
    // }));

    // jQuery('#tip_radio_business_writing_41').qtip($.extend({}, ppc_shared_options, {
    //     content:__('CV (Curriculum Vitae) is an extended synopsis about one\'s academic, scientific, teaching experience, including information about honors, publications, research work, affiliations, etc.')
    // }));

    // /* elements tips */
    // jQuery('#spacing_double_btn').qtip($.extend({}, ppc_shared_options, {
    //     content: __('275 words per page')
    // }));

    // jQuery('#spacing_single_btn').qtip($.extend({}, ppc_shared_options, {
    //     content: __('550 words per page')
    // }));

    // jQuery('#tip_radio_writer_preferences_2').qtip($.extend({}, ppc_shared_options, {
    //     content: __('Best-suited writer out of those available at the moment (standard pricing)')
    // }));

    // jQuery('#tip_radio_writer_preferences_5').qtip($.extend({}, ppc_shared_options, {
    //     content: __('<span class="tip_attention">English as a Native Language</span><div>advanced writer</div>')
    // }));

    // jQuery('#tip_page_summary').qtip($.extend({}, ppc_shared_options, {
    //     content: __('Order one page summary of your whole paper. Brief bullet points, concise descriptions and general conclusions to help you organize your work. ')
    // }));

    // jQuery('#tip_progressive_delivery').qtip($.extend({}, ppc_shared_options, {
    //     content: $(
    //         "<div>" +
    //         __('Get your paper delivered step by step. For more information, please check our Progressive Delivery web page.') +
    //         " <div class=\"tip-progressibe-delivery-unavailable\">It is available only for orders with a deadline of 5 days and longer, and with the value of $200 and more.</div>" +
    //         "</div>"
    //     )
    // }));

    // jQuery('#tip_order_samples').qtip($.extend({}, ppc_shared_options, {
    //     content: __('Review 3 samples of work previously completed by the writer assigned to your order.')
    // }));

    // jQuery('#tip_used_sources').qtip($.extend({}, ppc_shared_options, {
    //     content: __('Get excerpts of the articles and e-books or the links to these excerpts cited in the paper.')
    // }));





    // if (disableSWTips) {
    //     return;
    // }






    // jQuery('.side_tip_type_of_service').qtip($.extend({}, sw_shared_options, {
    //     content: __('Please select the most suitable type of service based on the service description in the pop-up hints.')
    // }));

    // var academic_level_msg = __('Make sure to select an appropriate academic level which corresponds to your level of writing.');
    // jQuery('.side_tip_academic_level').qtip($.extend({}, sw_shared_options, {
    //     content: academic_level_msg
    // }));
    // jQuery('#box_academiclevel .mobile-tip').qtip($.extend({}, mob_shared_options, {
    //     content: academic_level_msg
    // }));

    // var paper_language_msg = __('Choose the language your paper will be written in');
    // jQuery('.side_tip_paper_language').qtip($.extend({}, sw_shared_options, {
    //     content: paper_language_msg
    // }));
    // jQuery('#box_paperlanguage .mobile-tip').qtip($.extend({}, mob_shared_options, {
    //     content: paper_language_msg
    // }));

    // var paper_type_msg = __('Please select the most suitable type of paper needed. This will help the writer follow your exact instructions. If the type of paper you need is not on the list, please choose the option "Other".');
    // jQuery('.side_tip_type_of_paper').qtip($.extend({}, sw_shared_options, {
    //     content: paper_type_msg
    // }));
    // jQuery('#box_paper_type_id .mobile-tip').qtip($.extend({}, mob_shared_options, {
    //     content: paper_type_msg
    // }));

    // var discipline_msg = __('If your subject is not on the list, please place a free inquiry first and specify your subject in the field provided, after you select the option "Other". This is needed to make sure that we have specialists to help you with your assignment.');
    // jQuery('.side_tip_discipline').qtip($.extend({}, sw_shared_options, {
    //     content: discipline_msg
    // }));
    // jQuery('#box_topcat_id .mobile-tip').qtip($.extend({}, mob_shared_options, {
    //     content: discipline_msg
    // }));

    // var toptitle_msg = __('If you have a topic, please make it clear and concise. Otherwise, leave "Writer`s choice".');
    // jQuery('.side_tip_toptitle').qtip($.extend({}, sw_shared_options, {
    //     content: toptitle_msg
    // }));
    // jQuery('#box_toptitle .mobile-tip').qtip($.extend({}, mob_shared_options, {
    //     content: toptitle_msg
    // }));

    // var paperdets_msg = __('Please make sure to leave a detailed description of your requirements, to decrease chances of revision in your order.');
    // jQuery('.side_tip_paperdets').qtip($.extend({}, sw_shared_options, {
    //     content: paperdets_msg
    // }));
    // jQuery('#box_paperdets .mobile-tip').qtip($.extend({}, mob_shared_options, {
    //     content: paperdets_msg
    // }));

    // var addmaterials_msg = __('You may upload any useful materials for the writer after filling in the order form.');
    // jQuery('.side_tip_additional_materials').qtip($.extend({}, sw_shared_options, {
    //     content: addmaterials_msg
    // }));
    // jQuery('#box_addmaterials .mobile-tip').qtip($.extend({}, mob_shared_options, {
    //     content: addmaterials_msg
    // }));

    // var pages_msg = __('Title and bibliography pages are included into the order for free.');
    // jQuery('.side_tip_pages').qtip($.extend({}, sw_shared_options, {
    //     content: pages_msg
    // }));
    // jQuery('#box_pagesreq .mobile-tip').qtip($.extend({}, mob_shared_options, {
    //     content: pages_msg
    // }));

    // var deadlines_msg = __('Please consider that it takes approximately 1 hour to complete 1 page of quality text.');
    // jQuery('.side_tip_deadlines').qtip($.extend({}, sw_shared_options, {
    //     content: deadlines_msg
    // }));
    // jQuery('#box_first_draft_deadline .mobile-tip').qtip($.extend({}, mob_shared_options, {
    //     content: deadlines_msg
    // }));

    // jQuery('.field_tooltip_deadlines').qtip($.extend({}, sw_shared_options, {
    //     //content: 'Final Submission deadline is the exact date when you have to submit your assignment. Please make sure that your Final Submission Deadline exceeds your First Draft Deadline by at least 30%, so that there is enough time for any possible revisions of your order.'
    //     content: __('Final Submission deadline is the exact date when you have to submit your assignment. Please make sure that your Final Submission Deadline exceeds your First Draft Deadline by at least 30%, so that there is enough time for any possible revisions of your order. To prevent future revisions, we recommend you to regularly check on your order, especially if it is urgent.')
    // }));

    // var category_of_writer_msg = __('You can expect higher quality from writers of the ENL category. However, all our writers are qualified enough to meet your basic requirements.');
    // jQuery('.side_tip_category_of_writer').qtip($.extend({}, sw_shared_options, {
    //     content: category_of_writer_msg
    // }));
    // jQuery('#box_writer_preferences .mobile-tip').qtip($.extend({}, mob_shared_options, {
    //     content: category_of_writer_msg
    // }));

    // jQuery('.side_tip_slidesreq').qtip($.extend({}, sw_shared_options, {
    //     prerender: true,
    //     content: __('Add slides to make your paper more informative and impressive (price depends on deadline).')
    // }));
    // jQuery('.side_tip_chartsreq').qtip($.extend({}, sw_shared_options, {
    //     prerender: true,
    //     content: __('Charts will help convey ideas in your paper better (price depends on deadline).')
    // }));

    // jQuery('.side_tip_coupon').qtip($.extend({}, sw_shared_options, {
    //     content:__('We have developed a very flexible discount system that will benefit both new and existing customers. Please visit our Pricing web page to read more about it. If you already have a discount code, please do not forget to use it while placing the order. You can apply only one discount per one order.')
    // }));

    // /* side tips */
    // jQuery('.side_tip_contact_phone').qtip($.extend({}, sw_shared_options, {
    //     content: __('Please insert your contact phone number, at which you can be reached to verify the order or provide additional information about it. Completion of your order depends on our ability to contact you in time.')
    // }));

    // var payment_system_msg = __('Please, choose a suitable payment system.');
    // jQuery('.side_tip_payment_system').qtip($.extend({}, sw_shared_options, {
    //     content: payment_system_msg
    // }));
    // jQuery('#box_payment_system .mobile-tip').qtip($.extend({}, mob_shared_options, {
    //     content: payment_system_msg
    // }));


    /* elements tips */
    //jQuery('label[for="progressive_delivery"]').qtip($.extend({}, sw_shared_options, {
    //    content: __('It is available only for orders with a deadline of 5 days and longer, and with the value of $200 and more.')
    //}));
});
